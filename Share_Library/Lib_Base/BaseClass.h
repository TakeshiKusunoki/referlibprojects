#pragma once
/**
* @file BaseClass.h
* @brief 基底クラスが記述されている
*/
//#include <typeinfo.h>
//#include <list>

//! @brief Baseライブラリに入っている
namespace Lib_Base {
	/**
	* @brief 基底クラス(BaseTask)
	* @par 詳細
	* このクラスを継承するとvoid型の関数群として扱うことができる<br>
	* ポインタを渡すことができる
	*/
	class BaseTask
	{
	protected:
		virtual void MultiTask() = 0;
		virtual ~BaseTask() = 0;
	};


	/**
	* @brief 基底クラス(BaseDecide)
	* @par 詳細
	* このクラスを継承したクラスは主にあるクラスのsubクラス(あるクラスのfriend)として扱う<br>
	* subクラスはmainクラスより先に定義する(mainクラスを前方宣言する)<br>
	* mainクラスとこのクラスを継承したsubクラスは交互に互いの実態を持つ（この時、subクラスはmainクラスのポインタ型の実態を持たせる）<br>
	* 互いのコンストラクタで互いの実態を初期化する<br>
	* main関数で処理を作る<br>
	* subクラスでその関数を呼ぶ<br>
	* @par subクラスのポインタをほかのクラスに渡す。
	*/
	//! 判断関数格納用
	class Decides
	{
	protected:
		Decides() = default;
	public:
		//! 派生クラスに判断させる(変数も変わる)
		//! @return 判断の結果
		virtual bool Decide() = 0;
		
		virtual ~Decides() {}
	};
	class DecidesResult
	{
	protected:
		DecidesResult() = default;
	public:
		//! @return 現在の結果
		virtual bool Result()const = 0;
		virtual ~DecidesResult() {}
	};
	//! このクラスの派生クラスは絶対Decides関数を作る
	template<typename MainClass>//DecideClのDecides関数の中身のクラス
	class BaseDecide : virtual protected Decides, DecidesResult
	{
	private:
		//MainClassのDecides関数の中身を持つ
		class DecideCl final : virtual private Decides
		{
			MainClass& mainCl;
		public:
			//? なぜかpublicじゃないとコンパイルエラー
			DecideCl(MainClass& cl)//このクラスからだけ
				:mainCl(cl)
			{}
			bool Decide() override
			{
				return mainCl.Decide();
			}
			~DecideCl() = default;
		};
		class DecideResultCl final : virtual private DecidesResult
		{
			MainClass& mainCl;
		public:
			//? なぜかpublicじゃないとコンパイルエラー
			DecideResultCl(MainClass& cl)//このクラスからだけ
				:mainCl(cl)
			{}
			bool Result()const override
			{
				return mainCl.Result();
			}
			~DecideResultCl() = default;
		};
		friend DecideCl;//mainクラスからはアクセスできない
		friend DecideResultCl;
	protected:
		BaseDecide(MainClass& main) //派生クラスからだけ
			: decideCl(main)
			, pDecideCl(&decideCl)
			, pDecideResultCl(&decideResultCl)
		{}
		DecideCl decideCl;//インスタンス化用
		DecideResultCl  decideResultCl;
	public:
		DecideCl* pDecideCl;//! Desidesの派生クラスのインスタンスで変えれる
		DecideResultCl*  pDecideResultCl;//結果が分かれているので切り替え時注意	
		//! インスタンス変更
		void ChangeDecide(Decides* decide){pDecideCl = decide;}
		void ChangeDecideResult(DecidesResult* result) { pDecideResultCl = result; }
		virtual ~BaseDecide() {};
	};
	//! 判断関数格納用
	//! コピーじゃなく本体の変数を使う
	class DecidesCopy
	{
	protected:
		DecidesCopy() = default;
	public:
		//! //! コピークラスに判断させる(派生クラスとは違う変数)
		//! @return 判断の結果
		virtual bool Decide() { return false; };
		virtual ~DecidesCopy() {}
	};
	//! このクラスの派生クラスは絶対DecidesCopy関数を作る
	//! 派生クラスにこのクラスのfriendを作る
	template<typename MainClass>//DecideClのDecides関数の中身のクラス
	class BaseDecideCopy : virtual protected DecidesCopy
	{
	private:
		//MainClassのDecides関数の中身を持つ
		class DecideCl final: virtual private DecidesCopy
		{
			using TYPE = MainClass;
			DecidesCopy mainCl;
			//派生クラスを基底クラスへコピー
			DecideCl(MainClass cl)//このクラス内のみ
				: mainCl(cl)
			{
				DecidesCopy* x = &cl;
				mainCl = *x;
			}
		public:
			bool Decide() override
			{
				return mainCl.Decide();
			}
			~DecideCl() = default;
		};
		friend DecideCl;
	protected:
		BaseDecideCopy(MainClass main)//派生クラスのみ
			: decideCl(main)
			, pDecideCl(&decideCl)
		{}
		DecideCl decideCl;//インスタンス化用
	public:
		DecideCl* pDecideCl;//! Desidesの派生クラスのインスタンスで変えれる
		//! インスタンス変更
		void ChangeDecide(Decides* decide){pDecideCl = decide}
		virtual ~BaseDecideCopy() {};
	};


	/*template <typename T>
	class BaseLoop
	{
	public:
		virtual BaseLoop* LoopFunc(BaseLoop* loopFunc) = 0;
	};*/


	////! @brief 管理クラスの設計(基底クラス)
	//template<typename T>
	//class BaseManager
	//{
	//protected:
	//	std::list<T> list;
	//public:
	//	BaseManager()
	//	{
	//		list.clear();
	//	}
	//	virtual ~BaseManager()
	//	{
	//		list.clear()
	//	}
	//	//! @brief 実行
	//	virtual void Update() = 0;
	//	//! @brief 削除
	//	void Clear() { list.clear(); }
	//protected:
	//};

	////! @brief 管理クラスの設計(基底クラス,シングルトン)
	////! @details : public BaseManagerSingleton<リストにしたいオブジェクト名>
	////! @par 設計
	////! （add関数は自作する）<br>
	////! Update() override;<br>
	////! std::list<T> list;<br>
	////! void Clear() { list.clear(); }
	//template<typename T>
	//class BaseManagerSingleton : public Singleton<BaseManagerSingleton<T>>
	//{
	//protected:
	//	std::list<T> list;
	//public:
	//	//! @brief 実行
	//	virtual void Update() = 0;
	//	//! @brief 削除
	//	void Clear() { list.clear(); }
	//protected:

	//};
}




