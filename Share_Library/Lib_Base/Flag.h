#pragma once
/**
* @file Flag.h
* @brief フラグ管理クラスが記述されている
*/
#include "BaseClass.h"



namespace Lib_Base
{
	namespace Utility
	{
	/**
	* @brief フラグ管理クラス
	* @details boolの変数をクラスに定義したいときにboolがたの代わりとして使う
	*/
	class FlagM
	{
	private:
		bool flag;
	public:
		FlagM()
			:flag(false)
		{}
		//! @brief コピーコンストラクタ
		explicit FlagM(const FlagM&) {}
		~FlagM() {}
		//! @brief コピーコンストラクタ
		void operator=(const FlagM&) {}
	public:
		//! @brief コピーコンストラクタ
		void operator=(bool f)
		{
			flag = f;
		}
		//! @brief フラグを反転する
		void Reverce()
		{
			flag ^= flag;
		}
		//!  @brief フラグを立てる
		void OnFlag()
		{
			flag = true;
		};
		//! @brief デフラグ
		void DeFlag()
		{
			flag = false;
		};
		//! @brief flagの値とは逆を返す(ゲッター)
		bool GetReverce()
		{
			bool f = flag;
			f ^= flag;
			return f;
		}
		//! @brief (ゲッター)
		//! @return flagを返す
		bool get()
		{
			return flag;
		}

	};


	/**
	* @brief 呼ばれている時だけ、フラグを立てるクラス
	* @detailes BaseDecide* baseCl: MultiTaskが呼ばれる条件(戻り値がtrueなら呼ぶ)
	*/
	class FlagCallOn : public BaseTask
	{
	public:

		/**
		* @fn void sum()
		* @brief
		/* @detailes bool(*func)() : MultiTaskが呼ばれる条件(戻り値がtrueなら呼ぶ)
		*/
		/*FlagCallOn(BaseDecide* baseCl)
		{
			Call.DeFlag();
			TermsCl = baseCl;
		}*/
		~FlagCallOn() {}
		//! @brief 条件を満たすならフラグを立てる
		void MultiTask()override
		{
			/*if (TermsCl->Decide())
			{
				Call.OnFlag();
			}
			else
			{
				Call.DeFlag();
			}*/

		}
		//! @brief フラグの取得
		bool GetFlag()
		{
			return Call.get();
		}
	private:
		//! MultiTaskが呼ばれる条件(戻り値がtrueなら呼ぶ)
		//BaseDecide* TermsCl;
		//! MultiTaskが呼ばれたかのフラグ
		FlagM Call;
	};
	
	//! @brief 呼ばれた時、１回だけ真
	class FlagForOneTimeCall final : public BaseDecide<FlagForOneTimeCall>
	{
	private:
		//! このクラスの判断フラグ
		bool decideFlag;
		friend BaseDecide<FlagForOneTimeCall>;
	public:
		//! @brief 呼ばれた時、１回だけ真
		bool Decide() override
		{
			if (decideFlag)
				return false;
			decideFlag = true;
			return true;
		}
		bool Result()const override
		{
			return !decideFlag;
		}
		FlagForOneTimeCall()
			: BaseDecide(*this)
			, decideFlag(false)
		{}
		~FlagForOneTimeCall() = default;
	};
	//! @brief 呼ばれた時、１回だけ真
	class FlagForOneTimeCallCopy final : public BaseDecideCopy<FlagForOneTimeCall>
	{
	private:
		bool decideFlag;//! このクラスの判断フラグ
		friend BaseDecideCopy<FlagForOneTimeCall>;
	public:
		FlagForOneTimeCallCopy()
			: BaseDecideCopy(*this)
			, decideFlag(false)
		{}
		~FlagForOneTimeCallCopy() = default;
		//! @brief 呼ばれた時、１回だけ真
		bool Decide() override
		{
			if (decideFlag)
				return false;
			decideFlag = true;
			return true;
		}
		
	};


	//! @brief ビットフラグ
	class BitFlag
	{
		unsigned int bitFlag = 0;//! ビットフラグ
		unsigned int _max = 255;//! 最大数
	public:
		BitFlag() = default;
		//! @param[in] max_ 最大数を決める
		BitFlag(int max_)
			: _max(max_)
		{}
		~BitFlag() = default;
		//! ビットフラグをセット
		void SetBitFlag(BYTE f)
		{
			if (_max < f)
				return;
			bitFlag |= (1 << f);
		}
		//! ビットフラグデフラグ
		void DeBitFlag(BYTE f)
		{
			if (_max < f)
				return;
			bitFlag |= (1 << f);
			bitFlag ^= (1 << f);
		}
		//! ビットフラグがあっているか？
		//! @return 合っているならtrue
		bool IsRightBitFlag(BYTE f)
		{
			if (_max < f)
				return false;
			if ((bitFlag & f) != 0)
				return true;
		}
	};

	/*void x()
	{
		FlagForOneTimeCallCopy f{};
		f.pDecideCl->Decide();
	}*/
	namespace DecideSupport
	{
		//! クラスに持たせる(一般的にクラス内の判断クラスに対して使う)
		//! oneCallされるタイミングを決められるフラグ
	//! oneCallされる前まではtrueを返すだけ oneCallされた後、はfalseを返す
	//! このクラスが判断するとき、別のdecideClassの判断がtrueならfalse 、falseならtrue
	//! そして、このクラスがtrueなら別のクラスをfalseに
		template<typename AnotherDecideClass>//BaseDecide継承クラス
		class RecognizeFlagOn final : private BaseDecide<AnotherDecideClass>
		{
			BaseDecide<AnotherDecideClass>* anotherDecideClass;
			//BaseDecide<AnotherDecideClass>* anotherDecideClClone = nullptr;//判断クラスを再生成する用
			//! このクラスの判断フラグ
			bool tomegane;//falseで留め金が外れる
			bool FirstAnotherDecideResult;//初めの別判断クラスの判断結果
			
	
			//! @brief 呼ばれた時、１回だけ真
			bool Decide() override
			{

			}
			bool Result()const override
			{

			}
		public:
			//! 別の判断クラスをバインド
			RecognizeFlagOn(AnotherDecideClass* desideCl)
				: BaseDecide(desideCl)
				, anotherDecideClass(desideCl)
				, tomegane(true)
			{
				//this->ChangeDecide(desideCl);
				FirstAnotherDecideResult = this->anotherDecideClass->Result();
			}
			~RecognizeFlagOn() = default;
			//! 判断クラスが切り替わったとき、この関数がまた呼ばれたら、判断クラスを初期化する
			bool ReCreateDecideCL()
			{
				//! 判断が変わったら留め金をはずす
				if (FirstAnotherDecideResult != this->anotherDecideClass->Result())
					tomegane = false;
				//留め金が外れていたら//その判断クラスを初期化
				if (!tomegane)
				{
					BaseDecide<AnotherDecideClass> reCreate;
					*this->anotherDecideClass->pDecideCl = *reCreate.pDecideCl;
					//this->anotherDecideClass->ChangeDecide(reCreate.pDecideCl);
					tomegane = true:
				}
			}
		};
	}
	
	

	}//namespace Utility
}//namespace Lib_Base