#pragma once
//#include <cstdlib>
#include "../Lib_Template/Swallow.h"
#include <variant>
#include <vector>
namespace Lib_Template
{
	namespace Utility
	{
		//! このインデックスの時のバリアントをゲットする
		class VariantThis
		{
		public:
			struct SETTER : public SWALOW_VARIANT_SETTER<SETTER>
			{
			protected:
				SETTER() = default;
				~SETTER() = default;
				//! 同じ型なら、値を格納
				//! @param[in] i シェーダー番号
				//! @return 値がセットされるとtrue
				template<std::size_t I, typename T, typename TVariant>
				static bool SWALLOWHH_SETTER_V(T* outvar, size_t i, TVariant variant)
				{
					auto x = std::get<I>(variant);//i番目のバリアントを取得
					return Lib_Template::Set(outvar, x);//戻り値へセット
				}
			};
			
			struct SETTER_P : public SWALOW_VARIANT_SETTER<SETTER>
			{
			protected:
				SETTER_P() = default;
				~SETTER_P() = default;
				//! ポインタをセット
				//! 同じ型なら、値を格納
				//! @param[in] i シェーダー番号
				//! @return 値がセットされるとtrue
				template<std::size_t I, typename T, typename TVariant>
				static bool SWALLOWHH_SETTER_V(T* outvar, size_t i, TVariant variant)
				{
					auto x = std::get<I>(variant);//i番目のバリアントを取得
					return Lib_Template::SetP(outvar, x);//戻り値へセット
				}
			};
		private:
		public:
			//生成時バリアント取得
			VariantThis() = default;
			~VariantThis() = default;
			//! @param[out] invar バリアントの値を格納する変数ポインタ
			//! @return set出来たらtrue
			template<typename INVar, typename TVariant>
			static bool ThisVar(INVar* invar, TVariant variant)
			{
				return SETTER::MakeSequence_V(variant.index(), invar, NULL, variant);//i2は使わない
			}
			//! @ポインタを格納するバリアントから値を得る
			//! @param[out] invar バリアントの値を格納するポインタ
			//! @return set出来たらtrue
			template<typename INVar, typename TVariant>
			static bool ThisVar_P(INVar* invar, TVariant variant)
			{
				return SETTER_P::MakeSequence_V(variant.index(), invar, NULL, variant);//i2は使わない
			}
		};






		//! 圧縮クラス
		//! 別々の型を昇順ソート
		//! 配列を値を保持しつつ、endで配列へ値の順番に入れる
		//template<size_t SIZE>//SIZE 値を保持する数,ArrayT 配列タイプ
		template<class TVarinat>
		class ArrayDimensGrapAny
		{
			struct AA
			{
				int index;
				std::variant<TVarinat> var;
			};
			using IndexAnyT = AA;//インデックスを持つanyクラス配列
		public:
			std::vector<AA> indexVars;
		private:
			int intimes = 0;//要素を入れた回数
		public:
			ArrayDimensGrapAny() = default;
			~ArrayDimensGrapAny() = default;
			
			
			//! 要素を入れる(ここでポインタを入れたならGetVariableElementPを使う)
			//! @param[in] index 要素のid
			//! @param[in] data クラステンプレートで指定した要素
			template<typename T>
			void InElement(int index, T data)
			{
				indexVars.emplace_back();
				indexVars[intimes] = data;
				indexVars[intimes].index = index;
				intimes++;
			}
			//! @brief 昇順ソートで入れる
			//! @details 要素を入れ終わった後に呼ぶ
			void DimensUpSort()
			{
				//インデックスで配列をソート
				/* 数値を昇順にソート */
				for (int i = 0; i < indexVars.size(); ++i)
				{
					for (int j = i + 1; j < indexVars.size(); ++j)
					{
						if (indexVars[i].index > indexVars[j].index)
						{
							auto tmp = indexVars[i];
							indexVars[i] = indexVars[j];
							indexVars[j] = tmp;
						}
					}
				}
			}
			//! ポインタ取得
			//! @return trueなら取得成功
			template<typename TInVar>
			bool GetVariableElementP(TInVar* invar, int i)
			{
				return VariantThis::ThisVar_P(invar, indexVars[i]);
			}
			//! 値を取得
			//! @return trueなら取得成功
			template<typename TInVar>
			bool GetVariableElement(TInVar* invar, int i)
			{
				return VariantThis::ThisVar(invar, indexVars[i]);
			}
		};

		//! 同じ型版
		template<class CL>
		class ArrayDimensGrap
		{
			struct AA
			{
				int index;
				CL var;
			};
			
			int intimes = 0;//要素を入れた回数
		public:
			std::vector<AA> arr;
			//! @param[in] index 要素のid
			//! @param[in] data idにバインドされたデータ
			void InElement(int index, CL data)
			{
				arr.emplace_back();
				arr[intimes] = data;
				arr[intimes].index = index;
				intimes++;
			}
			void Sort()
			{
				//インデックスで配列をソート
				/* 数値を昇順にソート */
				for (int i = 0; i < arr.size(); ++i)
				{
					for (int j = i + 1; j < arr.size(); ++j)
					{
						if (arr[i].index > arr[j].index)
						{
							auto tmp = arr[i];
							arr[i] = arr[j];
							arr[j] = tmp;
						}
					}
				}
			}
			//要素クリア
			void Clear()
			{
				arr.clear();
				intimes = 0;
			}
		};
	}
}

