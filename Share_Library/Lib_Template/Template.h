#pragma once
#include <type_traits>

#include <iostream>
namespace Lib_Template
{
	//! 配列の長さ
	template<typename TYPE, size_t SIZE>
	inline constexpr size_t ArrayLength(const TYPE(&array)[SIZE])
	{
		return SIZE;
	}
	//! enum classをキャスト
	template<typename ENUM>
	constexpr typename std::underlying_type_t<ENUM> to_underlying(ENUM e)noexcept
	{
		return static_cast<typename std::underlying_type_t<ENUM>>(e);
	}
	//enum class I
	//{
	//	i,
	//};
	////! type define enum class
	//class EnumClint 
	//{
	//public:
	//	const char* arr[256];
	//	EnumClint(int index[], const char* elem[]) :
	//	{
	//		for (size_t i = 0; i < length; i++)
	//		{
	//			ParamIn(index[i], elem[i]);
	//		}
	//	}
	//	//! 
	//	constexpr void ParamIn(int index, const char* elem)
	//	{
	//		//! 名前が違うなら入れる
	//		for (size_t i = 0; i < length; i++)
	//		{
	//			if (elem == arr[i])
	//				return;
	//		}
	//		arr[index] = elem;
	//	}
	//	constexpr const char* Get(unsigned int i)const
	//	{
	//		return arr[i];
	//	}
	//	constexpr int Get(const char* elemname)const
	//	{
	//		for (size_t i = 0; i < length; i++)
	//		{
	//			if (elemname == arr[i])
	//				return i;
	//		}
	//	}
	//};
	//void x()
	//{
	//	constexpr EnumClint v;
	//	
	//	constexpr auto l = v.Get();
	//}
	namespace details
	{

		template< typename E >
		using enable_enum_t = typename std::enable_if< std::is_enum<E>::value,
			typename std::underlying_type<E>::type
		>::type;

	}   // namespace details


	template< typename E >
	constexpr inline details::enable_enum_t<E> underlying_value(E e)noexcept
	{
		return static_cast<typename std::underlying_type<E>::type>(e);
	}


	template< typename E, typename T>
	constexpr inline typename std::enable_if< std::is_enum<E>::value &&
		std::is_integral<T>::value, E
	>::type
		to_enum(T value) noexcept
	{
		return static_cast<E>(value);
	}

};
