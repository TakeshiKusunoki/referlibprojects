#pragma once
#include <cstdint>
#include <functional>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

// T から const/volatile/reference を除いた型
template <typename T>
using remove_cv_reference_t =
typename std::remove_cv<typename std::remove_reference<T>::type>::type;
// T0,...,Ts の中で T が出現する最初の位置
template <size_t I, typename T, typename T0, typename... Ts>
struct index_of_impl
{
	static const size_t value = (std::is_same<T, T0>::value)
		? I
		: index_of_impl<I + 1, T, Ts...>::value;
};
// T0,...,Ts の中で T が出現する最初の位置
template <size_t I, typename T, typename T0>
struct index_of_impl<I, T, T0>
{
	static const size_t value =
		(std::is_same<T, T0>::value) ? I : static_cast<size_t>(-1);
};
// T0,...,Ts の中で I 番目の型
template <size_t I, typename T0, typename... Ts>
struct at_impl
{
	using type =
		typename std::conditional<I == 0,
		T0,
		typename at_impl<I - 1, Ts...>::type>::type;
};
// T0,...,Ts の中で I 番目の型
template <size_t I, typename T0>
struct at_impl<I, T0>
{
	using type = typename std::conditional<I == 0, T0, void>::type;
};
// 型のリスト
template <typename... Ts>
struct type_list_t
{
	// 型数
	static constexpr size_t size() { return sizeof...(Ts); };
	// T が最初に現れる位置
	template <typename T>
	static constexpr size_t index_of()
	{
		return index_of_impl<0,
			remove_cv_reference_t<T>,
			remove_cv_reference_t<Ts>...>::value;
	}
	// I 番目の型
	template <size_t I>
	using at_t = typename at_impl<I, Ts...>::type;

	// idx 番目の型 T について, f.operator()<T>() を実行する
	template <typename F, typename Index>
	static auto apply(F&& f, Index idx) -> decltype(auto)
	{
		using R = decltype(f.template operator() < at_t<0> > ());  // 戻り値の型
		static std::make_index_sequence<size()> seq;  // 整数シーケンス
		return apply<R>(seq, std::forward<F>(f), static_cast<int>(idx));
	}

private:
	// idx 番目の型 T について, f.operator()<T>() を実行する
	template <typename R, typename F, size_t... Is>
	static R apply(std::index_sequence<Is...>, F&& f, int idx)
	{
		using func_t = decltype(&apply<R, F, at_t<0>>);  // 関数ポインタの型
		// 関数ポインタテーブルを生成
		// idx 番目の関数ポインタは apply<R, F, at_t<idx>> である
		static func_t func_table[] = { &apply<R, F, at_t<Is>>... };
		return func_table[idx](std::forward<F>(f));
	}
	// 型 T について, f.operator()<T>() を実行する
	template <typename R, typename F, typename T>
	static R apply(F&& f)
	{
		return f.template operator() < T > ();
	}
};

// 加算
struct plus_t
{
	template <typename T1, typename T2>
	auto operator()(T1&& lhs, T2&& rhs) const -> decltype(auto)
	{
		return std::forward<T1>(lhs) + std::forward<T2>(rhs);
	}
};
// 減算
struct minus_t
{
	template <typename T1, typename T2>
	auto operator()(T1&& lhs, T2&& rhs) const -> decltype(auto)
	{
		return std::forward<T1>(lhs) - std::forward<T2>(rhs);
	}
};
// 乗算
struct multiply_t
{
	template <typename T1, typename T2>
	auto operator()(T1&& lhs, T2&& rhs) const -> decltype(auto)
	{
		return std::forward<T1>(lhs) * std::forward<T2>(rhs);
	}
};
// 除算(ゼロ割チェック付き)
struct divide_t
{
	template <typename T1, typename T2>
	auto operator()(T1&& lhs, T2&& rhs) const -> decltype(auto)
	{
		if (rhs == static_cast<remove_cv_reference_t<T2>>(0))
		{
			throw std::invalid_argument("zero division");
		}
		return std::forward<T1>(lhs) / std::forward<T2>(rhs);
	}
};
// 演算子ファンクタリスト
using op_type_list_t = type_list_t<plus_t, minus_t, multiply_t, divide_t>;
// 演算子を表す Enum
enum class op_type_e : int8_t
{
	PLUS = op_type_list_t::index_of<plus_t>(),
	MINUS = op_type_list_t::index_of<minus_t>(),
	MULTIPLY = op_type_list_t::index_of<multiply_t>(),
	DIVIDE = op_type_list_t::index_of<divide_t>()
};
// 演算子文字からEnum
static const std::unordered_map<std::string, op_type_e> g_char_to_enum{
	{"+", op_type_e::PLUS},
	{"-", op_type_e::MINUS},
	{"*", op_type_e::MULTIPLY},
	{"/", op_type_e::DIVIDE} };



// op_type_list_t::apply に渡す計算ファンクタ
struct calculator_t
{
	int lhs{ 0 };
	int rhs{ 0 };

	// テンプレート引数を部分適用
	template <typename T0, typename... Ts>
	struct curried_t
	{
		const calculator_t* self{ nullptr };
		template <typename T>
		value_t operator()() const
		{
			assert(self);
			return self->operator() < T0, Ts..., T > ();
		}

	};

	template <typename Op>
	int operator()() const
	{
		return Op()(lhs, rhs);
	}
};

static int calculate(const std::string& op, int lhs, int rhs)
{
	op_type_e op_type = [op]()
	{
		auto iter = g_char_to_enum.find(op);
		if (iter == g_char_to_enum.end())
		{
			throw std::invalid_argument("invalid operator");
		}
		return iter->second;
	}();
	return op_type_list_t::apply(calculator_t{ lhs, rhs }, op_type);
}

void l()
{
	auto x = calculate("+", 0, 2);
}