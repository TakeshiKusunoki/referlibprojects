#pragma once
//
//  variant_util.h
//  メタ関数群
//  Created by atosh on 2/22/15.
//

#ifndef VARIANT_UTIL_H_
#define VARIANT_UTIL_H_

#include <type_traits>
namespace Lib_Base
{
	namespace metatemplate
	{
		//! 可変引数の中で最大サイズを持つ型のサイズ
		//! T 
		//! ...Rest 可変引数
		template <typename T, typename... Rest>
		struct max_size_t
		{
		private:
			static const int kRestMax = max_size_t<Rest...>::value;//! 再帰的に調査
		public:
			static const int value = sizeof(T) > kRestMax ? sizeof(T) : kRestMax;//! １番大きい型を洗い出す(可変長テンプレートなので随時サイズを調べている)
		};

		template <typename T>
		struct max_size_t<T>
		{
			static const int value = sizeof(T);
		};

		//! 指定した型が含まれているか調べるメタ関数
		template <typename Target, typename T, typename... Rest>
		struct contains_t
		{
			static const bool value = (std::is_same<Target, T>::value) ? true : contains_t<Target, Rest...>::value;
		};

		template <typename Target, typename T>
		struct contains_t<Target, T>
		{
			static const bool value = (std::is_same<Target, T>::value);
		};

		template <int I, typename Target, typename T, typename... Rest>
		struct index_of_impl_t
		{
			static const int value = (std::is_same<Target, T>::value) ? I : index_of_impl_t<I + 1, Target, Rest...>::value;
		};

		template <int I, typename Target, typename T>
		struct index_of_impl_t<I, Target, T>
		{
			static const int value = (std::is_same<Target, T>::value) ? I : -1;
		};

		//! 指定した型のインデックスを返すメタ関数
		template <typename Target, typename... Types>
		struct index_of_t
		{
			static const int value = index_of_impl_t<0, Target, Types...>::value;
		};

		//! 指定したインデックスの型を返すメタ関数
		template <int I, typename T, typename... Rest>
		struct at_t
		{
			using type = typename at_t<I - 1, Rest...>::type;
		};

		template <typename T, typename... Rest>
		struct at_t<0, T, Rest...>
		{
			using type = T;
		};

	}  // metatemplate
}
#endif  // VARIANT_UTIL_H_