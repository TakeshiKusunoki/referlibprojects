#pragma once
namespace Lib_Template
{
	template<typename ENUM>
	constexpr typename std::underlying_type_t<ENUM> to_underlying(ENUM e)noexcept
	{
		return static_cast<typename std::underlying_type_t<ENUM>>(e);
	}
};