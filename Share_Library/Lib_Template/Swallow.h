#pragma once
#include <array>

namespace Lib_Template
{
	//タイプが同じときセット
	//! @param[in] get ポインタ
	//! @param[in] in 値
	template<typename T>
	bool Set(T* get, T in)
	{
		*get = in;
		return true;
	}
	//タイプが違うとき
	template<typename T, typename  CBufElemType>
	bool Set(T* get, CBufElemType in)
	{
		return false;
	}

	//ポインタのタイプが同じときポインタをセット
	template<typename T>
	bool SetP(T* get, T* in)
	{
		get = in;
		return true;
	}
	//ポインタのタイプが違うとき
	template<typename T, typename  CBufElemType>
	bool SetP(T* get, CBufElemType* in)
	{
		return false;
	}

#define SWALLOWHH_FUNC Call
//#define FuncExecute FuncExecute
	//! @brief template<size_t i> の関数を格納
	//! @detailes
	//! このクラスを継承した構造体内に
	//! staticの関数をSWALLOWHH_FUNCの名前でtemplate<size_t i>SWALLOWHH_FUNCを自由に作る
	//! このクラスの派生クラスではint型のコンストラクタをオーバーロードする
	//! FuncExecuteを使う
	//! template (R FuncExecuteの戻り値 : SIZE インデックスサイズ)
	template<typename R, size_t SIZE>
	class SWALOWHH
	{
	private:
		using Func = R(*)();
		std::array<Func, SIZE> ff;//staticで入れる
		template<typename T, std::size_t I>
		struct CALL : private T
		{
			CALL() : T(NULL)//再起継承を派生クラスのコンストラクタのオーバーロードで防ぐ
			{
				f = T::SWALLOWHH_FUNC<I>;
			}
			Func f;
		};

		template<typename FUNCT, std::size_t... Indices>
		static std::array<Func, SIZE> Swallow_(FUNCT funcCl, std::index_sequence<Indices...>)
		{
			std::array<Func, SIZE> funcArray = {
				(CALL<FUNCT, Indices>().f)...
			};
			/*Func funcArray[] = {
				(FuncT::Call<Indices>)...
			};*/
			return funcArray;
		}
		template<typename FUNCT>
		static std::array<Func, SIZE> MakeSequence_(FUNCT funcCl)
		{
			return Swallow_(funcCl, std::make_index_sequence<SIZE>{});
		}
	protected:
		//! 関数配列を確保
		template<typename FUNCT>
		SWALOWHH(FUNCT funcCl)
			: ff(MakeSequence_(funcCl))
		{}
		SWALOWHH() = default;
	public:
		virtual ~SWALOWHH() {}
		//! 格納した関数を実行
		//! staticな関数から値を得る
		//! (複数種作れるようこの関数をstaticにするのは避ける)
		//! @param[in] i i番目の関数
		R FuncExecute(size_t i)
		{
			if (i >= SIZE)
				i = SIZE - 1;
			return ff[i]();
		}
	};




#define SWALLOWHH_SETTER Sets
	//! template<size_t i> の関数を格納
	//! このクラスを継承した構造体内に
	//! staticの関数をSWALLOWHH_SETTERの名前でtemplate<size_t i>SWALLOWHH_SETTERを自由に作る
	//! MakeSequence_を使う
	//! template(SIZE インデックスサイズ : FUNCT このクラスを派生させたクラス)
	template<size_t SIZE, typename FUNCT>
	class SWALOW_SETTER
	{
	private:
		template<typename T, std::size_t I>
		struct SETS : private FUNCT
		{
			SETS()
			{
				f = FUNCT::SWALLOWHH_SETTER<I>;
			}
			using Func = bool(*)(T*, size_t);
			Func f;
		};

		template<typename T, std::size_t... Indices>
		static bool Swallow_(size_t i, size_t i2, T* invar, std::index_sequence<Indices...>)
		{
			using Func = bool(*)(T*, size_t);
			Func funcArray[] = {
				(SETS<T, Indices>().f)...
			};
			return funcArray[i](invar, i2);
		}
	protected:
		//! 関数配列を確保
		SWALOW_SETTER() = default;
	public:
		virtual ~SWALOW_SETTER() {}
		//! i 1次 bind_variant : i2 R(*)() funcs
		//! @param[in] i 関数配列何番目を使うか？
		//! @param[in] i2　もう一つのi(別のswallowを使うときとか)
		//! @param[in] invar 値を格納する変数
		template<typename T>
		static bool MakeSequence_(size_t i, T* invar, size_t i2 )
		{
			if (i >= SIZE)
				return false;
			return Swallow_(i, i2, invar, std::make_index_sequence<SIZE>{});
		}
	};

#define SWALLOWHH_SETTER_V Sets
	//! バリアント専用セッター
	//! template<size_t i> の関数を格納
	//! このクラスを継承した構造体内に
	//! staticの関数をSWALLOWHH_SETTERの名前でtemplate<size_t i>SWALLOWHH_SETTERを自由に作る
	//! MakeSequence_を使う
	//! template(SIZE インデックスサイズ : FUNCT このクラスを派生させたクラス)
	template<typename FUNCT>
	class SWALOW_VARIANT_SETTER
	{
	private:
		template<typename T, typename TVariant, std::size_t I>
		struct SETS : private FUNCT
		{
			SETS()
			{
				f = FUNCT::SWALLOWHH_SETTER_V<I>;
			}
			using Func = bool(*)(T*, size_t, TVariant);
			Func f;
		};

		template<typename T, typename TVariant, std::size_t... Indices>
		static bool Swallow_(size_t i, size_t i2, T* invar, TVariant variant, std::index_sequence<Indices...>)
		{
			using Func = bool(*)(T*, size_t, TVariant);
			Func funcArray[] = {
				(SETS<T, TVariant, Indices>().f)...
			};
			return funcArray[i](invar, i2, variant);
		}
	protected:
		//! 関数配列を確保
		SWALOW_VARIANT_SETTER() = default;
	public:
		virtual ~SWALOW_VARIANT_SETTER() {}
		//! i 1次 bind_variant : i2 R(*)() funcs
		//! @param[in] i 関数配列何番目を使うか？
		//! @param[in] i2　もう一つのi(別のswallowを使うときとか)
		//! @param[in] invar 値を格納する変数
		template<typename T, typename TVariant>
		static bool MakeSequence_V(size_t i, T* invar, size_t i2, TVariant variant)
		{
			constexpr size_t SIZE = std::variant_size_v<TVariant>;
			if (i >= SIZE)
				return false;
			return Swallow_(i, i2, invar, variant, std::make_index_sequence<SIZE>{});
		}
	};


}//Lib_Template
		