#pragma once
#ifndef INCLUDED_VECTOR
#define INCLUDED_VECTOR
//******************************************************************************
//
//
//      Vectorクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include <type_traits>
#include <cmath>

//==============================================================================
//
//      VECTOR2クラス
//
//==============================================================================
namespace Lib_Math
{
	class VECTOR2 final
	{
	public:
		float x;
		float y;

		//VECTOR2() = default;
		//VECTOR2(const  VECTOR2&) = default;
		//VECTOR2& operator=(const  VECTOR2&) = default;
		VECTOR2(VECTOR2&&) = default;
		VECTOR2& operator=(VECTOR2&&) = default;
		constexpr  VECTOR2(float _x = 0, float _y = 0) : x(_x), y(_y) {}
		explicit  VECTOR2(_In_reads_(2) const float* pArray) : x(pArray[0]), y(pArray[1]) {}
		VECTOR2(const VECTOR2& v) { x = v.x; y = v.y; }
		~VECTOR2() = default;

		VECTOR2& operator=(const VECTOR2&) noexcept;
		VECTOR2& operator+=(const VECTOR2&) noexcept;
		VECTOR2& operator-=(const VECTOR2&) noexcept;
		VECTOR2& operator+=(float) noexcept;
		VECTOR2& operator-=(float) noexcept;
		VECTOR2& operator*=(float) noexcept;
		VECTOR2& operator*=(const VECTOR2&) noexcept;
		VECTOR2& operator/=(float) noexcept;
		VECTOR2& operator/=(const VECTOR2&) noexcept;

		VECTOR2 operator+() const noexcept;
		VECTOR2 operator-() const noexcept;

		VECTOR2 operator+(const VECTOR2&) const noexcept;
		VECTOR2 operator-(const VECTOR2&) const noexcept;
		VECTOR2 operator*(float) const noexcept;
		VECTOR2 operator*(const VECTOR2&) const noexcept;
		friend VECTOR2 operator*(float, const VECTOR2&) noexcept;
		VECTOR2 operator/(float) const noexcept;
		VECTOR2 operator/(const VECTOR2&) const noexcept;

		bool operator == (const VECTOR2&) const noexcept;
		bool operator != (const VECTOR2&) const noexcept;
		//bool operator == (const VECTOR2&) const noexcept;
		//bool operator != (const VECTOR2&) const noexcept;

		float vec2LengthSq() const noexcept;
		float vec2Length() const noexcept;
		VECTOR2 vec2Normalize() const noexcept;
	};

	//------< プロトタイプ宣言 >-----------------------------------------------------



	//==============================================================================
	//
	//      VECTOR3クラス
	//
	//==============================================================================

	class VECTOR3 final
	{
		static constexpr  float EPSILON = 0.000001f;	// 誤差
	public:
		float x;
		float y;
		float z;

		VECTOR3(const VECTOR3&) = default;
		VECTOR3& operator=(const VECTOR3&) = default;
		VECTOR3(VECTOR3&&) = default;
		VECTOR3& operator=(VECTOR3&&) = default;
		constexpr VECTOR3(float _x=0, float _y=0, float _z=0) : x(_x), y(_y), z(_z) {}
		explicit VECTOR3(_In_reads_(3) const float* pArray) : x(pArray[0]), y(pArray[1]), z(pArray[2]) {}
		//constexpr VECTOR3(const VECTOR3& v) : x(v.x), y(v.y), z(v.z) {}
		~VECTOR3() = default;

		//VECTOR3& operator=(const VECTOR3&);
		VECTOR3& operator+=(const VECTOR3&);
		VECTOR3& operator-=(const VECTOR3&);
		VECTOR3& operator*=(float);
		VECTOR3& operator/=(float);

		VECTOR3& operator*=(const VECTOR3& v);

		VECTOR3& operator/=(const VECTOR3& v);

		VECTOR3 operator+() const;
		VECTOR3 operator-() const;
		VECTOR3 operator+(const VECTOR3&) const;
		VECTOR3 operator+(float f) const;
		VECTOR3 operator-(const VECTOR3& v) const;
		VECTOR3 operator-(float f) const;
		VECTOR3 operator*(float) const;
		VECTOR3 operator*(const VECTOR3& v) const;
		friend VECTOR3 operator*(float, const VECTOR3&)noexcept;
		VECTOR3 operator/(float) const;

		VECTOR3 operator/(const VECTOR3& v) const;

		bool operator == (const VECTOR3&) const;
		bool operator != (const VECTOR3&) const;

	private:
		//! length用のみ
		template < typename T=float >
		constexpr T sqrt_(T s)const noexcept
		{
			T x = s / 2.0f;
			T prev = 0.0f;

			while (x != prev)
			{
				prev = x;
				x = (x + s / x) / 2.0f;
			}
			return x;
		}
	public:
		//! @brief 長さ
		[[nodiscard]]
		constexpr float Length() const noexcept
		{
			return sqrt_<float>(x * x + y * y + z * z);
		}
		//! @brief べき乗長さ
		[[nodiscard]]
		constexpr float LengthSq() const noexcept
		{
			return x * x + y * y + z * z;
		}
		//! @return 点と点との距離を返す
		[[nodiscard]]
		float Length(const VECTOR3& v) const
		{
			auto V = *this - v;
			return sqrt_<float>(V.x * V.x + V.y * V.y + V.z * V.z);
		}
		//! @return 点と点との距離の2乗を返す
		[[nodiscard]]
		float LengthSq(const VECTOR3& v) const
		{
			auto V = *this - v;
			return V.x * V.x + V.y * V.y + V.z * V.z;
		}
		//! @brief 正規化
		//! o,o,oなら
		[[nodiscard]]
		VECTOR3 Normalize() const noexcept;
		
		//! 内積
		[[nodiscard]]
		constexpr float Dot(const VECTOR3& v) const noexcept
		{
			return x * v.x + y * v.y + z * v.z;
		}
		//! 外積
		[[nodiscard]]
		VECTOR3 Cross(const VECTOR3& v) const noexcept;
		


		//! 垂直関係にあるならtrue
		[[nodiscard]]
		constexpr bool IsVertical(const VECTOR3& r) const noexcept
		{
			float d = Dot(r);
			return (-EPSILON < d && d < EPSILON);	//誤差範囲内なら垂直と判定 //内積が０なら直角
		}

		//! 平行関係にあるならtrue
		[[nodiscard]]
		constexpr bool IsParallel(const VECTOR3& r) const noexcept
		{
			float d = Cross(r).LengthSq();
			return (-EPSILON < d && d < EPSILON);	//誤差範囲内なら平行と判定//外積が０なら平行
		}

		// 鋭角ならtrue
		[[nodiscard]]
		constexpr bool IsSharpAngle(const VECTOR3& r) const noexcept
		{
			return (Dot(r) >= 0.0f);//内積が０以上なら鋭角
		}

	};




	//==============================================================================
	//
	//      VECTOR4クラス
	//
	//==============================================================================

	class VECTOR4 final
	{
	public:
		union MyUnion
		{

		};
		float x;
		float y;
		float z;
		float w;

		//VECTOR4() = default;
		VECTOR4(const VECTOR4&) = default;
		VECTOR4& operator=(const VECTOR4&) = default;
		VECTOR4(VECTOR4&&) = default;
		VECTOR4& operator=(VECTOR4&&) = default;
		constexpr VECTOR4(float _x = 0, float _y = 0, float _z = 0, float _w = 0) : x(_x), y(_y), z(_z), w(_w) {}
		explicit VECTOR4(_In_reads_(4) const float* pArray) : x(pArray[0]), y(pArray[1]), z(pArray[2]), w(pArray[3]) {}
		~VECTOR4() = default;
		constexpr float operator[](int idx) const
		{
			switch (idx)
			{
			default:
				break;
			case 0:
				return x;
				break;
			case 1:
				return y;
				break;
			case 2:
				return z;
				break;
			case 3:
				return w;
				break;
			}
			return x;
		}
		VECTOR4 operator+(const VECTOR4& v) const noexcept
		{
			return std::move(VECTOR4{ x + v.x, y + v.y, z + v.z, w + v.w });
		}
		constexpr VECTOR4 operator-(const VECTOR4& v) const noexcept
		{
			return std::move(VECTOR4{ x - v.x, y - v.y, z - v.z, w - v.w });
		}
		VECTOR4 operator*(const VECTOR4& v) const noexcept
		{
			return std::move(VECTOR4{ x * v.x, y * v.y, z * v.z, w * v.w });
		}
		VECTOR4 operator*(float v) const noexcept
		{
			return std::move(VECTOR4{ x * v, y * v, z * v, w * v });
		}
//		constexpr VECTOR4 Cross4(VECTOR4 U, VECTOR4 V, VECTOR4 W)const
//		{
//			//中間値を計算します。
//#define A (V[0] * W[1]) - (V[1] * W[0])
//#define B (V[0] * W[2]) - (V[2] * W[0])
//#define C (V[0] * W[3]) - (V[3] * W[0])
//#define D (V[1] * W[2]) - (V[2] * W[1])
//#define E (V[1] * W[3]) - (V[3] * W[1])
//#define F (V[2] * W[3]) - (V[3] * W[2])
//
////結果ベクトル成分を計算します。
//			return VECTOR4{
//				 (U[1] * F) - (U[2] * E) + (U[3] * D),
//				-(U[0] * F) + (U[2] * C) - (U[3] * B),
//				 (U[0] * E) - (U[1] * C) + (U[3] * A),
//				-(U[0] * D) + (U[1] * B) - (U[2] * A)
//			};
//		}

		static constexpr float Dot4(VECTOR4 U, VECTOR4 V)
		{
			return ((V[0] * U[0]) + (V[1] * U[1]) + (V[2] * U[2]) + (V[3] * U[3]));
		}
	};



}//Lib_Math
//******************************************************************************

#endif // !INCLUDED_VECTOR