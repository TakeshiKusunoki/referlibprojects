#include "PrimitivClass.h"

namespace Lib_Collition {


	const VECTOR3 & Point::CalcVectorToPlane(const Plane & plane) const
	{
		float l = this->p.Dot(plane.n);
		float l2 = plane.d - l;
		return plane.n*l;//点から平面への最短距離ベクトル
	}
	const Point & Point::CalcVectorToLine(const Line & l)const
	{
		float lenSqV = l.v.LengthSq();
		float t = 0.0f;
		if (lenSqV > 0.0f)
			t = l.v.Dot(this->p - l.p.p) / lenSqV;

		return t * l.v;
	}
	float Point::CalculatePointLineDist(const Line & l)const
	{
		VECTOR3 h = l.p.p + CalcVectorToLine(l).p;
		return static_cast<VECTOR3>(h - this->p).Length();
	}

	bool Point::isSharpAngle(const Point& p1, const Point& p2)
	{
		return static_cast<VECTOR3>(p1.p - this->p).IsSharpAngle(p2.p - this->p);
	}









	//+ 共用体--------------------------------------------------------
	
}