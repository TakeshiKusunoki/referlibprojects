#ifndef __PRIMITIVE_CLASS_H__
#define __PRIMITIVE_CLASS_H__

#pragma once
#include <cmath>
#include <array>
#include <typeinfo>
#include <Lib_Math/vector_const.h>
#include <Lib_Math/MyMath.h>
//#include <Lib_Template/Property.h>
//#include "..//Lib_Base/Variant.h"
#include <vector>
#include <variant>
#include <functional>
#include <map>

namespace Lib_Math
{
	namespace Primitive
	{
		struct Line;
		struct Segment;
		struct AABB;
		struct Plane;
		//! 点
	//! ポリモーフィズムを利用しない
		struct Point final
		{
			VECTOR3_CONST p = VECTOR3_CONST(0, 0, 0);//! 座標

			Point() = default;
			Point(const Point&) = default;
			constexpr Point(float x, float y, float z) : p(x, y, z) {}
			constexpr Point(const VECTOR3_CONST v) : p(v) {}
			//constexpr explicit Point(VECTOR3_CONST&& v) : p(v) {}
			//constexpr Point(const VECTOR3_CONST v) : p(v) {}
			//constexpr Point(const VECTOR3_CONST& v) : p(v) {}
			//constexpr Point(const DirectX::XMVECTOR& other) : p(other) {}
			//DEFAULT_COPY_MOVE_CONSTRACTOR(Point)
			~Point() = default;

			//デバッグでは遅い[[deprecated]] のけた
			constexpr const VECTOR3_CONST& operator()() const noexcept
			{
				return this->p;
			}
			constexpr void operator=(const VECTOR3_CONST& v)
			{
				p = v;
			}
			constexpr bool operator == (const Point& v)const
			{
				return (p == v.p);
			}
			
			
			////! 点から直線へ下したベクトル(点から直線への最短距離ベクトル)
			////! @param[in]  p : 点
			////! @param[in]  l : 直線
			////! @return 戻り値: 直線上の点の座標
			//constexpr const Point& CalcVectorToLine(const Line& l)const;

			////! 点と直線の最短距離
			////! @param[in]  l : 直線
			////! @return 戻り値: 最短距離
			//constexpr float CalculatePointLineDist(const Line& l)const;

			//! この点からp1、p2へのびる角は鋭角か？
			//! param[in] 角をなす3つの頂点座標 this→p2 this→p1
			//! @return 鋭角ならtrue
			constexpr bool isSharpAngle(const Point& p1, const Point& p2);

			/*constexpr const Point& LineOnPoint(float t) const
			{
				return Point(p + t * v);
			}*/
			static constexpr Point NanPoint() { return Point{ NAN, NAN, NAN }; };//頂点なし
		private:

		};
		//! AABB
		struct AABB final
		{
			Point _MinPos = Point();	//! １番座標上で小さい座標
			Point _MaxPos = Point();	//! １番座標上で大きい座標
			//float diagonalLength;
			//VECTOR3_CONST _hl;	// 各軸の辺の長さの半分
			AABB() = default;
			//! @param[in] p 中心点
			//! @param[in] hl 各軸の辺の長さの半分
			constexpr AABB(const Point& p, const VECTOR3_CONST& hl) /*: _hl(hl)*/
				: _MinPos(p.p - hl)
				, _MaxPos(p.p + hl)
			{}
			//! @param[in] p 中心点
			//! @param[in] hl 最大幅
			constexpr AABB(const Point& p, const float hl)/* : _hl{ hl, hl, hl }*/
				: _MinPos(p.p - hl / 2)
				, _MaxPos(p.p + hl / 2)
			{}
			//DEFAULT_COPY_MOVE_CONSTRACTOR(AABB)
			~AABB() = default;

			//! @対角線の距離
			constexpr float DiagonalLength() const noexcept
			{
				return _MinPos.p.Length(_MaxPos.p);
			}


			// 辺の長さを取得
			/*float lenX() const { return _hl.x * 2.0f; };
			float lenY() const { return _hl.y * 2.0f; };
			float lenZ() const { return _hl.z * 2.0f; };
			float len(int i) const
			{
				return *((&_hl.x) + i) * 2.0f;
			}*/
		};
		//! 平面
		struct Plane final
		{
			VECTOR3_CONST n = VECTOR3_CONST();//! 法線(normalizeされている)
			float height = 0;//! 原点からの高さ
			Plane() = default;
			constexpr Plane(const float height, const VECTOR3_CONST& n)
				: height(height)
				, n(n.Normalize())
			{};
			//DEFAULT_COPY_MOVE_CONSTRACTOR(Plane)
			~Plane() = default;

			//! 平面原点から(この点の法線への射影長分)のベクトル
			constexpr VECTOR3_CONST CalcVectorToPlane(const Point& point)const
			{
				float l = point.p.Dot(this->n.Normalize());//この位置からの法線へ卸した射影長(pは原点からのベクトルなのに注意)
				float l2 = this->ThisOriginPiliodDistance() - l;
				return { this->n.Normalize() * l2 };//平面原点からこの点の射影長分のベクトル
			}

			//! 原点からの距離
			constexpr float ThisOriginPiliodDistance() const noexcept
			{
				return VECTOR3_CONST(0, height, 0).Dot(n.Normalize());
			}

			//! 原点から１番近い点
			constexpr const VECTOR3_CONST& GetOriginNearPiliodPos() const noexcept
			{
				return ThisOriginPiliodDistance() * n;
			}
			
			
		};

	

		//! 直線
		struct Line final
		{
			Point p = { 0,0,0 };//! 線上のある点
			VECTOR3_CONST v = VECTOR3_CONST(0.0f, 0.0f, 0.0f);//! 方向ベクトル

			Line() = default;
			constexpr Line(const Point& p, const VECTOR3_CONST& v) : p(p), v(v.Normalize()) {}
			//2つの点(方向ベクトルはpからp2へのベクトル)
			constexpr Line(const Point& p, const Point& p2) : p(p) ,v((p2.p - p.p).Normalize()) {}
			//DEFAULT_COPY_MOVE_CONSTRACTOR(Line)
			~Line() = default;
			//! 点上の座標を取得
			//! ベクトルに掛け算する係数
			const Point&& LineOnPoint(float t) const
			{
				return p.p + t * v.Normalize();
			}
			//! @return AABB
			constexpr const AABB ThisAABB() const noexcept
			{
				return { p , 0 };
			}
		};


		//! 線分
		struct Segment final
		{
			Point start = Point();//! 始点座標
			Point end = Point();//! 終点座標

			Segment() = default;
			constexpr Segment(const Point& p, const Point& p2) : start(p), end(p2) {}
			//DEFAULT_COPY_MOVE_CONSTRACTOR(Segment)
			~Segment() = default;
			//! 傾き
			constexpr VECTOR3_CONST ThisDirection() const noexcept
			{
				VECTOR3_CONST v;
				v.x = Lib_Math::epsilos((end.p.x - start.p.x));
				v.y = Lib_Math::epsilos((end.p.y - start.p.y));
				v.z = Lib_Math::epsilos((end.p.z - start.p.z));
				return v.Normalize();
			}

			//! 内分比 1:2なら　i = 1; k = 3
			//! @param i 分子
			//! @param k 分母
			constexpr const VECTOR3_CONST& GetInternalDivision(float i, float k) const noexcept
			{
				float l_ = start.p.Length() * i / k;
				return start.p + start.p.Normalize() * l_;
			}
			//! @brief 線分
			constexpr const Line ThisLine()const noexcept
			{
				return Line(start, ThisDirection());
			}
			
			//! @return AABB
			constexpr const AABB ThisAABB() const noexcept
			{
				Point halfPos = VECTOR3_CONST(end() - start()) / 2 + start();//真ん中の座標
				VECTOR3_CONST hl;//辺の長さ
				hl.x = fabsC(start.p.x - halfPos.p.x);
				hl.y = fabsC(start.p.y - halfPos.p.y);
				hl.z = fabsC(start.p.z - halfPos.p.z);
				return AABB{ halfPos , hl };
			}
		};




		//! 球
		struct Sphere final
		{
			Point p = Point();//! 中心点
			float r = 0;	// 半径
			Sphere() = default;
			constexpr Sphere(const Point& p, float r) : p(p), r(r) {}
			//DEFAULT_COPY_MOVE_CONSTRACTOR(Sphere)
			~Sphere() = default;
			//! @return AABB
			constexpr const AABB ThisAABB() const noexcept
			{
				return AABB{ p , r };
			}
		};


		//! カプセル
		struct Capsule final
		{
			Segment s{ Point(), Point() };//! 線分
			float r = 0;	//! 半径

			Capsule() = default;
			constexpr Capsule(const Segment& s, float r) : s(s), r(r) {}
			constexpr Capsule(const Point& p1, const Point& p2, float r) : s(p1, p2), r(r) {}
			//DEFAULT_COPY_MOVE_CONSTRACTOR(Capsule)
			~Capsule() = default;

			//! @return AABB
			constexpr const AABB ThisAABB() const/* noexcept*/
			{
				auto halfl = (s.end() - s.start()).Length() / 2 + r;//カプセルのの半分の長さ

				Point halfPos = (s.end() - s.start()) / 2 + s.start();//真ん中の座標
				auto tol = (s.end() - halfPos()).Dot({ 1,0,0 });//射影長
				auto ansl = halfl - tol;
				auto ansv = s.ThisDirection() * ansl + s.end();

				VECTOR3_CONST hl;//辺の長さ
				hl.x =fabsC(ansv.x - halfPos.p.x);
				hl.y =fabsC(ansv.y - halfPos.p.y);
				hl.z =fabsC(ansv.z - halfPos.p.z);
				return AABB{ halfPos , hl };
			}
		};

		

		//! @brief 三角形ポリゴン
		struct Triangle final
		{
			Point p1 = Point();//! ポリゴンの頂点
			Point p2 = Point();//! ポリゴンの頂点
			Point p3 = Point();//! ポリゴンの頂点
			Triangle() = default;
			constexpr Triangle(const Point& p1_, const Point& p2_, const Point& p3_) :p1(p1_), p2(p2_), p3(p3_) {}
			//DEFAULT_COPY_MOVE_CONSTRACTOR(Triangle)
			~Triangle() = default;
			//! @return 線分
			constexpr const std::array<Segment, 3>& GetSegment() const
			{
				return std::array<Segment, 3>{Segment{ p1, p2 }, Segment{ p2, p3 }, Segment{ p3, p1 }};
			}
		};


		//! 四角形ポリゴン
		struct Square final
		{
			Point p1;//! ポリゴンの頂点
			Point p2;//! ポリゴンの頂点
			Point p3;//! ポリゴンの頂点
			Point p4;//! ポリゴンの頂点
			Square() = default;
			//constexpr explicit Square(const VECTOR3_CONST& n) {};
			~Square() = default;
			float GetOriginNearPiliodDistance() = delete;
			VECTOR3_CONST GetOriginNearPiliodPos() = delete;
		};















		using PrimitiveVariant = std::variant<
			std::monostate,
			Plane,
			Point,
			Line,
			Segment,
			Sphere,
			Capsule,
			Triangle,
			Square,
			AABB
		>;

		/*using PrimitiveVariantIndex = Lib_Base::VariantT<
			std::monostate,
			Plane,
			Point,
			Line,
			Segment,
			Sphere,
			Capsule,
			Triangle,
			Square,
			AABB
		>;

		template<int I>
		using PrimitiveVariantC = Lib_Base::VariantC<I,
			std::monostate,
			Plane,
			Point,
			Line,
			Segment,
			Sphere,
			Capsule,
			Triangle,
			Square,
			AABB
		>;*/


		//	namespace
		//	{
		//#define MACRO(type0, type1) std::make_pair(PrimitiveVariantIndex::index_of_t<type0>::value,PrimitiveVariantIndex::index_of_t<type1>::value)
		//		
		//		std::map<std::pair<std::size_t, std::size_t>, bool(*)()> mip{
		//			{MACRO(Plane, Point),}
		//		};
		//		
		//		template<std::size_t i, class tVariant>
		//		void printElement(tVariant const& iVariant0, tVariant const& iVariant1)
		//		{
		//			constexpr std::size_t n = std::variant_size<tVariant>::size;
		//			constexpr std::size_t siz0 = i % n;//1番目のindex
		//			constexpr std::size_t siz1 = i / n;//2番目のindex
		//			using t0 = std::variant_alternative< siz0, tVariant >::type;
		//			using t1 = std::variant_alternative< siz1, tVariant >::type;
		//			auto var0 = std::get<siz0>(iVariant0);//1番目のvariantの型
		//			auto var1 = std::get<siz1>(iVariant1);//2番目のvariantの型
		//			auto pair = std::make_pair(siz0, siz1);
		//			const auto& func = mip[pair];
		//			func(iVariant0, iVariant1);
		//		}
		//
		//		template<class tVariant, std::size_t... indices>
		//		void printImpl(tVariant const& iVariant0, tVariant const& iVariant1, std::size_t i, std::index_sequence<indices...>)
		//		{
		//			using Func = void (*)(tVariant const&);
		//			Func aFuncArray[] =
		//			{
		//				{(printElement<indices, tVariant>)...}
		//			};
		//			aFuncArray[i](iVariant0, iVariant1);
		//		}
		//
		//		template<class tVariant>
		//		void print(tVariant const& iVariant0, tVariant const& iVariant1)
		//		{
		//			constexpr std::size_t n = std::variant_size<tVariant>::size;
		//			printImpl(iVariant0, iVariant1, iVariant0.index() + iVariant1.index() * n, std::make_index_sequence<n*n>{});
		//		}
		//	}





			////! 最初に一回だけ型を決めるvariant
			//template<const int I>
			//class PrimitiveVariantC_
			//{
			//	///using PrimitiveType = PrimitiveVariant::at_t<I>::type;
			//	_PROPERTY_READABLE_REFERENCE(PrimitiveVariant, Variant)
			//		constexpr static int n = 0;
			//private:
			//public:
			//	PrimitiveVariantC_(int n)
			//		:_Variant(n)
			//	{
			//		constexpr int x[3];
			//		constexpr auto xx = array_length(x);
			//		sizeof(PrimitiveVariant);
			//	}
			//	//PrimitiveVariant::at_t<I>::type
			//	
			//	PrimitiveVariant::at_t<I>::type GetThis()
			//	{

			//		return Lib_Base::apply_visitor(Lib_Base::is_this_visitor_t<I, PrimitiveVariant>(), _Variant);
			//	}

			//	void x(PrimitiveVariant::at_t<I>::type v)
			//	{}
			//};



		struct VisitorGetAABB
		{

			AABB operator()(Plane p) { return inf; }
			AABB operator()(Point p) { return inf; }
			AABB operator()(Line p) { return inf; }
			AABB operator()(Segment p) { return p.ThisAABB(); }
			AABB operator()(Sphere p) { return p.ThisAABB(); }
			AABB operator()(Capsule p) { return p.ThisAABB(); }
			//AABB operator()(Triangle p) { return p.ThisAABB(); }
			//AABB operator()(Square p) { return p.ThisAABB(); }
			AABB operator()(AABB p) { return p; }
		private:
			static constexpr AABB inf{ Point(INFINITY,INFINITY,INFINITY), VECTOR3_CONST(INFINITY,INFINITY,INFINITY) };
		};

		/*int x()
		{
			PrimitiveVariantIndex::index_of_t<Point>::value;
			Lib_Base::metatemplate::index_of_t<Plane, PrimitiveVariant >::value;
			PrimitiveVariantC<1> n{};
			n.GetVariant();
			n.GetThis();
			auto x_ = n.GetThis();

			auto x = std::variant_alternative<1, PrimitiveVariant>::type{};
			PrimitiveVariant pj;
			auto l = std::get<2>(pj);

			PrimitiveVariantC<0>::at_t;
			n.GetThis();
			std::visit(Visi{}, ly);
		}*/



		//+ -------------------------------
		////! 当たり判定形共用体
		//union PrimitiveUnion
		//{
		//	const std::type_info& _PrimitiveType;
		//public:
		//	const std::type_info& GetPrimitiveType(){
		//		return _PrimitiveType;
		//	}
		//	Plane plane;
		//	Point point;
		//	Line line;
		//	Segment seg;
		//	Sphere sphere;
		//	Capsule capsule;
		//	Triangle tryangle;
		//	Square square;
		//	AABB aabb;
		//	constexpr explicit PrimitiveUnion::PrimitiveUnion(const Plane& p)
		//		: plane(p)
		//		, _PrimitiveType(typeid(p))
		//	{}
		//	constexpr explicit PrimitiveUnion::PrimitiveUnion(const Point& p)
		//		: point(p)
		//		, _PrimitiveType(typeid(p))
		//	{}
		//	constexpr explicit PrimitiveUnion::PrimitiveUnion(const Line& p)
		//		: line(p)
		//		, _PrimitiveType(typeid(p))
		//	{}
		//	constexpr explicit PrimitiveUnion::PrimitiveUnion(const Segment& p)
		//		: seg(p)
		//		, _PrimitiveType(typeid(p))
		//	{}
		//	constexpr explicit PrimitiveUnion::PrimitiveUnion(const Sphere& p)
		//		: sphere(p)
		//		, _PrimitiveType(typeid(p))
		//	{}
		//	constexpr explicit PrimitiveUnion::PrimitiveUnion(const Capsule& p)
		//		: capsule(p)
		//		, _PrimitiveType(typeid(p))
		//	{}
		//	constexpr explicit PrimitiveUnion::PrimitiveUnion(const Triangle& p)
		//		: tryangle(p)
		//		, _PrimitiveType(typeid(p))
		//	{}
		//	constexpr explicit PrimitiveUnion::PrimitiveUnion(const Square& p)
		//		: square(p)
		//		, _PrimitiveType(typeid(p))
		//	{}
		//	constexpr explicit PrimitiveUnion::PrimitiveUnion(const AABB& p)
		//		: aabb(p)
		//		, _PrimitiveType(typeid(p))
		//	{}
		//	//DEFAULT_COPY_MOVE_CONSTRACTOR(PrimitiveUnion)
		//	~PrimitiveUnion() = default;
		//};
	}

}//Lib_Math
#endif





