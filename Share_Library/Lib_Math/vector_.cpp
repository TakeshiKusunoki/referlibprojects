//******************************************************************************
//
//
//      Vector
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "vector_.h"

namespace Lib_Math
{
	//******************************************************************************
	//
	//      VECTOR2
	//
	//******************************************************************************

	//--------------------------------
	//  =
	//--------------------------------
	VECTOR2& VECTOR2::operator=(const VECTOR2& v)noexcept
	{
		x = v.x;
		y = v.y;
		return *this;
	}

	//--------------------------------
	//  +=
	//--------------------------------
	VECTOR2& VECTOR2::operator+=(const VECTOR2& v)noexcept
	{
		x += v.x;
		y += v.y;
		return *this;
	}

	//--------------------------------
	//  -=
	//--------------------------------
	VECTOR2& VECTOR2::operator-=(const VECTOR2& v)noexcept
	{
		x -= v.x;
		y -= v.y;
		return *this;
	}

	VECTOR2& VECTOR2::operator+=(float f)noexcept
	{
		// TODO: return ステートメントをここに挿入します
		x += f;
		y += f;
		return *this;
	}

	VECTOR2& VECTOR2::operator-=(float f)noexcept
	{
		// TODO: return ステートメントをここに挿入します
		x -= f;
		y -= f;
		return *this;
	}

	//--------------------------------
	//  *=
	//--------------------------------
	VECTOR2& VECTOR2::operator*=(float f)noexcept
	{
		x *= f;
		y *= f;
		return *this;
	}

	VECTOR2& VECTOR2::operator*=(const VECTOR2& v)noexcept
	{
		// TODO: return ステートメントをここに挿入します
		x *= v.x;
		y *= v.y;
		return *this;
	}

	//--------------------------------
	//  /=
	//--------------------------------
	VECTOR2& VECTOR2::operator/=(float f)noexcept
	{
		x /= f;
		y /= f;
		return *this;
	}

	VECTOR2& VECTOR2::operator/=(const VECTOR2& v)noexcept
	{
		// TODO: return ステートメントをここに挿入します
		x /= v.x;
		y /= v.y;
		return *this;
	}

	//--------------------------------
	//  +（符号）
	//--------------------------------
	VECTOR2 VECTOR2::operator+() const noexcept
	{
		return (VECTOR2(x, y));
	}

	//--------------------------------
	//  -（符号）
	//--------------------------------
	VECTOR2 VECTOR2::operator-() const noexcept
	{
		return (VECTOR2(-x, -y));
	}

	//--------------------------------
	//  +（和）
	//--------------------------------
	VECTOR2 VECTOR2::operator+(const VECTOR2& v) const noexcept
	{
		return (VECTOR2(x + v.x, y + v.y));
	}

	//--------------------------------
	//  -（差）
	//--------------------------------
	VECTOR2 VECTOR2::operator-(const VECTOR2& v) const noexcept
	{
		return (VECTOR2(x - v.x, y - v.y));
	}

	//--------------------------------
	//  *
	//--------------------------------
	VECTOR2 VECTOR2::operator*(float f) const noexcept
	{
		return (VECTOR2(x * f, y * f));
	}

	VECTOR2 VECTOR2::operator*(const VECTOR2& v) const noexcept
	{
		return (VECTOR2(x * v.x, y * v.y));
	}


	//--------------------------------
	//  *
	//--------------------------------
	VECTOR2 operator*(float f, const VECTOR2& v) noexcept
	{
		return (VECTOR2(v.x * f, v.y * f));
	}



	//--------------------------------
	//  /
	//--------------------------------
	VECTOR2 VECTOR2::operator/(float f) const noexcept
	{
		return (VECTOR2(x / f, y / f));
	}

	VECTOR2 VECTOR2::operator/(const VECTOR2& v) const noexcept
	{
		return (VECTOR2(x / v.x, y / v.y));
	}

	//--------------------------------
	//  ==
	//--------------------------------
	bool VECTOR2::operator == (const VECTOR2& v) const noexcept
	{
		return (x == v.x) && (y == v.y);
	}

	//--------------------------------
	//  !=
	//--------------------------------
	bool VECTOR2::operator != (const VECTOR2& v) const noexcept
	{
		return (x != v.x) || (y != v.y);
	}

	////--------------------------------
	////  ==
	////--------------------------------
	//bool VECTOR2::operator==(const VECTOR2& v) const
	//{
	//	return (x == v.x) && (y == v.y);
	//}

	////--------------------------------
	////  !=
	////--------------------------------
	//bool VECTOR2::operator!=(const VECTOR2& v) const
	//{
	//	return (x != v.x) || (y != v.y);
	//}

	// ! 長さの2乗を取得
	float VECTOR2::vec2LengthSq() const noexcept
	{
		return x * x + y * y;
	}

	//!  長さを取得
	float VECTOR2::vec2Length() const noexcept
	{
		return sqrtf(vec2LengthSq());
	}

	//!  長さを1にする
	VECTOR2 VECTOR2::vec2Normalize() const noexcept
	{
		float d = vec2Length();
		if (d == 0.0f)
			return  VECTOR2{};

		return (*this * (1 / d));
	}




















	//VECTOR3 operator*(float f, const VECTOR3& v) noexcept
	//{
	//	return (VECTOR3(v.x * f, v.y * f, v.z * f));
	//}

	//******************************************************************************
	//
	//      VECTOR3
	//
	//******************************************************************************

	//--------------------------------
	//  =
	//--------------------------------
	/*VECTOR3& VECTOR3::operator=(const VECTOR3& v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
		return *this;
	}*/


	//--------------------------------
	//  +=
	//--------------------------------
	VECTOR3& VECTOR3::operator+=(const VECTOR3& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		return *this;
	}

	//--------------------------------
	//  -=
	//--------------------------------
	VECTOR3& VECTOR3::operator-=(const VECTOR3& v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return *this;
	}

	//--------------------------------
	//  *=
	//--------------------------------
	VECTOR3& VECTOR3::operator*=(float f)
	{
		x *= f;
		y *= f;
		z *= f;
		return *this;
	}

	VECTOR3& VECTOR3::operator*=(const VECTOR3& v)
	{
		// TODO: return ステートメントをここに挿入します
		x *= v.x;
		y *= v.y;
		z *= v.z;
		return *this;
	}
	//--------------------------------
	//  /=
	//--------------------------------
	VECTOR3& VECTOR3::operator/=(float f)
	{
		x /= f;
		y /= f;
		z /= f;
		return *this;
	}


	VECTOR3& VECTOR3::operator/=(const VECTOR3& v)
	{
		// TODO: return ステートメントをここに挿入します
		x /= v.x;
		y /= v.y;
		z /= v.z;
		return *this;
	}


	//--------------------------------
	//  +（符号）
	//--------------------------------
	VECTOR3 VECTOR3::operator+() const
	{
		return (VECTOR3(x, y, z));
	}

	//--------------------------------
	//  -（符号）
	//--------------------------------
	VECTOR3 VECTOR3::operator-() const
	{
		return (VECTOR3(-x, -y, -z));
	}


	//--------------------------------
	//  +（和）
	//--------------------------------
	VECTOR3 VECTOR3::operator+(const VECTOR3& v) const
	{
		return (VECTOR3(x + v.x, y + v.y, z + v.z));
	}

	VECTOR3 VECTOR3::operator+(float f) const
	{
		return (VECTOR3(x + f, y + f, z + f));
	}


	//--------------------------------
	//  -（差）
	//--------------------------------
	//constexpr VECTOR3 VECTOR3::operator-(const VECTOR3 &v) const
	//{
	//	
	//}
	VECTOR3 VECTOR3::operator-(const VECTOR3& v) const
	{
		return  (VECTOR3(x - v.x, y - v.y, z - v.z));
	}

	VECTOR3 VECTOR3::operator-(float f) const
	{
		return (VECTOR3(x - f, y - f, z - f));
	}

	//--------------------------------
	//  *
	//--------------------------------
	VECTOR3 VECTOR3::operator*(float f) const
	{
		return (VECTOR3(x * f, y * f, z * f));
	}

	VECTOR3 VECTOR3::operator*(const VECTOR3& v) const
	{
		return (VECTOR3(x * v.x, y * v.y, z * v.z));
	}

	//--------------------------------
	//  *
	//--------------------------------
	VECTOR3 operator*(float f, const VECTOR3& v) noexcept
	{
		return (VECTOR3(v.x * f, v.y * f, v.z * f));
	}

	//--------------------------------
	//  /
	//--------------------------------
	VECTOR3 VECTOR3::operator/(float f) const
	{
		return (VECTOR3(x / f, y / f, z / f));
	}

	VECTOR3 VECTOR3::operator/(const VECTOR3& v) const
	{
		return (VECTOR3(x / v.x, y / v.y, z / v.z));
	}

	//VECTOR3 VECTOR3::operator*()
	//{
	//	return *this;
	//}

	//--------------------------------
	//  ==
	//--------------------------------
	bool VECTOR3::operator==(const VECTOR3& v) const
	{
		return (x == v.x) && (y == v.y) && (z == v.z);
	}

	//--------------------------------
	//  !=
	//--------------------------------
	bool VECTOR3::operator!=(const VECTOR3& v) const
	{
		return (x != v.x) || (y != v.y) || (z != v.z);
	}

	////--------------------------------
	////  ==
	////--------------------------------
	//bool VECTOR3::operator== (const VECTOR3& v) const
	//{
	//	return (x == v.x) && (y == v.y) && (z == v.z);
	//}
	//
	////--------------------------------
	////  !=
	////--------------------------------
	//bool VECTOR3::operator!= (const VECTOR3& v) const
	//{
	//	return (x != v.x) || (y != v.y) || (z != v.z);
	//}

	VECTOR3 VECTOR3::Normalize() const noexcept
	{
		float l = this->Length();
		return ((.0f != l) ?
			(VECTOR3{ *this / l }) :(VECTOR3{ 0, 0, 0 }));
	}

	VECTOR3 VECTOR3::Cross(const VECTOR3& v) const noexcept
	{
		return VECTOR3{
			y * v.z - z * v.y,
			z * v.x - x * v.z,
			x * v.y - y * v.x };
	}










}//Lib_Math
////******************************************************************************
