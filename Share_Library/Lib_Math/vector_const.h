#pragma once
#include "MyMath.h"
#include "vector_.h"
namespace Lib_Math
{
	class VECTOR3_CONST final
	{
		static constexpr float EPSILON = 0.000001f;	// 誤差
	public:
		float x;
		float y;
		float z;

		//VECTOR3_CONST() = default;
		VECTOR3_CONST(const VECTOR3_CONST&) = default;
		//VECTOR3_CONST& operator=(const VECTOR3_CONST&) = default;
		VECTOR3_CONST(VECTOR3_CONST&&) = default;
		//VECTOR3_CONST& operator=(VECTOR3_CONST&&) = default;
		constexpr VECTOR3_CONST(float _x = 0, float _y = 0, float _z = 0) : x(_x), y(_y), z(_z) {}
		explicit VECTOR3_CONST(_In_reads_(3) const float* pArray) : x(pArray[0]), y(pArray[1]), z(pArray[2]) {}
		constexpr VECTOR3_CONST(const VECTOR3& v) : x(v.x), y(v.y), z(v.z) {}
		~VECTOR3_CONST() = default;
		constexpr VECTOR3_CONST& operator=(const VECTOR3_CONST& v) noexcept
		{
			x = v.x;		y = v.y;	z = v.z;
			return *this;
		}
		constexpr VECTOR3_CONST& operator=(const VECTOR3& v) noexcept
		{
			x = v.x;		y = v.y;	z = v.z;
			return *this;
		}

		constexpr VECTOR3_CONST& operator+=(const VECTOR3_CONST& v) noexcept
		{
			x += v.x;	y += v.y;	z += v.z;
			return *this;
		}
		constexpr VECTOR3_CONST& operator+=(const VECTOR3& v) noexcept
		{
			x += v.x;	y += v.y;	z += v.z;
			return *this;
		}
		constexpr VECTOR3_CONST& operator-=(const VECTOR3_CONST& v) noexcept
		{
			x -= v.x;	y -= v.y;	z -= v.z;
			return *this;
		}
		constexpr VECTOR3_CONST& operator-=(const VECTOR3& v) noexcept
		{
			x -= v.x;	y -= v.y;	z -= v.z;
			return *this;
		}
		constexpr VECTOR3_CONST& operator*=(float f) noexcept
		{
			x *= f;	y *= f;	z *= f;
			return *this;
		}
		constexpr VECTOR3_CONST& operator/=(float f) noexcept
		{
			x /= f;	y /= f;	z /= f;
			return *this;
		}

		constexpr VECTOR3_CONST& operator*=(const VECTOR3_CONST& v) noexcept
		{
			x *= v.x;	y *= v.y;	z *= v.z;
			return *this;
		}
		constexpr VECTOR3_CONST& operator/=(const VECTOR3_CONST& v) noexcept
		{
			x /= v.x;	y /= v.y;	z /= v.z;
			return *this;
		}
		constexpr VECTOR3_CONST& operator*=(const VECTOR3& v) noexcept
		{
			x *= v.x;	y *= v.y;	z *= v.z;
			return *this;
		}
		constexpr VECTOR3_CONST& operator/=(const VECTOR3& v) noexcept
		{
			x /= v.x;	y /= v.y;	z /= v.z;
			return *this;
		}

		constexpr VECTOR3_CONST operator+() const noexcept
		{
			return { x, y, z };
		}
		constexpr VECTOR3_CONST operator-() const noexcept
		{
			return { -x, -y, -z };
		}

		constexpr VECTOR3_CONST operator+(const VECTOR3_CONST& v) const noexcept
		{
			return { x + v.x, y + v.y, z + v.z };
		}
		constexpr VECTOR3_CONST operator+(const VECTOR3& v) const noexcept
		{
			return { x + v.x, y + v.y, z + v.z };
		}
		constexpr VECTOR3_CONST operator+(float f) const noexcept
		{
			return { x + f, y + f, z + f };
		}
		constexpr VECTOR3_CONST operator-(const VECTOR3_CONST& v) const noexcept
		{
			return { x - v.x, y - v.y, z - v.z };
		}
		constexpr VECTOR3_CONST operator-(const VECTOR3& v) const noexcept
		{
			return { x - v.x, y - v.y, z - v.z };
		}
		constexpr VECTOR3_CONST operator-(float f) const noexcept
		{
			return { x - f, y - f, z - f };
		}
		constexpr VECTOR3_CONST operator*(float f) const noexcept
		{
			return { x * f, y * f, z * f };
		}
		constexpr VECTOR3_CONST operator*(const VECTOR3_CONST& v) const noexcept
		{
			return { x * v.x, y * v.y, z * v.z };
		}
		constexpr VECTOR3_CONST operator*(const VECTOR3& v) const noexcept
		{
			return { x * v.x, y * v.y, z * v.z };
		}
		friend constexpr VECTOR3_CONST operator*(float f, const VECTOR3_CONST& v) noexcept;
		constexpr VECTOR3_CONST operator/(float f) const noexcept
		{
			return { x / f, y / f, z / f };
		}
		constexpr VECTOR3_CONST operator/(const VECTOR3_CONST& v) const noexcept
		{
			return { x / v.x, y / v.y, z / v.z };
		}
		constexpr VECTOR3_CONST operator/(const VECTOR3& v) const noexcept
		{
			return { x / v.x, y / v.y, z / v.z };
		}
		//VECTOR3 operator*();

		constexpr bool operator == (const VECTOR3_CONST& v) const noexcept
		{
			return (x == v.x) && (y == v.y) && (z == v.z);
		}
		constexpr bool operator != (const VECTOR3_CONST& v) const noexcept
		{
			return (x != v.x) || (y != v.y) || (z != v.z);
		}
		constexpr bool operator == (const VECTOR3& v) const noexcept
		{
			return (x == v.x) && (y == v.y) && (z == v.z);
		}
		constexpr bool operator != (const VECTOR3& v) const noexcept
		{
			return (x != v.x) || (y != v.y) || (z != v.z);
		}

	private:
		//! length用のみ
		template < typename T >
		constexpr T sqrt_(T s)const noexcept
		{
			T x = s / 2.0;
			T prev = 0.0;

			while (x != prev)
			{
				prev = x;
				x = (x + s / x) / 2.0;
			}
			return x;
		}
	public:
		//! @brief 長さ
		constexpr float Length() const
		{
			return sqrt_<float>(x * x + y * y + z * z);
		}
		//! @brief べき乗長さ
		constexpr float LengthSq() const noexcept
		{
			return x * x + y * y + z * z;
		}
		//! @return 点と点との距離を返す
		constexpr float Length(const VECTOR3_CONST& v) const
		{
			auto V = *this - v;
			return sqrt_<float>(V.x * V.x + V.y * V.y + V.z * V.z);
		}
		//! @return 点と点との距離の2乗を返す
		constexpr float LengthSq(const VECTOR3_CONST& v) const
		{
			auto V = *this - v;
			return V.x * V.x + V.y * V.y + V.z * V.z;
		}
		//! @brief 正規化
		//! o,o,oなら
		constexpr VECTOR3_CONST Normalize() const
		{
			float l = this->Length();
			return ((.0f != l) ?
				(VECTOR3_CONST{ *this / l }) : (VECTOR3_CONST{ 0, 0, 0 }));

		}
		//! 内積
		//! @deatiles 
		//! this.vまたは、引数vどちらかを正規化したら、
		//! (原点O→this.v)を(原点O→引数vへ)の射影した長さを返す
		//!
		//! this.vと引数vを正規化したら
		//! (原点O→this.v)と(原点O→引数vへ)のなすcosθを返す
		//!
		//! @param[in] v.length : Adjacent(底)
		//! this.v.length　: Hypotenuse(ななめ)	
		//! cosθ = Adjacent / Hypotenuse   (/ cosθ = Hypotenuse / Adjacent)
		constexpr float Dot(const VECTOR3_CONST v) const noexcept
		{
			return x * v.x + y * v.y + z * v.z;
		}
		//! 外積
		//! @return ２つの線に垂直な線
		constexpr VECTOR3_CONST Cross(const VECTOR3_CONST& v) const noexcept
		{
			return { y * v.z - z * v.y,	z * v.x - x * v.z,	x * v.y - y * v.x };
		}


		//! 垂直関係にあるならtrue
		constexpr bool IsVertical(const VECTOR3_CONST& r) const noexcept
		{
			float d = Dot(r);
			return (-EPSILON < d && d < EPSILON);	//誤差範囲内なら垂直と判定 //内積が０なら直角
		}

		//! 平行関係にあるならtrue
		constexpr bool IsParallel(const VECTOR3_CONST& r) const noexcept
		{
			float d = Cross(r).LengthSq();
			return (-EPSILON < d && d < EPSILON);	//誤差範囲内なら平行と判定//外積が０なら平行
		}

		// 鋭角ならtrue
		constexpr bool IsSharpAngle(const VECTOR3_CONST& r) const noexcept
		{
			return (Dot(r) >= 0.0f);//内積が０以上なら鋭角
		}
	};

	class VECTOR4_CONST final
	{
	public:
		float x;
		float y;
		float z;
		float w;

		VECTOR4_CONST() = default;
		VECTOR4_CONST(const VECTOR4_CONST&) = default;
		VECTOR4_CONST& operator=(const VECTOR4_CONST&) = default;
		//VECTOR4_CONST(VECTOR4_CONST&&) = default;
		VECTOR4_CONST& operator=(VECTOR4_CONST&&) = default;
		constexpr VECTOR4_CONST(float _x, float _y, float _z, float _w) : x(_x), y(_y), z(_z), w(_w) {}
		explicit VECTOR4_CONST(_In_reads_(4) const float* pArray) : x(pArray[0]), y(pArray[1]), z(pArray[2]), w(pArray[3]) {}
		//constexpr VECTOR4_CONST() : VECTOR4_CONST(0, 0, 0, 0) {}
		//constexpr VECTOR4_CONST(float x, float y, float z, float w) : VECTOR4_CONST(x, y, z, w) {}
		~VECTOR4_CONST() = default;
		constexpr float operator[](int idx) const
		{
			switch (idx)
			{
			default:
				break;
			case 0:
				return x;
				break;
			case 1:
				return y;
				break;
			case 2:
				return z;
				break;
			case 3:
				return w;
				break;
			}
			return x;
		}
		constexpr VECTOR4_CONST operator+(const VECTOR4_CONST& v) const noexcept
		{
			return (VECTOR4_CONST{ x + v.x, y + v.y, z + v.z, w + v.w });
		}
		constexpr VECTOR4_CONST operator-(const VECTOR4_CONST& v) const noexcept
		{
			return (VECTOR4_CONST{ x - v.x, y - v.y, z - v.z, w - v.w });
		}
		constexpr VECTOR4_CONST operator*(const VECTOR4_CONST& v) const noexcept
		{
			return (VECTOR4_CONST{ x * v.x, y * v.y, z * v.z, w * v.w });
		}
		constexpr VECTOR4_CONST operator*(float v) const noexcept
		{
			return (VECTOR4_CONST{ x * v, y * v, z * v, w * v });
		}
		static constexpr VECTOR4_CONST Cross4(VECTOR4_CONST U, VECTOR4_CONST V, VECTOR4_CONST W)
		{
			//中間値を計算します。
			float A = (V[0] * W[1]) - (V[1] * W[0]);
			float B = (V[0] * W[2]) - (V[2] * W[0]);
			float C = (V[0] * W[3]) - (V[3] * W[0]);
			float D = (V[1] * W[2]) - (V[2] * W[1]);
			float E = (V[1] * W[3]) - (V[3] * W[1]);
			float F = (V[2] * W[3]) - (V[3] * W[2]);

//結果ベクトル成分を計算します。
			return VECTOR4_CONST{
				 (U[1] * F) - (U[2] * E) + (U[3] * D),
				-(U[0] * F) + (U[2] * C) - (U[3] * B),
				 (U[0] * E) - (U[1] * C) + (U[3] * A),
				-(U[0] * D) + (U[1] * B) - (U[2] * A)
			};
		}

		constexpr float Dot4(VECTOR4_CONST V)const
		{
			return x * V.x + y * V.y + z * V.z + w * V.w;
			//return ((V[0] * U[0]) + (V[1] * U[1]) + (V[2] * U[2]) + (V[3] * U[3]));
		}
	};



}