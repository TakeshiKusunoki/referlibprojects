#pragma once
#include "vector_const.h"
#include "MyMath.h"
namespace Lib_Math
{
	//! 行列クラス
	//template<int N>
	struct MATRIX_CONST
	{
		float _11, _12, _13, _14;
		float _21, _22, _23, _24;
		float _31, _32, _33, _34;
		float _41, _42, _43, _44;

		constexpr MATRIX_CONST(
			float _11 = 0, float _12 = 0, float _13 = 0, float _14 = 0,
			float _21 = 0, float _22 = 0, float _23 = 0, float _24 = 0,
			float _31 = 0, float _32 = 0, float _33 = 0, float _34 = 0,
			float _41 = 0, float _42 = 0, float _43 = 0, float _44 = 0)
			: _11(_11), _12(_12), _13(_13), _14(_14)
			, _21(_21), _22(_22), _23(_13), _24(_24)
			, _31(_31), _32(_32), _33(_33), _34(_34)
			, _41(_41), _42(_42), _43(_43), _44(_44)
		{}

		constexpr decltype(auto) XYPlaneAngle(float angle) const
		{
			return MATRIX_CONST{
				my_cos(angle, 10),my_sin(angle, 5),0,0,
				-my_sin(angle, 5),my_cos(angle, 5),0,0,
				0,0,1,0,
				0,0,0,1
			};
		}
		constexpr decltype(auto) YZPlaneAngle(float angle) const
		{
			return MATRIX_CONST{
				1,0,0,0,
				0,my_cos(angle, 10),my_sin(angle, 5),0,
				0,-my_sin(angle, 5),my_cos(angle, 5),0,
				0,0,0,1
			};
		}
		constexpr decltype(auto) ZXPlaneAngle(float angle) const
		{
			return MATRIX_CONST{
				my_cos(angle, 10),0,-my_sin(angle, 5),0,
				0,1,0,0,
				my_sin(angle, 5),0,my_cos(angle, 5),0,
				0,0,0,1
			};
		}
		constexpr decltype(auto) XWPlaneAngle(float angle) const
		{
			return MATRIX_CONST{
				my_cos(angle, 10),0,0,my_sin(angle, 5),
				0,1,0,0,
				0,0,1,0,
				-my_sin(angle, 5),my_cos(angle, 5),0,0
			};
		}
		constexpr decltype(auto) YWPlaneAngle(float angle) const
		{
			return MATRIX_CONST{
				1,0,0,0,
				0,my_cos(angle, 10),0,-my_sin(angle, 5),
				0,0,1,0,
				0,my_sin(angle, 5),0,my_cos(angle, 5)
			};
		}
		constexpr decltype(auto) ZWPlaneAngle(float angle) const
		{
			return MATRIX_CONST{
				   1,0,0,0,
				   0,1,0,0,
				   0,0,my_cos(angle, 10),-my_sin(angle, 5),
				   0,0,my_sin(angle, 5),my_cos(angle, 5)
			};
		}
		void Rotate(float angle)
		{

		}

		/*
		ワールド座標の頂点位置を取得
		Returns the vector result of multiplying a matrix M by a column vector v; a row vector v by a matrix M; or a matrix A by a second matrix B.
		     以下は同義（違う時もある）
			mul（M、v） == mul（v、tranpose（M））
			mul（v、M） == mul（tranpose（M）、v）
		*/
		constexpr VECTOR4_CONST Mul(VECTOR4_CONST v)
		{
		  VECTOR4_CONST r;
		  r.x = VECTOR4_CONST{ _11, _12, _13, _14 }.Dot4(v);
		  r.y = VECTOR4_CONST{ _21, _22, _23, _24 }.Dot4(v);
		  r.z = VECTOR4_CONST{ _31, _32, _33, _34 }.Dot4(v);
		  r.w = VECTOR4_CONST{ _41, _42, _43, _44 }.Dot4(v);
		  return r;
		}

	};

}//Lib_Math