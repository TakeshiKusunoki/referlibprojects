#include "Matrix.h"


////////////////////////////////////////////////////////////////////////////
//	Matrix
////////////////////////////////////////////////////////////////////////////

namespace Lib_Math
{
	//------------------------------------------------------------------------------

	void MATRIX::operator=(const MATRIX& M)
	{
		for (size_t i = 0; i < 4; i++)
		{
			for (size_t i2 = 0; i2 < 4; i2++)
			{
				m[i][i2] = M.m[i][i2];
			}
		}
	}

	MATRIX& MATRIX::operator+=(const MATRIX& M)
	{
		for (size_t i = 0; i < 4; i++)
		{
			for (size_t i2 = 0; i2 < 4; i2++)
			{
				m[i][i2] += M.m[i][i2];
			}
		}
		return *this;
	}

	MATRIX& MATRIX::operator-=(const MATRIX& M)
	{
		for (size_t i = 0; i < 4; i++)
		{
			for (size_t i2 = 0; i2 < 4; i2++)
			{
				m[i][i2] -= M.m[i][i2];
			}
		}
		return *this;
	}

	MATRIX& MATRIX::operator*=(const MATRIX& M)
	{
		//1���
		for (size_t i = 0; i < 4; i++)
		{
			for (size_t i2 = 0; i2 < 4; i2++)
			{
				m[0][i2] *= M.m[i2][i];
			}
		}
		//2���
		for (size_t i = 0; i < 4; i++)
		{
			for (size_t i2 = 0; i2 < 4; i2++)
			{
				m[1][i2] *= M.m[i2][i];
			}
		}
		//3���
		for (size_t i = 0; i < 4; i++)
		{
			for (size_t i2 = 0; i2 < 4; i2++)
			{
				m[2][i2] *= M.m[i2][i];
			}
		}
		//4���
		for (size_t i = 0; i < 4; i++)
		{
			for (size_t i2 = 0; i2 < 4; i2++)
			{
				m[3][i2] *= M.m[i2][i];
			}
		}
		return *this;
	}

	MATRIX MATRIX::operator+() const
	{
		return  (MATRIX{
			m[0][0], m[0][1],m[0][2],m[0][3],
			m[1][0], m[1][1],m[1][2],m[1][3],
			m[2][0], m[2][1],m[2][2],m[2][3],
			m[3][0], m[3][1],m[3][2],m[3][3]
			});
	}

	MATRIX MATRIX::operator-() const
	{
		return  (MATRIX{
			-m[0][0], -m[0][1],-m[0][2],-m[0][3],
			-m[1][0], -m[1][1],-m[1][2],-m[1][3],
			-m[2][0], -m[2][1],-m[2][2],-m[2][3],
			-m[3][0], -m[3][1],-m[3][2],-m[3][3]
			});
	}
	MATRIX MATRIX::operator+(const MATRIX& M) const
	{
		return (MATRIX{
			m[0][0] + M.m[0][0] ,m[0][1] + M.m[0][1], m[0][2] + M.m[0][2], m[0][3] + M.m[0][3],
			m[1][0] + M.m[1][0] ,m[1][1] + M.m[1][1], m[1][2] + M.m[1][2], m[1][3] + M.m[1][3],
			m[2][0] + M.m[2][0] ,m[2][1] + M.m[2][1], m[2][2] + M.m[2][2], m[2][3] + M.m[2][3],
			m[3][0] + M.m[3][0] ,m[3][1] + M.m[3][1], m[3][2] + M.m[3][2], m[3][3] + M.m[3][3]
			});
	}
	MATRIX MATRIX::operator-(const MATRIX& M) const
	{
		return (MATRIX{
			m[0][0] - M.m[0][0] ,m[0][1] - M.m[0][1], m[0][2] - M.m[0][2], m[0][3] - M.m[0][3],
			m[1][0] - M.m[1][0] ,m[1][1] - M.m[1][1], m[1][2] - M.m[1][2], m[1][3] - M.m[1][3],
			m[2][0] - M.m[2][0] ,m[2][1] - M.m[2][1], m[2][2] - M.m[2][2], m[2][3] - M.m[2][3],
			m[3][0] - M.m[3][0] ,m[3][1] - M.m[3][1], m[3][2] - M.m[3][2], m[3][3] - M.m[3][3]
			});
	}
	// param[in] M �v�f
	MATRIX MATRIX::operator*(const MATRIX& M) const
	{
		MATRIX mat{};
		//1���

		for (size_t i = 0; i < 4; i++)//�v�f���ׂĂɑ΂���
		{
			for (size_t i2 = 0; i2 < 4; i2++)//���όv�Z
			{
				mat.m[0][i] += m[0][i2] * M.m[i2][i];
			}
		}
		//2���
		for (size_t i = 0; i < 4; i++)
		{
			for (size_t i2 = 0; i2 < 4; i2++)
			{
				mat.m[1][i] += m[1][i2] * M.m[i2][i];
			}
		}
		//3���
		for (size_t i = 0; i < 4; i++)
		{
			for (size_t i2 = 0; i2 < 4; i2++)
			{
				mat.m[2][i] += m[2][i2] * M.m[i2][i];
			}
		}
		//4���
		for (size_t i = 0; i < 4; i++)
		{
			for (size_t i2 = 0; i2 < 4; i2++)
			{
				mat.m[3][i] += m[3][i2] * M.m[i2][i];
			}
		}
		return mat;
	}

	MATRIX MATRIX::operator*(float S) const
	{
		return (MATRIX{
			m[0][0] * S ,m[0][1] * S, m[0][2] * S, m[0][3] * S,
			m[1][0] * S ,m[1][1] * S, m[1][2] * S, m[1][3] * S,
			m[2][0] * S ,m[2][1] * S, m[2][2] * S, m[2][3] * S,
			m[3][0] * S ,m[3][1] * S, m[3][2] * S, m[3][3] * S
			});
	}

	MATRIX MATRIX::operator/(float S) const
	{
		return (MATRIX{
			m[0][0] / S ,m[0][1] / S, m[0][2] / S, m[0][3] / S,
			m[1][0] / S ,m[1][1] / S, m[1][2] / S, m[1][3] / S,
			m[2][0] / S ,m[2][1] / S, m[2][2] / S, m[2][3] / S,
			m[3][0] / S ,m[3][1] / S, m[3][2] / S, m[3][3] / S
			});
	}

	void MATRIX::Identyfy()
	{
		m[0][0] = 1; m[0][1] = 0; m[0][2] = 0; m[0][3] = 0;
		m[1][0] = 0; m[1][1] = 1; m[1][2] = 0; m[1][3] = 0;
		m[2][0] = 0; m[2][1] = 0; m[2][2] = 1; m[2][3] = 0;
		m[3][0] = 0; m[3][1] = 0; m[3][2] = 0; m[3][3] = 1;
	}

	MATRIX MATRIX::Inverce()
	{
		MATRIX mat{};
		//�O�p�s����쐬
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (i >= j)
					continue;
				float buf = m[j][i] / m[i][i];
				for (int k = 0; k < 4; k++)
				{
					mat.m[j][k] = m[j][k] - m[i][k] * buf;
				}
			}
		}
		//�Ίp�����̐�
		float det = 1.0f;
		for (int i = 0; i < 4; i++)
		{
			det *= mat.m[i][i];
		}
		det = 1 / det;
		return (mat * det);
	}

	void MATRIX::LookAt(const VECTOR3& position, const VECTOR3& target, const VECTOR3& up)
	{
		VECTOR3 zaxis{ position - target };
		zaxis = zaxis.Normalize();

		VECTOR3 xaxis{ up.Cross(zaxis) };
		VECTOR3 yaxis{ zaxis.Cross(xaxis) };

		m[0][0] = xaxis.x;	m[0][1] = yaxis.x;	m[0][2] = zaxis.x;	m[0][3] = 0;
		m[1][0] = xaxis.y;	m[1][1] = yaxis.y;	m[1][2] = zaxis.y;	m[1][3] = 0;
		m[2][0] = xaxis.z;	m[2][1] = yaxis.z;	m[2][2] = zaxis.z;	m[2][3] = 0;
		m[3][0] = -xaxis.Dot(position);
		m[3][1] = -yaxis.Dot(position);
		m[3][2] = -zaxis.Dot(position);
		m[3][3] = 1;
	}

	void MATRIX::RotationZXY(float x, float y, float z)
	{
		float	sx, sy, sz, cx, cy, cz;

		sx = sinf(x);	sy = sinf(y);	sz = sinf(z);
		cx = cosf(x);	cy = cosf(y);	cz = cosf(z);

		m[0][0] = cz * cy + sz * sx * sy;
		m[0][1] = sz * cx;
		m[0][2] = -cz * sy + sz * sx * cy;
		m[0][3] = 0;

		m[1][0] = -sz * cy + cz * sx * sy;
		m[1][1] = cz * cx;
		m[1][2] = sz * sy + cz * sx * cy;
		m[1][3] = 0;

		m[2][0] = cx * sy;
		m[2][1] = -sx;
		m[2][2] = cx * cy;
		m[2][3] = 0;

		m[3][0] = 0;
		m[3][1] = 0;
		m[3][2] = 0;
		m[3][3] = 1;
	}

	void MATRIX::Scale(float x, float y, float z)
	{
		m[0][0] *= x;
		m[0][1] *= x;
		m[0][2] *= x;
		m[1][0] *= y;
		m[1][1] *= y;
		m[1][2] *= y;
		m[2][0] *= z;
		m[2][1] *= z;
		m[2][2] *= z;
	}

	void MATRIX::Translate(float x, float y, float z)
	{
		m[3][0] = x;
		m[3][1] = y;
		m[3][2] = z;
	}


	void MATRIX::PerspectiveFov(float fovY, float aspect, float znear, float zfar)
	{
		float top = znear * tanf(fovY);
		float bottom = -top;
		float left = bottom * aspect;
		float right = top * aspect;
		m[0][0] = 2 * znear / (right - left);
		m[0][1] = 0;
		m[0][2] = 0;
		m[0][3] = 0;

		m[1][0] = 0;
		m[1][1] = 2 * znear / (top - bottom);
		m[1][2] = 0;
		m[1][3] = 0;

		m[2][0] = (right + left) / (right - left);
		m[2][1] = (top + bottom) / (top - bottom);
		m[2][2] = -(zfar + znear) / (zfar - znear);
		m[2][3] = -1;

		m[3][0] = 0;
		m[3][1] = 0;
		m[3][2] = -2 * zfar * znear / (zfar - znear);
		m[3][3] = 0;
	}

	void MATRIX::Ortho(float w, float h, float zn, float zf)
	{
		m[0][0] = 2 / w; m[0][1] = 0;   m[0][2] = 0;          m[0][3] = 0;
		m[1][0] = 0;   m[1][1] = 2 / h; m[1][2] = 0;          m[1][3] = 0;
		m[2][0] = 0;   m[2][1] = 0;   m[2][2] = 1 / (zn - zf);  m[2][3] = 0;
		m[3][0] = 0;   m[3][1] = 0;   m[3][2] = zn / (zn - zf); m[3][3] = 1;
	}

	//------------------------------------------------------------------------------

	MATRIX& MATRIX::operator*= (float S)
	{
		for (size_t i = 0; i < 4; i++)
		{
			for (size_t i2 = 0; i2 < 4; i2++)
			{
				m[i][i2] *= S;
			}
		}
		return *this;
	}
	MATRIX& MATRIX::operator/=(float S)
	{
		for (size_t i = 0; i < 4; i++)
		{
			for (size_t i2 = 0; i2 < 4; i2++)
			{
				m[i][i2] /= S;
			}
		}
		return *this;
	}

}//Lib_Math