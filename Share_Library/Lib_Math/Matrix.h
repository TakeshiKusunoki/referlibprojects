#pragma once
#include "vector_.h"
namespace Lib_Math
{
	//! 3次元用行列
	struct MATRIX
	{
		float m[4][4];
		explicit MATRIX(_In_reads_(16) const float* pArray)
		{
			for (size_t i = 0; i < 4; i++)
			{
				for (size_t i2 = 0; i2 < 4; i2++)
				{
					m[i][i2] = pArray[i * 4 + i2];
				}
			}
		}
		MATRIX(
			float _11 = 0, float _12 = 0, float _13 = 0, float _14 = 0,
			float _21 = 0, float _22 = 0, float _23 = 0, float _24 = 0,
			float _31 = 0, float _32 = 0, float _33 = 0, float _34 = 0,
			float _41 = 0, float _42 = 0, float _43 = 0, float _44 = 0)
		{
			m[0][0] = _11; m[0][1] = _12; m[0][2] = _13; m[0][3] = _14;
			m[1][0] = _21; m[1][1] = _22; m[1][2] = _23; m[1][3] = _24;
			m[2][0] = _31; m[2][1] = _32; m[2][2] = _33; m[2][3] = _34;
			m[3][0] = _41; m[3][1] = _42; m[3][2] = _43; m[3][3] = _44;
		}
		MATRIX(
			const VECTOR4& v1,
			const VECTOR4& v2,
			const VECTOR4& v3,
			const VECTOR4& v4)
		{
			m[0][0] = v1.x; m[0][1] = v1.y; m[0][2] = v1.z; m[0][3] = v1.w;
			m[1][0] = v2.x; m[1][1] = v2.y; m[1][2] = v2.z; m[1][3] = v2.w;
			m[2][0] = v3.x; m[2][1] = v3.y; m[2][2] = v3.z; m[2][3] = v3.w;
			m[3][0] = v4.x; m[3][1] = v4.y; m[3][2] = v4.z; m[3][3] = v4.w;
		}
		MATRIX(const MATRIX& M)
		{
			for (size_t i = 0; i < 4; i++)
			{
				for (size_t i2 = 0; i2 < 4; i2++)
				{
					m[i][i2] = M.m[i][i2];
				}
			}
		}
		//MATRIX(MATRIX&) = default;
		MATRIX(MATRIX&& M) = default;
		MATRIX& operator=(MATRIX&&) = default;
		void operator=(const MATRIX&);

		MATRIX& operator+=(const MATRIX& M);
		MATRIX& operator-=(const MATRIX& M);
		MATRIX& operator*=(const MATRIX& M);
		MATRIX& operator*=(float S);
		MATRIX& operator/=(float S);

		MATRIX operator+() const;
		MATRIX operator-() const;
		MATRIX operator+(const MATRIX& M) const;
		MATRIX operator-(const MATRIX& M) const;
		MATRIX operator*(const MATRIX& M) const;
		MATRIX operator*(float S) const;
		MATRIX operator/(float S) const;

		void Identyfy();
		//! 逆行列
		//! @return この行列の逆行列を返す
		[[nodiscard]]
		MATRIX Inverce();

		//! ビュー行列作成
		//! @param[in] position 位置
		//! @param[in] target　視線のターゲット
		//! @param[in] up 視線の上方向
		void LookAt(const VECTOR3& position, const VECTOR3& target, const VECTOR3& up);

		//------------------------------------------------------
		void RotationZXY(float x, float y, float z);
		void Scale(float x, float y, float z);
		void Translate(float x, float y, float z);

		//------------------------------------------------------
		//	透視変換行列作成
		//------------------------------------------------------
		//! @param[in] fovY 視野角
		//! @param[in] aspect アスペクト比
		//! @param[in] znear ｚ限界
		//! @param[in] zfar ｚ限界
		void PerspectiveFov(float fovY, float aspect, float znear, float zfar);


		//*****************************************************************************
		//	平行投影行列
		//*****************************************************************************
		//! @param[in] w 
		//! @param[in] h 
		//! @param[in] zne ｚ限界
		//! @param[in] zf ｚ限界
		void Ortho(
			float w, float h,
			float zn, float zf);
		/*
		Returns the vector result of multiplying a matrix M by a column vector v; a row vector v by a matrix M; or a matrix A by a second matrix B.

			mul（M、v） == mul（v、tranpose（M））
			mul（v、M） == mul（tranpose（M）、v）
		*/
		/*VECTOR4 mul（float4x3 M,VECTOR3_CONST v）
		{
		  VECTOR4 r;

		  rx = dot（M._m00_m01_m02、v）;
		  ry = dot（M._m10_m11_m12、v）;
		  rz = dot（M._m20_m21_m22、v）;
		  rw = dot（M._m30_m31_m32、v）;

		  return r;
		}*/

		// 4D用ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
		void XYPlaneRotate4D(float angle)
		{
			float cos = cosf(angle);
			float sin = sinf(angle);
			m[0][0] += cos;  m[0][1] += sin;
			m[1][0] -= sin; m[1][1] += cos;
		}

		void YZPlaneRotate4D(float angle)
		{
			float cos = cosf(angle);
			float sin = sinf(angle);
			m[1][1] += cos; m[1][2] += sin;
			m[2][1] -= sin; m[2][2] += cos;
		}
		void ZXPlaneRotate4D(float angle)
		{
			float cos = cosf(angle);
			float sin = sinf(angle);
			m[0][0] += cos; m[0][2] -= sin;
			m[2][1] += sin; m[2][2] += cos;
		}
		void XWPlaneRotate4D(float angle)
		{
			float cos = cosf(angle);
			float sin = sinf(angle);
			m[0][0] += cos; m[0][3] += sin;
			m[3][0] -= sin; m[3][1] += cos;
		}
		void YWPlaneRotate4D(float angle)
		{
			float cos = cosf(angle);
			float sin = sinf(angle);
			m[1][1] += cos; m[1][3] -= sin;
			m[3][1] += sin; m[3][3] += cos;
		}
		void ZWPlaneAngle4D(float angle)
		{
			float cos = cosf(angle);
			float sin = sinf(angle);
			m[2][2] += cos; m[2][3] -= sin;
			m[3][2] += sin; m[3][3] += cos;
		}

		/*
		ワールド座標の頂点位置を取得
		Returns the vector result of multiplying a matrix M by a column vector v; a row vector v by a matrix M; or a matrix A by a second matrix B.
			 以下は同義（違う時もある）
			mul（M、v） == mul（v、tranpose（M））
			mul（v、M） == mul（tranpose（M）、v）
		*/
		VECTOR4 Mul(VECTOR4 v)
		{
			VECTOR4 r;
			r.x = VECTOR4::Dot4(VECTOR4{ m[0][0], m[0][1], m[0][2], m[0][3] }, v);
			r.y = VECTOR4::Dot4(VECTOR4{ m[1][0], m[1][1], m[1][2], m[1][3] }, v);
			r.z = VECTOR4::Dot4(VECTOR4{ m[2][0], m[2][1], m[2][2], m[2][3] }, v);
			r.w = VECTOR4::Dot4(VECTOR4{ m[3][0], m[3][1], m[3][2], m[3][3] }, v);
			return r;
		}
	};


	//! 行列クラス
	//template<int N>
	struct MATRIX4D
	{
		//位置情報はない
		float m[4][4] = { {0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0} };

		MATRIX4D(
			float _11 = 1, float _12 = 0, float _13 = 0, float _14 = 0,
			float _21 = 0, float _22 = 1, float _23 = 0, float _24 = 0,
			float _31 = 0, float _32 = 0, float _33 = 1, float _34 = 0,
			float _41 = 0, float _42 = 0, float _43 = 0, float _44 = 1)
		{
			m[0][0] = _11; m[0][1] = _12; m[0][2] = _13; m[0][3] = _14;
			m[1][0] = _21; m[1][1] = _22; m[1][2] = _23; m[1][3] = _24;
			m[2][0] = _31; m[2][1] = _32; m[2][2] = _33; m[2][3] = _34;
			m[3][0] = _41; m[3][1] = _42; m[3][2] = _43; m[3][3] = _44;
		}

		void XYPlaneRotate(float angle)
		{
			float cos = cosf(angle);
			float sin = sinf(angle);
			m[0][0] += cos;  m[0][1] += sin;
			m[1][0] -= sin; m[1][1] += cos;
		}

		void YZPlaneRotate(float angle)
		{
			float cos = cosf(angle);
			float sin = sinf(angle);
			m[1][1] += cos; m[1][2] += sin;
			m[2][1] -= sin; m[2][2] += cos;
		}
		void ZXPlaneRotate(float angle)
		{
			float cos = cosf(angle);
			float sin = sinf(angle);
			m[0][0] += cos; m[0][2] -= sin;
			m[2][1] += sin; m[2][2] += cos;
		}
		void XWPlaneRotate(float angle)
		{
			float cos = cosf(angle);
			float sin = sinf(angle);
			m[0][0] += cos; m[0][3] += sin;
			m[3][0] -= sin; m[3][1] += cos;
		}
		void YWPlaneRotate(float angle)
		{
			float cos = cosf(angle);
			float sin = sinf(angle);
			m[1][1] += cos; m[1][3] -= sin;
			m[3][1] += sin; m[3][3] += cos;
		}
		void ZWPlaneAngle(float angle)
		{
			float cos = cosf(angle);
			float sin = sinf(angle);
			m[2][2] += cos; m[2][3] -= sin;
			m[3][2] += sin; m[3][3] += cos;
		}

	};

	//! 4*3行列
	//! ボーン用行列
	struct BONE_MATRIX
	{
		float m[3][4] = { {0,0,0,0},{0,0,0,0},{0,0,0,0} };//普通の行列の転置行列
		
		BONE_MATRIX(
			float _11 = 1, float _12 = 0, float _13 = 0, float _14 = 0,
			float _21 = 0, float _22 = 1, float _23 = 0, float _24 = 0,
			float _31 = 0, float _32 = 0, float _33 = 1, float _34 = 0,
			float _41 = 0, float _42 = 0, float _43 = 0, float _44 = 1)
		{
			m[0][0] = _11; m[0][1] = _12; m[0][2] = _13; m[0][2] = _14;
			m[1][0] = _21; m[1][1] = _22; m[1][2] = _23; m[1][2] = _24;
			m[2][0] = _31; m[2][1] = _32; m[2][2] = _33; m[2][2] = _34;
			//m[3][0] = _41; m[3][1] = _42; m[3][2] = _43;
		}
		//! 転置行列
		void operator=(const MATRIX& M)
		{
			m[0][0] = M.m[0][0]; m[0][1] = M.m[1][0]; m[0][2] = M.m[2][0]; m[0][3] = M.m[3][0];
			m[1][0] = M.m[0][1]; m[1][1] = M.m[1][1]; m[1][2] = M.m[2][1]; m[1][3] = M.m[3][1];
			m[2][0] = M.m[0][2]; m[2][1] = M.m[1][2]; m[2][2] = M.m[2][2]; m[2][3] = M.m[3][2];
		}
	};
	//! 4*3行列
//! ボーン用行列 影シェーダー用
	struct BONE_MATRIX_SHADOW
	{
		float m[4][3] = { {0,0,0},{0,0,0},{0,0,0},{0,0,0} };//普通の行列の転置行列
		
		BONE_MATRIX_SHADOW(
			float _11 = 1, float _12 = 0, float _13 = 0, float _14 = 0,
			float _21 = 0, float _22 = 1, float _23 = 0, float _24 = 0,
			float _31 = 0, float _32 = 0, float _33 = 1, float _34 = 0,
			float _41 = 0, float _42 = 0, float _43 = 0, float _44 = 1)
		{
			m[0][0] = _11; m[0][1] = _12; m[0][2] = _13; m[0][2] = _14;
			m[1][0] = _21; m[1][1] = _22; m[1][2] = _23; m[1][2] = _24;
			m[2][0] = _31; m[2][1] = _32; m[2][2] = _33; m[2][2] = _34;
			//m[3][0] = _41; m[3][1] = _42; m[3][2] = _43;
		}
		void operator=(const MATRIX& M)
		{
			m[0][0] = M.m[0][0]; m[0][1] = M.m[1][0]; m[0][2] = M.m[2][0];
			m[1][0] = M.m[0][1]; m[1][1] = M.m[1][1]; m[1][2] = M.m[2][1];
			m[2][0] = M.m[0][2]; m[2][1] = M.m[1][2]; m[2][2] = M.m[2][2];
			m[3][0] = M.m[0][3]; m[3][1] = M.m[1][3]; m[3][2] = M.m[2][3];
		}
	};
}//Lib_Math