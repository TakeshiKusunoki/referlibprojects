var indexSectionsWithContent =
{
  0: "im~",
  1: "m",
  2: "m",
  3: "m~",
  4: "m",
  5: "i"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines"
};

var indexSectionLabels =
{
  0: "全て",
  1: "クラス",
  2: "ファイル",
  3: "関数",
  4: "変数",
  5: "マクロ定義"
};

