var indexSectionsWithContent =
{
  0: "_abcdeghilmprstuw~",
  1: "hw",
  2: "lmw",
  3: "hw",
  4: "abcdeghiprstuw~",
  5: "_",
  6: "w",
  7: "w",
  8: "bpw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "related",
  8: "defines"
};

var indexSectionLabels =
{
  0: "全て",
  1: "クラス",
  2: "名前空間",
  3: "ファイル",
  4: "関数",
  5: "変数",
  6: "型定義",
  7: "フレンド",
  8: "マクロ定義"
};

