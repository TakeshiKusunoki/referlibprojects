#pragma once
/**
* @file DirectX11Init.h
* @brief DirectX11Initクラスが記述されている
* @details 詳細な説明
*/

#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
#include <windows.h>

#include <d3d11.h>//追加
#include <dxgi.h>
#include "Blend.h"
#include <Lib_Template/DimensGrap.h>
#include <Lib_Base/Flag.h>
#include "RenderTarget.h"

namespace
{
	namespace at_compile_time
	{
		class encapsulation_DirectX11InitDevice;
	}
}



namespace Lib_3D {
	
	
	/**
	* @brief directX11デバイス初期化
	* @par 詳細
	* デバイスとイミィディエイトコンテキストを定義取得できる
	* *cppで初めにグローバル変数1つだけ作った*
	* * 最初から最後まで使うためグローバルに置いた
	*/
	class DirectX11InitDevice final
	{
		friend at_compile_time::encapsulation_DirectX11InitDevice;
	private:
		static ID3D11Device* p_Device;//! デバイス
		static ID3D11DeviceContext* p_ImidiateContext;//! イミィディエイトコンテキスト
		DirectX11InitDevice();
	public:
		~DirectX11InitDevice();
		//! @returnデバイス取得
		static ID3D11Device* GetDevice()
		{
			return p_Device;
		}

		//! @returnデバイスコンテキスト取得
		static ID3D11DeviceContext* GetImidiateContext()
		{
			return p_ImidiateContext;
		}
	};


	/**
	* @brief directX11初期化
	* @par 詳細
	* ウインドウごとにインスタンスを作る(複数ウインドウのための設計)
	* *Device初期化前に呼ばない*
	*/
	class DirectX11ComInit final
	{
	private:
		IDXGIFactory* p_Factory;//! スワップチェイン作るとき使う
		IDXGISwapChain* p_SwapChain;//! スワップチェイン
		ID3D11RenderTargetView* p_RenderTargetView;//! レンダーターゲット（描画ターゲットを決めるとき使う）
		ID3D11DepthStencilView* p_DepthStencilView;//! 深度（描画ターゲットを決めるとき使う）
		BlendMode p_Blend;//! ブレンドモード設定
		static constexpr size_t VIEW_MAX = 10;//ビューポート数
		//D3D11_VIEWPORT viewport[VIEW_MAX];//ビューポート数
		//VIEW_PORT_SORT sorts[VIEW_MAX];
		Lib_Base::Utility::FlagForOneTimeCall flagCallOne;
		Lib_Base::Utility::DecideSupport::RecognizeFlagOn<Lib_Base::Utility::FlagForOneTimeCall> recognyflagCallOne;
		Lib_Template::Utility::ArrayDimensGrap<D3D11_VIEWPORT*> grap;//インデックス１つに１つの実態を格納する
		int viewPortNum = 0;//ビューポートの数

		HWND hwnd;//! ウインドウハンドル
		size_t SCREEN_WIDTH;//! ウインドウ幅
		size_t SCREEN_HEIGHT;//! ウインドウ高さ
	public:
		RenderTargetCl target[VIEW_MAX];//ビューポートとレンダーターゲットクラス(コンストラクタで定義しない)

		//! @brief 描画するウインドウの情報をもらう(コンストラクタ)
		//! @param[in] hwnd_ ウインドウハンドル
		//! @param[in] SCREEN_WIDTH_ 画面幅
		//! @param[in] SCREEN_HEIGHT_ 画面高さ
		DirectX11ComInit(HWND hwnd_, size_t SCREEN_WIDTH_, size_t SCREEN_HEIGHT_);
		DirectX11ComInit(DirectX11ComInit&&);
		//! @brief COMオブジェクトを破棄する
		~DirectX11ComInit();
		
		//! @brief ゲッター
		//! @return HWND ウインドウハンドル
		HWND GetHwnd() const
		{
			return hwnd;
		}

		//! @brief ゲッター
		//! @return UINT ウインドウ幅
		size_t GetScreenWidth() const
		{
			return SCREEN_WIDTH;
		}

		//! @brief ゲッター
		//! @return UINT ウインドウ高さ
		size_t GetScreenHeight() const
		{
			return SCREEN_HEIGHT;
		}
		//! ビューの数
		int GetViewNum(){
			return this->viewPortNum;
		}
		////! @brief ゲッター
		////! @return スワップチェイン
		//IDXGISwapChain* GetSwapChain() const
		//{
		//	return this->p_SwapChain;
		//}

		////! @brief ゲッター
		////! @return レンダーターゲット
		//ID3D11RenderTargetView* GetRenderTargetView() const
		//{
		//	return this->p_RenderTargetView;
		//}

		//ID3D11RenderTargetView** GetRenderTargetViewA()
		//{
		//	return &(this->p_RenderTargetView);
		//}
		//
		////! @brief ゲッター
		////! @return 深度
		//ID3D11DepthStencilView* GetDepthStencilView() const
		//{
		//	return this->p_DepthStencilView;
		//}
		//! アルファブレンドモードセット
		void SetBlendModeAlpha()
		{
			p_Blend.Set(BlendMode::BLEND_MODE::ALPHA);
		}
		//! 加算合成ブレンドモードセット
		void SetBlendModeAdd()
		{
			p_Blend.Set(BlendMode::BLEND_MODE::ADD);
		}
		//!  ビューポートを設定する
		//! @param[in] viewportSetNum セットするビューポートの番号
		//! @param[in] centerx 中心位置
		//! @param[in] width 幅
		void ViewPortPropatySeting(int viewportSetNum, float centerx = 0, float centery = 0, float width = 0, float height = 0);
		//! ビューポートをセットする
		//! 必ずビューポートのセッティングをしたのと同じのを入れる
		//! @pram[in] sort 使うビューの種類
		//! @pram[in] ビューの数
		void ViewSet(int viewportSetNum);
		//! ビューポートの配列をパイプラインのラスタライザーステージにバインドします。(デバイスコンテキストに１時的にセット)
		void ViewSet(int i);
		//! 描画ターゲットのクリア
		void ClearRenderTarget(float r, float g, float b, float a);

		void RendetTargetSet();

		//! @brief フリップ処理
		//! @details データを保持しない
		void Flip0() { p_SwapChain->Present(DXGI_SWAP_EFFECT_DISCARD, 0); }

		//! @details レンダーターゲットの描画データを同期する時、使う
		void Flip1() { p_SwapChain->Present(DXGI_SWAP_EFFECT_SEQUENTIAL, 0); }

		//! @return hr == DXGI_STATUS_OCCLUDED なら描画する必要がない
		[[nodiscard]]
		HRESULT FlipTest() { return p_SwapChain->Present(DXGI_SWAP_EFFECT_DISCARD, DXGI_PRESENT_TEST); }

	private:
		//! @brief 必要なCOMを定義
		bool initialize();
		//明示的に破棄
		void UnInitialize();
	};
	
}

