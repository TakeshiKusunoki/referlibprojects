#pragma once
#include "DirectXTex\DirectXTex.h"
#include <string>
namespace
{
	namespace at_compile_time
	{
		class encapsulation_TextureResourceManager0;//まだ作っていい
		class encapsulation_TextureResourceManager1;
	}
}

namespace Lib_3D
{
	class TextureResourceManager;

	//! テクスチャークラス
	//! * デストラクタに副作用がある
	class Texture final
	{
		friend TextureResourceManager;
	private:
		//ムーブ用、２重開放を防ぐ
		struct COM_RESOURCE
		{
			ID3D11ShaderResourceView* ShaderResourceView;
			ID3D11SamplerState* SamplerState;
			COM_RESOURCE();
			COM_RESOURCE(COM_RESOURCE&) = default;
			//vectorでmoveすると、1時データがデストラクトされるので、ムーブしたときインスタンスが破棄されないようにしておく
			COM_RESOURCE(COM_RESOURCE&& o) noexcept;
			//副作用を持っているデストラクタ
			~COM_RESOURCE();
		};
		COM_RESOURCE texResource;
		int width = 0;
		int height = 0;
	public:
		Texture() = default;
		Texture(Texture&) = default;
		Texture(Texture&&) = default;
		~Texture() = default;


		
		void Set(int slot = 0);

		int GetWidth() { return width; }
		int GetHeight() { return height; }
		//! @return 必ずデリーとする (newしてる)
		[[nodiscard]]
		BYTE* GetPixels();
	private:
		// ファイル名をもとにShaderResourceViewとSamplerStateを作る
		//[[nodiscard]]
		bool Load(const char* filename);

		void Release();
	};

	//! 画像参照
	//! リソースマネージャーはそれぞれに作ってあげて必要なくなったらreleaseしてあげるとメモリが圧迫されない(releaseを使うときはinit時のロードを忘れない)
	class TextureResourceManager final
	{
		friend at_compile_time::encapsulation_TextureResourceManager0;
		friend at_compile_time::encapsulation_TextureResourceManager1;
	private:
		//? ローカル構造体
		struct RESOURCE_INFO final
		{
			// 変数
			std::string filename;
			Texture texture;//テクスチャーインスタンス
			// RESOURCE_INFOの変数を要素に持つ、ローカル関数
			//? ローカル関数のコンストラクタ(class ResourceManagerからしか見えない)
			RESOURCE_INFO(const char* filename_, Texture** texture_)
			{
				filename = filename_;
				texture.Load(filename_);//ここでテクスチャーをロード
				*texture_ = &texture;
			}
			RESOURCE_INFO(RESOURCE_INFO&) = delete;
			//vectorを使うのでムーブさせる
			RESOURCE_INFO(RESOURCE_INFO&&) = default;
			
			void Release()
			{
				texture.Release();
				filename = "";
			}
		};
	private:
		static constexpr size_t RESERVE_SIZE = (1028 + 1) / sizeof(RESOURCE_INFO);//リソース予約数 
		std::vector<RESOURCE_INFO> ResourceInfo;
		//staticでないので複数作っていい
		TextureResourceManager()
		{
			ResourceInfo.reserve(RESERVE_SIZE);
		}
	public:
		TextureResourceManager(TextureResourceManager&) = delete;
		TextureResourceManager(TextureResourceManager&&) = delete;
		~TextureResourceManager() = default;
		//! 所持するデータを破棄(リソースを削減したい時に使う)
		void Release();
		//! 読み込んだテクスチャーインスタンスへの参照を返す
		//! @return 何もないならnullを返す
		[[nodiscard]]
		Texture* const LoadTexture(const char*);
		//! @return 何もないならnullを返す
		[[nodiscard]]
		Texture* const FindTexture(const char* texturefileName);
	};
}


