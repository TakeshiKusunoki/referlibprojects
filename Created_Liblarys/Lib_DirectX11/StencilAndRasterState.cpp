#include "StencilAndRasterState.h"
#include <cassert>
#include <Lib_DirectX11/DirectX11Init.h>
#include <stdexcept>
namespace
{
	//! @namespace call_externaly
	//! @brief 外のソースコードから呼んでいるもの(このファイル専用に作ったソースではないもの)
	namespace call_externaly
	{
#define GetDevice Lib_3D::DirectX11InitDevice::GetDevice()
#define GetDeviceContext Lib_3D::DirectX11InitDevice::GetImidiateContext()
	}
}
namespace Lib_3D
{
	void StencilAndRasterState::CreateDefaultRasutrizerState(ID3D11RasterizerState** pRasterizerState)
	{
		HRESULT hr = S_OK;
		D3D11_RASTERIZER_DESC RasteriserDesk;
		ZeroMemory(&RasteriserDesk, sizeof(D3D11_RASTERIZER_DESC));
		//-線描画の場合
		RasteriserDesk.FillMode = D3D11_FILL_SOLID;		//レンダリング時に使用する描画モードを決定します
		RasteriserDesk.CullMode = /*D3D11_CULL_BACK*//*D3D11_CULL_FRONT*/D3D11_CULL_NONE;		//特定の方向を向いている三角形の描画の有無を示します。
		RasteriserDesk.FrontCounterClockwise = FALSE;	//三角形が前向きか後ろ向きかを決定します。
		RasteriserDesk.DepthBias = 0;					//指定のピクセルに加算する深度値です。
		RasteriserDesk.DepthBiasClamp = 0;				//ピクセルの最大深度バイアスです。
		RasteriserDesk.SlopeScaledDepthBias = 0;		//指定のピクセルのスロープに対するスカラです。
		RasteriserDesk.DepthClipEnable = FALSE;			//距離に基づいてクリッピングを有効にします。
		RasteriserDesk.ScissorEnable = FALSE;			//シザーカリング
		RasteriserDesk.MultisampleEnable = TRUE;		//マルチサンプリングのアンチエイリアシング
		RasteriserDesk.AntialiasedLineEnable = FALSE;	//線のアンチエイリアシング(線を描画中で MultisampleEnable が false の場合にのみ)


		//-塗りつぶし描画の場合
		hr = GetDevice->CreateRasterizerState(&RasteriserDesk, pRasterizerState);
		if (FAILED(hr))
		{
			assert(!"ラスタライザーステートオブジェクト(塗りつぶし描画)の生成ができません");
			return;
		}
	}

	void StencilAndRasterState::CreateLineRasutrizerState(ID3D11RasterizerState** pRasterizerState)
	{
		HRESULT hr = S_OK;
		D3D11_RASTERIZER_DESC RasteriserDesk;
		ZeroMemory(&RasteriserDesk, sizeof(D3D11_RASTERIZER_DESC));
		//-線描画の場合
		RasteriserDesk.FillMode = D3D11_FILL_WIREFRAME;	//レンダリング時に使用する描画モードを決定します
		RasteriserDesk.CullMode = D3D11_CULL_NONE;		//特定の方向を向いている三角形の描画の有無を示します。
		RasteriserDesk.FrontCounterClockwise = FALSE;	//三角形が前向きか後ろ向きかを決定します。
		RasteriserDesk.DepthBias = 0;					//指定のピクセルに加算する深度値です。
		RasteriserDesk.DepthBiasClamp = 0;				//ピクセルの最大深度バイアスです。
		RasteriserDesk.SlopeScaledDepthBias = 0;		//指定のピクセルのスロープに対するスカラです。
		RasteriserDesk.DepthClipEnable = FALSE;			//距離に基づいてクリッピングを有効にします。
		RasteriserDesk.ScissorEnable = FALSE;			//シザーカリング
		RasteriserDesk.MultisampleEnable = FALSE;		//マルチサンプリングのアンチエイリアシング
		RasteriserDesk.AntialiasedLineEnable = TRUE;	//線のアンチエイリアシング(線を描画中で MultisampleEnable が false の場合にのみ)
		hr = GetDevice->CreateRasterizerState(&RasteriserDesk, pRasterizerState);
		if (FAILED(hr))
		{
			assert(!"ラスタライザーステートオブジェクトの生成ができません");
			return;
		}
	}

	void StencilAndRasterState::CreateDefaultStencilState(ID3D11DepthStencilState** pDepthStencilState)
	{
		HRESULT hr = S_OK;
		D3D11_DEPTH_STENCIL_DESC DepthDesc;
		ZeroMemory(&DepthDesc, sizeof(DepthDesc));
		DepthDesc.DepthEnable = TRUE;									//深度テストあり
		DepthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;			//書き込む
		DepthDesc.DepthFunc = D3D11_COMPARISON_LESS;					//手前の物体を描画
		DepthDesc.StencilEnable = FALSE;								//ステンシル テストなし
		DepthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;		//ステンシル読み込みマスク
		DepthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;	//ステンシル書き込みマスク
		//面が表を向いている場合のステンシルステートの設定
		DepthDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
		DepthDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
		 //面が裏を向いている場合のステンシルステートの設定
		DepthDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
		DepthDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
		hr = GetDevice->CreateDepthStencilState(&DepthDesc, pDepthStencilState);	//
		if (FAILED(hr))
		{
			throw std::runtime_error("深度ステンシル ステート オブジェクトの生成ができません");
		}
	}

	void StencilAndRasterState::SetRastarizerState(ID3D11RasterizerState* pRasterizerState)
	{
																	// �Cラスタライザ・ステート・オブジェクトの設定
		GetDeviceContext->RSSetState(pRasterizerState);//ラスタライザステート

	}

	void StencilAndRasterState::SetDepthStencilState(ID3D11DepthStencilState* pDepthStencilState)
	{//震度ステンシルステート
		GetDeviceContext->OMSetDepthStencilState(pDepthStencilState, 0);//震度ステンシルステート


	}

}//Lib_3D
