#include "ShaderResourceManager.h"
//#include <cstdio>
#include <d3dcompiler.h>
#include <cassert>
#include "DirectX11Init.h"

#if _DEBUG
#define new  ::new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif

namespace
{
	//! @namespace call_externaly
	//! @brief 外のソースコードから呼んでいるもの(このファイル専用に作ったソースではないもの)
	namespace call_externaly
	{
#define GetDevice Lib_3D::DirectX11InitDevice::GetDevice()
#define GetDeviceContext Lib_3D::DirectX11InitDevice::GetImidiateContext()
	}

	const std::string Directory{ "../../_Data/Shader/" };
	//! シェーダーをコンパイルする
	HRESULT Compile(WCHAR* filename, LPCSTR method, LPCSTR shaderModel, ID3DBlob** ppBlobOut)
	{
		DWORD ShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
		//ShaderFlags |= D3DCOMPILE_DEBUG;
		//ShaderFlags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;

		ID3DBlob* BlobError = NULL;
		// コンパイル
		HRESULT hr = D3DCompileFromFile(
			filename,
			NULL,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			method,
			shaderModel,
			ShaderFlags,
			0,
			ppBlobOut,
			&BlobError
		);

		// エラー出力
		if (BlobError != NULL)
		{
			OutputDebugStringA((char*)BlobError->GetBufferPointer());
			BlobError->Release();
			BlobError = NULL;
		}

		return hr;
	}

	bool ReadBinaryFile(const char* filename, BYTE** data, unsigned int& size)
	{
		FILE* fp = nullptr;
		if (fopen_s(&fp, filename, "rb"))
		{
			return false;
		}

		fseek(fp, 0, SEEK_END);

		size = ftell(fp);

		fseek(fp, 0, SEEK_SET);
		*data = new unsigned char[size];
		fread(*data, size, 1, fp);

		fclose(fp);

		return true;
	}
}


namespace Lib_3D
{
	//std::vector<ShaderResourceManager::RESOURCE_VERTEXSHADER> ShaderResourceManager::ResourceVertexShader{};
	//std::vector<ShaderResourceManager::RESOURCE_PIXELSHADER> ShaderResourceManager::ResourcePixcelShader{};
	bool ShaderResourceManager::LoadVertexShader(const char* shadername, const char* csofileName, const D3D11_INPUT_ELEMENT_DESC* p_INPUT_ELEMENT_DESC, const UINT ELEMENTS_ARRAY_NUM)
	{
		//初期化としてのNULL代入
		bool flagFind = false;
		ID3D11VertexShader* p_VertexShader_ = nullptr;
		ID3D11InputLayout* p_InputLayout_ = nullptr;
		//////////////////////
		//データ検索
		for (int n = 0, size = ResourceVertexShader.size(); n < size; n++)
		{
			RESOURCE_VERTEXSHADER* p = &ResourceVertexShader[n];
			//ファイルパスが違うなら虫
			if (p->filename != shadername)
				continue;
			//同名ファイルが存在した
			p_VertexShader_ = p->p_VertexShader;
			p_InputLayout_ = p->p_InputLayout;
			flagFind = true;
			break;
		}

		////////////////////////////////////////////////
		// データが見つからなかった = 新規読み込み
		if (!flagFind)
		{
			BYTE* data = nullptr;//頂点シェーダーデータ
			UINT size;//頂点シェーダーデータサイズ
					  // コンパイル済み頂点シェーダーオブジェクトの読み込み
			if (!ReadBinaryFile((Directory + std::string(csofileName)+".hlsl").c_str(), &data, size))return false;

			// 頂点シェーダーオブジェクトの生成
			HRESULT hr = GetDevice->CreateVertexShader(data, size, nullptr, &p_VertexShader_);
			if (FAILED(hr))
			{
				if(data)
					delete[] data;
				assert(!"頂点シェーダーオブジェクトの生成ができません");
				return false;
			}

			// 入力レイアウトの作成
			hr = GetDevice->CreateInputLayout(p_INPUT_ELEMENT_DESC, ELEMENTS_ARRAY_NUM, data, size, &p_InputLayout_);
			if (data)
				delete[] data;
			if (FAILED(hr))
			{
				assert(!"入力レイアウトの作成ができません");
				return false;
			}
				

			// 新規データの保存
			//リソースマネージャーにデータを格納
			ResourceVertexShader.emplace_back(shadername, p_VertexShader_, p_InputLayout_);
		}
		////////////////////////////////////////////////

		return true;
	}


	bool ShaderResourceManager::LoadPixelShader(const char* shadername, const char* csofileName)
	{
		//初期化としてのNULL代入
		bool flagFind = false;
		ID3D11PixelShader* pPixelShader = nullptr;
		//////////////////////
		// データ検索
		for (size_t n = 0, size = ResourcePixcelShader.size(); n < size; n++)
		{
			RESOURCE_PIXELSHADER* p = &ResourcePixcelShader[n];
			//ファイルパスが違うなら虫
			if (p->filename != shadername)
				continue;


			//同名ファイルが存在した
			pPixelShader = p->p_PixelShader;
			flagFind = true;//発見
			break;
		}

		////////////////////////////////////////////////
		// データが見つからなかった = 新規読み込み
		if (!flagFind)
		{
			BYTE* data = nullptr;//ピクセルシェーダデータ
			UINT size;//ピクセルシェーダデータサイズ
					  // コンパイル済み頂点シェーダーオブジェクトの読み込み
			if (!ReadBinaryFile((Directory + std::string(csofileName) + ".hlsl").c_str(), &data, size))return false;
			// ピクセルシェーダーオブジェクトの生成
			HRESULT hr = GetDevice->CreatePixelShader(data, size, nullptr, &pPixelShader);
			if (data)
				delete[] data;
			if (FAILED(hr))
			{
				assert(!"ピクセルシェーダーオブジェクトの生成ができません");
				return false;
			}
				

			// 新規データの保存
			ResourcePixcelShader.emplace_back(shadername, pPixelShader);
		}
		////////////////////////////////////////////////
		return true;
	}
	







	bool ShaderResourceManager::LoadCompileVertexShader(const char* shadername, const char* vsfilename, LPCSTR VSFunc, const D3D11_INPUT_ELEMENT_DESC* element, const UINT elementNum)
	{
		HRESULT hr = S_OK;

		//初期化としてのNULL代入
		ID3D11VertexShader* p_VertexShader_ = nullptr;
		ID3D11InputLayout* p_InputLayout_ = nullptr;
		///////////////////////
		//const char*->wchar_t*(検索保存で使用)
		
		//////////////////////
		//データ検索
		for (int n = 0, size = this->ResourceVertexShader.size(); n < size; n++)
		{
			RESOURCE_VERTEXSHADER* p = &this->ResourceVertexShader[n];
			//ファイルパスが違うなら虫
			if (p->filename != shadername)
				continue;
			//同名ファイルが存在した
			p_VertexShader_ = p->p_VertexShader;
			p_InputLayout_ = p->p_InputLayout;
			return true;
		}
		////////////////////////////////////////////////
		// データが見つからなかった = 新規読み込み
		
		wchar_t fileName[256];
		size_t stringSize = 0;
		mbstowcs_s(&stringSize, fileName, (Directory + std::string(vsfilename) + ".hlsl").c_str(), (Directory + std::string(vsfilename) + ".hlsl").length());

		ID3DBlob* VSBlob = NULL;
		// 頂点シェーダ
		hr = Compile(fileName, VSFunc, "vs_4_0", &VSBlob);
		if (FAILED(hr))
		{
			assert(!"頂点シェーダコンパイルができません");
			return false;
		}

		// 頂点シェーダ生成
		hr = GetDevice->CreateVertexShader(VSBlob->GetBufferPointer(), VSBlob->GetBufferSize(), NULL, &p_VertexShader_);
		if (FAILED(hr))
		{
			VSBlob->Release();
			assert(!"頂点シェーダ生成ができません");
			return false;
		}
		// 入力レイアウト生成
		hr = GetDevice->CreateInputLayout(
			element,
			elementNum,
			VSBlob->GetBufferPointer(),
			VSBlob->GetBufferSize(),
			&p_InputLayout_
		);
		this->ResourceVertexShader.emplace_back(shadername, p_VertexShader_, p_InputLayout_);

		return true;
	}

	bool ShaderResourceManager::LoadCompileGeometryShader(const char* shadername, const char* gsfilename, LPCSTR GSFunc)
	{
		HRESULT hr = S_OK;

		//初期化としてのNULL代入
		ID3D11GeometryShader* p_GeometryShader_ = nullptr;
		//////////////////////
		//データ検索
		for (int n = 0, size = this->ResourceGeometryShader.size(); n < size; n++)
		{
			RESOURCE_GEOMETRYSHADER* p = &this->ResourceGeometryShader[n];
			//ファイルパスが違うなら虫
			if (p->filename != shadername)
				continue;
			//同名ファイルが存在した
			p_GeometryShader_ = p->p_GeometryShader;
			return true;
		}
		////////////////////////////////////////////////
	// データが見つからなかった = 新規読み込み
		wchar_t fileName[256];
		size_t stringSize = 0;
		mbstowcs_s(&stringSize, fileName, (Directory + std::string(gsfilename) + ".hlsl").c_str(), (Directory + std::string(gsfilename) + ".hlsl").length());

		ID3DBlob* GSBlob = NULL;
		// ジオメトリシェーダー
		hr = Compile(fileName, GSFunc, "gs_4_0", &GSBlob);
		if (FAILED(hr))
		{
			assert(!"ジオメトリシェーダーコンパイルができません");
			return false;
		}

		// ピクセルシェーダ生成
		hr = GetDevice->CreateGeometryShader(GSBlob->GetBufferPointer(), GSBlob->GetBufferSize(), NULL, &p_GeometryShader_);
		if (FAILED(hr))
		{
			assert(!"ジオメトリシェーダー生成ができません");
			GSBlob->Release();
			return false;
		}
		this->ResourceGeometryShader.emplace_back(shadername, p_GeometryShader_);
		return true;
	}


	bool ShaderResourceManager::LoadCompilePixelShader(const char* shadername,const char* psfilename, LPCSTR PSFunc)
	{
		HRESULT hr = S_OK;

		//初期化としてのNULL代入
		ID3D11PixelShader* p_PixelShader_ = nullptr;
		//////////////////////
		//データ検索
		for (int n = 0, size = this->ResourcePixcelShader.size(); n < size; n++)
		{
			RESOURCE_PIXELSHADER* p = &this->ResourcePixcelShader[n];
			//ファイルパスが違うなら虫
			if (p->filename != shadername)
				continue;
			//同名ファイルが存在した
			p_PixelShader_ = p->p_PixelShader;
			return true;
		}
		////////////////////////////////////////////////
	// データが見つからなかった = 新規読み込み
		wchar_t fileName[256];
		size_t stringSize = 0;
		mbstowcs_s(&stringSize, fileName, (Directory + std::string(psfilename) + ".hlsl").c_str(), (Directory + std::string(psfilename) + ".hlsl").length());

		ID3DBlob* PSBlob = NULL;
		// ピクセルシェーダ
		hr = Compile(fileName, PSFunc, "ps_4_0", &PSBlob);
		if (FAILED(hr))
		{
			assert(!"ピクセルシェーダコンパイルができません");
			return false;
		}

		// ピクセルシェーダ生成
		hr = GetDevice->CreatePixelShader(PSBlob->GetBufferPointer(), PSBlob->GetBufferSize(), NULL, &p_PixelShader_);
		if (FAILED(hr))
		{
			assert(!"ピクセルシェーダ生成ができません");
			PSBlob->Release();
			return false;
		}
		this->ResourcePixcelShader.emplace_back(shadername, p_PixelShader_);
		return true;
	}

	ID3D11VertexShader* ShaderResourceManager::FindVertexShader(const char* shadername)
	{
		//////////////////////
		//データ検索
		for (int n = 0,size = this->ResourceVertexShader.size(); n < size; n++)
		{
			RESOURCE_VERTEXSHADER* p = &this->ResourceVertexShader[n];
			//ファイルパスが違うなら虫
			if (p->filename != shadername)
				continue;
			//同名ファイルが存在した
			return p->p_VertexShader;
		}
		return nullptr;
	}

	ID3D11InputLayout* ShaderResourceManager::FindInputLayout(const char* shadername)
	{
		//////////////////////
		//データ検索
		for (int n = 0, size = this->ResourceVertexShader.size(); n < size; n++)
		{
			RESOURCE_VERTEXSHADER* p = &this->ResourceVertexShader[n];
			//ファイルパスが違うなら虫
			if (p->filename != shadername)
				continue;
			//同名ファイルが存在した
			return p->p_InputLayout;
		}
		return nullptr;
	}

	ID3D11GeometryShader* ShaderResourceManager::FindGeometryShader(const char* shadername)
	{
		for (int n = 0, size = this->ResourceGeometryShader.size(); n < size; n++)
		{
			RESOURCE_GEOMETRYSHADER* p = &this->ResourceGeometryShader[n];
			//ファイルパスが違うなら虫
			if (p->filename != shadername)
				continue;
			//同名ファイルが存在した
			return p->p_GeometryShader;
		}
		return nullptr;
	}

	ID3D11PixelShader* ShaderResourceManager::FindPixelShader(const char* shadername)
	{
		//////////////////////
		//データ検索
		for (int n = 0,size = this->ResourcePixcelShader.size(); n < size; n++)
		{
			RESOURCE_PIXELSHADER* p = &this->ResourcePixcelShader[n];
			//ファイルパスが違うなら虫
			if (p->filename != shadername)
				continue;
			//同名ファイルが存在した
			return p->p_PixelShader;
		}
		return nullptr;
	}


	void ShaderResourceManager::Release()
	{
		for (auto& it : ResourceVertexShader)
		{
			it.Release();
		}
		for (auto& it : ResourceGeometryShader)
		{
			it.Release();
		}
		for (auto& it : ResourcePixcelShader)
		{
			it.Release();
		}
		ResourceVertexShader.clear();
		ResourceGeometryShader.clear();
		ResourcePixcelShader.clear();
	}

	//void ShaderResourceManager::ReleaseVertexShader(ID3D11VertexShader* p_VertexShader, ID3D11InputLayout* p_InputLayout)
	//{

	//	if (!p_VertexShader)
	//		return;

	//	for (int n = 0; n < ResourceVertexShader.size(); n++)
	//	{
	//		RESOURCE_VERTEXSHADER* p = &ResourceVertexShader[n];
	//		//データが違うなら虫
	//		if (p_VertexShader != p->p_VertexShader)
	//			continue;
	//		if (p_InputLayout != p->p_InputLayout)
	//			continue;
	//		//データが存在した
	//		p->Release();//データ開放
	//		break;
	//	}
	//}

	//void ShaderResourceManager::ReleasePixelShader(ID3D11PixelShader* p_PixelShader)
	//{
	//	if (!p_PixelShader)
	//		return;

	//	for (int n = 0; n < ResourcePixcelShader.size(); n++)
	//	{
	//		RESOURCE_PIXELSHADER* p = &ResourcePixcelShader[n];
	//		//データが違うなら虫
	//		if (p_PixelShader != p->p_PixelShader)
	//			continue;
	//		//データが存在した
	//		p->Release();//データ開放
	//		break;
	//	}
	//}
}

