#pragma once
//#include  <string>
#include <d3d11.h>
#include <vector>
#include <string>
namespace
{
	namespace at_compile_time
	{
		class  encapsulation_ShaderResourceManager;//これ以上は作らない
	}
}

namespace Lib_3D {
	

	class ShaderResourceManager final
	{
		//! このfriendは他のソースコードでコンストラクタを呼んでる場所を知れる
		friend at_compile_time::encapsulation_ShaderResourceManager;
	private:
		struct RESOURCE_VERTEXSHADER final
		{
			// 変数
			std::string filename;
			ID3D11VertexShader* p_VertexShader;//シェーダーリソースビューオブジェクト(ID3D11ShaderResoyrceView)の作成
			ID3D11InputLayout* p_InputLayout;
			//? ローカル関数のコンストラクタ(class ResourceManagerからしか見えない)
			RESOURCE_VERTEXSHADER(const char* filename_, ID3D11VertexShader* p_VertexShader_, ID3D11InputLayout* p_InputLayout_)
				: p_VertexShader(p_VertexShader_)
				, p_InputLayout(p_InputLayout_)
			{
				filename = filename_;
			}
			//ベクターで領域確保時ムーブさせたいので
			RESOURCE_VERTEXSHADER(RESOURCE_VERTEXSHADER&&) = default;
			~RESOURCE_VERTEXSHADER() = default;
			//画像が消えたらcounterを減らす//画像をデリートする関数
			void Release()
			{
				if (p_VertexShader)
				{
					p_VertexShader->Release();
					p_VertexShader = nullptr;
				}
				if (p_InputLayout)
				{
					p_InputLayout->Release();
					p_InputLayout = nullptr;
				}
				filename = "";
			}
		};
		//ジオメトリシェーダー
		struct RESOURCE_GEOMETRYSHADER final
		{
			// 変数
			std::string filename;
			ID3D11GeometryShader* p_GeometryShader;//シェーダーリソースビューオブジェクト(ID3D11ShaderResoyrceView)の作成
											 //? ローカル関数のコンストラクタ(class ResourceManagerからしか見えない)
			RESOURCE_GEOMETRYSHADER(const char* filename_, ID3D11GeometryShader* p_GeometryShader)
				: p_GeometryShader(p_GeometryShader)
			{
				filename = filename_;
			}
			//ベクターで領域確保時ムーブさせたいので
			RESOURCE_GEOMETRYSHADER(RESOURCE_GEOMETRYSHADER&&) = default;
			~RESOURCE_GEOMETRYSHADER() = default;
			//画像が消えたらcounterを減らす//画像をデリートする関数
			void Release()
			{
				if (p_GeometryShader)
				{
					p_GeometryShader->Release();
					p_GeometryShader = nullptr;
				}
				filename = "";
			}
		};

		struct RESOURCE_PIXELSHADER final
		{
			// 変数
			std::string filename;
			ID3D11PixelShader* p_PixelShader;//シェーダーリソースビューオブジェクト(ID3D11ShaderResoyrceView)の作成
											 //? ローカル関数のコンストラクタ(class ResourceManagerからしか見えない)
			RESOURCE_PIXELSHADER(const char* filename_, ID3D11PixelShader* p_PixelShader)
				: p_PixelShader(p_PixelShader)
			{
				filename = filename_;
			}
			//ベクターで領域確保時ムーブさせたいので
			RESOURCE_PIXELSHADER(RESOURCE_PIXELSHADER&&) = default;
			~RESOURCE_PIXELSHADER() = default;
			//画像が消えたらcounterを減らす//画像をデリートする関数
			void Release()
			{
				if (p_PixelShader)
				{
					p_PixelShader->Release();
					p_PixelShader = nullptr;
				}
				filename = "";
			}
		};
	private:
		static constexpr size_t RES_VS_SIZE = (256 + 1) * 2 / sizeof(RESOURCE_VERTEXSHADER);//ベクター予約サイズ
		static constexpr size_t RES_GS_SIZE = (256 + 1) * 2 / sizeof(RESOURCE_GEOMETRYSHADER);//ベクター予約サイズ
		static constexpr size_t RES_PS_SIZE = (256 + 1) * 2 / sizeof(RESOURCE_PIXELSHADER);//ベクター予約サイズ
		std::vector<RESOURCE_VERTEXSHADER> ResourceVertexShader;//vectorはstaticにするとメモリリークを起こすので,本当は１つしか作らないけどstaticにしない
		std::vector<RESOURCE_GEOMETRYSHADER> ResourceGeometryShader;
		std::vector<RESOURCE_PIXELSHADER> ResourcePixcelShader;
		//staticなので複数作らない
		ShaderResourceManager()
		{
			ResourceVertexShader.reserve(RES_VS_SIZE);
			ResourceGeometryShader.reserve(RES_GS_SIZE);
			ResourcePixcelShader.reserve(RES_PS_SIZE);
		}
	public:
		
		~ShaderResourceManager()
		{
			Release();
			std::vector<RESOURCE_VERTEXSHADER>().swap(ResourceVertexShader);
			std::vector<RESOURCE_GEOMETRYSHADER>().swap(ResourceGeometryShader);
			std::vector<RESOURCE_PIXELSHADER>().swap(ResourcePixcelShader);
		}
		// csoファイルを使う、シェーダーのロード
		bool LoadVertexShader(const char* shadername, const char* vsfilename, const D3D11_INPUT_ELEMENT_DESC*, const UINT ELEMENT_ARRAY_NUM);
		
		//! @param[in] shadername シェーダー名
		//! @param[in] psfilename ピクセルシェーダーファイル名
		bool LoadPixelShader(const char* shadername, const char* psfilename);
		// コンパイルを使う、シェーダーのロード
		bool LoadCompileVertexShader(const char* shadername, const char* vsfilename, LPCSTR VSFunc, const D3D11_INPUT_ELEMENT_DESC*, const UINT ELEMENT_ARRAY_NUM);
		bool LoadCompileGeometryShader(const char* shadername, const char* gsfilename, LPCSTR GSFunc);
		bool LoadCompilePixelShader(const char* shadername, const char* psfilename, LPCSTR PSFunc);
		
		
		// ファイル名からシェーダーを見つける
		//! @return バーテックスシェーダー　ないならnullを返す
		ID3D11VertexShader* FindVertexShader(const char* shadername);
		//! @return インプットレイアウトを返す
		ID3D11InputLayout* FindInputLayout(const char* shadername);
		//! @return ジオメトリシェーダー　ないならnullを返す
		ID3D11GeometryShader* FindGeometryShader(const char* shadername);
		//! @return ピクセルシェーダー　ないならnullを返す
		ID3D11PixelShader* FindPixelShader(const char* shadername);
		//! 所持するデータを破棄
		void Release();
	private:
	};
}
