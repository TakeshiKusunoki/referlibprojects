﻿#include "Texture.h"
#include <d3d11.h>
#include <cassert>
#include "DirectX11Init.h"


#if _DEBUG
#define new  ::new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif



namespace
{
	//! @namespace call_externaly
	//! @brief 外のソースコードから呼んでいるもの(このファイル専用に作ったソースではないもの)
	namespace call_externaly
	{
#define GetDevice Lib_3D::DirectX11InitDevice::GetDevice()
#define GetDeviceContext Lib_3D::DirectX11InitDevice::GetImidiateContext()
	}

	HRESULT make_dummy_texture(ID3D11ShaderResourceView** shader_resource_view)
	{
		HRESULT hr = S_OK;

		D3D11_TEXTURE2D_DESC texture2d_desc = {};
		texture2d_desc.Width = 1;
		texture2d_desc.Height = 1;
		texture2d_desc.MipLevels = 1;
		texture2d_desc.ArraySize = 1;
		texture2d_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		texture2d_desc.SampleDesc.Count = 1;
		texture2d_desc.SampleDesc.Quality = 0;
		texture2d_desc.Usage = D3D11_USAGE_DEFAULT;
		texture2d_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		texture2d_desc.CPUAccessFlags = 0;
		texture2d_desc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA subresource_data = {};
		u_int color = 0xFFFFFFFF;
		subresource_data.pSysMem = &color;
		subresource_data.SysMemPitch = 4;
		subresource_data.SysMemSlicePitch = 4;

		ID3D11Texture2D* texture2d;
		hr = GetDevice->CreateTexture2D(&texture2d_desc, &subresource_data, &texture2d);
		if (FAILED(hr))
		{
			assert(!"テクスチャーを作れませんでした");
			return hr;
		}

		D3D11_SHADER_RESOURCE_VIEW_DESC shader_resource_view_desc = {};
		shader_resource_view_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		shader_resource_view_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		shader_resource_view_desc.Texture2D.MipLevels = 1;

		hr = GetDevice->CreateShaderResourceView(texture2d, &shader_resource_view_desc, shader_resource_view);
		if (FAILED(hr))
		{
			assert(!"シェーダーリソースビューを作るときにエラー");
			return hr;
		}

		texture2d->Release();

		return hr;
	}
}

namespace Lib_3D
{

	Texture::COM_RESOURCE::COM_RESOURCE()
		: ShaderResourceView(nullptr)
		, SamplerState(nullptr)
	{}

	Texture::COM_RESOURCE::COM_RESOURCE(COM_RESOURCE&& o) noexcept
		: ShaderResourceView(o.ShaderResourceView)
		, SamplerState(o.SamplerState)
	{
		o.ShaderResourceView = nullptr;
		o.SamplerState = nullptr;
	}
#define RELEASE_IF(x) if (x) { x->Release(); x = nullptr; }
	Texture::COM_RESOURCE::~COM_RESOURCE()
	{
		RELEASE_IF(ShaderResourceView);
		RELEASE_IF(SamplerState);
	}
#undef RELEASE_IF

	
	void Texture::Release()
	{
		if (texResource.ShaderResourceView)
		{
			texResource.ShaderResourceView->Release();
			texResource.ShaderResourceView = nullptr;
		}

		if (texResource.SamplerState)
		{
			texResource.SamplerState->Release();
			texResource.SamplerState = nullptr;
		}
	}

	bool Texture::Load(const char* filename)
	{
		Release();//リソースがあるなら破棄

		wchar_t	wchar[256];
		size_t wLen = 0;
		errno_t err = 0;

		//変換
		mbstowcs_s(&wLen, wchar, 256, filename, _TRUNCATE);

		// 画像ファイル読み込み DirectXTex
		DirectX::TexMetadata metadata;
		DirectX::ScratchImage image;
		//HRESULT hr = LoadFromWICFile(wchar, 0, &metadata, image);
		HRESULT hr;
		if (_strcmpi(&filename[strlen(filename) - 3], "TGA") == 0)
		{
			hr = LoadFromTGAFile(wchar, &metadata, image);
		}
		else
		{
			hr = LoadFromWICFile(wchar, 0, &metadata, image);
		}

		if (FAILED(hr))
		{
			assert(!"テクスチャーファイルが読み込めませんでした。");
			return false;
		}

		width = metadata.width;
		height = metadata.height;

		// 画像からシェーダリソースView
		hr = CreateShaderResourceView(GetDevice, image.GetImages(), image.GetImageCount(), metadata, &texResource.ShaderResourceView);
		if (FAILED(hr))
		{
			assert(!"シェーダリソースビューが作れませんでした。");
			return false;
		}

		// SamplerState作成
		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory(&sampDesc, sizeof(sampDesc));
		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
		hr = GetDevice->CreateSamplerState(&sampDesc, &texResource.SamplerState);
		if (FAILED(hr))
		{
			assert(!"SamplerState作成できませんでした。");
			return false;
		}

		return true;
	}

	


	void Texture::Set(int slot)
	{
		GetDeviceContext->PSSetShaderResources(slot, 1, &texResource.ShaderResourceView);
		GetDeviceContext->PSSetSamplers(slot, 1, &texResource.SamplerState);
	}

	BYTE* Texture::GetPixels()
	{
		DirectX::ScratchImage image;
		ID3D11Resource* source = nullptr;
		texResource.ShaderResourceView->GetResource(&source);
		HRESULT hr = CaptureTexture(GetDevice, GetDeviceContext, source, image);
		if (FAILED(hr))
		{
			assert(!"CaptureTexture error");
			return false;
		}
		uint8_t* ptr = image.GetImage(0, 0, 0)->pixels;

		BYTE* buf = new BYTE[width * height * 4];
		CopyMemory(buf, ptr, width * height * 4);
		return buf;
	}














	Texture* const  TextureResourceManager::LoadTexture(const char* texturefileName)
	{
		
		//////////////////////////
		//同じデータ検索
		Texture* pTexture = nullptr;
		for (int i = 0, size = this->ResourceInfo.size(); i < size; i++)
		{
			RESOURCE_INFO* p = &this->ResourceInfo.at(i);

			//ファイルパスが違うなら無視
			if (p->filename != texturefileName)
				continue;
			//同名ファイルが存在するなら
			pTexture = &p->texture;
			return pTexture;
		}
		////////////////////////////////////////////////
		//同じデータが見つからなかった = 新規読み込み
		//テクスチャーインスタンスのアドレスを保持する
		//リソースマネージャーにデータを格納
		this->ResourceInfo.emplace_back(texturefileName, &pTexture);

		return pTexture;//アドレスを返す
	}

	Texture* const TextureResourceManager::FindTexture(const char* texturefileName)
	{
		//////////////////////
		//データ検索
		for (int n = 0, size = this->ResourceInfo.size(); n < size; n++)
		{
			RESOURCE_INFO* p = &this->ResourceInfo[n];
			//ファイルパスが違うなら虫
			if (p->filename != texturefileName)
				continue;
			//同名ファイルが存在した
			return &p->texture;
		}
		return nullptr;
	}

	void TextureResourceManager::Release()
	{
		for (auto& it : this->ResourceInfo)
		{
			it.Release();
		}
		this->ResourceInfo.clear();
	}

}