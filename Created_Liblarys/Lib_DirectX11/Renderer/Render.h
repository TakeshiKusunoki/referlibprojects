#pragma once

#include "Fbx/FbxLoad.h"
#include "../ShaderInfo.h"
#include <Lib_Math/Matrix.h>
#include "Xvertex/XVertexMesh.h"
#include <Lib_Base/Flag.h>

namespace Lib_3D
{
	namespace Lib_Render
	{
		
		//struct Camera3D
		//{
		//	ShaderInfo::VIEW view;
		//	ShaderInfo::PROJECTION proj;
		//	Lib_Math::VECTOR3 target;
		//	Lib_Math::VECTOR3 up;
		//	Lib_Math::VECTOR3 position;//カメラ位置
		//	float fovY;
		//	float aspect;
		//	float znear;
		//	float zfar;
		//public:
		//	const Lib_Math::VECTOR3& GetTarget() const
		//	{
		//		return target;
		//	}
		//	const Lib_Math::VECTOR3& GetUp() const
		//	{
		//		return up;
		//	}
		//	const ShaderInfo::VIEW& GetView() const
		//	{
		//		return view;
		//	}
		//	const ShaderInfo::PROJECTION& GetProj() const
		//	{
		//		return proj;
		//	}
		//	void ConvertViewProjection()
		//	{
		//		view.var.LookAt(position, target, up);
		//		proj.var.PerspectiveFov(fovY, aspect, znear, zfar);
		//	}
		//	//! set variable to tuple
		//	bool SetWVPVariable(const ShaderInfo::WORLD& w, int shaderTypeNumber)
		//	{
		//		bool f = true;
		//		f = ShaderInfo::CBufferSelector::SetVariableToTuple(w, shaderTypeNumber);
		//		f = ShaderInfo::CBufferSelector::SetVariableToTuple(this->GetView(), shaderTypeNumber);
		//		f = ShaderInfo::CBufferSelector::SetVariableToTuple(this->GetProj(), shaderTypeNumber);
		//		ShaderInfo::WVP wvp;
		//		auto x = this->GetView().var;
		//		auto y = this->GetProj().var;
		//		wvp.var = w.var * x * y;
		//		f = ShaderInfo::CBufferSelector::SetVariableToTuple(wvp, shaderTypeNumber);
		//		return f;
		//	}
		//	
		//};
		struct Camera4D
		{
		private:
			Lib_Math::MATRIX4D mat4D;
			Lib_Math::MATRIX4D view;
			Lib_Math::MATRIX4D proj;
			Lib_Math::VECTOR4 target;
			Lib_Math::VECTOR4 up;
			Lib_Math::VECTOR4 position;//カメラ位置
			float fovY;
			float aspect;
			float znear;
			float zfar;
		public:
			const Lib_Math::VECTOR4& GetTarget() const
			{
				return target;
			}
			const Lib_Math::VECTOR4& GetUp() const
			{
				return up;
			}
			const Lib_Math::MATRIX4D& GetView() const
			{
				return view;
			}
			const Lib_Math::MATRIX4D& GetProj() const
			{
				return proj;
			}
			void ConvertViewProjection()
			{
				//view.var.LookAt(position, target, up);
				//proj.var.PerspectiveFov(fovY, aspect, znear, zfar);
			}
			
		};
		//点光源
		struct POINT_LIGHT
		{

		};
		//平行光
		//こっち側でコンスタントバッファ用データへセットする
		struct DIRECTIONAL_LIGHT
		{
			ShaderInfo::LIGHT_CALOR color;
			ShaderInfo::NYUTORAL_COLOR nyutralColor;
			ShaderInfo::LIGHT_DIRECTION lightDirection;

			//! set variable to tuple
			bool SetLightVariable(int shaderTypeNumber)
			{
				bool f = true;
				f = ShaderInfo::CBufferSelector::SetVariableToTuple(color, shaderTypeNumber);
				f = ShaderInfo::CBufferSelector::SetVariableToTuple(nyutralColor, shaderTypeNumber);
				f = ShaderInfo::CBufferSelector::SetVariableToTuple(lightDirection, shaderTypeNumber);
				return f;
			};
		};
		//モデル名+シェーダー名でソート
		//! 変数に値を入れて、Update呼ぶだけ
		//! 本当の名はrenderObj
		class OBJ//継承クラスにするので
		{
		public:
			ShaderInfo::WORLD w;
			//ソート価値 modelname > shadername > rendertype
			const ShaderInfo::RENDER_TYPE renderType;//描画タイプ
			UINT shaderTypeNum;//シェーダータイプ番号
			const char* modelname;//モデル名
			Lib_Base::Utility::BitFlag usingViewFlag;//! どのビューポートを使うかのフラグ
			static constexpr int SAME_DRAW_WINDOW_MAX = 10;//! 同時ウインドウ描画最大数
			std::string windowName[SAME_DRAW_WINDOW_MAX];//! ウインドウ名
			
			OBJ(ShaderInfo::RENDER_TYPE rtype)
				: renderType(rtype)
				, shaderTypeNum(0)
				, modelname("")
			{
				for (size_t i = 0; i < SAME_DRAW_WINDOW_MAX; i++)
				{
					windowName[i] = "";
				}
			}
			virtual void Update() = 0;
			virtual ~OBJ(){}
		};
		//---------------------------------
		//renderのtype 例)描画するモデルが3Dの時は、OBJ3Dを使う
		struct OBJ3D : public OBJ
		{
			OBJ3D():OBJ(ShaderInfo::RENDER_TYPE::OBJ3D)
			{}
			virtual ~OBJ3D(){}
			
			ShaderInfo::WORLD w;
			Lib_Math::VECTOR3 position;
			Lib_Math::VECTOR3 scale;
			Lib_Math::VECTOR3 angle;
			void Update() override
			{
				w.var.RotationZXY(angle.x, angle.y, angle.z);
				w.var.Scale(scale.x, scale.y, scale.z);
				w.var.Translate(position.x, position.y, position.z);
			}
		};
		//! fbxObj仕様
		struct FBXOBJ : public OBJ
		{
			FBXOBJ() :OBJ(ShaderInfo::RENDER_TYPE::FBXOBJ)
				, motionSpeed(1.0f / 5.0f)
				, flame(0)
				, motionname("default")
			{
				w.var.Identyfy();
			}
			virtual ~FBXOBJ(){}
			ShaderInfo::WORLD w;
			Lib_Math::VECTOR3 position;
			Lib_Math::VECTOR3 scale;
			Lib_Math::VECTOR3 angle;
			ShaderInfo::BONE_TRANSFORM boneTransform;
			const char* motionname;//モーション名//default
			float flame;//モーションフレーム//そのフレームを見たいときとかに使う
			float motionSpeed;//モーション速度// 1/60fsec
			void Update() override
			{
				w.var.RotationZXY(angle.x, angle.y, angle.z);
				w.var.Scale(scale.x, scale.y, scale.z);
				w.var.Translate(position.x, position.y, position.z);
				this->Skinning();
			}
		private:
			void Skinning();
		};
		//atメッシュ
		struct OBJAT : public OBJ
		{
			OBJAT() :OBJ(ShaderInfo::RENDER_TYPE::OBJAT)
			{}
			virtual ~OBJAT() {}
			void Update() override
			{}
		};
		//vertexは自分で決める
		struct XVERTEX : public OBJ
		{
			XVERTEX() :OBJ(ShaderInfo::RENDER_TYPE::XVERTEX)
			{
				w.var.Identyfy();
				parts[0] = nullptr;
				parts[1] = nullptr;
				devideFlag = false;
			}
			//XVertex xvertex;
			Lib_Math::VECTOR3_CONST at;//飛ぶ方向
			ShaderInfo::WORLD w;
			Lib_Math::VECTOR3 position;
			Lib_Math::VECTOR3 scale;
			Lib_Math::VECTOR3 angle;
			bool devideFlag;
			bool destroyFlag;
			Lib_Math::Primitive::Plane devidePlane;
			std::array<std::unique_ptr<XVERTEX>, 2> parts;
			virtual ~XVERTEX() {}
			void Update() override
			{
				w.var.RotationZXY(angle.x, angle.y, angle.z);
				w.var.Scale(scale.x, scale.y, scale.z);
				w.var.Translate(position.x, position.y, position.z);
				if (devideFlag)
				{
					//.DevideVertex(devidePlane);
					//(外に新しいXVertexRenderのオブジェクトへ分割頂点情報を渡す)
					//xvertex.nextDevide->vertexDeviedPosition[DeviderForXVertex::UP];
					//xvertex.nextDevide->vertexDeviedPosition[DeviderForXVertex::UNDER]
				}
			}
		};
		struct LIVE2D : public OBJ
		{
			LIVE2D() :OBJ(ShaderInfo::RENDER_TYPE::LIVE2D)
			{}
			virtual ~LIVE2D() {}
			void Update() override
			{}
		};
		struct OBJ4D : public OBJ
		{
			OBJ4D() :OBJ(ShaderInfo::RENDER_TYPE::OBJ4D)
			{}
			virtual ~OBJ4D() {}
			void Update() override
			{}
		};

		class ModelManager
		{
		public:
			ModelManager() = default;
			~ModelManager() = default;

		//private:
			static void SetData(const char* modelname, LoadFbx*);
			static void SetData(const char* modelname, LoadXVertex*);
		};


		//! レンダーを使えるクラス(派生させて使う)
		class Model
		{
		public:
			Model() = default;
			virtual ~Model() {}
		protected://Adapterクラスに公開
		public:
			//#define DEFINE_SetRenderParam SetRenderParam
			virtual void SetRenderParam(Lib_3D::Lib_Render::FBXOBJ* renderParam)const = 0;
			virtual void SetRenderParam(Lib_3D::Lib_Render::OBJ3D* renderParam)const = 0;
			virtual void SetRenderParam(Lib_3D::Lib_Render::XVERTEX* renderParam)const = 0;
			virtual void SetRenderParam(Lib_3D::Lib_Render::OBJAT* renderParam)const = 0;
			virtual void SetRenderParam(Lib_3D::Lib_Render::LIVE2D* renderParam)const = 0;
			virtual void SetRenderParam(Lib_3D::Lib_Render::OBJ4D* renderParam)const = 0;
		};


		
		//! renderer基底クラス
		//! 
		class Renderer final
		{
		public:
			Renderer() = default;
			Renderer(Renderer&) = delete;
			Renderer(Renderer&&) = delete;
			~Renderer() = default;
			
			//! 半透明かどうかは考えない
			//! @param[in] isDrawTransparent 半透明なオブジェクトを描画するか?(trueで半透明なオブジェクトを描画)
			//void RenderLoop();
			static void RenderSwitch(OBJ* shadername);
			//void ModelChange();
			//static void CBufVarSetSwitch();
			//static void EasyFbxRender(FBXOBJ* obj);
			//static void EasyObj3DRender(OBJ3D* obj);
			//static void EasyXVertexRender(XVERTEX* obj);
			//! Modelクラスを使うタイプのレンダー
			//! @param[in] renderType シェーダーのレンダータイプ
			static void RenderSwitchModelUse(const Model* model, int shaderNum);
			
		private:
			static void FbxRender(FBXOBJ* obj);
			static void Obj3DRender(OBJ3D* obj);
			static void XVertexRender(XVERTEX* obj);
			static void ObjAtRender(OBJAT* obj);
			static void Live2DRender(LIVE2D* obj);
			static void Obj4DRender(OBJ4D* obj);
			//static void Render(FBXDATA* loadData);
			//static void Render(XVERTEXDATA* vertexNum);
			//static void BufferSetSwitch();
		};




		

		
	}
}
