#include "XVertexMesh.h"
#include <Lib_DirectX11/DirectX11Init.h>
#include <Lib_Math/MyMath.h>
#include <Lib_Math/PrimitiveRilation.h>
#include <Lib_Collision/CollisionFunc.h>
namespace
{
	//! @namespace at_compile_time
	//! @brief コンパイル時、処理
	namespace at_compile_time
	{
#ifdef _DEBUG
#define new  ::new(_NORMAL_BLOCK, __FILE__, __LINE__)//メモリリーク無いならなくす(newをあぶりだしたいので)
#endif // DEBUG
		//! インスタンス生成ラップテンプレート(コピペして使う)
		template<typename T>
		class  encapsulation_template
		{
		protected:
			static T* Init;
		public:
			encapsulation_template() = default;
			encapsulation_template(encapsulation_template<T>&) = delete;
			encapsulation_template(encapsulation_template<T>&&) = delete;
			virtual ~encapsulation_template() { if (Init) { delete Init; Init = nullptr; } }
		};
		//! インスタンスを生成
		class  encapsulation_TextureResourceManager1 final : encapsulation_template<Lib_3D::TextureResourceManager>
		{
		public:
			encapsulation_TextureResourceManager1() { if (!Init)Init = new Lib_3D::TextureResourceManager; }
			static Lib_3D::TextureResourceManager* const  GetInstance() { return Init; }
		};
		Lib_3D::TextureResourceManager* encapsulation_TextureResourceManager1::Init = nullptr;
		const  at_compile_time::encapsulation_TextureResourceManager1 Init{};//コンパイル時、初期化
	}//at_compile_time
	//! @namespace call_externaly
	//! @brief 外のソースコードから呼んでいるもの(このファイル専用に作ったソースではないもの)
	namespace call_externaly
	{
		using TextureResourceManagerEx = at_compile_time::encapsulation_TextureResourceManager1;
#define GetDevice Lib_3D::DirectX11InitDevice::GetDevice()
#define GetDeviceContext Lib_3D::DirectX11InitDevice::GetImidiateContext()
	}
}
namespace Lib_3D
{
	namespace Lib_Render
	{
		/*
		void Lib_3D::Lib_Render::XVertex::CreateBuffer(int NUM_VERTEX)
		{
			HRESULT hr = S_OK;
			// 頂点バッファ定義
			D3D11_BUFFER_DESC Bufferdesk;
			ZeroMemory(&Bufferdesk, sizeof(Bufferdesk));
			Bufferdesk.Usage = D3D11_USAGE_DYNAMIC;	//CPUからも使える
			Bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			Bufferdesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			Bufferdesk.MiscFlags = 0;
			Bufferdesk.StructureByteStride = 0;//float?sizeof(DirectX::XMFLOAT3)

			 // サブリソースの初期化に使用されるデータを指定します。
			D3D11_SUBRESOURCE_DATA SubResourceData;
			ZeroMemory(&SubResourceData, sizeof(SubResourceData));
			SubResourceData.SysMemPitch = 0;					//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
			SubResourceData.SysMemSlicePitch = 0;				//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。

			for (size_t i = 0; i < static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END); i++)
			{
				bool exef = true;
				ShaderInfo::VBUFFER_TYPE_INDEX typeIndex = static_cast<ShaderInfo::VBUFFER_TYPE_INDEX>(i);
				switch (typeIndex)
				{
				default:
					exef = false;
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::POSITION:
					Bufferdesk.ByteWidth = NUM_VERTEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::POSITION);
					SubResourceData.pSysMem = this->vertexPosition.data();
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::NORMAL:
					Bufferdesk.ByteWidth = NUM_VERTEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL);
					SubResourceData.pSysMem = this->vertexNormal.data();
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::TEXCOORD:
					Bufferdesk.ByteWidth = NUM_VERTEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD);
					SubResourceData.pSysMem = this->vertexTexcoord.data();
					break;
				}
				if (exef)
				{
					//バッファを再確保
					hr = GetDevice->CreateBuffer(&Bufferdesk, nullptr, p_VertexBuffer[i].ReleaseAndGetAddressOf());
					if (FAILED(hr))
					{
						assert(!"頂点バッファオブジェクトの生成ができません");
						return;
					}
				}

			}

		}

		void Lib_3D::Lib_Render::XVertex::PoligonUpdate()
		{


		}

		void RegularPorigon::ChangeShape(size_t n, const Lib_Math::VECTOR2& TEX_SCALE, const Lib_Math::VECTOR2& TEX_CENTER)
		{
			vertexNum = n + 1;
			this->vertexPosition.resize(vertexNum);
			this->vertexNormal.resize(vertexNum);
			this->vertexTexcoord.resize(vertexNum);
			//頂点位置
			{
				const float ONE_ANGLE = Lib_Math::ToRadian(360.0f / vertexNum);
				constexpr float ANG90 = Lib_Math::ToRadian(90);
				//周り
				for (size_t i = 0; i < vertexNum; i++)
				{
					//右回り
					this->vertexPosition[i].position = { cosf(-i * ONE_ANGLE + ANG90), sinf(-i * ONE_ANGLE + ANG90), 0 };//0~1の大きさ
				}
			}
			this->vertexPosition[vertexNum].position = { 0,0,0 };//真ん中頂点最後に追加
			//インデックス
			{//最後の頂点番号を除く
				indices.resize((vertexNum) * 3);
				for (size_t i2 = 0; i2 < vertexNum - 1; i2++)
				{
					indices[i2 * 3 + 0] = vertexNum;//真ん中
					indices[i2 * 3 + 1] = i2 + 0;//右回り
					indices[i2 * 3 + 2] = i2 + 1;
				}
				//最後
				indices[vertexNum - 1] = vertexNum - 1;
				indices[vertexNum - 1] = 0;//最初の頂点につなぐ
			}
			//法線
			{
				constexpr Lib_Math::VECTOR3 NORMAL = { 0.0f, 0.0f, -1.0f };
				for (size_t i = 0; i < vertexNum; i++)
				{
					this->vertexNormal[i].normal = NORMAL;
				}
			}

			//テクスチャ
			for (size_t i = 0; i < vertexNum; i++)
			{
				//constexpr Lib_Math::VECTOR2 TEX_SCALE = { 1.0f, 1.0f};//
				//constexpr Lib_Math::VECTOR2 TEX_CENTER = { 1.0f, 1.0f};//切り抜く位置
				this->vertexTexcoord[i].texcoord = { this->vertexPosition[i].position.x, this->vertexPosition[i].position.y };
				this->vertexTexcoord[i].texcoord.x *= TEX_SCALE.x;
				this->vertexTexcoord[i].texcoord.y *= TEX_SCALE.y;
				this->vertexTexcoord[i].texcoord.x += TEX_CENTER.x;
				this->vertexTexcoord[i].texcoord.y += TEX_CENTER.y;
			}
			// 頂点を動的生成
			D3D11_MAPPED_SUBRESOURCE msr;
			for (size_t i = 0; i < static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END); i++)
			{
				ShaderInfo::VBUFFER_TYPE_INDEX typeIndex = static_cast<ShaderInfo::VBUFFER_TYPE_INDEX>(i);
				switch (typeIndex)
				{
				default:
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::POSITION:
					GetDeviceContext->Map(p_VertexBuffer[i].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
					memcpy(msr.pData, vertexPosition.data(), vertexNum * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::POSITION));
					GetDeviceContext->Unmap(p_VertexBuffer[i].Get(), 0);
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::NORMAL:
					GetDeviceContext->Map(p_VertexBuffer[i].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
					memcpy(msr.pData, vertexNormal.data(), vertexNum * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL));
					GetDeviceContext->Unmap(p_VertexBuffer[i].Get(), 0);
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::TEXCOORD:
					GetDeviceContext->Map(p_VertexBuffer[i].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
					memcpy(msr.pData, vertexTexcoord.data(), vertexNum * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD));
					GetDeviceContext->Unmap(p_VertexBuffer[i].Get(), 0);
					break;
				}
			}
		}


		void RegularPorigon::RecleateRegularPoligon()
		{
			//! 頂点数に応じて情報を更新
			this->ChangeShape();
			//! バッファを再構成
			this->CreateBuffer(vertexNum);
		}

		void RegularPorigon::Render()
		{}

		void GenerateVertex2D::GenerateVertex(const Lib_Math::VECTOR3& deviedpos0, const Lib_Math::VECTOR3& deviedpos1)
		{
			vertexDeviedPosition.emplace_back(deviedpos0);
			vertexDeviedPosition.emplace_back(deviedpos1);
		}

		void GenerateVertex2D::DevideVertex(const std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION>& parentVertex, const Lib_Math::VECTOR3& deviedpos0, const Lib_Math::VECTOR3& deviedpos1)
		{
			//分割する頂点を決める
			parentVertex;
			//分割の真ん中を求める
			Lib_Math::VECTOR3 centerPos = (deviedpos0 + deviedpos1) / 2;

			//線とプリミティブの線との交差位置


			///2頂点を結んだ線を定義
			///その線に平行な平面
			///モデル頂点を平面に落とす
			///その線に垂直でcenterPosを通る線が境界線
			///2次元平面の上側か下側かを調べる (2つの軸、三角関数、位置⇒角度)
			///頂点位置がcenterPosからみてdeviedpos0側にあるなら１番近い頂点をdeviedpos0とつなげる
			Lib_Math::VECTOR3 nearPos0;//deviedpos0に１番近い近い
			Lib_Math::VECTOR3 nearPos1;//deviedpos1に１番近い近い
			//もしnearposがないなら一番近い頂点とつなぐ
			//切断位置がポリゴンより上ならリターン
			bool direction0 = true;
			//上と下に分割
			for (size_t i = 0; i < parentVertex.size(); i++)//頂点を一つずつ調べる
			{
				//どちらがわか
				if (direction0)
				{
					//分割頂点のy座標より上
					if (deviedpos0.y < parentVertex[i].position.y)
						vertexDeviedPosition[deviedTimes][UP].emplace_back(parentVertex[i]);
					else
						vertexDeviedPosition[deviedTimes][UNDER].emplace_back(parentVertex[i]);
				}
				else
				{
					//分割頂点のy座標より上
					if (deviedpos1.y < parentVertex[i].position.y)
						vertexDeviedPosition[deviedTimes][UP].emplace_back(parentVertex[i]);
					else
						vertexDeviedPosition[deviedTimes][UNDER].emplace_back(parentVertex[i]);
				}
			}
			//切断頂点でくっつける
		}

		std::array<std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION>, 2>
			GenerateVertex2D::DevideVertex_(const std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION>& parentVertex, const Lib_Math::Primitive::Plane& devidePlane)
		{
			std::vector<Lib_Math::Primitive::Line> parentLine;
			std::vector <UINT> parentIndices;
			Lib_Math::Primitive::Point DeviedPoint[10];//分割した座標
			int deviedLineIndex[10] = {};//分割される辺のインデックス
			int deviedTimes = 0;//分割された辺の数
			for (size_t i = 0, size = parentLine.size(); i < size; i++)
			{
				//分割した座標
				auto point = Lib_Math::Primitive::CaluculateInterSectionPlaneWithLine(devidePlane, parentLine[i]);
				if (point == Lib_Math::Primitive::Point::NanPoint())//分割されていない
					continue;
				//分割された
				DeviedPoint[deviedTimes] = point;
				deviedLineIndex[deviedTimes] = i;
				deviedTimes++;
			}
			if (deviedTimes < 2)//2つ以上線が切れてないなら、分割しない
				return;
			for (size_t i = 0; i < deviedTimes; i++)
			{
				parentLine[deviedLineIndex[i]];//分割された線
				DeviedPoint[i];//線を切った頂点

			}

			// 切り口の中心の頂点
			DeviedPoint[0];
			DeviedPoint[1];//の中心点をつなぐ用の頂点にする


			//親の頂点を分割平面と同じx,z座標を比較
			std::array<std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION>, 2> separateVertex;
			std::array<std::vector <UINT>, 2> Indices;
			Indices[UP] = parentIndices;
			Indices[UNDER] = parentIndices;


			separateVertex[UP].reserve(parentVertex.size());
			separateVertex[UNDER].reserve(parentVertex.size());



			for (size_t i = 0, size = parentVertex.size(); i < size; i++)
			{
				auto index0 = parentIndices[i * 3 + 0];//元のインデックス
				auto index1 = parentIndices[i * 3 + 1];
				auto index2 = parentIndices[i * 3 + 2];
				//その頂点xzの時の、平面のy
				auto planeY = Lib_Math::Primitive::PlanePosAtCertainXZ(devidePlane, parentVertex[i].position.x, parentVertex[i].position.z);
				if (planeY == Lib_Math::Primitive::Point::NanPoint()().y)
					continue;
				Indices[UP].emplace_back(index0);

				if (planeY > parentVertex[i].position.y)//上に切り離された頂点
				{
					separateVertex[UP].emplace_back(parentVertex[i]);
					Indices[UP].emplace_back(index0);
					Indices[UP].emplace_back(index1);
					Indices[UP].emplace_back(index2);
					//それが切り離された頂点ならインデックス更新
					for (size_t i = 0; i < deviedTimes; i++)
					{
						if (parentVertex[i].position != parentLine[deviedLineIndex[i]].p.p &&
							parentVertex[i].position != parentLine[deviedLineIndex[i]].v)//それが切り離されたか？
							continue;
						//きりはなされた線の頂点
						DeviedPoint[i];//線を切った頂点
					}
				}
				else//下に切り離された頂点
				{
					separateVertex[UNDER].emplace_back(parentVertex[i]);
					Indices[UNDER].emplace_back(index0);
					Indices[UNDER].emplace_back(index1);
					Indices[UNDER].emplace_back(index2);
				}
			}
			return separateVertex;//次の関数でこの頂点を使ってバッファを作る
		}
		*/
		


/*
void XVertex::DevideVertex(Lib_Math::Primitive::Plane& deviedPlane)
		{
			if(nextDevide)
				nextDevide = new DeviderForXVertex();
			if (!nextDevide->DevideVertex(*this, deviedPlane))
			{
				delete nextDevide;
				nextDevide = nullptr;
			}
		}
*/

		
		/*
		void XVertex::SetBuffer()
		{
			std::vector<UINT> indices;
			indices.resize(this->vertecies.poligons.size() * 3);
			for (size_t i = 0; i < this->vertecies.poligons.size(); i++)
			{
				indices[i * 3 + 0] = this->vertecies.poligons[i].indicies[0];
				indices[i * 3 + 1] = this->vertecies.poligons[i].indicies[1];//右回り
				indices[i * 3 + 2] = this->vertecies.centerVertex.id;//真ん中
			}
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION> vertexPosition;
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL> vertexNormal;
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD> vertexTexcoord;
			//id順にソート
			//頂点をインデックス順に入れる
			for (size_t i = 0; i < this->vertecies.vertexNum; i++)
			{
				for (size_t i2 = 0; i2 < this->vertecies.poligons.size(); i2++)
				{
					if (i == this->vertecies.poligons[i2].linepos[0].id)//その順番の時の頂点を入れる
					{
						vertexPosition[i] = this->vertecies.poligons[i2].linepos[0].position;
						vertexNormal[i] = this->vertecies.poligons[i2].linepos[0].normal;
						vertexTexcoord[i] = this->vertecies.poligons[i2].linepos[0].texcoord;
						break;
					}
					else if (i == this->vertecies.poligons[i2].linepos[1].id)
					{
						vertexPosition[i] = this->vertecies.poligons[i2].linepos[1].position;
						vertexNormal[i] = this->vertecies.poligons[i2].linepos[1].normal;
						vertexTexcoord[i] = this->vertecies.poligons[i2].linepos[1].texcoord;
						break;
					}
				}
			}
			buffer.CreateBuffer(&vertexPosition, &vertexNormal, &vertexTexcoord, &indices);
		}

		void XVertex::UpdateBuffer()
		{
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION> vertexPosition;
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL> vertexNormal;
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD> vertexTexcoord;
			//id順にソート
			//頂点をインデックス順に入れる
			for (size_t i = 0; i < this->vertecies.vertexNum; i++)
			{
				for (size_t i2 = 0; i2 < this->vertecies.poligons.size(); i2++)
				{
					if (i == this->vertecies.poligons[i2].linepos[0].id)//その順番の時の頂点を入れる
					{
						vertexPosition[i] = this->vertecies.poligons[i2].linepos[0].position;
						vertexNormal[i] = this->vertecies.poligons[i2].linepos[0].normal;
						vertexTexcoord[i] = this->vertecies.poligons[i2].linepos[0].texcoord;
						break;
					}
					else if (i == this->vertecies.poligons[i2].linepos[1].id)
					{
						vertexPosition[i] = this->vertecies.poligons[i2].linepos[1].position;
						vertexNormal[i] = this->vertecies.poligons[i2].linepos[1].normal;
						vertexTexcoord[i] = this->vertecies.poligons[i2].linepos[1].texcoord;
						break;
					}
				}
			}
			buffer.DynamixVertexUpdate(&vertexPosition, &vertexNormal, &vertexTexcoord);
		}
		*/
		
/*
bool DeviderForXVertex::DevideVertex(const XVertex& XVertex, Lib_Math::Primitive::Plane& devidePlane)
		{
			Index_Vertex DeviedPoint[10];//分割した座標
			int deviedTimes = 0;//分割された辺の数
			Lib_Math::Primitive::PrimitiveVariant plane = devidePlane;

			UINT indexup = 0;
			UINT indexunder = 0;

			auto& poligonUp = vertexDeviedPosition[UP].vertecies.poligons;//ポリゴン
			auto& poligonUnder = vertexDeviedPosition[UNDER].vertecies.poligons;
			poligonUp.reserve(poligons.size());
			poligonUnder.reserve(poligons.size());

			//辺と平面の交点を求める(ポリゴンの数分)
			for (size_t i = 0, size = poligons.size(); i < size; i++)
			{
				auto& parentPorigon = poligons[i];

				//分割されて上かどうか
				//0番目(もう一方の頂点は同じポリゴンなので０番目一つが分かれば、頂点がどの空間にあるかわかる)
				float planeY = Lib_Math::Primitive::PlanePosAtCertainXZ(devidePlane,
					poligons[i].linepos[0].position.position.x,
					poligons[i].linepos[0].position.position.z);
				if (planeY == Lib_Math::Primitive::Point::NanPoint()().y)
					continue;

				Lib_Math::Primitive::Segment side{
			Lib_Math::Primitive::Point(poligons[i].linepos[0].position.position) ,
			Lib_Math::Primitive::Point(poligons[i].linepos[1].position.position)
				};//辺
				Lib_Math::Primitive::PrimitiveVariant seg = side;

				//このポリゴンの辺と平面が交差していないなら
				if (!Lib_Collition::CollisionSegmentPlane(seg, plane))
				{
					//平面より下なら
					bool isUpSeparate = true;
					if (planeY < poligons[i].linepos[0].position.position.y);
					isUpSeparate = false;
					TriangleList newPoligon;
					//そのままのポリゴンを入れる
					if (isUpSeparate)
					{
						// インデックスを更新
						newPoligon.indicies[0] = indexup;
						newPoligon.indicies[1] = indexup + 1;
						newPoligon.linepos[0] = XVertex.vertecies.poligons[i].linepos[0];
						newPoligon.linepos[1] = XVertex.vertecies.poligons[i].linepos[1];
						poligonUp.emplace_back(newPoligon);
						indexup++;
					}
					else
					{
						// インデックスを更新
						newPoligon.indicies[0] = indexunder;
						newPoligon.indicies[1] = indexunder + 1;
						newPoligon.linepos[0] = XVertex.vertecies.poligons[i].linepos[0];
						newPoligon.linepos[1] = XVertex.vertecies.poligons[i].linepos[1];
						poligonUnder.emplace_back(newPoligon);
						indexunder++;
					}
					continue;
				}
				//線分と平面が交差しているなら、交差点を調べる
				//分割した座標
				auto point = Lib_Math::Primitive::CaluculateInterSectionPlaneWithLine(devidePlane, std::get<Lib_Math::Primitive::Segment>(seg).ThisLine());
				if (point == Lib_Math::Primitive::Point::NanPoint())//分割されていない
					continue;
				//分割されるポリゴンなら-------------------
				//分割する頂点情報
				DeviedPoint[deviedTimes].position.position = point.p;//ここはおそらく二つになるだろう
				DeviedPoint[deviedTimes].texcoord = {};
				DeviedPoint[deviedTimes].normal = {};


				//平面より下なら
				bool isUpSeparate0 = true;//0番目
				if (planeY < XVertex.vertecies.poligons[i].linepos[0].position.position.y);
					isUpSeparate0 = false;
				///bool isUpSeparate1 = !isUpSeparate0;//1番目の頂点は0番目の逆

				Index_Vertex upPos{};
				UINT upIndex = 0;
				Index_Vertex downPos{};
				UINT downIndex = 0;
				if (isUpSeparate0)//0番目の頂点が上の空間
				{
					upPos = parentPorigon.linepos[0];
					upIndex = parentPorigon.indicies[0];
					downPos = parentPorigon.linepos[1];
					downIndex = parentPorigon.indicies[1];
				}
				else////1番目の頂点が上の空間
				{
					upPos = parentPorigon.linepos[1];
					upIndex = parentPorigon.indicies[1];
					downPos = parentPorigon.linepos[0];
					downIndex = parentPorigon.indicies[0];
				}
				//分割されたところに新しく上と下で2つ三角形を作る(既存のポリゴンは入れない)
				{
					TriangleList newPoligon;
					newPoligon.indicies[0] = upIndex;//上空間頂点(既存のポリゴンのインデックス)
					newPoligon.linepos[0] = upPos;
					if (isUpSeparate0)//0番目の頂点が上の空間
						DeviedPoint[deviedTimes].id = upIndex + 1;
					else
						DeviedPoint[deviedTimes].id = upIndex - 1;
					newPoligon.indicies[1] = DeviedPoint[deviedTimes].id;//分割頂点(空いたインデックス　この頂点はこのポリゴンですか使われない)
					newPoligon.linepos[1] = DeviedPoint[deviedTimes];
					//追加
					poligonUp.emplace_back(newPoligon);
					indexup += 2;
				}
				{
					TriangleList newPoligonUnder;
					newPoligonUnder.indicies[0] = downIndex;//下空間頂点
					newPoligonUnder.linepos[0] = downPos;
					if (isUpSeparate0)//0番目の頂点が上の空間
						DeviedPoint[deviedTimes].id = downIndex - 1;
					else
						DeviedPoint[deviedTimes].id = downIndex + 1;
					newPoligonUnder.indicies[1] = DeviedPoint[deviedTimes].id;//分割頂点
					newPoligonUnder.linepos[1] = DeviedPoint[deviedTimes];
					//追加
					poligonUnder.emplace_back(newPoligonUnder);
					indexunder += 2;
				}
				deviedTimes++;
			}
			if (deviedTimes < 2)//2つ以上線が切れてないなら、分割しない
				return false;
			else
			{
				//切り口に中心点を追加
				this->vertexDeviedPosition[UP].centerpos.id = poligonUp.size();
				this->vertexDeviedPosition[UP].centerpos.normal;
				this->vertexDeviedPosition[UP].centerpos.texcoord;
				this->vertexDeviedPosition[UP].centerpos.position.position = (DeviedPoint[0].position.position - DeviedPoint[1].position.position) / 2;
				this->vertexDeviedPosition[UNDER].centerpos.id = poligonUnder.size();
				this->vertexDeviedPosition[UNDER].centerpos.normal;
				this->vertexDeviedPosition[UNDER].centerpos.texcoord;
				this->vertexDeviedPosition[UNDER].centerpos.position.position = (DeviedPoint[0].position.position - DeviedPoint[1].position.position) / 2;
				return true;
			}
		}

		void RegularPoligonShape::ChangeShape(const size_t sidesnum)
		{
			vertexNum = sidesnum + 1;

			std::vector<Index_Vertex> XVertex;
			XVertex.resize(vertexNum);
			poligons.resize(vertexNum);
			//頂点位置
			{
				const float ONE_ANGLE = Lib_Math::ToRadian(360.0f / vertexNum);
				constexpr float ANG90 = Lib_Math::ToRadian(90);
				Index_Vertex vertex;
				//周り
				for (size_t i = 0; i < sidesnum; i++)
				{
					//右回り
					XVertex[i].position.position =
						VECTOR3_CONST{ cosf(-float(i) * ONE_ANGLE + ANG90), sinf(-float(i) * ONE_ANGLE + ANG90), 0.f };//0~1の大きさ
					XVertex[i].id = i;
				}
				XVertex[vertexNum].position.position = VECTOR3_CONST{ 0,0,0 };//真ん中頂点最後に追加

				for (size_t i = 0; i < sidesnum - 1; i++)
				{
					poligons[i].indicies[0] = i;
					poligons[i].indicies[1] = i + 1;
					poligons[i].linepos[0].position = XVertex[i].position;
					poligons[i].linepos[1].position = XVertex[i + 1].position;
				}
				this->centerVertex.id = vertexNum;//中央インデックス

				std::vector<UINT> indices;
				//インデックス
				{//最後の頂点番号を除く
					indices.resize((vertexNum) * 3);
					for (size_t i = 0; i < sidesnum; i++)
					{
						indices[i * 3 + 0] = poligons[i].indicies[0];//右回り
						indices[i * 3 + 1] = poligons[i].indicies[1];
						indices[i * 3 + 2] = this->centerVertex.id;//真ん中
					}
					//最後
					indices[vertexNum - 1] = vertexNum - 1;
					indices[vertexNum - 1] = 0;//最初の頂点につなぐ
				}

				//法線
				{
					constexpr Lib_Math::VECTOR3_CONST NORMAL = { 0.0f, 0.0f, -1.0f };
					for (size_t i = 0; i < sidesnum; i++)
					{
						XVertex[i].normal.normal = NORMAL;
					}
				}

				//テクスチャ
				for (size_t i = 0; i < vertexNum; i++)
				{
					constexpr Lib_Math::VECTOR2 TEX_SCALE = { 1.0f, 1.0f};//
					constexpr Lib_Math::VECTOR2 TEX_CENTER = { 1.0f, 1.0f};//切り抜く位置
					//XVertex[i].texcoord.texcoord = ;
					this->poligons[i].linepos[0].texcoord.texcoord = { this->poligons[i].linepos[0].position.position.x, this->poligons[i].linepos[0].position.position.y };
					this->poligons[i].linepos[1].texcoord.texcoord = { this->poligons[i].linepos[1].position.position.x, this->poligons[i].linepos[1].position.position.y };
					this->poligons[i].linepos[0].texcoord.texcoord.x *= TEX_SCALE.x;
					this->poligons[i].linepos[0].texcoord.texcoord.y *= TEX_SCALE.y;
					this->poligons[i].linepos[1].texcoord.texcoord.x *= TEX_SCALE.x;
					this->poligons[i].linepos[1].texcoord.texcoord.y *= TEX_SCALE.y;
					this->poligons[i].linepos[0].texcoord.texcoord.x += TEX_CENTER.x;
					this->poligons[i].linepos[0].texcoord.texcoord.y += TEX_CENTER.y;
					this->poligons[i].linepos[1].texcoord.texcoord.x += TEX_CENTER.x;
					this->poligons[i].linepos[1].texcoord.texcoord.y += TEX_CENTER.y;
				}
			}
		}

*/

		
		/*
		void XVertexBuffer::CreateBuffer(
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION>* position,
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL>* normal,
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD>* texcoord,
			std::vector<UINT>* indices)
		{
			const auto NUM_VERTEX = position->size();//頂点数
			HRESULT hr = S_OK;
			// 頂点バッファ定義
			D3D11_BUFFER_DESC Bufferdesk;
			ZeroMemory(&Bufferdesk, sizeof(Bufferdesk));
			Bufferdesk.Usage = D3D11_USAGE_DYNAMIC;	//CPUからも使える
			Bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			Bufferdesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			Bufferdesk.MiscFlags = 0;
			Bufferdesk.StructureByteStride = 0;//float?sizeof(DirectX::XMFLOAT3)

			 // サブリソースの初期化に使用されるデータを指定します。
			D3D11_SUBRESOURCE_DATA SubResourceData;
			ZeroMemory(&SubResourceData, sizeof(SubResourceData));
			SubResourceData.SysMemPitch = 0;					//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
			SubResourceData.SysMemSlicePitch = 0;				//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。

			for (size_t i = 0; i < static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END); i++)
			{
				bool exef = true;
				ShaderInfo::VBUFFER_TYPE_INDEX typeIndex = static_cast<ShaderInfo::VBUFFER_TYPE_INDEX>(i);
				switch (typeIndex)
				{
				default:
					exef = false;
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::POSITION:
					Bufferdesk.ByteWidth = NUM_VERTEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::POSITION);
					SubResourceData.pSysMem = position->data();
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::NORMAL:
					Bufferdesk.ByteWidth = NUM_VERTEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL);
					SubResourceData.pSysMem = normal->data();
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::TEXCOORD:
					Bufferdesk.ByteWidth = NUM_VERTEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD);
					SubResourceData.pSysMem = texcoord->data();
					break;
				}
				if (exef)
				{
					//バッファを再確保
					hr = GetDevice->CreateBuffer(&Bufferdesk, nullptr, p_VertexBuffer[i].ReleaseAndGetAddressOf());
					if (FAILED(hr))
					{
						assert(!"頂点バッファオブジェクトの生成ができません");
						return;
					}
				}
			}

			///////////////////////////////////////////////////
		// インデックスバッファオブジェクトの生成
		///////////////////////////////////////////////////
			hr = S_OK;
			const auto NUM_INDEX = indices->size();

			// インデックスバッファの定義
			D3D11_BUFFER_DESC IndexBufferDesc;
			ZeroMemory(&IndexBufferDesc, sizeof(IndexBufferDesc));
			IndexBufferDesc.ByteWidth = NUM_INDEX * sizeof(UINT);
			IndexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
			IndexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			IndexBufferDesc.CPUAccessFlags = 0;
			IndexBufferDesc.MiscFlags = 0;
			IndexBufferDesc.StructureByteStride = 0;
			//インデックスの数を補完
			//numIndices = NUM_INDEX;

			// インデックス・バッファのサブリソースの定義
			D3D11_SUBRESOURCE_DATA IndexSubResource;
			ZeroMemory(&IndexSubResource, sizeof(IndexSubResource));
			IndexSubResource.pSysMem = indices;
			IndexSubResource.SysMemPitch = 0;
			IndexSubResource.SysMemSlicePitch = 0;

			// インデックス・バッファの作成
			hr = GetDevice->CreateBuffer(&IndexBufferDesc, &IndexSubResource, this->p_IndexBuffer.GetAddressOf());
			if (FAILED(hr))
			{
				assert(!"インデックス・バッファの作成ができません");
				return;
			}
		}

		void XVertexBuffer::DynamixVertexUpdate(
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION>* position,
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL>* normal,
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD>* texcoord
		)
		{
			const auto vertexNum = position->size();
			// 頂点を動的生成
			D3D11_MAPPED_SUBRESOURCE msr;
			for (size_t i = 0; i < static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END); i++)
			{
				ShaderInfo::VBUFFER_TYPE_INDEX typeIndex = static_cast<ShaderInfo::VBUFFER_TYPE_INDEX>(i);
				switch (typeIndex)
				{
				default:
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::POSITION:
					GetDeviceContext->Map(p_VertexBuffer[i].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
					memcpy(msr.pData, position->data(), vertexNum * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::POSITION));
					GetDeviceContext->Unmap(p_VertexBuffer[i].Get(), 0);
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::NORMAL:
					GetDeviceContext->Map(p_VertexBuffer[i].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
					memcpy(msr.pData, normal->data(), vertexNum * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL));
					GetDeviceContext->Unmap(p_VertexBuffer[i].Get(), 0);
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::TEXCOORD:
					GetDeviceContext->Map(p_VertexBuffer[i].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
					memcpy(msr.pData, texcoord->data(), vertexNum * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD));
					GetDeviceContext->Unmap(p_VertexBuffer[i].Get(), 0);
					break;
				}
			}
		}
		*/
		

		void LoadXVertex::Load(const char* texturename, const Lib_Math::VECTOR2& TEX_SCALE, const Lib_Math::VECTOR2& TEX_CENTER, const size_t sidesnum)
		{
			//テクスチャをロード
			this->texturename = texturename;
			this->texture = call_externaly::TextureResourceManagerEx::GetInstance()->LoadTexture(texturename);
			this->texScale = TEX_SCALE;
			this->texCenterPos = TEX_CENTER;
			this->nowSidesNum = sidesnum;//辺の数
			this->ChangeShape(sidesnum);
		}

		void LoadXVertex::ChangeShape(const size_t sidesnum)
		{
			this->nowSidesNum = sidesnum;//辺の数
			vertexNum = sidesnum + 1;

			std::vector<Index_Vertex> XVertex;
			XVertex.resize(vertexNum);
			poligons.resize(sidesnum);
			//頂点位置
			{
				const float ONE_ANGLE = Lib_Math::ToRadian(360.0f / sidesnum);
				//constexpr float ANG90 = Lib_Math::ToRadian(90);
				Index_Vertex vertex;
				//周り
				for (int i = 0; i < (int)sidesnum; i++)
				{
					float cos = cosf(-i * ONE_ANGLE + ONE_ANGLE/2);
					float sin = sinf(-i * ONE_ANGLE + ONE_ANGLE / 2);

					VECTOR3_CONST pos = { cos, sin, 0.f };//0~1の大きさ
					
																														  //右回り
					XVertex[i].position.position = pos;//0~1の大きさ
					XVertex[i].id = i;
				}
				XVertex[vertexNum - 1].position.position = VECTOR3_CONST{ 0,0,0 };//真ん中頂点最後に追加

				

				//法線
				{
					constexpr Lib_Math::VECTOR3_CONST NORMAL = { 0.0f, 0.0f, -1.0f };
					for (size_t i = 0; i < sidesnum; i++)
					{
						XVertex[i].normal.normal = NORMAL;
					}
				}

			
				//頂点情報をポリゴン情報群にいれる
				for (size_t i = 0; i < sidesnum - 1; i++)
				{
					poligons[i].indicies[0] = i;
					poligons[i].indicies[1] = i + 1;
					poligons[i].linepos[0].position = XVertex[i].position;
					poligons[i].linepos[1].position = XVertex[i + 1].position;
					poligons[i].linepos[0].id = XVertex[i].id;
					poligons[i].linepos[1].id = XVertex[i + 1].id;
					poligons[i].linepos[0].normal = XVertex[i].normal;
					poligons[i].linepos[1].normal = XVertex[i + 1].normal;
				}
				poligons[sidesnum - 1].indicies[0] = sidesnum - 1;
				poligons[sidesnum - 1].indicies[1] = 0;
				poligons[sidesnum - 1].linepos[0].position = XVertex[sidesnum - 1].position;
				poligons[sidesnum - 1].linepos[1].position = XVertex[0].position;
				poligons[sidesnum - 1].linepos[0].id = XVertex[sidesnum - 1].id;
				poligons[sidesnum - 1].linepos[1].id = XVertex[0].id;
				poligons[sidesnum - 1].linepos[0].normal = XVertex[sidesnum - 1].normal;
				poligons[sidesnum - 1].linepos[1].normal = XVertex[0].normal;

				this->centerVertex.position.position = XVertex[vertexNum - 1].position.position;
				this->centerVertex.id = sidesnum;//中央インデックス

				//テクスチャ
				for (size_t i = 0; i < sidesnum; i++)
				{
					//constexpr Lib_Math::VECTOR2 TEX_SCALE = { 1.0f, 1.0f };//
					//constexpr Lib_Math::VECTOR2 TEX_CENTER = { 1.0f, 1.0f };//切り抜く位置
					//XVertex[i].texcoord.texcoord = ;
					this->poligons[i].linepos[0].texcoord.texcoord = { this->poligons[i].linepos[0].position.position.x, this->poligons[i].linepos[0].position.position.y };
					this->poligons[i].linepos[1].texcoord.texcoord = { this->poligons[i].linepos[1].position.position.x, this->poligons[i].linepos[1].position.position.y };
					this->poligons[i].linepos[0].texcoord.texcoord.x *= texScale.x;
					this->poligons[i].linepos[0].texcoord.texcoord.y *= texScale.y;
					this->poligons[i].linepos[1].texcoord.texcoord.x *= texScale.x;
					this->poligons[i].linepos[1].texcoord.texcoord.y *= texScale.y;
					this->poligons[i].linepos[0].texcoord.texcoord.x += texCenterPos.x;
					this->poligons[i].linepos[0].texcoord.texcoord.y += texCenterPos.y;
					this->poligons[i].linepos[1].texcoord.texcoord.x += texCenterPos.x;
					this->poligons[i].linepos[1].texcoord.texcoord.y += texCenterPos.y;
				}
			}

			this->CreateBuffer();
		}

		bool LoadXVertex::DevideVertex(Lib_Math::Primitive::Plane& devidePlane)
		{
			//std::array<LoadXVertex, 2> load;
			load[UP] = std::make_unique<LoadXVertex>();
			load[UNDER] = std::make_unique<LoadXVertex>();
			load[UP]->Load(texturename, this->texScale, this->texCenterPos, this->nowSidesNum);
			load[UNDER]->Load(texturename, this->texScale, this->texCenterPos, this->nowSidesNum);
			load[UP]->poligons.clear();
			load[UNDER]->poligons.clear();

			Index_Vertex DeviedPoint[10];//分割した座標
			int deviedTimes = 0;//分割された辺の数
			//int devideI[10];//（devideTimeの時に）分割したＩ(I以下の時、のIDの頂点ならIndexに、その時のdevideTimesをプラス)
			//int devideThenNextId[10];//切った時の次のID(upの時はdown、downの時はupのid)
			Lib_Math::Primitive::PrimitiveVariant plane = devidePlane;

			UINT indexup = 0;
			UINT indexunder = 0;

			auto& poligonUp = load[UP]->poligons;//ポリゴン
			auto& poligonUnder = load[UNDER]->poligons;
			poligonUp.reserve(poligons.size());
			poligonUnder.reserve(poligons.size());
			//辺と平面の交点を求める(ポリゴンの数分)
			for (size_t i = 0, size = this->nowSidesNum; i < size; i++)
			{
				const auto& parentPorigon = this->poligons[i];

				//分割されて上かどうか
				//0番目(もう一方の頂点は同じポリゴンなので０番目一つが分かれば、頂点がどの空間にあるかわかる)
				float planeY = Lib_Math::Primitive::PlanePosAtCertainXZ(devidePlane,
					parentPorigon.linepos[0].position.position.x,
					parentPorigon.linepos[0].position.position.z);
				if (std::isnan(planeY))
					continue;

				Lib_Math::Primitive::Segment side{
			Lib_Math::Primitive::Point(parentPorigon.linepos[0].position.position) ,
			Lib_Math::Primitive::Point(parentPorigon.linepos[1].position.position)
				};//辺	
				Lib_Math::Primitive::PrimitiveVariant seg = side;

				//このポリゴンの辺と平面が交差していないなら
				if (!Lib_Collition::CollisionSegmentPlane(seg, plane))
				{
					//平面より下なら
					bool isUpSeparate = true;
					if (planeY > parentPorigon.linepos[0].position.position.y)
						isUpSeparate = false;
					TriangleList newPoligon;


					//そのままのポリゴンを入れる
					if (isUpSeparate)
					{
						// インデックスを更新
						newPoligon.indicies[0] = indexup;
						newPoligon.indicies[1] = indexup + 1;
						newPoligon.linepos[0] = parentPorigon.linepos[0];
						newPoligon.linepos[0].id = indexup;
						newPoligon.linepos[1] = parentPorigon.linepos[1];
						newPoligon.linepos[1].id = indexup + 1;
						poligonUp.emplace_back(newPoligon);
						indexup++;
					}
					else
					{
						// インデックスを更新
						newPoligon.indicies[0] = indexunder;
						newPoligon.indicies[1] = indexunder + 1;
						newPoligon.linepos[0] = parentPorigon.linepos[0];
						newPoligon.linepos[0].id = indexunder;
						newPoligon.linepos[1] = parentPorigon.linepos[1];
						newPoligon.linepos[1].id = indexunder + 1;
						poligonUnder.emplace_back(newPoligon);
						indexunder++;
					}
					continue;
				}
				//線分と平面が交差しているなら、交差点を調べる
				//分割した座標
				auto point = Lib_Math::Primitive::CaluculateInterSectionPlaneWithLine(devidePlane, std::get<Lib_Math::Primitive::Segment>(seg).ThisLine());
				if (std::isnan(point.p.x) )//分割されていない
					continue;
				//分割されるポリゴンなら-------------------
				//分割する頂点情報
				DeviedPoint[deviedTimes].position.position = point.p;//ここはおそらく二つになるだろう
				DeviedPoint[deviedTimes].normal = {};
				load[UP]->centerVertex.texcoord.texcoord.x = DeviedPoint[deviedTimes].position.position.x;
				load[UP]->centerVertex.texcoord.texcoord.y = DeviedPoint[deviedTimes].position.position.y;
				load[UP]->centerVertex.texcoord.texcoord.x *= texScale.x;
				load[UP]->centerVertex.texcoord.texcoord.y *= texScale.y;
				load[UP]->centerVertex.texcoord.texcoord.y += texCenterPos.x;
				load[UP]->centerVertex.texcoord.texcoord.y += texCenterPos.y;


				//平面より下なら
				bool isUpSeparate0 = true;//0番目
				if (planeY > parentPorigon.linepos[0].position.position.y)
					isUpSeparate0 = false;
				///bool isUpSeparate1 = !isUpSeparate0;//1番目の頂点は0番目の逆

				Index_Vertex upPos{};
				UINT upIndex = 0;
				Index_Vertex downPos{};
				UINT downIndex = 0;
				
				//分割されたところに新しく上と下で2つ三角形を作る(既存のポリゴンは入れない)

				if (isUpSeparate0)//0番目の頂点が上の空間
				{
					upPos = parentPorigon.linepos[0];//0番目頂点
					upPos.id = indexup;
					upIndex = upPos.id;

					DeviedPoint[deviedTimes].id = indexup + 1;
					downPos = DeviedPoint[deviedTimes];//1番目頂点
					downIndex = DeviedPoint[deviedTimes].id;

					TriangleList newPoligon;
					newPoligon.indicies[0] = upIndex;//上空間頂点(既存のポリゴンのインデックス)
					newPoligon.linepos[0] = upPos;
					newPoligon.indicies[1] = downIndex;//分割頂点(空いたインデックス　この頂点はこのポリゴンですか使われない)
					newPoligon.linepos[1] = downPos;
					//追加
					poligonUp.emplace_back(newPoligon);
					indexup++;
					indexup++;
				}
				else////1番目の頂点が上の空間
				{
					upPos = parentPorigon.linepos[0];//0番目頂点
					upPos.id = indexunder;
					upIndex = upPos.id;

					DeviedPoint[deviedTimes].id = indexunder + 1;
					downPos = DeviedPoint[deviedTimes];//1番目頂点
					downIndex = DeviedPoint[deviedTimes].id;

					TriangleList newPoligonUnder;
					newPoligonUnder.indicies[0] = upIndex;//下空間頂点
					newPoligonUnder.linepos[0] = upPos;
					newPoligonUnder.indicies[1] = downIndex;//分割頂点
					newPoligonUnder.linepos[1] = downPos;
					//追加
					poligonUnder.emplace_back(newPoligonUnder);
					indexunder++;
					indexunder++;
				}

					
				
				//次に作るポリゴン
				
				if (isUpSeparate0)//0番目の頂点が上の空間
				{
					DeviedPoint[deviedTimes].id = indexunder;
					upPos = DeviedPoint[deviedTimes];//0番目頂点
					upIndex = DeviedPoint[deviedTimes].id;

					downPos = parentPorigon.linepos[1];//1番目頂点
					downPos.id = indexunder + 1;
					downIndex = downPos.id;

					TriangleList newPoligonUnder;
					newPoligonUnder.indicies[0] = upIndex;//下空間頂点
					newPoligonUnder.linepos[0] = upPos;
					newPoligonUnder.indicies[1] = downIndex;//分割頂点
					newPoligonUnder.linepos[1] = downPos;
					//追加
					poligonUnder.emplace_back(newPoligonUnder);
					indexunder++;
				}
				else////1番目の頂点が上の空間
				{
					DeviedPoint[deviedTimes].id = indexup;
					upPos = DeviedPoint[deviedTimes];//0番目頂点
					upIndex = DeviedPoint[deviedTimes].id;

					downPos = parentPorigon.linepos[1];//1番目頂点
					downPos.id = indexup + 1;
					downIndex = downPos.id;

					TriangleList newPoligon;
					newPoligon.indicies[0] = upIndex;//上空間頂点(既存のポリゴンのインデックス)
					newPoligon.linepos[0] = upPos;
					newPoligon.indicies[1] = downIndex;//分割頂点(空いたインデックス　この頂点はこのポリゴンですか使われない)
					newPoligon.linepos[1] = downPos;
					//追加
					poligonUp.emplace_back(newPoligon);
					indexup++;
				}
				deviedTimes++;
			}

			

			if (deviedTimes < 2)//2つ以上線が切れてないなら、分割しない
			{
				load[UP].reset(nullptr);
				load[UNDER].reset(nullptr);
				return false;
			}
			else
			{
				//切り口に中心点を追加
				load[UP]->centerVertex.id = indexup;
				load[UP]->centerVertex.normal = {};
				load[UP]->centerVertex.position.position = (DeviedPoint[0].position.position + DeviedPoint[1].position.position) / 2;
				load[UP]->centerVertex.texcoord.texcoord.x = load[UP]->centerVertex.position.position.x;
				load[UP]->centerVertex.texcoord.texcoord.y = load[UP]->centerVertex.position.position.y;
				load[UP]->centerVertex.texcoord.texcoord.x *= texScale.x;
				load[UP]->centerVertex.texcoord.texcoord.y *= texScale.y;
				load[UP]->centerVertex.texcoord.texcoord.y += texCenterPos.x;
				load[UP]->centerVertex.texcoord.texcoord.y += texCenterPos.y;

				load[UNDER]->centerVertex.id = indexunder;
				load[UNDER]->centerVertex.normal = {};
				load[UNDER]->centerVertex.position.position = (DeviedPoint[0].position.position + DeviedPoint[1].position.position) / 2;
				load[UNDER]->centerVertex.texcoord.texcoord.x = load[UNDER]->centerVertex.position.position.x;
				load[UNDER]->centerVertex.texcoord.texcoord.y = load[UNDER]->centerVertex.position.position.y;
				load[UNDER]->centerVertex.texcoord.texcoord.x *= texScale.x;
				load[UNDER]->centerVertex.texcoord.texcoord.y *= texScale.y;
				load[UNDER]->centerVertex.texcoord.texcoord.y += texCenterPos.x;
				load[UNDER]->centerVertex.texcoord.texcoord.y += texCenterPos.y;

				//中央と同じIDを0に変える
				for (size_t i = 0; i < poligonUp.size(); i++)
				{
					if (poligonUp[i].indicies[0] == load[UP]->centerVertex.id)
					{
						poligonUp[i].indicies[0] = 0;
						poligonUp[i].linepos[0].id = 0;
						continue;
					}
					if (poligonUp[i].indicies[1] == load[UP]->centerVertex.id)
					{
						poligonUp[i].indicies[1] = 0;
						poligonUp[i].linepos[1].id = 0;
						continue;
					}
				}
				//下は処理しない
				/*for (size_t i = 0; i < poligonUnder.size(); i++)
				{
					if (poligonUnder[i].indicies[0] == load[UNDER]->centerVertex.id)
					{
						poligonUnder[i].indicies[0] = 0;
						continue;
					}
					if (poligonUnder[i].indicies[1] == load[UNDER]->centerVertex.id)
					{
						poligonUnder[i].indicies[1] = 0;
						continue;
					}
				}*/

				//バッファ作成
				load[UP]->vertexNum = indexup + 1;
				load[UP]->CreateBuffer();
				load[UNDER]->vertexNum = indexunder + 1;
				load[UNDER]->CreateBuffer();
				return true;
			}
		}

		void LoadXVertex::CreateBuffer()
		{
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION> vertexPosition;
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL> vertexNormal;
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD> vertexTexcoord;
			vertexPosition.resize(this->vertexNum);
			vertexNormal.resize(this->vertexNum);
			vertexTexcoord.resize(this->vertexNum);
			//id順にソート
			//頂点をインデックス順に入れる
			for (size_t i = 0; i < this->vertexNum - 1; i++)
			{
				for (size_t i2 = 0; i2 < this->poligons.size(); i2++)
				{
					//線分の２つの点のidを順になるように入れる
					if (i == this->poligons[i2].linepos[0].id)//その順番の時の頂点を入れる
					{
						vertexPosition[i] = this->poligons[i2].linepos[0].position;
						vertexNormal[i] = this->poligons[i2].linepos[0].normal;
						vertexTexcoord[i] = this->poligons[i2].linepos[0].texcoord;
						break;
					}
					else if (i == this->poligons[i2].linepos[1].id)
					{
						vertexPosition[i] = this->poligons[i2].linepos[1].position;
						vertexNormal[i] = this->poligons[i2].linepos[1].normal;
						vertexTexcoord[i] = this->poligons[i2].linepos[1].texcoord;
						break;
					}
				}
			}
			vertexPosition[this->vertexNum - 1] = this->centerVertex.position;
			vertexNormal[this->vertexNum - 1] = this->centerVertex.normal;
			vertexTexcoord[this->vertexNum - 1] = this->centerVertex.texcoord;

			std::vector<UINT> indices;//インデックス
			const auto poligonNum = poligons.size();//ポリゴン数
			{//最後の頂点番号を除く
				indices.resize(poligonNum * 3);
				for (size_t i = 0; i < poligonNum; i++)
				{
					indices[i * 3 + 0] = poligons[i].indicies[0];//右回り
					indices[i * 3 + 1] = poligons[i].indicies[1];
					indices[i * 3 + 2] = this->centerVertex.id;//真ん中
				}
			}

			const auto NUM_VERTEX = this->vertexNum;//頂点数
			HRESULT hr = S_OK;
			// 頂点バッファ定義
			D3D11_BUFFER_DESC Bufferdesk;
			ZeroMemory(&Bufferdesk, sizeof(Bufferdesk));
			//Bufferdesk.Usage = D3D11_USAGE_DYNAMIC;	//CPUからも使える
			Bufferdesk.Usage = D3D11_USAGE_IMMUTABLE;
			Bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			Bufferdesk.CPUAccessFlags = 0;
			//Bufferdesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			Bufferdesk.MiscFlags = 0;
			Bufferdesk.StructureByteStride = 0;//float?sizeof(DirectX::XMFLOAT3)

			 // サブリソースの初期化に使用されるデータを指定します。
			D3D11_SUBRESOURCE_DATA SubResourceData;
			ZeroMemory(&SubResourceData, sizeof(SubResourceData));
			SubResourceData.SysMemPitch = 0;					//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
			SubResourceData.SysMemSlicePitch = 0;				//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。
			int createCount = 0;//頂点バッファ生成回数(要素を順番に配列に格納するため)
			for (size_t i = 0; i < static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END); i++)
			{
				bool exef = true;
				ShaderInfo::VBUFFER_TYPE_INDEX typeIndex = static_cast<ShaderInfo::VBUFFER_TYPE_INDEX>(i);
				switch (typeIndex)
				{
				default:
					exef = false;
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::POSITION:
					Bufferdesk.ByteWidth = NUM_VERTEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::POSITION);
					SubResourceData.pSysMem = vertexPosition.data();
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::NORMAL:
					Bufferdesk.ByteWidth = NUM_VERTEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL);
					SubResourceData.pSysMem = vertexNormal.data();
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::TEXCOORD:
					Bufferdesk.ByteWidth = NUM_VERTEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD);
					SubResourceData.pSysMem = vertexTexcoord.data();
					break;
				}
				if (exef)
				{
					//バッファを再確保
					hr = GetDevice->CreateBuffer(&Bufferdesk, &SubResourceData, p_VertexBuffer[createCount].ReleaseAndGetAddressOf());
					if (FAILED(hr))
					{
						assert(!"頂点バッファオブジェクトの生成ができません");
						return;
					}
					createCount++;
				}
			}

			

			///////////////////////////////////////////////////
		// インデックスバッファオブジェクトの生成
		///////////////////////////////////////////////////
			hr = S_OK;
			const auto NUM_INDEX = indices.size();

			// インデックスバッファの定義
			D3D11_BUFFER_DESC IndexBufferDesc;
			ZeroMemory(&IndexBufferDesc, sizeof(IndexBufferDesc));
			IndexBufferDesc.ByteWidth = NUM_INDEX * sizeof(UINT);
			//IndexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
			IndexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
			IndexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			//Bufferdesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			IndexBufferDesc.CPUAccessFlags = 0;
			IndexBufferDesc.MiscFlags = 0;
			IndexBufferDesc.StructureByteStride = 0;
			//インデックスの数を補完
			//numIndices = NUM_INDEX;

			// インデックス・バッファのサブリソースの定義
			D3D11_SUBRESOURCE_DATA IndexSubResource;
			ZeroMemory(&IndexSubResource, sizeof(IndexSubResource));
			IndexSubResource.pSysMem = indices.data();
			IndexSubResource.SysMemPitch = 0;
			IndexSubResource.SysMemSlicePitch = 0;

			// インデックス・バッファの作成
			hr = GetDevice->CreateBuffer(&IndexBufferDesc, &IndexSubResource, this->p_IndexBuffer.ReleaseAndGetAddressOf());
			if (FAILED(hr))
			{
				assert(!"インデックス・バッファの作成ができません");
				return;
			}
		}





		void LoadXVertex::UpdateBuffer()
		{
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION> vertexPosition;
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL> vertexNormal;
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD> vertexTexcoord;
			vertexPosition.resize(this->vertexNum);
			vertexNormal.resize(this->vertexNum);
			vertexTexcoord.resize(this->vertexNum);
			//id順にソート
			//頂点をインデックス順に入れる
			for (size_t i = 0; i < this->vertexNum; i++)
			{
				for (size_t i2 = 0; i2 < this->poligons.size(); i2++)
				{
					if (i == this->poligons[i2].linepos[0].id)//その順番の時の頂点を入れる
					{
						vertexPosition[i] = this->poligons[i2].linepos[0].position;
						vertexNormal[i] = this->poligons[i2].linepos[0].normal;
						vertexTexcoord[i] = this->poligons[i2].linepos[0].texcoord;
						break;
					}
					else if (i == this->poligons[i2].linepos[1].id)
					{
						vertexPosition[i] = this->poligons[i2].linepos[1].position;
						vertexNormal[i] = this->poligons[i2].linepos[1].normal;
						vertexTexcoord[i] = this->poligons[i2].linepos[1].texcoord;
						break;
					}
				}
			}

			// 頂点を動的生成
			D3D11_MAPPED_SUBRESOURCE msr;
			for (size_t i = 0; i < static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END); i++)
			{
				ShaderInfo::VBUFFER_TYPE_INDEX typeIndex = static_cast<ShaderInfo::VBUFFER_TYPE_INDEX>(i);
				switch (typeIndex)
				{
				default:
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::POSITION:
					GetDeviceContext->Map(p_VertexBuffer[i].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
					memcpy(msr.pData, vertexPosition.data(), vertexNum * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::POSITION));
					GetDeviceContext->Unmap(p_VertexBuffer[i].Get(), 0);
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::NORMAL:
					GetDeviceContext->Map(p_VertexBuffer[i].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
					memcpy(msr.pData, vertexNormal.data(), vertexNum * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL));
					GetDeviceContext->Unmap(p_VertexBuffer[i].Get(), 0);
					break;
				case ShaderInfo::VBUFFER_TYPE_INDEX::TEXCOORD:
					GetDeviceContext->Map(p_VertexBuffer[i].Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
					memcpy(msr.pData, vertexTexcoord.data(), vertexNum * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD));
					GetDeviceContext->Unmap(p_VertexBuffer[i].Get(), 0);
					break;
				}
			}
		}

}//Lib_Render
}//Lib_3D