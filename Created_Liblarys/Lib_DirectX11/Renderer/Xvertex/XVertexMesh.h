#pragma once
#include <vector>
#include "../../ShaderInfo.h"
#include "../../Texture.h"
#include <Lib_Math/PrimitivClass.h>
namespace Lib_3D
{
	namespace Lib_Render
	{
		
		
		//切るときこれを作り替える
		//↓
		//頂点情報とインデックスを保持
		//切断座標で上と下にポリゴンを作る
		//元の情報をもとにインデックスを付け直す


		/*
			//! 2Dの頂点モデル
		class XVertex
		{
		protected:
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION> vertexPosition;
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL> vertexNormal;
			std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD> vertexTexcoord;

			std::vector<UINT> indices;// インデックス
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_VertexBuffer[static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END)];//! [0]位置情報 [1]色情報 [2]ボーン情報//shaderによってセットする頂点バッファを変える
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_IndexBuffer;		//! インデックスバッファ
			Texture texture;
		public:
			XVertex() = default;
			XVertex(XVertex&) = delete;
			XVertex(XVertex&&) = default;
			~XVertex() = default;

			void CreateBuffer(int vertexNum);


		private:

		};
		//! 多角形//右回り
		class RegularPorigon : virtual protected XVertex
		{
			ShaderInfo::VERTEX_BUFFER_TYPE::POSITION centerVertex;//! 真ん中の頂点
			size_t vertexNum;
			void RecleateRegularPoligon();
		protected:
			void ChangeShape();
			void PoligonUpdate();
			void Render();
		};
		//! 頂点生成
		class GenerateVertex2D : virtual protected XVertex
		{
			static constexpr size_t UP = 0;//上空間
			static constexpr size_t UNDER = 1;//下空間
			int stdeviedTimes;//分割された回数
			static constexpr size_t DEVIED_MAX = 30;
			std::array<std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION>,2> vertexDeviedPosition[DEVIED_MAX];//分割頂点位置
			std::array<Microsoft::WRL::ComPtr<ID3D11Buffer>, 2> p_VertexBuffer[DEVIED_MAX][static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END)];//! [0]位置情報 [1]色情報 [2]ボーン情報//shaderによってセットする頂点バッファを変える
			//! ２つの位置（切り口）//
			void GenerateVertex();
			//! 頂点分割(DeviedPositionでポリゴンのつながりを分ける)
			//! DeviedPositionの真ん中からつながる頂点を分ける（そこから上か下かでポリゴンのつながりを分ける）
			void DevideVertex(const std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION>& parentVertex);
			//! @return 分割された２つの頂点群を返す
			std::array<std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION>, 2>
				DevideVertex_(const std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION>& parentVertex);

		};
		*/


		class LoadXVertex
		{
			//インデックスつき頂点データ
			struct Index_Vertex
			{
				ShaderInfo::VERTEX_BUFFER_TYPE::POSITION position;
				ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL normal;
				ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD texcoord;
				UINT id;//このポリゴンのID(頂点挿入順)
			};
			//↓
			//インデックス付きポリゴン(ポリゴンの外の辺で分割頂点を決めるため)
			//中央の頂点と線分の頂点（これでインデックスも設定する）
			struct TriangleList
			{
				Index_Vertex linepos[2];//辺を構成する頂点たち
				UINT indicies[2];//添え字に沿ったID
				//UINT* centerIndex;//中心頂点インデックス(切ったとき新しいのに変える)
				//.  .
				// .←中心頂点
			};

			static constexpr size_t UP = 0;//上空間
			static constexpr size_t UNDER = 1;//下空間
		public:
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_VertexBuffer[static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END)];//! [0]位置情報 [1]色情報 [2]ボーン情報//shaderによってセットする頂点バッファを変える
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_IndexBuffer;		//! インデックスバッファ

			//! テクスチャ
			Texture* texture;
			Lib_Math::VECTOR2 texScale;//! テクスチャ切り口拡大比
			Lib_Math::VECTOR2 texCenterPos;//! テクスチャ切り口位置
			const char* texturename;
			//! 頂点情報
			size_t vertexNum;//! 頂点数
			std::vector<TriangleList> poligons;//! ポリゴンたち
			Index_Vertex centerVertex;//! 真ん中の頂点
			size_t nowSidesNum;//! 現在の辺の数
			//! 分割されたデータでロードしたもの
			std::array<std::unique_ptr<LoadXVertex>, 2> load;
			
			LoadXVertex() = default;
			~LoadXVertex() = default;
			//! ロード(最初にロードする)
			//! @param[in] texturename テクスチャ名
			//! @param[in] TEX_SCALE テクスチャ切り口拡大比
			//! @param[in] TEX_CENTER テクスチャ切り口位置
			//! @param[in] sidesnum 多角形の辺の数
			void Load(const char* texturename, const Lib_Math::VECTOR2& TEX_SCALE, const Lib_Math::VECTOR2& TEX_CENTER, const size_t sidesnum);
			//! 形を多角形に変える
			void ChangeShape(const size_t sidesnum);
			//! @brief 頂点切断
			//! @param[in] deviedPlane 切断平面(ワールド座標分ずらす)
			//! @return 切断に成功したか?
			bool DevideVertex(Lib_Math::Primitive::Plane& deviedPlane);
		private:
			//! 頂点バッファを作成(頂点数を変えるとき使用)
			void CreateBuffer();
			//! 頂点バッファをアップデート（頂点数を変えないとき使用）
			void UpdateBuffer();
		};

		//class XVertexBuffer
		//{
		//	Microsoft::WRL::ComPtr<ID3D11Buffer> p_VertexBuffer[static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END)];//! [0]位置情報 [1]色情報 [2]ボーン情報//shaderによってセットする頂点バッファを変える
		//	Microsoft::WRL::ComPtr<ID3D11Buffer> p_IndexBuffer;		//! インデックスバッファ
		//public:
		//	void CreateBuffer(
		//		std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION>*,
		//		std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL>*,
		//		std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD>*,
		//		std::vector<UINT>* indices);
		//	void DynamixVertexUpdate(
		//		std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION>* position,
		//		std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL>* normal,
		//		std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD>* texcoord
		//	);
		//};
	
		/*
		//多角形ポリゴン形
		class RegularPoligonShape
		{
		public:
			Index_Vertex centerVertex;//! 真ん中の頂点
			size_t vertexNum;//頂点数
			std::vector<TriangleList> poligons;//ポリゴンたち
			void ChangeShape(const size_t sidesnum);
		};
		class DeviderForXVertex;
		class XVertex
		{

		public:
			XVertex() = default;
			XVertex(XVertex&) = delete;
			XVertex(XVertex&& v)
				: nextDevide(v.nextDevide)
				, vertecies(v.vertecies)
				, centerpos(v.centerpos)
			{
				v.nextDevide = nullptr;
			}
			~XVertex()
			{
				if (nextDevide)
					delete nextDevide;
			}

			//! @brief IDをセット
			//void SetBuffer();
			//void UpdateBuffer();
			//LoadXVertex load;
			//XVertexBuffer buffer;
			//! @brief 頂点切断
			//! @param[in] deviedPlane 切断平面(ワールド座標分ずらす)
			void DevideVertex(Lib_Math::Primitive::Plane& deviedPlane);
			DeviderForXVertex* nextDevide = nullptr;//このクラスが分割された時の分割先(切られたときnew)
			//RegularPoligonShape vertecies;//多角形
			//Index_Vertex centerpos;
		};
		//! このクラスに分割頂点を格納(このクラスはこのクラスを持つ)
		class DeviderForXVertex
		{
			static constexpr size_t UP = 0;//上空間
			static constexpr size_t UNDER = 1;//下空間
		public:
			std::array<XVertex, 2> vertexDeviedPosition;//分割頂点情報

			DeviderForXVertex() = default;
			~DeviderForXVertex() = default;
			bool DevideVertex(const XVertex& XVertex, Lib_Math::Primitive::Plane& devidePlane);
		};
		*/
		
		
	}//Lib_Render
}//Lib_3D
