#include "Render.h"
#include <cassert>
#include "../DirectX11Init.h"


namespace
{	
	namespace at_compile_time
	{

	}
	//! @namespace call_externaly
	//! @brief 外のソースコードから呼んでいるもの(このファイル専用に作ったソースではないもの)
	namespace call_externaly
	{
#define GetDeviceContext Lib_3D::DirectX11InitDevice::GetImidiateContext()
	}

	struct FBXDATA final
	{
		Lib_3D::Lib_Render::LoadFbx* loadData;
	};
	struct XVERTEXDATA final
	{
		Lib_3D::Lib_Render::LoadXVertex* loadData;
	};
	struct OBJ3DDATA final
	{};
	struct OBJATDATA final
	{};
	struct LIVE2DDATA final
	{};
	struct OBJ4DDATA final
	{};

	void Render(FBXDATA data)
	{
		GetDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		for (auto& mesh : data.loadData->meshs)
		{
			if (mesh.texture)
				mesh.texture->Set();
			GetDeviceContext->DrawIndexed(mesh.index_count, mesh.index_start, 0);
		}
	}

	void Render(XVERTEXDATA data)
	{
		GetDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		data.loadData->texture->Set();
		GetDeviceContext->DrawIndexed(data.loadData->nowSidesNum*3, 0, 0);
	}
	void Render(OBJ3DDATA data)
	{
	}
	void Render(OBJATDATA data)
	{
	}
	void Render(LIVE2DDATA data)
	{
	}
	void Render(OBJ4DDATA data)
	{
	}

	//! RenderクラスにRoleのセッターを適用
	template<typename RenderT>//ROLE 役割クラス Render renderクラス
	class Adapter
	{
	public:
		Adapter() = delete;
		Adapter(Adapter&) = delete;
		Adapter(Adapter&&) = delete;
		~Adapter() = default;
		//! モデルの持つoverrideした関数に応じてレンダーに値を設定
		//! param[in] role
		//! param[out] render
		template<typename RenderT>
		static void SetterRenderParam(const Lib_3D::Lib_Render::Model* role, RenderT* render)
		{
			role->SetRenderParam(render);
		}
	};
}


namespace Lib_3D
{
	namespace Lib_Render
	{
		

		std::unordered_map<const char*, FBXDATA> fbxdatamap;//fbx名がキーのオブジェクトデータマップ//追加時、その配列をシェーダ名デーソート
		std::unordered_map<const char*, XVERTEXDATA> xvertexDataMap;
		std::unordered_map<const char*, OBJ3DDATA> obj3Ddatamap;
		std::unordered_map<const char*, OBJATDATA> objAtdatamap;
		std::unordered_map<const char*, LIVE2DDATA> live2Ddatamap;
		std::unordered_map<const char*, OBJ4DDATA> obj4Ddatamap;

		void ModelManager::SetData(const char* modelname, LoadFbx* fbxdata)
		{
			FBXDATA loadData{};
			loadData.loadData = fbxdata;
			fbxdatamap[modelname] = loadData;
		}

		void ModelManager::SetData(const char* modelname, LoadXVertex* xvData)
		{
			XVERTEXDATA loadData{};
			loadData.loadData = xvData;
			xvertexDataMap[modelname] = loadData;
		}

		//  3Dshader 3D 2D
		//std::vector<std::string> fbxfilename{ "" };
		//const char* shaderfilename[]{TOSTRING(SHADER_NAME0), TOSTRING(SHADER_NAME1) };

		

		//レンダータイプで描画を分ける
		void Renderer::RenderSwitch(OBJ* modeldata)
		{
			//レンダータイプで描画を分岐
			switch (modeldata->renderType)
			{
			default:
				break;
			case ShaderInfo::RENDER_TYPE::OBJ3D:
			{
				auto d = dynamic_cast<OBJ3D*>(modeldata);
				Obj3DRender(d);
				break;
			}
			case ShaderInfo::RENDER_TYPE::FBXOBJ:
			{
				auto d = dynamic_cast<FBXOBJ*>(modeldata);
				FbxRender(d);
				break;
			}
			case ShaderInfo::RENDER_TYPE::XVERTEX:
			{
				auto d = dynamic_cast<XVERTEX*>(modeldata);
				XVertexRender(d);
				break;
			}
			case ShaderInfo::RENDER_TYPE::OBJAT:
			{
				auto d = dynamic_cast<OBJAT*>(modeldata);
				ObjAtRender(d);
				break;
			}
			case ShaderInfo::RENDER_TYPE::LIVE2D:
			{
				auto d = dynamic_cast<LIVE2D*>(modeldata);
				Live2DRender(d);
				break;
			}
			case ShaderInfo::RENDER_TYPE::OBJ4D:
			{
				auto d = dynamic_cast<OBJ4D*>(modeldata);
				Obj4DRender(d);
				break;
			}
			}
		}

		

		void Renderer::RenderSwitchModelUse(const Model* model, int shaderNum)
		{
			static ShaderInfo::GetShaderInfo::GET_RENDER_TYPE get;//シェーダーのレンダータイプ取得
			//シェーダーの持つレンダータイプで描画するモデルも変える
			auto rendertype = get.FuncExecute(shaderNum);
			
			switch (rendertype)
			{
			default:
				break;
			case Lib_3D::ShaderInfo::RENDER_TYPE::FBXOBJ:
			{
				//このコードの解説
				static Lib_3D::Lib_Render::FBXOBJ obj_;//staticなので、すべての値を書き換えること
				Adapter< Lib_3D::Lib_Render::FBXOBJ>::SetterRenderParam(model, &obj_);//この中でモデル名を変えたりする
				obj_.Update();//数値を入れた後、オブジェクトをアップデート
				Lib_3D::Lib_Render::Renderer::RenderSwitch(&obj_);//レンダーを呼ぶ
				break;
			}
			case Lib_3D::ShaderInfo::RENDER_TYPE::OBJ3D:
			{
				Lib_3D::Lib_Render::OBJ3D obj_;
				Adapter<Lib_3D::Lib_Render::OBJ3D>::SetterRenderParam(model, &obj_);
				obj_.Update();
				Lib_3D::Lib_Render::Renderer::RenderSwitch(&obj_);
				break;
			}
			case Lib_3D::ShaderInfo::RENDER_TYPE::XVERTEX:
			{
				Lib_3D::Lib_Render::XVERTEX obj_;
				Adapter<Lib_3D::Lib_Render::XVERTEX>::SetterRenderParam(model, &obj_);
				obj_.Update();
				Lib_3D::Lib_Render::Renderer::RenderSwitch(&obj_);
				break;
			}
			case Lib_3D::ShaderInfo::RENDER_TYPE::OBJAT:
			{
				Lib_3D::Lib_Render::OBJAT obj_;
				Adapter<Lib_3D::Lib_Render::OBJAT>::SetterRenderParam(model, &obj_);
				obj_.Update();
				Lib_3D::Lib_Render::Renderer::RenderSwitch(&obj_);
				break;
			}
			case Lib_3D::ShaderInfo::RENDER_TYPE::LIVE2D:
			{
				Lib_3D::Lib_Render::LIVE2D obj_;
				Adapter<Lib_3D::Lib_Render::LIVE2D>::SetterRenderParam(model, &obj_);
				obj_.Update();
				Lib_3D::Lib_Render::Renderer::RenderSwitch(&obj_);
				break;
			}
			case Lib_3D::ShaderInfo::RENDER_TYPE::OBJ4D:
			{
				Lib_3D::Lib_Render::OBJ4D obj_;
				Adapter<Lib_3D::Lib_Render::OBJ4D>::SetterRenderParam(model, &obj_);
				obj_.Update();
				Lib_3D::Lib_Render::Renderer::RenderSwitch(&obj_);
				break;
			}
			}
		}

		void Renderer::FbxRender(FBXOBJ* obj)
		{
			auto d = fbxdatamap[obj->modelname];
			
			GetDeviceContext->IASetIndexBuffer(d.loadData->p_IndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
			bool f = true;
			f = ShaderInfo::ShaderCorrespondsBufferSet::VertexBufferSet(obj->shaderTypeNum, d.loadData->p_VertexBuffer[0].GetAddressOf());
			if (!f)
			{
				assert(!"セットできませんでした。");
				return;
			}
			f = ShaderInfo::ShaderCorrespondsShaderLoad::ShaderSet(obj->shaderTypeNum);
			if (!f)
			{
				assert(!"セットできませんでした。");
				return;
			}
			ShaderInfo::CBufferSelector::SetVariableToTuple(obj->boneTransform, obj->shaderTypeNum);
			Lib_3D::ShaderInfo::TIMER timer;
			timer.var = obj->flame;
			ShaderInfo::CBufferSelector::SetVariableToTuple(timer, obj->shaderTypeNum);

			ShaderInfo::CBufferSelector::SetTupleToVariant(obj->shaderTypeNum);
			ShaderInfo::CBufferSelector::SetVariantToConstantBuffer(obj->shaderTypeNum);
			if (!f)
			{
				assert(!"セットできませんでした。");
				return;
			}
			Render(d);
		}

		void Renderer::Obj3DRender(OBJ3D* obj)
		{
			auto d = obj3Ddatamap[obj->modelname];
			Render(d);
		}

		void Renderer::XVertexRender(XVERTEX* obj)
		{
			auto d = xvertexDataMap[obj->modelname];
			
			if (obj->devideFlag)
			{
				if (d.loadData->DevideVertex(obj->devidePlane))
				{
					constexpr int UP = 0;
					constexpr int UNDER = 1;
					obj->parts[UP] = std::make_unique<XVERTEX>();
					obj->parts[UNDER] = std::make_unique<XVERTEX>();
					//atを適用(今はうえのみ)
					obj->parts[UP]->at = obj->at;
					obj->parts[UNDER]->at = Lib_Math::VECTOR3_CONST{0,0,0};
					//モデルを作る(名前でバインドする)
					obj->parts[UP]->modelname = (std::string(obj->modelname) + "UP").c_str();
					obj->parts[UNDER]->modelname = (std::string(obj->modelname) + "UNDER").c_str();
					//! 分割データセット
					ModelManager::SetData(obj->parts[UP]->modelname, d.loadData->load[UP].get());
					ModelManager::SetData(obj->parts[UNDER]->modelname, d.loadData->load[UNDER].get());
					obj->devideFlag = false;
				}
			}

			GetDeviceContext->IASetIndexBuffer(d.loadData->p_IndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
			bool f = true;
			f = ShaderInfo::ShaderCorrespondsBufferSet::VertexBufferSet(obj->shaderTypeNum, d.loadData->p_VertexBuffer[0].GetAddressOf());
			if (!f)
			{
				assert(!"セットできませんでした。");
				return;
			}
			f = ShaderInfo::ShaderCorrespondsShaderLoad::ShaderSet(obj->shaderTypeNum);
			if (!f)
			{
				assert(!"セットできませんでした。");
				return;
			}

			ShaderInfo::CBufferSelector::SetTupleToVariant(obj->shaderTypeNum);
			ShaderInfo::CBufferSelector::SetVariantToConstantBuffer(obj->shaderTypeNum);

			Render(d);
		}

		void Renderer::ObjAtRender(OBJAT* obj)
		{
			auto d = objAtdatamap[obj->modelname];
			Render(d);
		}

		void Renderer::Live2DRender(LIVE2D * obj)
		{
			auto d = live2Ddatamap[obj->modelname];
			Render(d);
		}

		void Renderer::Obj4DRender(OBJ4D * obj)
		{
			auto d = obj4Ddatamap[obj->modelname];
			Render(d);
		}

		

		

		void FBXOBJ::Skinning()
		{
			auto modeldata = fbxdatamap[this->modelname];
			modeldata.loadData->Animate(motionSpeed, flame, motionname);
			modeldata.loadData->Skinning(&this->boneTransform, motionname, flame);
		}

		

		
}//Lib_Render
}