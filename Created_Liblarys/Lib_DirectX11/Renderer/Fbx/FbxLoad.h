#pragma once
/**
* @file FbxLoad.h
* @brief fbxのロードクラスが記述されている
* @details ShaderInfo::VBUFFER_TYPE_INDEXはこのコード専用に使う(つまりこのfbxロードにShaderInfoは必須)
*/
#include <wrl.h>
#include <d3d11.h>
#include <DirectXMath.h>
#include <FBXSDK/include/fbxsdk.h>
#include <vector>
#include <string>
#include <map>
#include "../../ShaderInfo.h"
#include "../../Texture.h"
#include <Lib_Math/vector_.h>
//! @def VERTEX_USING_SKINING_USE_FLAG
//! @brief 頂点を更新するタイプのスキニングを使うかのフラグ
#define VERTEX_USING_SKINING_USE_FLAG 0

namespace Lib_3D 
{
	namespace Lib_Render
	{
		//! @brief fbx load
		//! @deatiles fbxモデル１個に対し、１個のローダーにデータを保存する
		//! このクラスは複数のインスタンスを作ると思う
		class LoadFbx final
		{
		private:
			int allNumBones;//ボーン総数
			int allVetexNum;//全頂点数

		public:
			//! メッシュごとに読み取ったデータ
			struct MESH
			{
				UINT index_start = 0;//そのメッシュの描画開始するときのインデックス
				UINT index_count = 0;//そのメッシュのインデックスの数
				Texture* texture;//テクスチャーの参照

				//Lib_Math::MATRIX GloabalTransform = {//姿勢行列
				//	1,0,0,0,
				//	0,1,0,0,
				//	0,0,1,0,
				//	0,0,0,1,
				//};
			};

			//! アニメーション
			struct Motion
			{
				int NumFrame;	// フレーム数	
				//D3DXMATRIX key[BONE_MAX][120];	// キーフレーム
				std::vector<std::vector<Lib_Math::MATRIX>> key;	// ボーンの数だけ、キーフレーム
			};
			//! ボーン構造体
			struct BONE
			{
				std::string name;
				Lib_Math::MATRIX OffsetMatrix;
			};

			std::vector<MESH> meshs;//! メッシュデータ
			std::vector<BONE> Skeltal;//! ボーンデータ
			std::map<std::string, Motion> motion;	// ! モーションデータ
			//move処理が面倒なのでCOM
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_VertexBuffer[static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END)];//! [0]位置情報 [1]色情報 [2]ボーン情報//shaderによってセットする頂点バッファを変える
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_IndexBuffer;		//! インデックスバッファ
			int StartFrame;//! アニメーション開始時間
			char FBXDir[128];
		public:
			LoadFbx() = default;
			LoadFbx(LoadFbx&) = delete;
			LoadFbx(LoadFbx&&) = default;//vectorで使うかもしれないので
			~LoadFbx() = default;

			//! fbxファイルのロード
			void LoadFbxFile(const char* fbx_filename);
			//! モーションの読み込み
			void AddMotion(std::string name, const char* filename);
			//! モーション再生(updateで呼ぶ)
			void Animate(float sec, float& frame, std::string motionName) const;
			
#if VERTEX_USING_SKINING_USE_FLAG
			//シェーダーでスキニングしない場合
			void Skinning(ShaderInfo::BONE_TRANSFORM* boneTransForm, std::string motionname, float frame);	// ボーンによる変形
#else
			//! @brief ボーンアニメーションを計算してそのフレームのボーン座標を返す
			//! @param[out] boneTransForm 今フレームのすべてのボーン位置
			//! @param[in] eachdata コンスタントバッファへ格納するデータ
			void Skinning(ShaderInfo::BONE_TRANSFORM* boneTransForm, std::string motionname, float frame)const;

#endif // VERTEX_USING_SKINING_USE_FLAG 
		private:
			//! @brief メッシュのテクスチャーを読み込み
			void LoadTexture(int index, FbxSurfaceMaterial* material);
			//! ボーンのあるメッシュデータロード
			void LoadBone(const FbxMesh& mesh, int& numBones);
			//! キーフレームリスト取得
			void LoadKeyFrames(std::string name, int bone, FbxNode* bone_node);
			//! ボーンのないメッシュデータロード
			void LoadMeshAnim(const FbxMesh& mesh, int numMesh, int numBone);
			//! インデックスバッファ作成
			void CreateIndexBuffer(unsigned int* indices, const int NUM_INDEX);
			// fbx element init
			class LoadFbxElement final
			{
			private:
			public:
				//バーテックスバッファに格納するデータ
				static std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION> pVerticesPosition;//! 頂点位置
				static std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL> pVerticesNormal;//! 頂点法線
				static std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD> pVerticesTexcoord;//! 頂点テクスチャ位置
				static std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::MATRIAL> pVerticesMaterial;//! 頂点色
				static std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::WEIGHT> pVerticesWeight;//! 頂点ボーン影響度
			public:
				//static std::vector<size_t> pIndices;//! インデックスバッファに格納するデータ
#if VERTEX_USING_SKINING_USE_FLAG
				std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION> pVerticesTransformMemory;//! 頂点位置情報保存
				std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL> pVerticesNormalMemory;//! 頂点法線情報保存
				std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::WEIGHT> pVerticesWeightMemory;//! ウェイト情報保存
#endif // VERTEX_USING_SKINING_USE_FLAG 
				static std::vector<short> boneinfluencecount;//! ボーン影響のカウント
			public:
				//! @param[in] 今代入する頂点カウント
				LoadFbxElement() = default;
				LoadFbxElement(LoadFbxElement&) = delete;
				LoadFbxElement(LoadFbxElement&&) = default;
				~LoadFbxElement() = default;
				void LoadPosition(FbxMesh* mesh, int numVertices);
				void LoadNormal(FbxMesh* mesh, int numVertices);
				void LoadUV(FbxMesh* mesh, int numVertices);
				void LoadVertexColor(FbxMesh* mesh, int numVertices);
				void LoadWeight(FbxMesh* mesh, const std::vector<LoadFbx::BONE>& Skeltal, int& numBones, int numVertices);
				//!	ボーンのないメッシュのボーンウェイト初期化
				void InitNonBoneWeight(FbxMesh* mesh, int& numBones, int numVertices);
				//!	頂点最適化
				void OptimizeVertices(int& numVertices, unsigned int* indices);
				//!	ウェイト正規化
				void WeightNormlize(int numVerTces);
				//!  バーテックスバッファ作成
				bool CreateVertexBuffer(Microsoft::WRL::ComPtr<ID3D11Buffer> vBuffer[], const int NUM_VRETEX);
				//! 頂点バッファ用データを頂点数分、メモリ確保
				//! @param[in] vertexNum 頂点数
				void VertexDatasResize(int verticesAllNum)
				{
					pVerticesPosition.resize(verticesAllNum);
					pVerticesNormal.resize(verticesAllNum);
					pVerticesTexcoord.resize(verticesAllNum);
					pVerticesMaterial.resize(verticesAllNum);
					pVerticesWeight.resize(verticesAllNum);
#if VERTEX_USING_SKINING_USE_FLAG
					pVerticesTransformMemory.resize(verticesAllNum);
					pVerticesWeightMemory.resize(verticesAllNum);
#endif // VERTEX_USING_SKINING_USE_FLAG 
					boneinfluencecount.resize(verticesAllNum);
				}
				//! 頂点バッファ用データをクリア
				void VertexDatasClear()
				{
					pVerticesPosition.clear();
					pVerticesNormal.clear();
					pVerticesTexcoord.clear();
					pVerticesMaterial.clear();
					pVerticesWeight.clear();
					boneinfluencecount.clear();
				}
				//! 頂点データを保存
				void DataMemorize()
				{
#if VERTEX_USING_SKINING_USE_FLAG
					pVerticesTransformMemory = pVerticesPosition;
					pVerticesNormalMemory = pVerticesNormal;
					pVerticesWeightMemory = pVerticesWeight;
#endif // VERTEX_USING_SKINING_USE_FLAG 
				}
			};
			LoadFbxElement loadElement;
		};
	}//Lib_Render
}



