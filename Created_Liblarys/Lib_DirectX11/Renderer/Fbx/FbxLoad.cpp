#include "FbxLoad.h"
#include "../../DirectX11Init.h"

namespace
{
	//! @namespace at_compile_time
	//! @brief コンパイル時、処理
	namespace at_compile_time
	{
#ifdef _DEBUG
#define new  ::new(_NORMAL_BLOCK, __FILE__, __LINE__)//メモリリーク無いならなくす(newをあぶりだしたいので)
#endif // DEBUG
		//! インスタンス生成ラップテンプレート(コピペして使う)
		template<typename T>
		class  encapsulation_template
		{
		protected:
			static T* Init;
		public:
			encapsulation_template() = default;
			encapsulation_template(encapsulation_template<T>&) = delete;
			encapsulation_template(encapsulation_template<T>&&) = delete;
			virtual ~encapsulation_template() { if (Init) { delete Init; Init = nullptr; } }
		};
		//! インスタンスを生成
		class  encapsulation_TextureResourceManager0 final : encapsulation_template<Lib_3D::TextureResourceManager>
		{
		public:
			encapsulation_TextureResourceManager0() { if (!Init)Init = new Lib_3D::TextureResourceManager; }
			static Lib_3D::TextureResourceManager* const  GetInstance(){return Init;}
		};
		Lib_3D::TextureResourceManager* encapsulation_TextureResourceManager0::Init = nullptr;
		const  at_compile_time::encapsulation_TextureResourceManager0 Init{};//コンパイル時、初期化
	}//at_compile_time

	//! @namespace call_externaly
	//! @brief 外のソースコードから呼んでいるもの(このファイル専用に作ったソースではないもの)
	namespace call_externaly
	{
		using TextureResourceManagerEx = at_compile_time::encapsulation_TextureResourceManager0;
#define GetDevice Lib_3D::DirectX11InitDevice::GetDevice()
#define GetDeviceContext Lib_3D::DirectX11InitDevice::GetImidiateContext()
	}
}

namespace Lib_3D
{
	namespace Lib_Render
	{

	std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::POSITION> LoadFbx::LoadFbxElement::pVerticesPosition{};
	std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL> LoadFbx::LoadFbxElement::pVerticesNormal{};
	std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD> LoadFbx::LoadFbxElement::pVerticesTexcoord{};
	std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::MATRIAL> LoadFbx::LoadFbxElement::pVerticesMaterial{};
	std::vector<ShaderInfo::VERTEX_BUFFER_TYPE::WEIGHT> LoadFbx::LoadFbxElement::pVerticesWeight{};
	std::vector<short> LoadFbx::LoadFbxElement::boneinfluencecount{};

	void LoadFbx::CreateIndexBuffer(unsigned int* indices, const int NUM_INDEX)
	{
		///////////////////////////////////////////////////
		// インデックスバッファオブジェクトの生成
		///////////////////////////////////////////////////
		HRESULT hr = S_OK;

		// インデックスバッファの定義
		D3D11_BUFFER_DESC IndexBufferDesc;
		ZeroMemory(&IndexBufferDesc, sizeof(IndexBufferDesc));
		IndexBufferDesc.ByteWidth = NUM_INDEX * sizeof(UINT);
		IndexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
		IndexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		IndexBufferDesc.CPUAccessFlags = 0;
		IndexBufferDesc.MiscFlags = 0;
		IndexBufferDesc.StructureByteStride = 0;
		//インデックスの数を補完
		//numIndices = NUM_INDEX;

		// インデックス・バッファのサブリソースの定義
		D3D11_SUBRESOURCE_DATA IndexSubResource;
		ZeroMemory(&IndexSubResource, sizeof(IndexSubResource));
		IndexSubResource.pSysMem = indices;
		IndexSubResource.SysMemPitch = 0;
		IndexSubResource.SysMemSlicePitch = 0;

		// インデックス・バッファの作成
		hr = GetDevice->CreateBuffer(&IndexBufferDesc, &IndexSubResource, this->p_IndexBuffer.GetAddressOf());
		if (FAILED(hr))
		{
			assert(!"インデックス・バッファの作成ができません");
			return;
		}
		return;
	}



















	void LoadFbx::LoadFbxFile(const char* fbx_filename)
	{	
		//ファイル名を取り除く
		strcpy_s(FBXDir, 128, fbx_filename);
		for (int n = strlen(FBXDir) - 1; n >= 0; n--)
		{
			if (FBXDir[n] == '/' || FBXDir[n] == '\\')
			{
				FBXDir[n + 1] = '\0';
				break;
			}
		}

		FbxManager* manager = FbxManager::Create();
		assert(manager);
		FbxScene* scene = FbxScene::Create(manager, "");
		assert(scene);
		//	ファイルからシーンに読み込み
		FbxImporter* importer = FbxImporter::Create(manager, "");
		//if ();
		bool f = false;
		f = importer->Initialize(fbx_filename);
		if(!f)
		{
			manager->Destroy();
			// 読み込み失敗 
			assert(!"Importer Initialize fail");
		}
		f = importer->Import(scene);
		if (!f)
		{
			manager->Destroy();
			assert(!"Importer Scene fail");
		}
		importer->Destroy();

		//	モーション情報取得
		FbxArray<FbxString*> names;
		scene->FillAnimStackNameArray(names);
		if (names != nullptr)
		{
			//	モーションが存在するとき
			FbxTakeInfo* take = scene->GetTakeInfo(names[0]->Buffer());
			FbxLongLong start = take->mLocalTimeSpan.GetStart().Get();
			FbxLongLong stop = take->mLocalTimeSpan.GetStop().Get();
			FbxLongLong fps60 = FbxTime::GetOneFrameValue(FbxTime::eFrames60);
			this->StartFrame = (int)(start / fps60);
			this->motion["default"].NumFrame = (int)((stop - start) / fps60);
		}
		else
		{
			this->StartFrame = 0;
			this->motion["default"].NumFrame = 0;
		}

		//	モデルを材質ごとに分割
		FbxGeometryConverter fgc(manager);
		fgc.SplitMeshesPerMaterial(scene, true);
		fgc.Triangulate(scene, true);

		//	メッシュ数
		const int NUM_MESH = scene->GetSrcObjectCount<FbxMesh>();
		//メモリ確保
		this->meshs.resize(NUM_MESH);
		
		//	頂点数計算
		int allVertexNum = 0;
		this->allNumBones = 0;
		
		

		for (int m = 0; m < NUM_MESH; m++)
		{
			FbxMesh* mesh = scene->GetSrcObject<FbxMesh>(m);
			int num = mesh->GetPolygonVertexCount();
			allVertexNum += num; // 合計頂点数
			
			//ボーン有無
			bool boneExist = true;
			int skinCount = mesh->GetDeformerCount(FbxDeformer::eSkin);
			if (skinCount <= 0)
			{
				boneExist = false;
			}
			if (boneExist)
			{
				FbxSkin* skin = static_cast<FbxSkin*>(mesh->GetDeformer(0, FbxDeformer::eSkin));
				//	ボーン数
				this->allNumBones = skin->GetClusterCount();
			}
		}
		
		//メモリ確保
		this->Skeltal.resize(this->allNumBones);
		if (this->allNumBones == 0)
			this->Skeltal.resize(1);

		std::vector<size_t> Indices;//インデックス情報
		Indices.resize(allVertexNum*3);

		int numBones = 0;		//ループ内で使うカウント
		int numVertices = 0;	//ループ内で使うカウント
		this->loadElement.VertexDatasResize(allVertexNum);//頂点数分メモリ確保
		//	頂点バッファに渡す情報読み込み--------------------------------------------------------
		for (int m = 0; m < NUM_MESH; m++)
		{
			FbxMesh* mesh = scene->GetSrcObject<FbxMesh>(m);
			const int MESH_VERTEX_NUM = mesh->GetPolygonVertexCount();//このメッシュのポリゴン頂点数
			
			//	頂点情報読み込み
			loadElement.LoadPosition(mesh, numVertices);	//	座標読み込み
			loadElement.LoadNormal(mesh, numVertices);		//	法線読み込み
			loadElement.LoadUV(mesh, numVertices);			//	テクスチャUV
			loadElement.LoadVertexColor(mesh, numVertices);	//	頂点カラー読み込み

					//	インデックス設定(三角形ごと)
			for (int i = 0; i < MESH_VERTEX_NUM; i += 3)
			{
				Indices[i + 0 + numVertices] = i + 0 + numVertices;
				Indices[i + 1 + numVertices] = i + 1 + numVertices;
				Indices[i + 2 + numVertices] = i + 2 + numVertices;
			}


			//	ボーン読み込み
			// スキン情報の有無
			int skinCount = mesh->GetDeformerCount(FbxDeformer::eSkin);
			if (skinCount <= 0)
			{
				//this->LoadMeshAnim(*mesh, m, numBones);
				//loadElement.InitNonBoneWeight(mesh, numBones, numVertices);//絶対ここ
			}
			else
			{
				this->LoadBone(*mesh, numBones);
				loadElement.LoadWeight(mesh, this->Skeltal, numBones, numVertices);//絶対ここ
			}
			

			//	メッシュの使用材質取得
			FbxLayerElementMaterial* LEM = mesh->GetElementMaterial();
			if (LEM != NULL)
			{
				//	ポリゴンに貼られている材質番号
				int material_index = LEM->GetIndexArray().GetAt(0);
				//	メッシュ材質のmaterial_index番目を取得
				FbxSurfaceMaterial* material = mesh->GetNode()->GetSrcObject<FbxSurfaceMaterial>(material_index);
				//if(!material)
				this->LoadTexture(m, material);
			}

			//姿勢制御行列取得
			//fbxsdk::FbxMatrix GlobalTransform = mesh->GetNode()->EvaluateGlobalTransform(0);
			//for (UINT row = 0; row < 4; row++)//行
			//{
			//	for (int column = 0; column < 4; column++)//列
			//	{
			//		meshs[m].GloabalTransform.m[row][column] = static_cast<float>(GlobalTransform[row][column]);
			//	}
			//}

			//描画インデックスセット
			meshs[m].index_count = MESH_VERTEX_NUM;
			if (m > 0)
			{
				meshs[m].index_start = meshs[m - 1].index_start;
				meshs[m].index_start += meshs[m - 1].index_count;
			}
			else
			{
				meshs[m].index_start = 0;
			}
			
			
			numVertices += MESH_VERTEX_NUM;
		}
		allVetexNum = numVertices;

		// 頂点最適化
#if VERTEX_USING_SKINING_USE_FLAG
		//シェーダーでskinningするとずれる(boneWeightがずれている)
		//loadElement.OptimizeVertices(allVetexNum, Indices.data());
#endif // VERTEX_USING_SKINING_USE_FLAG 
		//loadElement.OptimizeVertices(allVetexNum, Indices.data());
		// ウェイト正規化
		loadElement.WeightNormlize(allVetexNum);

		//　バッファ作成
		loadElement.CreateVertexBuffer(p_VertexBuffer, allVetexNum);
		this->CreateIndexBuffer(Indices.data(), allVetexNum *3);

		
		//Indices.clear();
		//データーをクリア+データを保存
		loadElement.DataMemorize();
		loadElement.VertexDatasClear();
		
		//	解放
		scene->Destroy();
		manager->Destroy();

		// TODO: return ステートメントをここに挿入します

	}


























	void LoadFbx::AddMotion(std::string name, const char* filename)
	{
		FbxManager* manager = FbxManager::Create();
		FbxScene* scene = FbxScene::Create(manager, "");
		//	ファイルからシーンに読み込み
		FbxImporter* importer = FbxImporter::Create(manager, "");
		importer->Initialize(filename);
		importer->Import(scene);
		importer->Destroy();

		//	モーション情報取得
		FbxArray<FbxString*> names;
		scene->FillAnimStackNameArray(names);

		FbxTakeInfo* take = scene->GetTakeInfo(names[0]->Buffer());
		FbxLongLong start = take->mLocalTimeSpan.GetStart().Get();
		FbxLongLong stop = take->mLocalTimeSpan.GetStop().Get();
		FbxLongLong fps60 = FbxTime::GetOneFrameValue(FbxTime::eFrames60);

		this->StartFrame = (int)(start / fps60);
		this->motion[name].NumFrame = (int)((stop - start) / fps60);
		//	ルートノード取得
		FbxNode* root = scene->GetRootNode();

		//	全ボーン読み込み
		for (int b = 0; b < this->allNumBones; b++)
		{
			//	ボーンノード検索
			FbxNode* bone = root->FindChild(this->Skeltal[b].name.c_str());
			if (bone == NULL) continue;
			//	キーフレーム読み込み
			LoadKeyFrames(name, b, bone);
		}
		//	解放
		scene->Destroy();
		manager->Destroy();
	}


	//------------------------------------------------
//	アニメーション
//------------------------------------------------
	void LoadFbx::Animate(float sec,  float& frame, std::string motionName)const
	{
		float DeltaTime = sec;
		//	モーション時間の更新
		frame += DeltaTime/* * 60*/;

		//	ループチェック------------------------

		auto itr = motion.find(motionName);
		if (itr == motion.end())
			return;
		//モーション名があったら
		if (frame >= itr->second.NumFrame - 1)
		{
			// ループ
			frame = 0;		// 全体をループ
		}
	}

	//	ボーン行列の補間
	Lib_Math::MATRIX MatrixInterporate(const Lib_Math::MATRIX& A_, const Lib_Math::MATRIX& B_, float rate)
	{
		return (A_ * (1.0f - rate) + B_ * rate);
	}


#if VERTEX_USING_SKINING_USE_FLAG
	void LoadFbx::Skinning(ShaderInfo::BONE_TRANSFORM* boneTransForm, std::string motionname, float frame)
	{
		//	int motion_no = MotionNo;
		Motion* M = &motion[motionname];
		if (M == NULL) return;

		//	配列用変数
		int f = (int)frame;
		//あまりよくないけどメモリ確保
		this->loadElement.VertexDatasResize(allVetexNum);
		//	行列準備
		Lib_Math::MATRIX KeyMatrix[256];
		for (int b = 0; b < this->allNumBones; b++)
		{
			//	行列補間
			Lib_Math::MATRIX m;
			m = MatrixInterporate(M->key[b][f], M->key[b][f + 1], frame - (int)frame);
			//	キーフレーム
			KeyMatrix[b] = Skeltal[b].OffsetMatrix * m;
		}

		//	頂点変形
		for (int v = 0; v < allVetexNum; v++)
		{
			//	頂点 * ボーン行列
			// b = v番目の頂点の影響ボーン[n]
			//if (loadElement.boneinfluencecount[v] <= 0)
				//continue;
			
			loadElement.pVerticesPosition[v].position.x = 0;
			loadElement.pVerticesPosition[v].position.y = 0;
			loadElement.pVerticesPosition[v].position.z = 0;

			//	影響個数分ループ
			for (int n = 0; n < 4; n++)
			{
				if (loadElement.pVerticesWeightMemory[v].bone_weights[n] == 0.0f)
					continue;
				int b = loadElement.pVerticesWeightMemory[v].bone_indices[n];

				float x = loadElement.pVerticesTransformMemory[v].position.x;
				float y = loadElement.pVerticesTransformMemory[v].position.y;
				float z = loadElement.pVerticesTransformMemory[v].position.z;
				//	座標を影響力分移動
				auto ii = (x * KeyMatrix[b].m[0][0] + y * KeyMatrix[b].m[1][0] + z * KeyMatrix[b].m[2][0] + 1 * KeyMatrix[b].m[3][0]) * loadElement.pVerticesWeightMemory[v].bone_weights[n];
				loadElement.pVerticesPosition[v].position.x += ii;
				loadElement.pVerticesPosition[v].position.y += (x * KeyMatrix[b].m[0][1] + y * KeyMatrix[b].m[1][1] + z * KeyMatrix[b].m[2][1] + 1 * KeyMatrix[b].m[3][1]) * loadElement.pVerticesWeightMemory[v].bone_weights[n];
				loadElement.pVerticesPosition[v].position.z += (x * KeyMatrix[b].m[0][2] + y * KeyMatrix[b].m[1][2] + z * KeyMatrix[b].m[2][2] + 1 * KeyMatrix[b].m[3][2]) * loadElement.pVerticesWeightMemory[v].bone_weights[n];

				float nx = loadElement.pVerticesNormalMemory[v].normal.x;
				float ny = loadElement.pVerticesNormalMemory[v].normal.y;
				float nz = loadElement.pVerticesNormalMemory[v].normal.z;
				//	法線を影響力分変換
				loadElement.pVerticesNormal[v].normal.x += (nx * KeyMatrix[b].m[0][0] + ny * KeyMatrix[b].m[1][0] + nz * KeyMatrix[b].m[2][0]) * loadElement.pVerticesWeightMemory[v].bone_weights[n];
				loadElement.pVerticesNormal[v].normal.y += (nx * KeyMatrix[b].m[0][1] + ny * KeyMatrix[b].m[1][1] + nz * KeyMatrix[b].m[2][1]) * loadElement.pVerticesWeightMemory[v].bone_weights[n];
				loadElement.pVerticesNormal[v].normal.z += (nx * KeyMatrix[b].m[0][2] + ny * KeyMatrix[b].m[1][2] + nz * KeyMatrix[b].m[2][2]) * loadElement.pVerticesWeightMemory[v].bone_weights[n];
			}
		}
		GetDeviceContext->UpdateSubresource(p_VertexBuffer[0].Get(), 0, NULL, loadElement.pVerticesPosition.data(), 0, 0);
		GetDeviceContext->UpdateSubresource(p_VertexBuffer[1].Get(), 0, NULL, loadElement.pVerticesNormal.data(), 0, 0);
		//データクリア
		loadElement.VertexDatasClear();
	}
#else
	void LoadFbx::Skinning(ShaderInfo::BONE_TRANSFORM* boneTransForm, std::string motionName, float frame)const
	{
		// ボーンアニメーション
		auto itr = motion.find(motionName);
		if (itr == motion.end())
			return;
		//モーション名があったら
		const Motion* M = &itr->second;
		if (M == NULL)
			return;
		//	配列用変数
		int f = static_cast<int>(frame);
		//	行列準備
		for (int b = 0; b < this->allNumBones; b++)
		{
			//	行列補間
			//このフレームの時の、各ボーンのキーフレーム間隔での位置を出してる
			boneTransForm->var[b] = Skeltal[b].OffsetMatrix * MatrixInterporate(M->key[b][f], M->key[b][f + 1], frame - (int)frame);
		}
	}
#endif // VERTEX_USING_SKINING_USE_FLAG 

	

	void LoadFbx::LoadTexture(int meshNum, FbxSurfaceMaterial* material)
	{
		FbxProperty prop = material->FindProperty(FbxSurfaceMaterial::sDiffuse);

		//	テクスチャ読み込み
		const char* path = NULL;
		int fileTextureCount = prop.GetSrcObjectCount<FbxFileTexture>();
		if (fileTextureCount > 0)
		{
			FbxFileTexture* FileTex = prop.GetSrcObject<FbxFileTexture>(0);
			path = FileTex->GetFileName();
		}
		else
		{
			int numLayer = prop.GetSrcObjectCount<FbxLayeredTexture>();
			if (numLayer > 0)
			{
				FbxLayeredTexture* LayerTex = prop.GetSrcObject<FbxLayeredTexture>(0);
				FbxFileTexture* FileTex = LayerTex->GetSrcObject<FbxFileTexture>(0);
				path = FileTex->GetFileName();
			}
		}
		if (path == NULL) return;
		//  C:\\AAA\\BBB\\a.fbx  C:/AAA/BBB/a.fbx
		const char* name = &path[strlen(path)];
		for (int i = 0; i < (int)strlen(path); i++)
		{
			name--;
			if (name[0] == '/') { name++; break; }
			if (name[0] == '\\') { name++; break; }
		}
		char work[128];
		strcpy_s(work, 128, FBXDir);		//"AAA/BBB/";
	//	strcat(work, "texture/");	//"AAA/BBB/texture/"
		strcat_s(work, 128, name);			//"AAA/BBB/texture/a.png

		char filename[128];
		strcpy_s(filename, 128, work);
		
		this->meshs[meshNum].texture = call_externaly::TextureResourceManagerEx::GetInstance()->LoadTexture(filename);
		if (!this->meshs[meshNum].texture)
			assert(!"テクスチャ読み込みエラー");
	}

	void LoadFbx::LoadBone(const fbxsdk::FbxMesh& mesh, int& numBones)
	{
		auto& Bones = this->Skeltal;//メッシュのボーン格納用
		//名前からボーンの番号を見つける
		auto FindBone = [numBones, Bones](const char* name)
		{
			int bone = -1; // 見つからない
			for (int i = 0; i < numBones; i++)
			{
				if (name == Bones[i].name)
				{
					bone = i;
					break;
				}
			}
			return bone;
		};

		//	メッシュ頂点数
		int num = mesh.GetPolygonVertexCount();



		////	スキン情報の有無
		//int skinCount = mesh->GetDeformerCount(FbxDeformer::eSkin);
		//if (skinCount <= 0)
		//{
		//	LoadMeshAnim(mesh, meshNum);
		//	return;
		//}

		FbxSkin* skin = static_cast<FbxSkin*>(mesh.GetDeformer(0, FbxDeformer::eSkin));
		//	ボーン数
		int nBone = skin->GetClusterCount();
		//	全ボーン情報取得
		for (int bone = 0; bone < nBone; bone++)
		{
			//	ボーン情報取得
			FbxCluster* cluster = skin->GetCluster(bone);
			FbxAMatrix trans;
			cluster->GetTransformMatrix(trans);
			/*		trans.mData[0][1] *= -1;
					trans.mData[0][2] *= -1;
					trans.mData[1][0] *= -1;
					trans.mData[2][0] *= -1;
					trans.mData[3][0] *= -1;
			*/
			//	ボーン名取得
			const char* name = cluster->GetLink()->GetName();

			//	ボーン検索
			bool isNewBone = false;
			int bone_no = FindBone(name);
			if (bone_no < 0)
			{
				bone_no = numBones;//ボーンオフセット更新
				numBones++;
				isNewBone = true;
			}
			if (isNewBone)
			{
				Bones[bone_no].name = name;
				//	オフセット行列作成
				FbxAMatrix LinkMatrix;
				cluster->GetTransformLinkMatrix(LinkMatrix);
				/*			LinkMatrix.mData[0][1] *= -1;
							LinkMatrix.mData[0][2] *= -1;
							LinkMatrix.mData[1][0] *= -1;
							LinkMatrix.mData[2][0] *= -1;
							LinkMatrix.mData[3][0] *= -1;
				*/
				FbxAMatrix Offset = LinkMatrix.Inverse() * trans;
				FbxDouble* OffsetM = (FbxDouble*)Offset;

				//	オフセット行列保存
				for (int i = 0; i < 4; i++)
				{
					for (size_t i2 = 0; i2 < 4; i2++)
						Bones[bone_no].OffsetMatrix.m[i][i2] = (float)OffsetM[i * 4 + i2];
				}

				//	キーフレーム読み込み
				LoadKeyFrames("default", bone_no, cluster->GetLink());
			}


		}
	}



	void LoadFbx::LoadKeyFrames(std::string name, int bone, FbxNode* bone_node)
	{
		Motion& M = motion[name];
		// 空ならメモリを確保
		if (M.key.empty())
		{
			M.key.resize(this->allNumBones);
		}
		// メモリ確保
		M.key[bone].resize(M.NumFrame + 1);
		//初期化
		for (size_t i = 0, size = M.NumFrame + 1; i < size; i++)
		{
			M.key[bone][i].Identyfy();
		}
		
		//M->key[bone] =
		//	new Matrix[M->NumFrame + 1];

		// キーフレーム設定
		double time = StartFrame * (1.0 / 60);
		FbxTime T;
		for (int f = 0; f < M.NumFrame; f++)
		{
			T.SetSecondDouble(time);
			//	T秒の姿勢行列をGet
			FbxMatrix m = bone_node->EvaluateGlobalTransform(T);
			/*		m.mData[0][1] *= -1;// _12
					m.mData[0][2] *= -1;// _13
					m.mData[1][0] *= -1;// _21
					m.mData[2][0] *= -1;// _31
					m.mData[3][0] *= -1;// _41
			*/
			
			//FbxDouble* mat = reinterpret_cast<FbxDouble*>(m);
			for (int i = 0; i < 4; i++)
			{
				for (int i2 = 0; i2 < 4; i2++)
				{
					//M.key[bone][f].m[i][i2] = static_cast<float>(mat[i * 4 + i2]);
					M.key[bone][f].m[i][i2] = static_cast<float>(m.mData[i][i2]);
				}
			}

			time += 1.0 / 60.0;
		}
	}

	//	ボーンのないメッシュのアニメーション
	void LoadFbx::LoadMeshAnim(const FbxMesh& mesh, int numMesh, int numBone)
	{
		FbxNode* node = mesh.GetNode();

		int bone_no = numBone;
		auto n  = node->GetName();
		this->Skeltal[bone_no].name = node->GetName();

		//	オフセット行列（原点に移動させる行列）
		this->Skeltal[bone_no].OffsetMatrix.Identyfy();

		FbxSkin* skin = static_cast<FbxSkin*>(mesh.GetDeformer(0, FbxDeformer::eSkin));
		//	ボーン数
		int nBone = skin->GetClusterCount();
		//	キーフレーム読み込み
		LoadKeyFrames("default", bone_no, node);


	}




























	bool LoadFbx::LoadFbxElement::CreateVertexBuffer(Microsoft::WRL::ComPtr<ID3D11Buffer> vBuffer[], const int NUM_VRETEX)
	{
		HRESULT hr = S_OK;

		// 頂点バッファ定義
		D3D11_BUFFER_DESC Bufferdesk;
		ZeroMemory(&Bufferdesk, sizeof(Bufferdesk));

		Bufferdesk.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
#if VERTEX_USING_SKINING_USE_FLAG
		Bufferdesk.Usage = D3D11_USAGE_DEFAULT;
#endif
		Bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		Bufferdesk.CPUAccessFlags = 0;
		Bufferdesk.MiscFlags = 0;
		Bufferdesk.StructureByteStride = 0;//float?sizeof(DirectX::XMFLOAT3)

		 // サブリソースの初期化に使用されるデータを指定します。
		D3D11_SUBRESOURCE_DATA SubResourceData;
		ZeroMemory(&SubResourceData, sizeof(SubResourceData));

		SubResourceData.SysMemPitch = 0;					//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
		SubResourceData.SysMemSlicePitch = 0;				//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。
		
		
		
		int createCount = 0;//頂点バッファ生成回数(要素を順番に配列に格納するため)

		//それぞれのバーテックスバッファへデータを入れる
		for (size_t i = 0; i < static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END); i++)
		{
			bool exef = true;//頂点バッファ生成フラグ
			switch (i)
			{
			default:
				exef = false;
				break;
				case static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::POSITION) :
					Bufferdesk.ByteWidth = NUM_VRETEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::POSITION);
					SubResourceData.pSysMem = pVerticesPosition.data();
					break;
					case static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::NORMAL) :
						Bufferdesk.ByteWidth = NUM_VRETEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL);
						SubResourceData.pSysMem = pVerticesNormal.data();
						break;
						case static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::TEXCOORD) :
							Bufferdesk.ByteWidth = NUM_VRETEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD);
							SubResourceData.pSysMem = pVerticesTexcoord.data();
							break;
							case static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::MATRIAL) :
								Bufferdesk.ByteWidth = NUM_VRETEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::MATRIAL);
								SubResourceData.pSysMem = pVerticesMaterial.data();
								break;
								case static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::WEIGHT) :
									Bufferdesk.ByteWidth = NUM_VRETEX * sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::WEIGHT);
									SubResourceData.pSysMem = pVerticesWeight.data();
									break;
			}
			if (exef)
			{
				// バッファー (頂点バッファー、インデックス バッファー、またはシェーダー定数バッファー) を作成します。
				hr = GetDevice->CreateBuffer(&Bufferdesk, &SubResourceData, vBuffer[createCount].GetAddressOf());
				if (FAILED(hr))
				{
					assert(!"頂点バッファの作成ができません");
					return false;
				}
				createCount++;
			}

		}

		return true;
	}




	
		void LoadFbx::LoadFbxElement::LoadPosition(FbxMesh* mesh, int numVertices)
		{
			int* index = mesh->GetPolygonVertices();
			FbxVector4* source = mesh->GetControlPoints();
			// メッシュのトランスフォーム
			FbxVector4 T = mesh->GetNode()->GetGeometricTranslation(FbxNode::eSourcePivot);
			FbxVector4 R = mesh->GetNode()->GetGeometricRotation(FbxNode::eSourcePivot);
			FbxVector4 S = mesh->GetNode()->GetGeometricScaling(FbxNode::eSourcePivot);
			FbxAMatrix TRS = FbxAMatrix(T, R, S);
			//	全頂点変換
			for (int v = 0; v < mesh->GetControlPointsCount(); v++)
			{
				source[v] = TRS.MultT(source[v]);
			}

			// 頂点座標読み込み
			int num = mesh->GetPolygonVertexCount();

			for (int v = 0; v < num; v++)
			{
				int vindex = index[v];

				pVerticesPosition[v + numVertices].position.x = (float)source[vindex][0];
				pVerticesPosition[v + numVertices].position.y = (float)source[vindex][1];
				pVerticesPosition[v + numVertices].position.z = (float)source[vindex][2];

				pVerticesTexcoord[v + numVertices].texcoord.x = 0;
				pVerticesTexcoord[v + numVertices].texcoord.y = 0;
				pVerticesMaterial[v + numVertices].color = { 1,1,1,1 };
			}
		}

		void LoadFbx::LoadFbxElement::LoadNormal(FbxMesh* mesh, int numVertices)
		{
			FbxArray<FbxVector4> normal;
			mesh->GetPolygonVertexNormals(normal);
			for (int v = 0; v < normal.Size(); v++)
			{
				pVerticesNormal[v + numVertices].normal.x = (float)normal[v][0];
				pVerticesNormal[v + numVertices].normal.y = (float)normal[v][1];
				pVerticesNormal[v + numVertices].normal.z = (float)normal[v][2];
			}
		}

		void LoadFbx::LoadFbxElement::LoadUV(FbxMesh* mesh, int numVertices)
		{
			FbxStringList names;
			mesh->GetUVSetNames(names);
			FbxArray<FbxVector2> uv;
			mesh->GetPolygonVertexUVs(names.GetStringAt(0), uv);
			for (int v = 0; v < uv.Size(); v++)
			{
				pVerticesTexcoord[v + numVertices].texcoord.x = (float)(uv[v][0]);
				pVerticesTexcoord[v + numVertices].texcoord.y = (float)(1.0 - uv[v][1]);
			}
		}

		void LoadFbx::LoadFbxElement::LoadVertexColor(FbxMesh* mesh, int numVertices)
		{
			int vColorLayerCount = mesh->GetElementVertexColorCount();
			if (mesh->GetElementVertexColorCount() <= 0) return;
			//    頂点カラーレイヤー取得
			FbxGeometryElementVertexColor* element = mesh->GetElementVertexColor(0);

			//  保存形式の取得
			FbxGeometryElement::EMappingMode mapmode = element->GetMappingMode();
			FbxGeometryElement::EReferenceMode refmode = element->GetReferenceMode();

			//    ポリゴン頂点に対するインデックス参照形式のみ対応
			if (mapmode == FbxGeometryElement::eByPolygonVertex)
			{
				if (refmode == FbxGeometryElement::eIndexToDirect)
				{
					FbxLayerElementArrayTemplate<int>* index = &element->GetIndexArray();
					int indexCount = index->GetCount();
					for (int j = 0; j < indexCount; j++)
					{
						// FbxColor取得
						fbxsdk::FbxColor c = element->GetDirectArray().GetAt(index->GetAt(j));

						//頂点色情報格納
						pVerticesMaterial[j + numVertices].color.x = static_cast<float>(c.mRed);
						pVerticesMaterial[j + numVertices].color.y = static_cast<float>(c.mGreen);
						pVerticesMaterial[j + numVertices].color.z = static_cast<float>(c.mBlue);
						pVerticesMaterial[j + numVertices].color.w = static_cast<float>(c.mAlpha);
					}
				}
			}
		}



		void LoadFbx::LoadFbxElement::LoadWeight(FbxMesh* mesh, const std::vector<LoadFbx::BONE>& Skeltal, int& numBones, int numVertices)
		{
			auto& Bones = Skeltal;
			auto FindBone = [numBones, Bones](const char* name)
			{
				int bone = -1; // 見つからない
				for (int i = 0; i < numBones; i++)
				{
					if (name == Bones[i].name)
					{
						bone = i;
						break;
					}
				}
				return bone;
			};

			FbxSkin* skin =reinterpret_cast<FbxSkin*>(mesh->GetDeformer(0, FbxDeformer::eSkin));
			//	ボーン数
			int nBone = skin->GetClusterCount();


			//	全ボーン情報取得
			for (int bone = 0; bone < nBone; bone++)
			{
				//	ボーン情報取得
				FbxCluster* cluster = skin->GetCluster(bone);
				//FbxAMatrix trans;
				//cluster->GetTransformMatrix(trans);
				//	ウェイト読み込み
				int wgtcount = cluster->GetControlPointIndicesCount();
				int* wgtindex = cluster->GetControlPointIndices();
				double* wgt = cluster->GetControlPointWeights();

				int* index = mesh->GetPolygonVertices();
				//	メッシュ頂点数
				const int VERTEX_NUM = mesh->GetPolygonVertexCount();

				//	ボーン名取得
				const char* name = cluster->GetLink()->GetName();
				int bone_no = FindBone(name);

				for (int i = 0; i < wgtcount; i++)
				{
					int wgtindex2 = wgtindex[i];
					//	全ポリゴンからwgtindex2番目の頂点検索
					for (int v = 0; v < VERTEX_NUM; v++)
					{
						if (index[v] != wgtindex2) continue;
						//	頂点にウェイト保存
						int w = boneinfluencecount[v + numVertices];
						if (w >= 4)
						{
							continue;
						}
						pVerticesWeight[v + numVertices].bone_indices[w] = bone_no;
						pVerticesWeight[v + numVertices].bone_weights[w] = (float)wgt[i];
						boneinfluencecount[v + numVertices]++;
					}
				}
			}
		}

		void LoadFbx::LoadFbxElement::InitNonBoneWeight(FbxMesh* mesh, int& numBones, int numVertices)
		{
			//	ウェイト設定
			int num = mesh->GetPolygonVertexCount();
			for (int i = 0; i < num; i++)
			{
				pVerticesWeight[i + numVertices].bone_indices[0] = numBones;
				pVerticesWeight[i + numVertices].bone_weights[0] = 1.0f;
				boneinfluencecount[i + numVertices] = 1;
			}
			numBones++;
		}

		void LoadFbx::LoadFbxElement::OptimizeVertices(int& numVertices, unsigned int* indices)
		{
			int currentNum = 0;
			ShaderInfo::VERTEX_BUFFER_TYPE::POSITION* bufP = new ShaderInfo::VERTEX_BUFFER_TYPE::POSITION[numVertices];
			ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL* bufN = new ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL[numVertices];
			ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD* bufT = new ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD[numVertices];
			ShaderInfo::VERTEX_BUFFER_TYPE::MATRIAL* bufM = new ShaderInfo::VERTEX_BUFFER_TYPE::MATRIAL[numVertices];
			ShaderInfo::VERTEX_BUFFER_TYPE::WEIGHT* bufW = new ShaderInfo::VERTEX_BUFFER_TYPE::WEIGHT[numVertices];
			for (int i = 0; i < numVertices; i++)
			{
				bufP[i] = pVerticesPosition[i];
				bufN[i] = pVerticesNormal[i];
				bufT[i] = pVerticesTexcoord[i];
				bufM[i] = pVerticesMaterial[i];
				for (size_t i2 = 0; i2 < 4; i2++)
				{
					bufW[i].bone_indices[i2] = pVerticesWeight[i].bone_indices[i2];
					bufW[i].bone_weights[i2] = pVerticesWeight[i].bone_weights[i2];
				}
			}
			
			for (int v = 0; v < numVertices; v++)//頂点番号
			{
				int sameIndex = -1;//同じ頂点の番号
				//	同一頂点検索
				for (int old = 0; old < currentNum; old++)
				{
					if (bufP[v].position.x != bufP[old].position.x) continue;
					if (bufP[v].position.y != bufP[old].position.y) continue;
					if (bufP[v].position.z != bufP[old].position.z) continue;
					if (bufN[v].normal.x != bufN[old].normal.x) continue;
					if (bufN[v].normal.y != bufN[old].normal.y) continue;
					if (bufN[v].normal.z != bufN[old].normal.z) continue;
					if (bufT[v].texcoord.x != bufT[old].texcoord.x) continue;
					if (bufT[v].texcoord.y != bufT[old].texcoord.y) continue;
					if (bufM[v].color.x != bufM[old].color.x) continue;
					if (bufM[v].color.y != bufM[old].color.y) continue;
					if (bufM[v].color.z != bufM[old].color.z) continue;
					/*for (size_t i = 0; i < 4; i++)
					{
						if (pVerticesWeight[v].bone_indices[i] != pVerticesWeight[old].bone_indices[i]) continue;
						if (pVerticesWeight[v].bone_weights[i] != pVerticesWeight[old].bone_weights[i]) continue;
					}*/

					sameIndex = old;//同じ頂点だった
					break;
				}

				int target = v;//同じインデックスの番号
				if (sameIndex == -1)//違う頂点だった
				{
					//pVerticesTransform[old];
					////	新規頂点
					CopyMemory(&bufP[currentNum], &bufP[v], sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::POSITION));
					CopyMemory(&bufN[currentNum], &bufN[v], sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::NORMAL));
					CopyMemory(&bufT[currentNum], &bufT[v], sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD));
					CopyMemory(&bufM[currentNum], &bufM[v], sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::MATRIAL));
					CopyMemory(&bufW[currentNum], &bufW[v], sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::WEIGHT));
					/*for (size_t i2 = 0; i2 < 4; i2++)
					{
						pVerticesWeight[currentNum].bone_indices[i2] = bufW[v].bone_indices[i2];
						pVerticesWeight[currentNum].bone_weights[i2] = bufW[v].bone_weights[i2];
					}*/
					target = currentNum;
					currentNum++;
				}
				else
				{
					target = sameIndex;
				}
				//	インデックス更新
				for (int i = 0; i < numVertices; i++)
				{
					if (indices[i] == v) 
						indices[i] = target;
				}
				
			}
			for (int i = 0; i < currentNum; i++)
			{
				pVerticesPosition[i] = bufP[i];
				pVerticesNormal[i] = bufN[i];
				pVerticesTexcoord[i] = bufT[i];
				pVerticesMaterial[i] = bufM[i];
				for (size_t i2 = 0; i2 < 4; i2++)
				{
					pVerticesWeight[i].bone_indices[i2] = bufW[i].bone_indices[i2];
					pVerticesWeight[i].bone_weights[i2] = bufW[i].bone_weights[i2];
				}
			}
			//	新バッファ確保
			pVerticesPosition.resize(currentNum );
			pVerticesNormal.resize(currentNum);
			pVerticesTexcoord.resize(currentNum);
			pVerticesMaterial.resize(currentNum );
			pVerticesWeight.resize(currentNum );
			numVertices = currentNum;
			delete[] bufP;
			delete[] bufN;
			delete[] bufT;
			delete[] bufM;
			delete[] bufW;
		}

		void LoadFbx::LoadFbxElement::WeightNormlize(int numVerTces)
		{
			//	ウェイト正規化
			// ５本以上にまたっがてる場合のため
			for (int v = 0; v < numVerTces; v++)
			{
				float n = 0;
				//	頂点のウェイトの合計値
				for (int w = 0; w < boneinfluencecount[v]; w++)
				{
					n += pVerticesWeight[v].bone_weights[w];
				}
				//	正規化
				for (int w = 0; w < boneinfluencecount[v]; w++)
				{
					pVerticesWeight[v].bone_weights[w] /= n;
				}
			}
		}



		}//Lib_Render

}//Lib_3D


