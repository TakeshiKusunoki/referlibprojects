// UNIT12
#include "ObjAtMeshInit.h"
//#include "misc.h"
//#include <fstream>
//#include "ResourceManager.h"
//#include "texture.h"


namespace Lib_3D {

	ObjAtMeshInit::ObjAtMeshInit(ID3D11Device * p_Device, const wchar_t * obj_filename, bool flipping_v_coordinates/*UNIT13*/)
	{

		// ファイルロード--------------------------------
		{
			ObjLoad loder(&resourceManager);
			mesh = loder.loadObjFile(p_Device, obj_filename, flipping_v_coordinates);
		}



		// 設定--------------------------------------
		HRESULT hr = S_OK;

		/////////////////////////////////////////////////
		// �@頂点データの構造を記述(記載した情報をIAステージに伝える)
		/////////////////////////////////////////////////
		D3D11_INPUT_ELEMENT_DESC InputElementDesk[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD",0,DXGI_FORMAT_R32G32_FLOAT,0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "AT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		const UINT ELEMENTS_ARRAY_NUM = ARRAYSIZE(InputElementDesk);
		/////////////////////////////////////////////////
		// �Aバーテックスシェーダーオブジェクトの生成
		/////////////////////////////////////////////////
		bool f = true;
		f = resourceManager.LoadVertexShader(p_Device, "Shader\\Static_mesh_vs.cso", InputElementDesk, ELEMENTS_ARRAY_NUM, &p_VertexShader, &p_InputLayout);
		if (!f)
		{
			assert(!"データが見つからなかった");
		}
		/////////////////////////////////////////////////
		// �Bピクセルシェーダーオブジェクトの生成
		/////////////////////////////////////////////////
		f = resourceManager.LoadPixelShader(p_Device, "Shader\\Static_mesh_ps.cso", &p_PixelShader);
		if (!f)
		{
			assert(!"データが見つからなかった");
		}
		////////////////////////////////////////////////////////
		// �Cラスタライザーステートオブジェクトの生成（線描画・塗りつぶし描画)
		////////////////////////////////////////////////////////
		D3D11_RASTERIZER_DESC RasteriserDesk;
		ZeroMemory(&RasteriserDesk, sizeof(D3D11_RASTERIZER_DESC));
		//-線描画の場合
		RasteriserDesk.FillMode = D3D11_FILL_WIREFRAME;	//レンダリング時に使用する描画モードを決定します
		RasteriserDesk.CullMode = D3D11_CULL_BACK;		//特定の方向を向いている三角形の描画の有無を示します。
		RasteriserDesk.FrontCounterClockwise = TRUE;	//三角形が前向きか後ろ向きかを決定します。
		RasteriserDesk.DepthBias = 0;					//指定のピクセルに加算する深度値です。
		RasteriserDesk.DepthBiasClamp = 0;				//ピクセルの最大深度バイアスです。
		RasteriserDesk.SlopeScaledDepthBias = 0;		//指定のピクセルのスロープに対するスカラです。
		RasteriserDesk.DepthClipEnable = FALSE;			//距離に基づいてクリッピングを有効にします。
		RasteriserDesk.ScissorEnable = FALSE;			//シザーカリング
		RasteriserDesk.MultisampleEnable = FALSE;		//マルチサンプリングのアンチエイリアシング
		RasteriserDesk.AntialiasedLineEnable = TRUE;	//線のアンチエイリアシング(線を描画中で MultisampleEnable が false の場合にのみ)
		hr = p_Device->CreateRasterizerState(&RasteriserDesk, &p_RasterizerStateLine);
		if (FAILED(hr))
		{
			assert(!"ラスタライザーステートオブジェクトの生成ができません");
			return;
		}

		//-塗りつぶし描画の場合
		RasteriserDesk.FillMode = D3D11_FILL_SOLID;		//レンダリング時に使用する描画モードを決定します
		RasteriserDesk.CullMode = D3D11_CULL_BACK;		//特定の方向を向いている三角形の描画の有無を示します。
		RasteriserDesk.FrontCounterClockwise = FALSE;	//三角形が前向きか後ろ向きかを決定します。
		RasteriserDesk.DepthClipEnable = FALSE;			//距離に基づいてクリッピングを有効にします。
		RasteriserDesk.MultisampleEnable = TRUE;		//マルチサンプリングのアンチエイリアシング
		RasteriserDesk.AntialiasedLineEnable = FALSE;	//線のアンチエイリアシング(線を描画中で MultisampleEnable が false の場合にのみ)
		hr = p_Device->CreateRasterizerState(&RasteriserDesk, &p_RasterizerStatePaint);
		if (FAILED(hr))
		{
			assert(!"ラスタライザーステートオブジェクト(塗りつぶし描画)の生成ができません");
			return;
		}

		/////////////////////////////////////////////////
		// �D深度ステンシル ステート オブジェクトの生成
		/////////////////////////////////////////////////
		D3D11_DEPTH_STENCIL_DESC DepthDesc;
		ZeroMemory(&DepthDesc, sizeof(DepthDesc));
		DepthDesc.DepthEnable = true;									//深度テストあり
		DepthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;			//書き込む
		DepthDesc.DepthFunc = D3D11_COMPARISON_LESS;					//手前の物体を描画
		DepthDesc.StencilEnable = FALSE;								//ステンシル テストなし
		DepthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;		//ステンシル読み込みマスク
		DepthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;	//ステンシル書き込みマスク
																		//面が表を向いている場合のステンシルステートの設定
		DepthDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
		DepthDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
																		  //面が裏を向いている場合のステンシルステートの設定
		DepthDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
		DepthDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
		DepthDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
		hr = p_Device->CreateDepthStencilState(&DepthDesc, &p_DepthStencilState);	//
		if (FAILED(hr))
		{
			assert(!"深度ステンシル ステート オブジェクトの生成ができません");
			return;
		}

		// UNIT.13
		// シェーダリソースビューのロード
		D3D11_TEXTURE2D_DESC Texture2dDesc;
		///f=ResourceManager::LoadShaderResourceView(p_Device, texture_filename.c_str(), &p_ShaderResourceView, &Texture2dDesc);
		for (auto& it : mesh.materials)
		{
			f = resourceManager.LoadShaderResourceView(p_Device, it.map_Kd.c_str(), &it.ShaderResourceView, &Texture2dDesc);
			if (!f)
			{
				assert(!"データが見つからなかった");
			}
		}

		// UNIT.13
		/////////////////////////////////////////////////
		// サンプラーステートオブジェクトの設定（テクスチャの描画）
		/////////////////////////////////////////////////
		D3D11_SAMPLER_DESC SamplerDesc;
		SamplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;		 //異方性フィルタリング
		SamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;	 //「ラップ・テクスチャ」アドレシング・モード
		SamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;	 //
		SamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;	 //
		SamplerDesc.MipLODBias = 0.0f;						//ミップマップの詳細レベル
		SamplerDesc.MaxAnisotropy = 16;					//異方性フィルタリングの次数
		SamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER; //
		memcpy(SamplerDesc.BorderColor, &DirectX::XMFLOAT4(0, 0, 0, 0), sizeof(DirectX::XMFLOAT4));
		/*SamplerDesc.BorderColor[0] = 0.0f;
		SamplerDesc.BorderColor[1] = 0.0f;
		SamplerDesc.BorderColor[2] = 0.0f;
		SamplerDesc.BorderColor[3] = 0.0f;*/
		SamplerDesc.MinLOD = 0;						 //0が最大で最も精細//-FLT_MAX;
		SamplerDesc.MaxLOD = FLT_MAX;

		// サンプラー・ステート・オブジェクトの作成
		hr = p_Device->CreateSamplerState(&SamplerDesc, &p_SampleState);
		if (FAILED(hr))
		{
			assert(!"サンプラー・ステート オブジェクトの生成ができません");
			return;
		}

	}

#define DELETE_IF(x) if(x){delete x;}
#define RELEASE_IF(x) if(x){x->Release();}
	ObjAtMeshInit::~ObjAtMeshInit()
	{
		RELEASE_IF(p_SampleState);// UNIT13
		RELEASE_IF(p_DepthStencilState);
		RELEASE_IF(p_RasterizerStateLine);
		RELEASE_IF(p_RasterizerStatePaint);
		RELEASE_IF(mesh.p_VertexBuffer.Get());
		RELEASE_IF(mesh.p_IndexBuffer.Get());
		resourceManager.ReleasePixelShader(p_PixelShader);
		resourceManager.ReleaseVertexShader(p_VertexShader, p_InputLayout);
		for (auto& it : mesh.materials)
		{
			resourceManager.ReleaseShaderResourceView(it.ShaderResourceView);// UNIT13
		}
		resourceManager.Release();
	}
#undef RELEASE_IF
#undef DELETE_IF



}
