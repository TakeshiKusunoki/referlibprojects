#include "ObjLoad.h"
#include "misc.h"
#include <fstream>
#include "ResourceManager.h"
#include "texture.h"

namespace Lib_3D {

	ObjLoad::ObjLoad(ResourceManager*const resourceManager)
	{
		this->resourceManager = resourceManager;
	}

#define DELETE_IF(x) if(x){delete x;}
#define RELEASE_IF(x) if(x){x->Release();}
	ObjLoad::~ObjLoad()
	{
	}
#undef RELEASE_IF
#undef DELETE_IF





	void ObjLoad::create_buffers(ID3D11Device* p_Device, MESH* const mesh, VERTEX* vertices, const int NUM_VRETEX, const UINT* indices, const int NUM_INDEX)
	{
		HRESULT hr = S_OK;

		//! 頂点リスト設計図を外へ渡すためにに格納
		for (size_t i = 0; i < NUM_VRETEX; i++)
		{
			//at初期化
			vertices[i].at = DirectX::XMFLOAT3(0, 0, 0);
			mesh->vertexList.push_back(vertices[i]);
		}

		/////////////////////////////////////////////////
		// �F頂点バッファオブジェクトの生成
		//////////////////////////////////////////////////
		// �E頂点情報・インデックス情報のセット// 一辺が 1.0 の正立方体データを作成する（重心を原点にする

		// 頂点バッファ定義
		D3D11_BUFFER_DESC Bufferdesk;
		ZeroMemory(&Bufferdesk, sizeof(Bufferdesk));
		Bufferdesk.ByteWidth = NUM_VRETEX * sizeof(VERTEX);
		Bufferdesk.Usage = D3D11_USAGE_DYNAMIC;	//GPUのみ
		Bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		Bufferdesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		Bufferdesk.MiscFlags = 0;
		Bufferdesk.StructureByteStride = 0;//float?sizeof(DirectX::XMFLOAT3)

		// サブリソースの初期化に使用されるデータを指定します。
		D3D11_SUBRESOURCE_DATA SubResourceData;
		ZeroMemory(&SubResourceData, sizeof(SubResourceData));
		SubResourceData.pSysMem = vertices;				//(バッファの初期値)初期化データへのポインターです。
		SubResourceData.SysMemPitch = 0;					//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
		SubResourceData.SysMemSlicePitch = 0;				//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。
															// バッファー (頂点バッファー、インデックス バッファー、またはシェーダー定数バッファー) を作成します。
		hr = p_Device->CreateBuffer(&Bufferdesk, &SubResourceData, &mesh->p_VertexBuffer);
		if (FAILED(hr))
		{
			assert(!"頂点バッファの作成ができません");
			return;
		}

		///////////////////////////////////////////////////
		// �Gインデックスバッファオブジェクトの生成
		///////////////////////////////////////////////////

		// インデックスバッファの定義
		D3D11_BUFFER_DESC IndexBufferDesc;
		ZeroMemory(&IndexBufferDesc, sizeof(IndexBufferDesc));
		IndexBufferDesc.ByteWidth = NUM_INDEX * sizeof(UINT);
		IndexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
		IndexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		IndexBufferDesc.CPUAccessFlags = 0;
		IndexBufferDesc.MiscFlags = 0;
		IndexBufferDesc.StructureByteStride = 0;
		//インデックスの数を補完
		//numIndices = NUM_INDEX;

		// インデックス・バッファのサブリソースの定義
		D3D11_SUBRESOURCE_DATA IndexSubResource;
		ZeroMemory(&IndexSubResource, sizeof(IndexSubResource));
		IndexSubResource.pSysMem = indices;
		IndexSubResource.SysMemPitch = 0;
		IndexSubResource.SysMemSlicePitch = 0;

		// インデックス・バッファの作成
		hr = p_Device->CreateBuffer(&IndexBufferDesc, &IndexSubResource, &mesh->p_IndexBuffer);
		if (FAILED(hr))
		{
			assert(!"インデックス・バッファの作成ができません");
			return;
		}
	}

	// objファイルのロード
	const ObjLoad::MESH& ObjLoad::loadObjFile(ID3D11Device * p_Device, const wchar_t * obj_filename, bool flipping_v_coordinates/*UNIT13*/)
	{
		MESH mesh;//! 外へ渡すデータ

		std::vector<std::wstring> mtl_filenames;//! UNIT13追加
		//+ ロード--------------------------------
		{
			std::vector<VERTEX> vertices;
			std::vector<u_int> indices;
			u_int current_index = 0;

			std::vector<DirectX::XMFLOAT3> positions;
			std::vector<DirectX::XMFLOAT2> texcoords;//! UNIT13追加
			std::vector<DirectX::XMFLOAT3> normals;


			///////////////////////////
			// OBJファイルロード
			///////////////////////////
			std::wifstream fin(obj_filename);
			_ASSERT_EXPR(fin, L"OBJ file not found.");
			wchar_t command[256];
			while (fin)
			{
				fin >> command;
				if (0 == wcscmp(command, L"v"))			//頂点位置情報読み取り
				{
					float x, y, z;
					fin >> x >> y >> z;
					positions.push_back(DirectX::XMFLOAT3(x, y, z));
					fin.ignore(1024, L'\n');//? L
				}
				// UNIT.13
				else if (0 == wcscmp(command, L"vt"))		//uv座標読み取り
				{
					float u, v;
					fin >> u >> v;
					texcoords.push_back(DirectX::XMFLOAT2(u, flipping_v_coordinates ? 1.0f - v : v));
					fin.ignore(1024, L'\n');
				}
				else if (0 == wcscmp(command, L"vn"))		//法線情報読み取り
				{
					FLOAT i, j, k;
					fin >> i >> j >> k;
					normals.push_back(DirectX::XMFLOAT3(i, j, k));
					fin.ignore(1024, L'\n');
				}
				else if (0 == wcscmp(command, L"f"))		//頂点番号情報読み取り//(リソースの番号情報読み取り)
				{
					static u_int index = 0;
					for (u_int i = 0; i < 3; i++)
					{
						VERTEX vertex;
						u_int v, vt, vn;

						fin >> v;
						vertex.position = positions[v - 1];
						if (L'/' == fin.peek())
						{
							fin.ignore();
							if (L'/' != fin.peek())
							{
								fin >> vt;
								// UNIT.13
								vertex.texcoord = texcoords[vt - 1];
							}
							if (L'/' == fin.peek())
							{
								fin.ignore();
								fin >> vn;
								///if (vn > 100)vn = 1;
								vertex.normal = normals[vn - 1];

							}
						}
						vertices.push_back(vertex);
						indices.push_back(current_index++);
					}
					fin.ignore(1024, L'\n');
				}
				// UNIT.13
				else if (0 == wcscmp(command, L"mtllib"))	//mtlファイル名読み取り
				{
					wchar_t mtllib[256];
					fin >> mtllib;
					mtl_filenames.push_back(mtllib);
				}
				// UNIT.14
				else if (0 == wcscmp(command, L"usemtl"))	// Question 使うmtlをサブセット構造体にセット
				{
					wchar_t usemtl[MAX_PATH] = { 0 };
					fin >> usemtl;

					SUBSET CurrentSubset = {};
					CurrentSubset.usemtl = usemtl;
					CurrentSubset.index_start = (u_int)indices.size();
					mesh.subset.push_back(CurrentSubset);
				}
				else
				{
					fin.ignore(1024, L'\n');
				}
			}
			fin.close();

			// バッファ生成
			create_buffers(p_Device, &mesh, vertices.data(), (int)vertices.size(), indices.data(), (int)indices.size());

			// UNIT.14
			std::vector<SUBSET>::reverse_iterator iterator = mesh.subset.rbegin();
			iterator->index_count = (u_int)indices.size() - iterator->index_start;
			for (iterator = mesh.subset.rbegin() + 1; iterator != mesh.subset.rend(); ++iterator)
			{
				iterator->index_count = (iterator - 1)->index_start - iterator->index_start;
			}
		}





		// UNIT.13
		//////////////////////////
		// テクスチャの読み込み
		/////////////////////////
		{
			std::wstring texture_filename;
			wchar_t mtl_filenameCopy[256];//mtlファイル名
										  ///const wchar_t FILENAME[] = L"Mr.Incredible.mtl";
										  // .objのファイル名と.mtlのファイル名を結合
			CombineResourcePath(mtl_filenameCopy, obj_filename, mtl_filenames[0].c_str());
			std::wifstream fin2(mtl_filenameCopy);
			_ASSERT_EXPR(fin2, L"MTL file not found.");

			wchar_t command2[256] = { 0 };
			while (fin2)
			{
				fin2 >> command2;
				if (0 == wcscmp(command2, L"#"))			//mtlファイルのコメント文を破棄
				{
					fin2.ignore(1024, L'\n');
				}
				else if (0 == wcscmp(command2, L"map_Kd"))	//テクスチャのカラー情報読み取り(マテリアルの拡散反射率にリンクされる)
				{
					fin2.ignore();
					wchar_t map_Kd[256];
					fin2 >> map_Kd;
					CombineResourcePath(map_Kd, obj_filename, map_Kd);
					//texture_filename = map_Kd;//delete UNIT.14
					mesh.materials.rbegin()->map_Kd = map_Kd;
					fin2.ignore(1024, L'\n');
				}
				// UNIT.14
				else if (0 == wcscmp(command2, L"newmtl"))	//マテリアル名の記述読み取り
				{
					fin2.ignore();
					wchar_t newmtl[256];
					MATERIAL materialCopy;
					fin2 >> newmtl;
					materialCopy.newmtl = newmtl;
					mesh.materials.push_back(materialCopy);
				}
				// UNIT.14
				else if (0 == wcscmp(command2, L"Ka"))		//アンビエント反射rgb値読み取り
				{
					//反射率 色
					float r, g, b;
					fin2 >> r >> g >> b;
					mesh.materials.rbegin()->Ka = DirectX::XMFLOAT3(r, g, b);
					fin2.ignore(1024, L'\n');
				}
				// UNIT.14
				else if (0 == wcscmp(command2, L"Kd"))		//減光反射の色読み取り
				{
					float r, g, b;
					fin2 >> r >> g >> b;
					mesh.materials.rbegin()->Kd = DirectX::XMFLOAT3(r, g, b);
					fin2.ignore(1024, L'\n');
				}
				// UNIT.14
				else if (0 == wcscmp(command2, L"Ks"))		//鏡面反射率を指定読み取り
				{
					float r, g, b;
					fin2 >> r >> g >> b;
					mesh.materials.rbegin()->Ks = DirectX::XMFLOAT3(r, g, b);
					fin2.ignore(1024, L'\n');
				}
				// UNIT.14
				else if (0 == wcscmp(command2, L"illum"))		//照明モデル読み取り(イルミネーションモデル)
				{
					u_int illum;
					fin2 >> illum;
					mesh.materials.rbegin()->illum = illum;
					fin2.ignore(1024, L'\n');
				}
				else//未実装または認識できないコマンド
				{
					fin2.ignore(1024, L'\n');
				}
			}
		}

		return mesh;

	}




}
