#pragma once

#include <d3d11.h>
#include <wrl.h>
#include <DirectXMath.h>
#include <vector>


namespace Lib_3D {
	class ResourceManager;

	class ObjLoad
	{
	public:
		struct SUBSET
		{
			std::wstring usemtl;
			UINT index_start = 0;
			UINT index_count = 0;
		};

		//! 色
		struct MATERIAL
		{
			std::wstring newmtl;
			DirectX::XMFLOAT3 Ka = { 0.2f,0.2f,0.2f };
			DirectX::XMFLOAT3 Kd = { 0.8f,0.8f,0.8f };
			DirectX::XMFLOAT3 Ks = { 1.0f,1.0f,1.0f };
			UINT illum = 1;
			std::wstring map_Kd;
			ID3D11ShaderResourceView* ShaderResourceView;
		};
		// --------------------------------------

		//! @breif 頂点バッファ
		struct VERTEX
		{
			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT3 normal;
			DirectX::XMFLOAT2 texcoord;
			DirectX::XMFLOAT3 at;//! 外で変える(mup unmupを使う)
		};

		//! 外に渡すデータ
		struct MESH
		{
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_VertexBuffer;//（頂点バッファ
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_IndexBuffer;//（インデックバッファ
			std::vector<SUBSET> subset;
			std::vector<MATERIAL> materials;
			std::vector<VERTEX> vertexList;//! 頂点リストの設計書(at用)
		};
	private:
		static ResourceManager* resourceManager;//リソースマネージャー
		//int numIndices;//インデックスの数

	public:
	public:
		ObjLoad(ResourceManager*const resourceManager);
		virtual ~ObjLoad();

		//objファイルのロード
		//! @param[in] obj_filename ファイル名
		//! @param[in] flipping_v_coordinates
		const MESH& loadObjFile(ID3D11Device * p_Device, const const wchar_t * obj_filename, bool flipping_v_coordinates = false);

	private:
		//引数
		//p_Device	:	デバイス
		//mesh : 外へ渡すデータ
		//VERTEX3D* vertices:頂点
		//const int NUM_VRETEX:超点数
		//UINT* indices:頂点番号
		//const int NUM_INDEX:頂点番号数
		void ObjLoad::create_buffers(ID3D11Device* p_Device, MESH* const mesh, VERTEX* vertices, const int NUM_VRETEX, const UINT* indices, const int NUM_INDEX);





	};
}
