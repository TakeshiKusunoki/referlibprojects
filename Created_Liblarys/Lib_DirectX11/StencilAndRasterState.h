#pragma once
#include <d3d11.h>
namespace Lib_3D
{

	//! ラスタライザーステートオブジェクトの生成,深度ステンシル ステート オブジェクトの生成
	//! いろんなタイプ
	class StencilAndRasterState
	{
	public:
		//! @param[out] ラスタライザーステート
		void CreateDefaultRasutrizerState(ID3D11RasterizerState** pRasterizerState);
		//! @param[out] ラスタライザーステート(モデルを線で描画)
		void CreateLineRasutrizerState(ID3D11RasterizerState** pRasterizerState);
		//! @param[out] ステンシルステート
		void CreateDefaultStencilState(ID3D11DepthStencilState** pDepthStencilState);

		//! @param[in] ラスタライザーステートセット
		void SetRastarizerState(ID3D11RasterizerState* pRasterizerState);
		//! @param[in] ステンシルステートセット
		void SetDepthStencilState(ID3D11DepthStencilState* pDepthStencilState);
	};

}
