#pragma once
/**
* @file ShaderInfo.h
* @brief　シェーダーに対応するためのクラス
* @details シェーダーを切り替えるとコンスタントバッファも対応する
* *シェーダーの仕様が変われば、fbxLoderのコードも対応させる
*/
#include <d3d11.h>
#include <Lib_Math/vector_const.h>
#include <Lib_Math/Matrix.h>
#include <string>
#include <array>
#include <wrl.h>
#include "Lib_DirectX11/DirectX11Init.h"
#include <cassert>
#include <Lib_Template/Swallow.h>
//#include <vector>
//! @def SHADER_NAME0
//! @brief シェーダーファイルタイプ名()
#define SHADER_NAME0 skinned_mesh
#define SHADER_NAME1 base
#define SHADER_NAME2 baseGeometry

#define STRING(str) #str
#define TOSTRING(str) STRING(str)

#include <variant>



namespace Lib_3D
{
	namespace ShaderInfo
	{
		//! 頂点バッファそれぞれの構造体
		struct VERTEX_BUFFER_TYPE final
		{
			//! 頂点座標
			//! ０番目の頂点バッファ構造体
			struct POSITION
			{
				Lib_Math::VECTOR3_CONST position;
			};
			//! 法線
			//! 1番目の頂点バッファ構造体
			struct NORMAL
			{
				Lib_Math::VECTOR3_CONST normal;
			};
			//! テクスチャ位置
			//! 2番目の頂点バッファ構造体
			struct TEXCOORD
			{
				Lib_Math::VECTOR2 texcoord;
			};

			//!	メッシュの材質
			//! 3番目の頂点バッファ構造体
			struct MATRIAL
			{
				Lib_Math::VECTOR4_CONST color;//頂点カラー
			};

			//!	そのメッシュのボーンウェイト関連
			//! 4番目の頂点バッファ構造体
			struct WEIGHT
			{
				static constexpr int MAX_BONE_INFLUENCES_ = 4;//ひとつの頂点が影響を受ける最大ボ−ン
				FLOAT bone_weights[MAX_BONE_INFLUENCES_] = { 1,0,0,0 };
				INT bone_indices[MAX_BONE_INFLUENCES_] = {};
				//int count;
			};

		};

		//! シェーダー名インデックス
		enum class SHADER_FILENAME_INFO_INDEX
		{
			VERTEX,//バーテックスシェーダー
			PIXEL,//ピクセルシェーダー
			GEOMETRY,//ジオメトリシェーダ
			HULL,//ハルシェーダー
			END//終わり
		};

		//! バーテックスバッファのインデックス
		enum class VBUFFER_TYPE_INDEX
		{
			POSITION = 0,//頂点座標
			NORMAL,//法線
			TEXCOORD,//テクスチャ位置
			MATRIAL,//メッシュの材質
			WEIGHT,//ボーンウェイト
			END//終わり
		};
		inline static constexpr size_t VERTEX_BUFFER_NUM = static_cast<size_t>(VBUFFER_TYPE_INDEX::END);//頂点バッファ数
		
		//! コンスタントバッファをどこで使うかのインデックス
		enum class CBUFFER_WHERE_USE_INDEX
		{
			PLUS = 1,
			START = 0,
			VERTEX = START,//バーテックスシェーダー
			PIXEL,//ピクセルシェーダー
			GEOMETRY,//ジオメトリシェーダ
			HULL,//ハルシェーダー
			END//終わり
		};

		enum class RENDER_TYPE
		{
			OBJ3D = 0,
			FBXOBJ,
			XVERTEX,
			LIVE2D,
			OBJ4D,
			OBJAT,
			MULTI_RENDER_TARGET,//マルチレンダーターゲット
			END
		};

		//! @brief シェーダーの定義基底クラス
		//これらディファインはこのファイルのテンプレート用
		template<typename CONSTANT_BUFFER_CLASS, size_t N, typename STRUCT_ELEMENT_TYPE_TUPLE>////CONSTANT_BUFFER_CLASS コンスタントバッファ構造体 //N インプットエレメントの配列数 //STRUCT_ELEMENT_TYPE_TUPLE コンスタントバッファ構造体のメンバのタプル
		class ShaderDefine
		{
		public:
			using TYPE = CONSTANT_BUFFER_CLASS;
			using TUPLE_T = STRUCT_ELEMENT_TYPE_TUPLE;
			virtual ~ShaderDefine() {}
			//シェーダー名
#define DEFINE_ELEM_shadername shadername
			static const char* DEFINE_ELEM_shadername;
			//インプットエレメント
#define DEFINE_ELEM_InputElementDesk InputElementDesk
			static const D3D11_INPUT_ELEMENT_DESC DEFINE_ELEM_InputElementDesk[N];
			//インプットエレメント要素数
#define DEFINE_ELEM_ARRAY_NUM ARRAY_NUM
			static constexpr const size_t DEFINE_ELEM_ARRAY_NUM = N;
			//cso使うか?
#define DEFINE_ELEM_csoUseFlag csoUseFlag
			static const bool DEFINE_ELEM_csoUseFlag;
			//使うシェーダーの種類
#define DEFINE_ELEM_SHADER_USE_FLAG SHADER_USE_FLAG 
			static const bool DEFINE_ELEM_SHADER_USE_FLAG[static_cast<size_t>(SHADER_FILENAME_INFO_INDEX::END)];
			//使うバーテックスバッファの種類
#define DEFINE_ELEM_USE_VBUFFER_FLAG USE_VBUFFER_FLAG
			static const bool DEFINE_ELEM_USE_VBUFFER_FLAG[static_cast<size_t>(VBUFFER_TYPE_INDEX::END)];//VBUFFER_TYPE_INDEXを参照
			//コンスタントバッファをどこに送るか
#define DEFINE_ELEM_SEND_CBUFFER_FLAG SEND_CBUFFER_FLAG
			static const bool DEFINE_ELEM_SEND_CBUFFER_FLAG[static_cast<size_t>(CBUFFER_WHERE_USE_INDEX::END)];// CBUFFER_WHEREUSE_INDEXを参照
			//どのレンダーで使うか
#define DEFINE_ELEM_renderType renderType
			static const RENDER_TYPE DEFINE_ELEM_renderType;
		};
		//構造体要素tupleテンプレート
		template<typename T, typename tTuple>
		struct StructElementTemplate
		{
			virtual ~StructElementTemplate() {}
			using TYPE = T;//シェーダータイプ
			//static const size_t NUM = N;//
			tTuple tup;
		};
		//-----------------------------------------------------------------------------------
		//コンスタントバッファ構造体変数の型
		// name 判別用, type 判別用、 var コンスタントバッファ要素型
		//! ワールド・ビュー・プロジェクション
		struct WVP
		{
			static constexpr const char* name = TOSTRING(WVP);//他と被らないように
			using type = WVP;
			Lib_Math::MATRIX var;
		};
		//! ワールド変換行列
		struct WORLD
		{
			static constexpr const char* name = TOSTRING(WORLD);
			using type = WORLD;
			Lib_Math::MATRIX var;
		};
		//! プロジェクション変換行列
		struct PROJECTION
		{
			static constexpr const char* name = TOSTRING(PROJECTION);
			using type = PROJECTION;
			Lib_Math::MATRIX var;
		};
		//! ビュー変換行列
		struct VIEW
		{
			static constexpr const char* name = TOSTRING(VIEW);
			using type = VIEW;
			Lib_Math::MATRIX var;
		};
		//! 光方向
		struct LIGHT_DIRECTION
		{
			static constexpr const char* name = TOSTRING(LIGHT_DIRECTION);
			using type = LIGHT_DIRECTION;
			Lib_Math::VECTOR4 var;
		};
		//! カメラ位置
		struct CAMERA_POSISION
		{
			static constexpr const char* name = TOSTRING(CAMERA_POSISION);
			using type = CAMERA_POSISION;
			Lib_Math::VECTOR4 var;
		};
		//! 光色
		struct LIGHT_CALOR
		{
			static constexpr const char* name = TOSTRING(LIGHT_CALOR);
			using type = LIGHT_CALOR;
			Lib_Math::VECTOR4 var;
		};
		//! 自然光
		struct NYUTORAL_COLOR
		{
			static constexpr const char* name = TOSTRING(NYUTORAL_COLOR);
			using type = NYUTORAL_COLOR;
			Lib_Math::VECTOR4 var;
		};
		inline constexpr size_t MAX_BONES = 72;
		//! ボーンアニメーションで移動したボーン位置
		struct BONE_TRANSFORM
		{
		private:
			//static constexpr size_t MAX_BONES = 72;//! 最大ボーン数
		public:
			static constexpr const char* name = TOSTRING(BONE_TRANSFORM);
			using type = BONE_TRANSFORM;
			std::array<Lib_Math::BONE_MATRIX, MAX_BONES> var;
		};

		//! タイマー
		struct TIMER
		{
			static constexpr const char* name = TOSTRING(TIMER);
			using type = TIMER;
			float var;
		};
		//-------------------------------------------------------------------------------------
		// シェーダー定義
		//-------------------------------------------------------------------------------------
		//! shader "skinned_mesh"
		struct SKINNED_MESH final
		{
			//! @brief コンスタントバッファクラス
			struct CONSTANT_BUFFER final
			{
				WVP wvp;  //ワールド・ビュー・プロジェクション合成行列
				WORLD world;      //ワールド変換行列
				//DirectX::XMFLOAT4 material_color;    //材質色
				LIGHT_DIRECTION lightDirection;    //ライト進行方向
				CAMERA_POSISION cameraPos;    //カメラ位置
				LIGHT_CALOR lightColor;    //光の色
				NYUTORAL_COLOR nyutoralLightColor;    //環境光
				BONE_TRANSFORM bone_transforms;//ボーン行列
				template<typename tTuple>
				void Setter(tTuple* tup)
				{
					wvp = std::get<0>(*tup);
					world = std::get<1>(*tup);
					lightDirection = std::get<2>(*tup);
					cameraPos = std::get<3>(*tup);
					lightColor = std::get<4>(*tup);
					nyutoralLightColor = std::get<5>(*tup);
					bone_transforms = std::get<6>(*tup);
				};
			};
		};
		struct SHADER_NAME0;
		//! コンスタントバッファ要素タプル
		struct SKINNED_MESH_TUP : public StructElementTemplate<SHADER_NAME0,
			std::tuple<WVP, WORLD, LIGHT_DIRECTION, CAMERA_POSISION, LIGHT_CALOR, NYUTORAL_COLOR, BONE_TRANSFORM>>
		{};
		//? シェーダーデータ群
		struct SHADER_NAME0 final : ShaderDefine<SKINNED_MESH, 6, SKINNED_MESH_TUP>
		{}; 
		
		
		

		struct BASE final
		{
			//! @brief コンスタントバッファクラス
			struct CONSTANT_BUFFER final
			{
				WORLD world;
				VIEW view;
				PROJECTION proj;
				WVP wvp;
				template<typename tTuple>
				void Setter(tTuple* tup)
				{
					world = std::get<0>(*tup);
					view = std::get<1>(*tup);
					proj = std::get<2>(*tup);
					wvp = std::get<3>(*tup);
				};
			};
		};
		struct SHADER_NAME1;
		//! コンスタントバッファ要素タプル
		struct BASE_TUP : public StructElementTemplate<SHADER_NAME1,
			std::tuple<WORLD, VIEW, PROJECTION, WVP>>
		{};
		//? シェーダーデータ群
		struct SHADER_NAME1 final : ShaderDefine<BASE, 4, BASE_TUP>
		{};




		struct BASE_GEOMETRY final
		{
			//! @brief コンスタントバッファクラス
			struct CONSTANT_BUFFER final
			{
				WORLD world;
				VIEW view;
				PROJECTION proj;
				WVP wvp;
				TIMER timer;
				TIMER pad0;
				TIMER pad1;
				TIMER pad2;
				template<typename tTuple>
				void Setter(tTuple* tup)
				{
					world = std::get<0>(*tup);
					view = std::get<1>(*tup);
					proj = std::get<2>(*tup);
					wvp = std::get<3>(*tup);
					timer = std::get<4>(*tup);
				};
			};
		};
		struct SHADER_NAME2;
		//! コンスタントバッファ要素タプル
		struct BASE_GEOMETRY_TUP : public StructElementTemplate<SHADER_NAME2,
			std::tuple<WORLD, VIEW, PROJECTION, WVP, TIMER, TIMER, TIMER, TIMER>>
		{};
		//? シェーダーデータ群
		struct SHADER_NAME2 final : ShaderDefine<BASE_GEOMETRY, 4, BASE_GEOMETRY_TUP>
		{};
		//----------------------------------------------------------------------------------------------------
		using TypeShader = std::tuple<SHADER_NAME0, SHADER_NAME1, SHADER_NAME2>;


		//シェーダーの情報取得クラス
		class GetShaderInfo
		{
			//戻り値の型が全部違うfuncにする
			
			//struct INIT
			//{
			//	friend GET_SHADER_INFO;
			
		public:
			static constexpr size_t SHADER_TORTAL_NUM = std::tuple_size_v<TypeShader>;//シェーダー総数

			GetShaderInfo() = default;
			~GetShaderInfo() = default;
			//シェーダーのレンダータイプ取得swallow
			struct GET_RENDER_TYPE : public Lib_Template::SWALOWHH<RENDER_TYPE, SHADER_TORTAL_NUM>
			{
				GET_RENDER_TYPE() : SWALOWHH(*this) {}
				GET_RENDER_TYPE(int i) : SWALOWHH() {}
			protected:
				//ここに処理を描く
				template<std::size_t I>
				static RENDER_TYPE SWALLOWHH_FUNC()
				{
					return std::tuple_element_t<I, TypeShader>::DEFINE_ELEM_renderType;
				}
			};

			//! シェーダー名取得
			struct GET_SHADER_NAME : public Lib_Template::SWALOWHH<const char*, SHADER_TORTAL_NUM>
			{
				GET_SHADER_NAME() : SWALOWHH(*this) {}
				GET_SHADER_NAME(int i) : SWALOWHH() {}
			protected:
				//ここに処理を描く
				template<std::size_t I>
				static const char* SWALLOWHH_FUNC()
				{
					return std::tuple_element_t<I, TypeShader>::DEFINE_ELEM_shadername;
				}
			};
			//};
			//using TypeShaderInfoVar = std::variant<INIT::GET_RENDER_TYPE>;
			//using TypeReturnVariableTypeVariant = std::variant<RENDER_TYPE>;//戻り値の型　これを配列にする
			//static constexpr size_t SHADER_INFO_VARIANT_SIZE = std::variant_size_v<TypeShaderInfoVar>;
			//
			//// funcの戻り値を外の変数へセット
			//struct SETTER : public Lib_Template::SWALOW_SETTER<SHADER_INFO_VARIANT_SIZE, SETTER>
			//{
			//	static TypeShaderInfoVar shaderInfo;
			//protected:
			//	SETTER() = default;
			//	//! 同じ型なら、値を格納
			//	//! @param[in] i シェーダー番号
			//	//! @return 値がセットされるとtrue
			//	template<std::size_t I, typename T>
			//	static bool SWALLOWHH_SETTER(T* outvar, size_t i)
			//	{
			//		auto x = std::get<I>(shaderInfo);
			//		return Lib_Template::Set(outvar, x.FuncExecute(i));
			//	}
			//};
		};
		

		//! 変数を選別してバッファ格納用データにセットする
		class CBufferSelector final
		{
		private:
			// TypeTupleVariantとTypeCbufStructVariantは同じ大きさ
			using TypeTupleVariant = std::variant<
				SHADER_NAME0::TUPLE_T,
				SHADER_NAME1::TUPLE_T,
				SHADER_NAME2::TUPLE_T
			>;
			static TypeTupleVariant tupVal;//! タプルバリアント//コンスタントバッファ構造体変数の受け渡しを行う
			
			using TypeCbufStructVariant = std::variant<
				SHADER_NAME0::TYPE::CONSTANT_BUFFER,
				SHADER_NAME1::TYPE::CONSTANT_BUFFER,
				SHADER_NAME2::TYPE::CONSTANT_BUFFER
			>;
			static TypeCbufStructVariant cbufVal;//! コンスタントバッファバリアント//実際に格納する(このバリアントの型を選別してセット)
			static Microsoft::WRL::ComPtr<ID3D11Buffer> cBuffer[std::variant_size_v<TypeCbufStructVariant>];//! コンスタントバッファ
			//------------------------------------------------------------------------------------------------------
		private:
			// !tupleへ変数をセット
			template<typename NOTUSE = void>
			struct SET_VARIABLE_TO_TUPLE
			{
			private:
				//タイプが同じとき
				template<typename T>
				static bool Set(T* get, T in)
				{
					*get = in;
					return true;
				}
				//タイプが違うとき
				template<typename T, typename  CBufElemType>
				static bool Set(T* get, CBufElemType in)
				{
					return false;
				}
				//! tupleへ変数をセット
				//! @return 値が入ったらtrue
				template<std::size_t I, class tTuple, typename  CBufElemType>
				static bool UnderSwallow(tTuple& iTuple, CBufElemType& inVar)
				{
					auto y = &std::get<I>(iTuple);//タプル中の１要素
					//auto x = std::get<1>(tupVal);// 確認用コメント
					//auto z = std::get<0>(x.tup); //確認用コメント
					using type = std::tuple_element_t<I, tTuple>;
					if (typeid(type) == typeid(CBufElemType) && std::string(CBufElemType::name) == std::string(type::name))//型が同じで、名前が一緒なら代入
					{
						//*y = inVar;
						return Set(y, inVar);
					}
					return false;
				}
				//配列それぞれの処理をする
				template<class tTuple, typename CBufElemType, std::size_t... Indices>
				static bool SwallowUnder(tTuple & iTuple, std::size_t i, CBufElemType& inVar, std::index_sequence<Indices...>)
				{
					using Func = bool (*)(tTuple&, CBufElemType&);
					Func aFuncArray[] =
					{
						(UnderSwallow<Indices, tTuple, CBufElemType>)...
					};
					if (aFuncArray[i](iTuple, inVar))
						return true;//値が入った
					return false;
				}
				//インデックスを渡す
				template<class tTuple, typename CBufElemType>
				static bool yyy(tTuple& iTuple, CBufElemType& inVar)
				{
					constexpr std::size_t SIZE = std::tuple_size_v<tTuple>;
					for (std::size_t i = 0; i < SIZE; ++i)
					{
						if (SwallowUnder(iTuple, i, inVar, std::make_index_sequence<SIZE>{}))
							return true;//値が入った
					}
					return false;
				}
				//tupleの要素探索//2回目のswallow
				template<std::size_t I, class tValiant, typename CBufElemType>
				static bool SetVaribleSwallow(tValiant& iVariant, CBufElemType& inVar)
				{
					//シェーダーが切り替わったときvariant切り替え
					if (tupVal.index() != I)
					{
						std::variant_alternative_t<I, tValiant>c;//ダミー
						tupVal = c;//初期化
					}
					auto y = &std::get<I>(tupVal);//ヴァリアントの１要素
					//auto z = &std::get<1>(tupVal);//確認用コメント
					//using tTuple = std::variant_alternative_t<I, tValiant>;
					//using tTuple_ = std::variant_alternative_t<0, TypeTupleVariant>;
					//auto t = std::get<0>(z->tup);
					//t = 0;
					if (yyy(y->tup, inVar))
						return true;//値が入った
					return false;
				}
				//配列それぞれの処理をする
				template<class tValiant, typename CBufElemType, std::size_t... Indices>
				bool SwallowSetVarible(tValiant& iVariant, std::size_t i, CBufElemType& inVar, std::index_sequence<Indices...>)
				{
					using Func = bool (*)(tValiant&, CBufElemType&);
					Func aFuncArray[] =
					{
						(SetVaribleSwallow<Indices, tValiant, CBufElemType>)...
					};
					if (aFuncArray[i](iVariant, inVar))
						return true;//関数が実行できた
					return false;
				}
				//インデックスを渡す
				template<class tVariant, typename CBufElemType>
				bool xxx(tVariant& iVariant, std::size_t i, CBufElemType& inVar)
				{
					constexpr std::size_t SIZE = std::variant_size_v<tVariant>;
					if(this->SwallowSetVarible(iVariant, i, inVar, std::make_index_sequence<SIZE>{}))
						return true;//関数が実行できた
					return false;
				}
			public:
				//コンスタントバッファ構造体を構成する要素を入れる//1回目のswallow
				//! @param[in] shaderVariantNumber シェーダータイプの番号(shader structの番号 (variantの型を選ぶインデックス))
				//! @return tupleへ値が入ったか
				template<typename CBufElemType>
				bool SetConstantVariable(CBufElemType& inVar, int shaderVariantNumber)
				{
					if (this->xxx(tupVal, shaderVariantNumber, inVar))//セットしたシェーダーのバリアント型だけ呼ぶ
						return true;//関数が実行できた
					return false;
				}
			};
			//----------------------------------------------------------------------------
			//! tupleからvariantへセット
			template<typename NOTUSE = void>
			struct SET_TUPLE_TO_VARIANT
			{
			private:
				//tupleをヴァリアントにセット
				template<std::size_t I, class tTupVar>//I この関数の配列番号 : tTupVar  tupleバリアントの型 
				static void nSwallow(tTupVar const& iTuple)
				{
					//シェーダーが切り替わったときvariant切り替え
					if (cbufVal.index() != I)
					{
						std::variant_alternative_t<I, TypeCbufStructVariant>c_;//ダミー
						cbufVal = c_;
					}
					auto x = &std::get<I>(cbufVal);
					auto y = &std::get<I>(tupVal);
					//auto x = &std::get<0>(cbufVal); //確認用コメント
					//auto y = &std::get<0>(tupVal); //確認用コメント
					x->Setter(&y->tup);
					
					//auto t = std::get<0>(y->tup);
				}
				template<class tTupVar, std::size_t... indices>//tTupVar  tupleバリアントの型  : indeces tTupVarの型数分展開
				void Swallown(tTupVar const& iTuple, std::size_t i, std::index_sequence<indices...>)
				{
					using Func = void  (*)(tTupVar const&);
					Func aFuncArray[] =
					{
						(nSwallow<indices, tTupVar>)...
					};
					aFuncArray[i](iTuple);
				}
				//インデックスを渡す
				template<class tTupVar>//tTupVar  tupleバリアントの型
				void zzz(tTupVar const& iVariant, std::size_t i)
				{
					constexpr std::size_t SIZE = std::variant_size_v<tTupVar>;//必然的にtupleも同じ数
					this->Swallown(iVariant, i, std::make_index_sequence<SIZE>{});
				}
			public:
				//tupleからvariantへセット
				//! @param[in] shaderVariantNumber シェーダータイプの番号(shader structの番号 (variantの型を選ぶインデックス))
				void SetTupleToVariant(int shaderVariantNumber)
				{
					this->zzz(tupVal, shaderVariantNumber);//型を推測したいのでtuplevariantを引数に指定
				}
			};
			//----------------------------------------------------------------------------
			template<typename NOTUSE = void>//上のクラスのメンバーを使わせるためのテンプレート
			struct SWALLOW_CBUFFER_INIT
			{
			private:
				using TYPE_CBUF_VARIANT = TypeCbufStructVariant;//ヴァリアントの型の大きさをコンスタントバッファ生成するのに使う
				static bool InitConstantBuffer(ID3D11Buffer** p_BufferConst, size_t bufferSize);
				//----------------------------------------------------------------------------
				template<std::size_t i, class tTuple>
				static bool ConstantBufferInitSwallow(tTuple const& iTuple, ID3D11Buffer** p_BufferConst)
				{
					using type = std::variant_alternative_t<i, TYPE_CBUF_VARIANT>;
					constexpr size_t SIZE = sizeof(type);//このコンスタントバッファ構造体の大きさ

					//auto x = &std::get<i>(iTuple);

					if (!InitConstantBuffer(p_BufferConst, SIZE))
					{
						assert(!"コンスタントバッファ作成失敗");
						return false;
					}
					return true;//シェーダー名があったら
				}
				template<class tVariant, std::size_t... indices>
				bool SwallowConstantBufferInit(tVariant const& iVariant, std::size_t i,ID3D11Buffer** p_BufferConst, std::index_sequence<indices...>)
				{
					using Func = bool (*)(tVariant const&, ID3D11Buffer**);
					Func aFuncArray[] =
					{
						(ConstantBufferInitSwallow<indices, tVariant>)...
					};
					if (aFuncArray[i](iVariant, p_BufferConst))
						return true;//関数が実行できた
					return false;
				}
				//インデックスを渡す
				template<class tVariant>//tVariant バリアントの型
				void zzz(tVariant const& iVariant, ID3D11Buffer** cBuffer, int i)
				{
					constexpr std::size_t SIZE = std::variant_size_v<tVariant>;
					this->SwallowConstantBufferInit(iVariant, i, cBuffer, std::make_index_sequence<SIZE>{});
				}
			public:
				//! @brief 規定数のコンスタントバッファを作る
				//! @param[in]  cBuffer コンスタントバッファのアドレスのアドレス
				void InitConstantBuffer_(ID3D11Buffer** cBuffer, int i)
				{
					this->zzz(cbufVal, cBuffer, i);//型を推測したいのでtuplevariantを引数に指定
				}
			};
			//----------------------------------------------------------------------------

			//! ヴァリアントを選別してコンスタントバッファへセット
			template<typename NOTUSE = void>//上のクラスのメンバーを使わせるためのテンプレート
			struct SWALLOW_CBUFFER_SET final
			{
			private:
				//----------------------------------------------------------------------------
				template<std::size_t i, class tVariant>
				static void ConstantBufferSetSwallow(tVariant const& iVariant)
				{
					auto x = &std::get<i>(cbufVal);
					//auto y = std::get<0>(cbufVal);//確認用コメント
					using ShaderType = std::tuple_element_t<i, TypeShader >;

					Lib_3D::DirectX11InitDevice::GetImidiateContext()->UpdateSubresource(cBuffer[i].Get(), 0, nullptr, x, 0, 0);
					for (size_t i2 = (size_t)CBUFFER_WHERE_USE_INDEX::START; i2 < (size_t)CBUFFER_WHERE_USE_INDEX::END; i2++)
					{
						if (ShaderType::DEFINE_ELEM_SEND_CBUFFER_FLAG[i2] == false)//そのシェーダーへ送らないなら、セットしない
							continue;
						switch ((CBUFFER_WHERE_USE_INDEX)i2)
						{
						default:
							break;
						case CBUFFER_WHERE_USE_INDEX::VERTEX:
							Lib_3D::DirectX11InitDevice::GetImidiateContext()->VSSetConstantBuffers(0, 1, cBuffer[i].GetAddressOf());
							break;
						case CBUFFER_WHERE_USE_INDEX::PIXEL:
							Lib_3D::DirectX11InitDevice::GetImidiateContext()->PSSetConstantBuffers(0, 1, cBuffer[i].GetAddressOf());
							break;
						case CBUFFER_WHERE_USE_INDEX::GEOMETRY:
							Lib_3D::DirectX11InitDevice::GetImidiateContext()->GSSetConstantBuffers(0, 1, cBuffer[i].GetAddressOf());
							break;
						case CBUFFER_WHERE_USE_INDEX::HULL:
							Lib_3D::DirectX11InitDevice::GetImidiateContext()->HSSetConstantBuffers(0, 1, cBuffer[i].GetAddressOf());
							break;
						}
					}	
				}
				template<class tVariant, std::size_t... indices>
				void SwallowConstantBufferSet(tVariant const& iVariant, std::size_t i,std::index_sequence<indices...>)
				{
					using Func = void (*)(tVariant const&);
					Func aFuncArray[] =
					{
						(SWALLOW_CBUFFER_SET<>::ConstantBufferSetSwallow<indices, tVariant>)...
					};
					aFuncArray[i](iVariant);
				}
				//インデックスを渡す
				template<class tVariant>//tVariant バリアントの型
				void zzz(tVariant const& iVariant, std::size_t i)
				{
					constexpr std::size_t SIZE = std::variant_size_v<tVariant>;
					this->SwallowConstantBufferSet(iVariant, i, std::make_index_sequence<SIZE>{});
				}
			public:
				//tupleからvariantへセット
				//! @param[in]  inVar コンスタントバッファ構造体変数の型
				//! @param[in] shaderVariantNumber シェーダータイプの番号(shader structの番号 (variantの型を選ぶインデックス))
				void SetVariantToConstantBuffer( int shaderVariantNumber)
				{
					this->zzz(cbufVal, shaderVariantNumber);//型を推測したいのでtuplevariantを引数に指定
				}
			};
			//----------------------------------------------------------------------------
			private:
				static SET_VARIABLE_TO_TUPLE<> x;
				static SET_TUPLE_TO_VARIANT<> y;
				static SWALLOW_CBUFFER_INIT<> z;
				static SWALLOW_CBUFFER_SET<> w;
			public:
				//! 変数をtupleへ入れる(最初渡し)
				//! @param[in] shaderVariantNumber シェーダータイプの番号(shader structの番号 (variantの型を選ぶインデックス))
				//! @return tupleへ値が入ったか
				template<typename CBufElemType>
				static bool SetVariableToTuple(const CBufElemType& inVar, int shaderVariantNumber)
				{
					if (x.SetConstantVariable(inVar, shaderVariantNumber))
						return true;//関数が実行できた
					return false;
				}
				//! tupleからvariantへセット(中間渡し)
				//! @param[in] shaderVariantNumber シェーダータイプの番号(shader structの番号 (variantの型を選ぶインデックス))
				static void SetTupleToVariant(int shaderVariantNumber)
				{
					y.SetTupleToVariant(shaderVariantNumber);//型を推測したいのでtuplevariantを引数に指定
				}
				//! @brief 規定数のコンスタントバッファを作る
				//! @param[in]  cBuffer コンスタントバッファのアドレスのアドレス
				static void InitConstantBuffer()
				{
					for (size_t i = 0; i < std::variant_size_v<TypeCbufStructVariant>; i++)
					{
						z.InitConstantBuffer_(cBuffer[i].GetAddressOf(), i);
					}
				}
				//! variantからコンスタントバッファへセット(最後渡し)
				//! @param[in]  cBufferコンスタントバッファ
				//! @param[in] shaderVariantNumber シェーダータイプの番号(shader structの番号 (variantの型を選ぶインデックス))
				static void SetVariantToConstantBuffer(int shaderVariantNumber)
				{
					w.SetVariantToConstantBuffer(shaderVariantNumber);
				}

				static void ReleaseCBuffer()
				{
					for (size_t i = 0; i < std::variant_size_v<TypeCbufStructVariant>; i++)
					{
						if (cBuffer[i])
						{
							cBuffer[i]->Release();
							cBuffer[i] = nullptr;
						}
					}
				}
		};








		// シェーダーごとにシェーダーを読み込む
		class ShaderCorrespondsShaderLoad final
		{
		private:
			//using TypeShader = std::tuple<SHADER_NAME0, SHADER_NAME1>;//シェーダをまとめたタプル
			// シェーダーをコンパイル
			struct SWALLOW_SHADER_CREATE
			{
			private:
				static bool ShaderCompileCso(const char* shadername, const D3D11_INPUT_ELEMENT_DESC* elementDesc, size_t ARRAY_NUM);//csoでシェーダー作成
				static bool ShaderCompile(const char* shadername, const D3D11_INPUT_ELEMENT_DESC* elementDesc, size_t ARRAY_NUM);//シェーダー作成
				//----------------------------------------------------------------------------
				template<std::size_t i>
				static bool ShaderCreateSwallow()
				{
					using shaderType = std::tuple_element_t<i, TypeShader>;//* 詳しくはシェーダー定義基底クラスを参照

					if (shaderType::DEFINE_ELEM_csoUseFlag)
					{
						if (!ShaderCompileCso(shaderType::DEFINE_ELEM_shadername, &shaderType::DEFINE_ELEM_InputElementDesk[0], shaderType::DEFINE_ELEM_ARRAY_NUM))
						{
							assert(!"シェーダーcso読み込み失敗");
							return false;
						}
					}
					else
					{
						if (!ShaderCompile(shaderType::DEFINE_ELEM_shadername, &shaderType::DEFINE_ELEM_InputElementDesk[0], shaderType::DEFINE_ELEM_ARRAY_NUM))
						{
							assert(!"シェーダーコンパイル失敗");
							return false;
						}
					}
					return true;
				}
				
				//str
				template<std::size_t... indices>
				static bool SwallowShaderCreate(std::size_t i, std::index_sequence<indices...>)
				{
					using Func = bool (*)();
					Func aFuncArray[] =
					{
						(ShaderCreateSwallow<indices>)...
					};
					auto f = aFuncArray[i]();
					if (f)
						return true;//関数が実行できた
					return false;
				}
			public:
				// シェーダをタプルから選別
				//すべてのシェーダーをコンパイル
				//! @return 成功すればtrue
				static bool ShaderCreate()
				{
					constexpr std::size_t SIZE = std::tuple_size_v<TypeShader>;
					for (size_t i = 0; i < SIZE; i++)//すべてのシェーダーに対して
					{
						if (!SwallowShaderCreate(i, std::make_index_sequence<SIZE>{}))
							return false;
					}
					return true;
				}
			};
			
			//! シェーダーをセット
			struct SWALLOW_SHADER_SET
			{
			private:
				static bool ShaderSetinng(const char* shadername, const bool* setShaderFlag);
				//----------------------------------------------------------------------------
				template<std::size_t i>
				static bool ShaderSetSwallow()
				{
					using shaderType = std::tuple_element_t<i, TypeShader>;//* 詳しくはシェーダー定義基底クラスを参照
					if (!ShaderSetinng(shaderType::DEFINE_ELEM_shadername, &shaderType::DEFINE_ELEM_SHADER_USE_FLAG[0]))
					{
						assert(!"シェーダーセット失敗");
						return false;
					}
					return true;//シェーダー名があったら
				}
				template<std::size_t... indices>
				static bool SwallowShaderSet(std::size_t i, std::index_sequence<indices...>)
				{
					using Func = bool (*)();
					Func aFuncArray[] =
					{
						(ShaderSetSwallow<indices>)...
					};
					if (aFuncArray[i]())
						return true;//関数が実行できた
					return false;
				}
			public:
				//! @param[in] セットするシェーダーの番号
				//! @return 成功すればtrue
				static bool ShaderSet(int shaderTypeNum)
				{
					constexpr std::size_t SIZE = std::tuple_size_v<TypeShader>;
					if (!SwallowShaderSet(shaderTypeNum, std::make_index_sequence<SIZE>{}))
						return false;
					return true;
				}
			};
		public:
			//! @breif シェーダーの読み込みをする（シェーダーの種類ごとにインプットレイアウトの要素が違うのでここでしている）
			//! @return 成功 true 失敗 false
			static bool ShaderCreate()
			{
				if (!SWALLOW_SHADER_CREATE::ShaderCreate())
					return false;
				return true;
			}

			//! @brief shaderをセットする
			//! @details そのシェーダーを適用するインスタンス前に呼べるようにする
			//! @param[in] shaderTypeNum シェーダータイプ番号
			//! @return 成功 true 失敗 false
			static bool ShaderSet(int shaderTypeNum)
			{
				if (!SWALLOW_SHADER_SET::ShaderSet(shaderTypeNum))
					return false;
				return true;
			}
		};
		
		//! @brief シェーダーごとに異なる設定をするクラス
		//! @detaile renderのコンスタントバッファの設定やインプットレイアウトの設定の時使う
		class ShaderCorrespondsBufferSet final
		{
		private:
			//using TypeShader = std::tuple<SHADER_NAME0, SHADER_NAME1>;//シェーダをまとめたタプル
			struct SWALLOW_VBUFFER_SET
			{
			private:
				static bool SetVertexBuffer(const char* shadername, ID3D11Buffer* p_BufferVertex[], const bool* useVBufferFlag);
				//----------------------------------------------------------------------------
				template<std::size_t i>
				static bool VertexBufferSetSwallow(ID3D11Buffer* p_BufferVertex[])
				{
					using ShaderType = std::tuple_element_t<i, TypeShader >;//シェーダータイプ
					if (!SetVertexBuffer(ShaderType::DEFINE_ELEM_shadername, p_BufferVertex, &ShaderType::DEFINE_ELEM_USE_VBUFFER_FLAG[0]))
					{
						assert(!"バーテックスバッファセット失敗");
						return false;
					}
					return true;//シェーダー名があったら
				}
				template<std::size_t... indices>
				static bool SwallowVertexBufferSet( std::size_t i, ID3D11Buffer* p_BufferVertex[], std::index_sequence<indices...>)
				{
					using Func = bool (*)(ID3D11Buffer**);
					Func aFuncArray[] =
					{
						(VertexBufferSetSwallow<indices>)...
					};
					if (aFuncArray[i](p_BufferVertex))
						return true;//関数が実行できた
					return false;
				}
			public:
				//! 頂点バッファセット
				//! @param[in] セットするシェーダーの番号
				//! @param[in] p_BufferVertex 頂点バッファ
				//! @return 成功すればtrue
				static bool VertexBufferSet(int shaderTypeNum, ID3D11Buffer* p_BufferVertex[])
				{
					constexpr std::size_t SIZE = std::tuple_size_v<TypeShader>;
					if (!SwallowVertexBufferSet(shaderTypeNum, p_BufferVertex, std::make_index_sequence<SIZE>{}))
						return false;
					return true;
				}
			};
		public:
			//! 頂点バッファセット
			//! @param[in] セットするシェーダーの番号
			//! @param[in] p_BufferVertex 頂点バッファ
			//! @return 成功すればtrue
			static bool VertexBufferSet(int shaderTypeNum, ID3D11Buffer* p_BufferVertex[])
			{
				if (SWALLOW_VBUFFER_SET::VertexBufferSet(shaderTypeNum, p_BufferVertex))
					return true;
				return false;
			}
		};

		
}//ShaderInfo
}