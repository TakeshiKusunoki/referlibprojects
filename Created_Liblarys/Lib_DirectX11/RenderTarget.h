#pragma once
#include <d3d11.h>
#include "ShaderInfo.h"
#include <wrl.h>
namespace Lib_3D
{
		class RenderTargetRenderer;
		class ScreenRenderInfo;
		//! マルチレンダーターゲット
		class RenderTarget final
		{
			struct RENDER_TARGET_RESOURCE
			{
				ID3D11Texture2D* pTEX;
				ID3D11RenderTargetView* pRTV;
				ID3D11ShaderResourceView* pSRV;
				RENDER_TARGET_RESOURCE() = default;
				RENDER_TARGET_RESOURCE(RENDER_TARGET_RESOURCE&) = default;
				RENDER_TARGET_RESOURCE(RENDER_TARGET_RESOURCE&& o) noexcept;
				~RENDER_TARGET_RESOURCE();
				
			};
			bool isDifferdUse;//マルチレンダーターゲットを使うか？
			
			//! 色テクスチャ
			RENDER_TARGET_RESOURCE colorRes;
			Microsoft::WRL::ComPtr<ID3D11SamplerState> pSamplerState;
			//! 深度テクスチャ
			RENDER_TARGET_RESOURCE depthRes;

			//! 深度ステンシルテクスチャ
			Microsoft::WRL::ComPtr<ID3D11Texture2D> pStencilTexture;
			//! 深度ステンシルビュー
			Microsoft::WRL::ComPtr<ID3D11DepthStencilView> pDepthStencilView;
			Microsoft::WRL::ComPtr<ID3D11Buffer> p_VertexBuffer[static_cast<size_t>(Lib_3D::ShaderInfo::VBUFFER_TYPE_INDEX::END)];//! [0]位置情報 [1]色情報 [2]ボーン情報//shaderによってセットする頂点バッファを変える
			Microsoft::WRL::ComPtr<ID3D11Buffer> pIndexBuffer;
			friend RenderTargetRenderer;
			friend ScreenRenderInfo;
		public:
			UINT screenWidth = 0;
			UINT screenHeight = 0;
			RenderTarget() 
				//:OBJ(ShaderInfo::RENDER_TYPE::RENDER_TARGET)
			{}
			RenderTarget(RenderTarget&& ) = default;
			~RenderTarget() = default;
			//! レンダーターゲットリソース生成
			void RenderTargetCreate();
			//! レンダーターゲット
			void RenderTargetSet();
			//! マルチレンダーターゲット
			void MultiRenderTargetSet();

			//! ポストエフェクトセット
			//void SetShaderNum(UINT shaderTypeNum) { this->shaderTypeNum = shaderTypeNum; }
			
			void RenderTargetClear(float r, float g, float b, float a);

			//void Update()override {};

		};

		//! 1つのウインドウでこのビューポート回数SRVを作る
		class ViewPort final
		{
		public:
			ViewPort() = default;
			~ViewPort() = default;
		};

		//! レンダーターゲット用レンダー
		//! ポストエフェクトを使う(レンダーターゲットの種類で、シェーダー情報のピクセルシェーダー戻り値からシェーダーを変更する)
		class RenderTargetRenderer final
		{
		public:
			RenderTargetRenderer() = default;
			RenderTargetRenderer(RenderTargetRenderer&) = delete;
			RenderTargetRenderer(RenderTargetRenderer&&) = delete;
			~RenderTargetRenderer() = default;
			static RenderTarget renderTarget;
			static void RenderTargetRender();
			
		};
		//SV_ViewportArrayIndex使わない
		class ViewPortCl
		{
		public:
			D3D11_VIEWPORT viewport;
			//! ビューポートをセットする
			//! 必ずビューポートのセッティングをしたのと同じのを入れる
			//! @pram[in] sort 使うビューの種類
			//! @pram[in] ビューの数
			void ViewSet();
		};


		//スクリーン描画情報バインダー
		//class ScreenRenderInfo final
		//{
		//public:
		//	Lib_3D::DirectX11ComInit com;//DirectX11com
		//	RenderTarget renderTarget;//レンダーターゲット
		//	ScreenRenderInfo(HWND hwnd_, size_t SCREEN_WIDTH_, size_t SCREEN_HEIGHT_)
		//		: com(hwnd_, SCREEN_WIDTH_, SCREEN_HEIGHT_)
		//	{
		//		renderTarget.screenWidth = SCREEN_WIDTH_;
		//		renderTarget.screenHeight = SCREEN_HEIGHT_;
		//	}
		//	ScreenRenderInfo(ScreenRenderInfo&&) = default;
		//	~ScreenRenderInfo() = default;
		//	//! 画面にポストエフェクト反映
		//	void RenderPostEffect();
		//};

		//! それぞれのビューポートのカメラ
		class Camera3D
		{
			ShaderInfo::VIEW view;
			ShaderInfo::PROJECTION proj;
			Lib_Math::VECTOR3 target;
			Lib_Math::VECTOR3 up;
			Lib_Math::VECTOR3 position;//カメラ位置
			float fovY;
			float aspect;
			float znear;
			float zfar;
		public:
			Camera3D() = default;
			~Camera3D() = default;
			const Lib_Math::VECTOR3& GetTarget() const
			{
				return target;
			}
			const Lib_Math::VECTOR3& GetUp() const
			{
				return up;
			}
			const ShaderInfo::VIEW& GetView() const
			{
				return view;
			}
			const ShaderInfo::PROJECTION& GetProj() const
			{
				return proj;
			}
			void ConvertViewProjection()
			{
				view.var.LookAt(position, target, up);
				proj.var.PerspectiveFov(fovY, aspect, znear, zfar);
			}
			//! set variable to tuple
			bool SetWVPVariable(const ShaderInfo::WORLD& w, int shaderTypeNumber)
			{
				bool f = true;
				f = ShaderInfo::CBufferSelector::SetVariableToTuple(w, shaderTypeNumber);
				f = ShaderInfo::CBufferSelector::SetVariableToTuple(this->GetView(), shaderTypeNumber);
				f = ShaderInfo::CBufferSelector::SetVariableToTuple(this->GetProj(), shaderTypeNumber);
				Lib_3D::ShaderInfo::WVP wvp;
				auto x = this->GetView().var;
				auto y = this->GetProj().var;
				wvp.var = w.var * x * y;
				f = ShaderInfo::CBufferSelector::SetVariableToTuple(wvp, shaderTypeNumber);
				return f;
			}
		private:
			void SetConstantBuffer();
		};

		//! レンダーターゲットクラス
		class RenderTargetCl final
		{
			
		public:
			D3D11_VIEWPORT viewport;
			//ViewPortCl viewport;//DirectX11com
			RenderTarget renderTarget;//レンダーターゲット
			bool isDifferdUse;//マルチレンダーターゲットを使うか？
			RenderTargetCl(bool differdUse, float topleftx, float toplefty, UINT width, UINT height)
			{
				viewport.MinDepth = 0;
				viewport.MaxDepth = 1;
				viewport.TopLeftX = topleftx;
				viewport.TopLeftY = topleftx;
				viewport.Width = width;
				viewport.Height = height;
				renderTarget.screenWidth = width;
				renderTarget.screenHeight = height;
				isDifferdUse = differdUse;
			}
			RenderTargetCl() = default;
			RenderTargetCl(RenderTargetCl&&) = default;
			~RenderTargetCl() = default;
			//! コンテキストにビューとレンダーターゲットをセット
			void SetView();
			void SetRenderTarget();
			void RenderTargetClear(float r, float g, float b, float a);
			Camera3D* camera;
			//! 画面にポストエフェクト反映
			void RenderPostEffect();
		};
	
}


