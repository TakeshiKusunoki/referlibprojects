#include "RenderTarget.h"
#include "Lib_DirectX11/ShaderInfo.h"

namespace
{
	//! @namespace call_externaly
		//! @brief 外のソースコードから呼んでいるもの(このファイル専用に作ったソースではないもの)
	namespace call_externaly
	{
#define GetDevice Lib_3D::DirectX11InitDevice::GetDevice()
#define GetDeviceContext Lib_3D::DirectX11InitDevice::GetImidiateContext()
	}
}

namespace Lib_3D
{
	RenderTarget::RENDER_TARGET_RESOURCE::RENDER_TARGET_RESOURCE(RENDER_TARGET_RESOURCE&& o)noexcept

		: pTEX(o.pTEX)
		, pRTV(o.pRTV)
		, pSRV(o.pSRV)
	{
		o.pTEX = nullptr;
		o.pRTV = nullptr;
		o.pSRV = nullptr;
	}
#define RELEASE_IF(x) if (x) { x->Release(); x = nullptr; }
	RenderTarget::RENDER_TARGET_RESOURCE::~RENDER_TARGET_RESOURCE()
	{
		RELEASE_IF(pTEX);
		RELEASE_IF(pRTV);
		RELEASE_IF(pSRV);
	}
#undef RELEASE_IF



	constexpr UINT VERTEX_NUM = 4;
	constexpr UINT INDEX_NUM = 4;



	void RenderTarget::RenderTargetCreate()
	{
		ShaderInfo::VERTEX_BUFFER_TYPE::POSITION pos[VERTEX_NUM];
		pos[0].position = Lib_Math::VECTOR3_CONST{ 1, 1, 0 };
		pos[1].position = Lib_Math::VECTOR3_CONST{ -1, 1, 0 };
		pos[2].position = Lib_Math::VECTOR3_CONST{ 1,-1, 0 };
		pos[3].position = Lib_Math::VECTOR3_CONST{ -1,-1, 0 };
		ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD texcoord[VERTEX_NUM];
		texcoord[0].texcoord = Lib_Math::VECTOR2{ 1,0 };
		texcoord[1].texcoord = Lib_Math::VECTOR2{ 0,0 };
		texcoord[2].texcoord = Lib_Math::VECTOR2{ 1,1 };
		texcoord[3].texcoord = Lib_Math::VECTOR2{ 0,1 };
		//インデックス
		UINT index[INDEX_NUM] = { 0,1,2,3 };

		HRESULT hr = S_OK;
		// 頂点バッファの生成.
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::POSITION) * VERTEX_NUM;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;

		// サブリソースの設定.
		D3D11_SUBRESOURCE_DATA initData;
		ZeroMemory(&initData, sizeof(D3D11_SUBRESOURCE_DATA));

		int createCount = 0;
		for (size_t i = 0; i < static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::END); i++)
		{
			bool exef = true;//頂点バッファ生成フラグ
			switch (i)
			{
			default:
				exef = false;
				break;
				case static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::POSITION) :
					bd.ByteWidth = sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::POSITION) * VERTEX_NUM;
					initData.pSysMem = pos;
					break;
					case static_cast<size_t>(ShaderInfo::VBUFFER_TYPE_INDEX::TEXCOORD) :
						bd.ByteWidth = sizeof(ShaderInfo::VERTEX_BUFFER_TYPE::TEXCOORD) * VERTEX_NUM;
						initData.pSysMem = pos;
						break;
			}
			if (exef)
			{
				hr = GetDevice->CreateBuffer(&bd, &initData, p_VertexBuffer[createCount].GetAddressOf());
				if (FAILED(hr))
				{
					assert(!"頂点バッファの作成ができません");
					return;
				}
				createCount++;
			}
		}

		//インデックスバッファ
		ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(UINT) * INDEX_NUM;
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;

		// サブリソースの設定.
		ZeroMemory(&initData, sizeof(D3D11_SUBRESOURCE_DATA));
		initData.pSysMem = index;
		GetDevice->CreateBuffer(&bd, &initData, pIndexBuffer.GetAddressOf());

		{
			// レンダーターゲットの設定
			D3D11_TEXTURE2D_DESC td;
			ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
			td.Width = screenWidth;
			td.Height = screenHeight;
			td.MipLevels = 1;
			td.ArraySize = 1;
			td.Format = DXGI_FORMAT_R8G8B8A8_TYPELESS;
			td.SampleDesc.Count = 1;
			td.SampleDesc.Quality = 0;
			td.Usage = D3D11_USAGE_DEFAULT;
			td.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
			td.CPUAccessFlags = 0;
			td.MiscFlags = 0;
			// テクスチャ生成
			HRESULT hr = GetDevice->CreateTexture2D(&td, NULL, &this->colorRes.pTEX);
			if (FAILED(hr))
			{
				assert(!"レンダーターゲットの設定ができません");
				return;
			}

			//	レンダーターゲットビュー
			D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
			memset(&rtvDesc, 0, sizeof(rtvDesc));
			rtvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
			hr = GetDevice->CreateRenderTargetView(this->colorRes.pTEX, &rtvDesc, &this->colorRes.pRTV);

			// シェーダリソースビューの設定
			D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
			memset(&srvDesc, 0, sizeof(srvDesc));
			srvDesc.Format = rtvDesc.Format;
			srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			srvDesc.Texture2D.MipLevels = 1;
			// シェーダリソースビューの生成
			hr = GetDevice->CreateShaderResourceView(this->colorRes.pTEX, &srvDesc, &this->colorRes.pSRV);
			if (FAILED(hr))
			{
				assert(!"レンダーターゲットビューの作成ができません");
				return;
			}
		}


	}

	void RenderTarget::RenderTargetSet()
	{
		GetDeviceContext->OMSetRenderTargets(1, &colorRes.pRTV, pDepthStencilView.Get());
	}

	void RenderTarget::MultiRenderTargetSet()
	{
		ID3D11RenderTargetView* targets[] = {
			colorRes.pRTV,
			depthRes.pRTV,
		};

		GetDeviceContext->OMSetRenderTargets(ARRAYSIZE(targets), targets, pDepthStencilView.Get());

	}

	void RenderTarget::RenderTargetClear(float r, float g, float b, float a)
	{
		float clearColor[4] = { r, g, b, a };
		GetDeviceContext->ClearRenderTargetView(colorRes.pRTV, clearColor);
		GetDeviceContext->ClearDepthStencilView(pDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
		float clearDepth[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
		GetDeviceContext->ClearRenderTargetView(depthRes.pRTV, clearDepth);

	}




	/*void RenderTargetRenderer::RenderTargetRender()
	{
		GetDeviceContext->IASetIndexBuffer(renderTarget.pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		bool f = true;
		f = ShaderInfo::ShaderCorrespondsBufferSet::VertexBufferSet(renderTarget.shaderTypeNum, renderTarget.p_VertexBuffer);
		if (!f)
		{
			assert(!"セットできませんでした。");
			return;
		}
		f = ShaderInfo::ShaderCorrespondsShaderLoad::ShaderSet(renderTarget.shaderTypeNum);
		if (!f)
		{
			assert(!"セットできませんでした。");
			return;
		}

		GetDeviceContext->PSSetShaderResources(0, 1, &renderTarget.pSRV_Color);
		GetDeviceContext->PSSetShaderResources(1, 1, &renderTarget.pSRV_Depth);
		GetDeviceContext->PSSetSamplers(0, 1, &renderTarget.pSamplerState);
		GetDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);


		GetDeviceContext->DrawIndexed(4, 0, 0);
	}*/

	void RenderTargetCl::RenderPostEffect()
	{

		GetDeviceContext->IASetIndexBuffer(renderTarget.pIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

		bool f = true;
		f = ShaderInfo::ShaderCorrespondsBufferSet::VertexBufferSet(renderTarget.shaderTypeNum, renderTarget.p_VertexBuffer[0].GetAddressOf());
		if (!f)
		{
			assert(!"セットできませんでした。");
			return;
		}
		f = ShaderInfo::ShaderCorrespondsShaderLoad::ShaderSet(renderTarget.shaderTypeNum);
		if (!f)
		{
			assert(!"セットできませんでした。");
			return;
		}
		GetDeviceContext->PSSetShaderResources(0, 1, &renderTarget.colorRes.pSRV);
		GetDeviceContext->PSSetSamplers(0, 1, renderTarget.pSamplerState.GetAddressOf());
		GetDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

		if ()
		{
			GetDeviceContext->PSSetShaderResources(1, 1, &renderTarget.depthRes.pSRV);
		}
		GetDeviceContext->DrawIndexed(VERTEX_NUM, 0, 0);
	}


	void RenderTargetCl::SetView()
	{
		GetDeviceContext->RSSetViewports(1, &this->viewport);
		
	}

	void RenderTargetCl::SetRenderTarget()
	{
		if (this->isDifferdUse)//defferd
			this->renderTarget.MultiRenderTargetSet();
		else
			this->renderTarget.RenderTargetSet();
	}

	void RenderTargetCl::RenderTargetClear(float r, float g, float b, float a)
	{
		this->renderTarget.RenderTargetClear(r, g, b, a);
	}

	void Camera::SetConstantBuffer()
	{
		ShaderInfo::CBufferSelector::SetVariableToTuple(obj->boneTransform, obj->shaderTypeNum);
	}

	/*void ViewPortCl::ViewSet()
	{
		GetDeviceContext->RSSetViewports(1, &this->viewport);
	}*/
}