#pragma once
#include <d3d11.h>//追加

//////////////////////////////////////////////
//Unit7
////////////////////////////////////////////////

namespace Lib_3D {
	class BlendMode
	{
	public:
		enum class BLEND_MODE
		{
			NONE = 0,//合成なし（デフォルト）
			ALPHA,	    //α合成
			ADD,	    //加算合成
			SUB,	    //減産合成
			REPLACE,    //置き換え
			MULTIPLY,   //乗算
			LIGHTEN,    //比較（明）
			DARKEN,	    //比較（暗）
			SCREEN,	    //スクリーン
			MODE_MAX,   //
		};
	private:
		ID3D11BlendState* BlendState[static_cast<size_t>(BLEND_MODE::MODE_MAX)];//ブレンド設定配列
		bool bLoad;//true:設定配列作成済み
		BLEND_MODE enumMode;//現在使用してるブレンドモード

	public:
		BlendMode()
		{
			for (size_t i = 0; i < static_cast<size_t>(BLEND_MODE::MODE_MAX); i++)
			{
				this->BlendState[i] = nullptr;
			}
			this->bLoad = false;
			this->enumMode = BLEND_MODE::ADD;
		};
		BlendMode(BlendMode&& o)throw()
			:bLoad(o.bLoad)
			,enumMode(enumMode)
		{
			for (BLEND_MODE mode = BLEND_MODE::NONE; mode < BLEND_MODE::MODE_MAX; mode = (BLEND_MODE)(size_t(mode) + 1))
			{
				BlendState[size_t(mode)] = o.BlendState[size_t(mode)];
				o.BlendState[size_t(mode)] = nullptr;
			}
		}
		~BlendMode() { Release(); };


	public:
		bool Initializer();
		void Release();
		//ブレンド設定用関数
		void Set(BLEND_MODE mode = BLEND_MODE::NONE);

	private:
	};
}


