#include "ShaderInfo.h"

#include <map>

#include <array>
#include "ShaderResourceManager.h"
#include "DirectX11Init.h"

namespace
{	
	//! @namespace at_compile_time
	//! @brief コンパイル時、処理
	namespace at_compile_time
	{
#ifdef _DEBUG
#define new  ::new(_NORMAL_BLOCK, __FILE__, __LINE__)//メモリリーク無いならなくす(newをあぶりだしたいので)
#endif // DEBUG
		//! インスタンス生成ラップテンプレート(コピペして使う)
		template<typename T>
		class  encapsulation_template
		{
		protected:
			static T* Init;//インスタンス
		public:
			encapsulation_template() = default;
			encapsulation_template(encapsulation_template<T>&) = delete;
			encapsulation_template(encapsulation_template<T>&&) = delete;
			virtual ~encapsulation_template() { if (Init) { 
				delete Init; Init = nullptr; } }
		};
		//! インスタンスを生成
		class  encapsulation_ShaderResourceManager final : encapsulation_template<Lib_3D::ShaderResourceManager>
		{
		public:
			encapsulation_ShaderResourceManager() { if (!Init)Init = new Lib_3D::ShaderResourceManager; }
			static Lib_3D::ShaderResourceManager* const  GetInstance() { return Init; }
		};
		Lib_3D::ShaderResourceManager* encapsulation_ShaderResourceManager::Init = nullptr;
		const  at_compile_time::encapsulation_ShaderResourceManager Init{};//コンパイル時、初期化
	}//at_compile_time

	//! @namespace call_externaly
	//! @brief 外のソースコードから呼んでいるもの(このファイル専用に作ったソースではないもの)
	namespace call_externaly
	{
		using ShaderResourceManagerEx = at_compile_time::encapsulation_ShaderResourceManager;
#define GetDevice Lib_3D::DirectX11InitDevice::GetDevice()
#define GetDeviceContext Lib_3D::DirectX11InitDevice::GetImidiateContext()
	};
}

namespace Lib_3D
{
	namespace ShaderInfo
	{
		CBufferSelector::TypeTupleVariant CBufferSelector::tupVal{};
		CBufferSelector::TypeCbufStructVariant CBufferSelector::cbufVal{};
		Microsoft::WRL::ComPtr<ID3D11Buffer> CBufferSelector::cBuffer[std::variant_size_v<TypeCbufStructVariant>]{};
		CBufferSelector::SET_VARIABLE_TO_TUPLE<> CBufferSelector::x{};
		CBufferSelector::SET_TUPLE_TO_VARIANT<> CBufferSelector::y{};
		CBufferSelector::SWALLOW_CBUFFER_INIT<> CBufferSelector::z{};
		CBufferSelector::SWALLOW_CBUFFER_SET<> CBufferSelector::w{};

		GET_SHADER_INFO::TypeShaderInfoVar GET_SHADER_INFO::SETTER::shaderInfo{};
		//////////////////////////////////////////////////////////////////////////////////
		namespace
		{
			using INDEX_TYPE = std::pair<SHADER_FILENAME_INFO_INDEX, const char*>;
#define INDEX_END static_cast<size_t>(SHADER_FILENAME_INFO_INDEX::END)
			using INDEX_ARRAY = std::array<INDEX_TYPE, INDEX_END>;
			//シェーダーファイル名シェーダー関数名とで、データの順番を全部揃える あわせてないとエラーが起きる
			//! シェーダーファイル名情報
			// first "shader File Type" second-(first "SHADER TYPE", second "Specific filename")
			static const std::map<const char*, INDEX_ARRAY> SHADER_FILENAME_MAP{
				std::make_pair(TOSTRING(SHADER_NAME0) , INDEX_ARRAY{
				std::make_pair(SHADER_FILENAME_INFO_INDEX::VERTEX ,"skinned_mesh_vs"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::PIXEL  ,"skinned_mesh_ps"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::END,"")}),
				std::make_pair(TOSTRING(SHADER_NAME1) , INDEX_ARRAY{
				std::make_pair(SHADER_FILENAME_INFO_INDEX::VERTEX ,"base"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::PIXEL  ,"base"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::END,"")}),
				std::make_pair(TOSTRING(SHADER_NAME2) , INDEX_ARRAY{
				std::make_pair(SHADER_FILENAME_INFO_INDEX::VERTEX ,"baseGeometry"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::PIXEL  ,"baseGeometry"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::GEOMETRY  ,"baseGeometry"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::END,"")})
			};

			//! シェーダー関数名情報
			// first "shader File Type" second-(first "SHADER TYPE", second "Specific funcname")
			static const std::map<const char*, INDEX_ARRAY> SHADER_FUNCNAME_MAP{
				std::make_pair(TOSTRING(SHADER_NAME0) , INDEX_ARRAY{
				std::make_pair(SHADER_FILENAME_INFO_INDEX::VERTEX ,"main"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::PIXEL  ,"main"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::END,"")}),
				std::make_pair(TOSTRING(SHADER_NAME1) , INDEX_ARRAY{
				std::make_pair(SHADER_FILENAME_INFO_INDEX::VERTEX ,"VSMain"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::PIXEL  ,"PSMain"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::END,"")}),
				std::make_pair(TOSTRING(SHADER_NAME2) , INDEX_ARRAY{
				std::make_pair(SHADER_FILENAME_INFO_INDEX::VERTEX ,"VSMain"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::PIXEL  ,"PSMain"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::GEOMETRY  ,"GSMain"),
				std::make_pair(SHADER_FILENAME_INFO_INDEX::END,"")})
			};
		}
		//////////////////////////////////////////////////////////////////////////////////
		const D3D11_INPUT_ELEMENT_DESC SHADER_NAME0::InputElementDesk[] =
		{
			{ "POSITION",	0,DXGI_FORMAT_R32G32B32_FLOAT,		0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL",		0,DXGI_FORMAT_R32G32B32_FLOAT,		1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD",	0,DXGI_FORMAT_R32G32_FLOAT,		2, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA ,0 },
			{ "COLOR",		0,DXGI_FORMAT_R32G32B32A32_FLOAT,	3, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA ,0 },
			{ "WEIGHTS",		0,DXGI_FORMAT_R32G32B32A32_FLOAT,	4, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BONES",		0,DXGI_FORMAT_R32G32B32A32_UINT,	4, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		const bool SHADER_NAME0::SHADER_USE_FLAG[]{ true, true, false, false };//SHADER_FILENAME_INFO_INDEXを参照
		const bool SHADER_NAME0::USE_VBUFFER_FLAG[]{ true, true, true, true, true };//VBUFFER_TYPE_INDEXを参照
		const bool SHADER_NAME0::SEND_CBUFFER_FLAG[]{ true, false, false, false };// CBUFFER_WHEREUSE_INDEXを参照
		const bool SHADER_NAME0::csoUseFlag = false;
		const char* SHADER_NAME0::shadername = TOSTRING(SHADER_NAME0);
		const RENDER_TYPE SHADER_NAME0::renderType = RENDER_TYPE::FBXOBJ;
		
		//! shader 頂点データ
		const D3D11_INPUT_ELEMENT_DESC SHADER_NAME1::InputElementDesk[] =
		{
			{ "POSITION",	0,DXGI_FORMAT_R32G32B32_FLOAT,		0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL",		0,DXGI_FORMAT_R32G32B32_FLOAT,		1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD",	0,DXGI_FORMAT_R32G32_FLOAT,		2, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA ,0 },
			{ "COLOR",		0,DXGI_FORMAT_R32G32B32A32_FLOAT,	3, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA ,0 }
		};
		const bool SHADER_NAME1::SHADER_USE_FLAG[]{ true, true, false, false };//SHADER_FILENAME_INFO_INDEXを参照
		const bool SHADER_NAME1::USE_VBUFFER_FLAG[]{ true, true, true, true, false };//VBUFFER_TYPE_INDEXを参照
		const bool SHADER_NAME1::SEND_CBUFFER_FLAG[]{ true, false, false, false };// CBUFFER_WHEREUSE_INDEXを参照
		const bool SHADER_NAME1::csoUseFlag = false;
		const char* SHADER_NAME1::shadername = TOSTRING(SHADER_NAME1);
		const RENDER_TYPE SHADER_NAME1::renderType = RENDER_TYPE::OBJ3D;
		
		const D3D11_INPUT_ELEMENT_DESC SHADER_NAME2::InputElementDesk[] =
		{
			{ "POSITION",	0,DXGI_FORMAT_R32G32B32_FLOAT,		0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL",		0,DXGI_FORMAT_R32G32B32_FLOAT,		1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD",	0,DXGI_FORMAT_R32G32_FLOAT,		2, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA ,0 },
			{ "COLOR",		0,DXGI_FORMAT_R32G32B32A32_FLOAT,	3, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA ,0 }
		};
		const bool SHADER_NAME2::SHADER_USE_FLAG[]{ true, true, true, false };//SHADER_FILENAME_INFO_INDEXを参照
		const bool SHADER_NAME2::USE_VBUFFER_FLAG[]{ true, true, true, true, false };//VBUFFER_TYPE_INDEXを参照
		const bool SHADER_NAME2::SEND_CBUFFER_FLAG[]{ false, false, true, false };// CBUFFER_WHEREUSE_INDEXを参照
		const bool SHADER_NAME2::csoUseFlag = false;
		const char* SHADER_NAME2::shadername = TOSTRING(SHADER_NAME2);
		const RENDER_TYPE SHADER_NAME2::renderType = RENDER_TYPE::OBJ3D;

		bool ShaderCorrespondsShaderLoad::SWALLOW_SHADER_CREATE::ShaderCompileCso(const char* shadername, const D3D11_INPUT_ELEMENT_DESC* elementDesc, size_t ARRAY_NUM)
		{	
			std::string shaderfilename[INDEX_END];

			{
				// シェーダファイル名を検索
				auto itr = SHADER_FILENAME_MAP.find(shadername);
				if (itr == SHADER_FILENAME_MAP.end())
				{
					assert(!"シェーダーリソースが見つからなかった");
					return false;
				}

				// ファイル名取得
				for (size_t i = 0; i < INDEX_END; i++)
				{
					if (itr->second[i].first == SHADER_FILENAME_INFO_INDEX::END)//配列の終わり
						break;
					//シェーダータイプ番号
					const auto SHADER_TYPE = itr->second[i].first;
					// シェーダー種類ごとのファイル名取得
					shaderfilename[static_cast<size_t>(SHADER_TYPE)] = itr->second[i].second;
				}
			}
			// シェーダーcso読みこみ-----------------------------------------------
				const char* const separetor = ".";
				auto offset = std::string::size_type(0);//.をみつけた時の位置
				//ファイル名をcso仕様に加工
				for (size_t i = 0; i < INDEX_END; i++)
				{
					//.が２個以上あるときもあるのでwhileで最後の.を探す
					while (true)
					{
						auto pos = shaderfilename[i].find(separetor, offset);//offset位置以降の.の位置を探す
						//文末まで来た
						if (pos == std::string::npos)
						{
							shaderfilename[i] = shaderfilename[i].substr(0, offset);//最後の.まで取る
							shaderfilename[i] += ".cso";
							break;
						}
						//.が見つかった
						offset += pos;
					}
				}

				bool f = true;
				//シェーダーの関数名検索
				auto itrfunc = SHADER_FUNCNAME_MAP.find(shadername);
				if (itrfunc == SHADER_FUNCNAME_MAP.end())
				{
					assert(!"シェーダーの関数名が見つからなかった");
					return false;
				}

				// 関数名読み込み
				for (size_t i = 0; i < INDEX_END; i++)
				{
					//シェーダータイプ番号
					const auto SHADER_TYPE = itrfunc->second[i].first;
					if (SHADER_TYPE == SHADER_FILENAME_INFO_INDEX::END)//配列の終わり
						break;
					switch (SHADER_TYPE)
					{
					default:
						break;
					case SHADER_FILENAME_INFO_INDEX::VERTEX:
						f = call_externaly::ShaderResourceManagerEx::GetInstance()->LoadVertexShader(shadername, shaderfilename[static_cast<size_t>(SHADER_TYPE)].c_str(), elementDesc, ARRAY_NUM);
						break;
					case SHADER_FILENAME_INFO_INDEX::PIXEL:
						f = call_externaly::ShaderResourceManagerEx::GetInstance()->LoadPixelShader(shadername, shaderfilename[static_cast<size_t>(SHADER_TYPE)].c_str());
						break;
					case SHADER_FILENAME_INFO_INDEX::GEOMETRY:
						break;
					case SHADER_FILENAME_INFO_INDEX::HULL:
						break;
					}
					if (!f)
					{
						assert(!"シェーダーの読み込みに失敗");
						return f;
					}
				}
			
			return f;
		}

		bool ShaderCorrespondsShaderLoad::SWALLOW_SHADER_CREATE::ShaderCompile(const char* shadername, const D3D11_INPUT_ELEMENT_DESC* elementDesc, size_t ARRAY_NUM)
		{
			std::string shaderfilename[INDEX_END];
			{
				// シェーダファイル名を検索
				auto itr = SHADER_FILENAME_MAP.find(shadername);
				if (itr == SHADER_FILENAME_MAP.end())
				{
					assert(!"シェーダーリソースが見つからなかった");
					return false;
				}

				// ファイル名取得
				for (size_t i = 0; i < INDEX_END; i++)
				{
					if (itr->second[i].first == SHADER_FILENAME_INFO_INDEX::END)//配列の終わり
						break;
					//シェーダータイプ番号
					const auto SHADER_TYPE = itr->second[i].first;
					// シェーダー種類ごとのファイル名取得
					shaderfilename[static_cast<size_t>(SHADER_TYPE)] = itr->second[i].second;
				}
			}
			//シェーダーの関数名検索
			auto itrfunc = SHADER_FUNCNAME_MAP.find(shadername);
			if (itrfunc == SHADER_FUNCNAME_MAP.end())
			{
				assert(!"シェーダーの関数名が見つからなかった");
				return false;
			}

			// 関数名読み込み
			bool f = true;
			for (size_t i = 0; i < INDEX_END; i++)
			{
				//シェーダータイプ番号
				const auto SHADER_TYPE = itrfunc->second[i].first;
				if (SHADER_TYPE == SHADER_FILENAME_INFO_INDEX::END)//配列の終わり
					break;
				switch (SHADER_TYPE)
				{
				default:
					break;
				case SHADER_FILENAME_INFO_INDEX::VERTEX:
					f = call_externaly::ShaderResourceManagerEx::GetInstance()->LoadCompileVertexShader(shadername, shaderfilename[static_cast<size_t>(SHADER_TYPE)].c_str(), itrfunc->second[i].second, elementDesc, ARRAY_NUM);
					break;
				case SHADER_FILENAME_INFO_INDEX::PIXEL:
					f = call_externaly::ShaderResourceManagerEx::GetInstance()->LoadCompilePixelShader(shadername, shaderfilename[static_cast<size_t>(SHADER_TYPE)].c_str(), itrfunc->second[i].second);
					break;
				case SHADER_FILENAME_INFO_INDEX::GEOMETRY:
					f = call_externaly::ShaderResourceManagerEx::GetInstance()->LoadCompileGeometryShader(shadername, shaderfilename[static_cast<size_t>(SHADER_TYPE)].c_str(), itrfunc->second[i].second);
					break;
				case SHADER_FILENAME_INFO_INDEX::HULL:
					break;
				}
				if (!f)
				{
					assert(!"シェーダーの読み込みに失敗");
					return f;
				}
			}
			return f;
		}

		
		bool ShaderCorrespondsShaderLoad::SWALLOW_SHADER_SET::ShaderSetinng(const char* shadername, const bool* setShaderFlag)
		{
			ID3D11VertexShader* vs = nullptr;
			ID3D11PixelShader* ps = nullptr;
			ID3D11GeometryShader* gs = nullptr;
			ID3D11HullShader* hs = nullptr;
			// セットフラグがあるシェーダーをセット
			if (setShaderFlag[static_cast<size_t>(SHADER_FILENAME_INFO_INDEX::VERTEX)])
			{
				vs = call_externaly::ShaderResourceManagerEx::GetInstance()->FindVertexShader(shadername);
				if (!vs)
				{
					assert(!"関数が見つからなかった");
					return false;
				}
			}
			if (setShaderFlag[static_cast<size_t>(SHADER_FILENAME_INFO_INDEX::PIXEL)])
			{
				ps = call_externaly::ShaderResourceManagerEx::GetInstance()->FindPixelShader(shadername);
				if (!ps)
				{
					assert(!"関数が見つからなかった");
					return false;
				}
			}
			if (setShaderFlag[static_cast<size_t>(SHADER_FILENAME_INFO_INDEX::GEOMETRY)])
			{
				gs = call_externaly::ShaderResourceManagerEx::GetInstance()->FindGeometryShader(shadername);
				if (!gs)
				{
					assert(!"関数が見つからなかった");
					return false;
				}
			}
			
			GetDeviceContext->VSSetShader(vs, NULL, 0);
			GetDeviceContext->PSSetShader(ps, NULL, 0);
			GetDeviceContext->GSSetShader(gs, NULL, 0);
			GetDeviceContext->HSSetShader(hs, NULL, 0);
			return true;
		}

		bool CBufferSelector::SWALLOW_CBUFFER_INIT<>::InitConstantBuffer(ID3D11Buffer** p_BufferConst, size_t bufferSize)
		{
			// COMオブジェクトの初期化-----------------------
			HRESULT hr = S_OK;

			// �H定数バッファオブジェクトの生成
			D3D11_BUFFER_DESC ConstantBufferDesc;
			ZeroMemory(&ConstantBufferDesc, sizeof(ConstantBufferDesc));
			ConstantBufferDesc.Usage = /*D3D11_USAGE_DYNAMIC*/D3D11_USAGE_DEFAULT;			//動的使用法
			ConstantBufferDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;//定数バッファ
			ConstantBufferDesc.CPUAccessFlags = 0/*D3D11_CPU_ACCESS_WRITE*/;//CPUから書き込む
			ConstantBufferDesc.ByteWidth = bufferSize;
			ConstantBufferDesc.MiscFlags = 0;
			ConstantBufferDesc.StructureByteStride = 0;

			hr = GetDevice->CreateBuffer(&ConstantBufferDesc, NULL, p_BufferConst);
			if (FAILED(hr))
			{
				assert(!"定数バッファオブジェクトの生成ができません");
				return false;
			}
			return true;
		}

		bool ShaderCorrespondsBufferSet::SWALLOW_VBUFFER_SET::SetVertexBuffer(const char* shadername, ID3D11Buffer* p_BufferVertex[], const bool* useVBufferFlag)
		{
			const bool* FLAG = useVBufferFlag;

			auto inputlayout = call_externaly::ShaderResourceManagerEx::GetInstance()->FindInputLayout(shadername);
			if (!inputlayout)
			{
				assert(!"インプットレイアウト見つからなかった");
				return false;
			}
			GetDeviceContext->IASetInputLayout(inputlayout);
			//頂点データのサイズ//シェーダーごとに異なる構造体を持つ
			int strides[][2] = {
				{-1, sizeof(VERTEX_BUFFER_TYPE::POSITION) },
				{-1, sizeof(VERTEX_BUFFER_TYPE::NORMAL) },
				{-1, sizeof(VERTEX_BUFFER_TYPE::TEXCOORD) },
				{-1, sizeof(VERTEX_BUFFER_TYPE::MATRIAL)},
				{-1, sizeof(VERTEX_BUFFER_TYPE::WEIGHT)}
			};

			int buffernum = 0;//使う頂点バッファ数
			for (size_t i = 0; i < static_cast<size_t>(VBUFFER_TYPE_INDEX::END); i++)
			{
				if (strides[i][FLAG[i]] != -1)//このバッファタイプをセットするかの選別
					buffernum++;
			}

			// ストライドをセットする
			auto SetStride = [FLAG, strides](UINT strides_[])
			{
				int count = 0;
				for (size_t i = 0; i < static_cast<size_t>(VBUFFER_TYPE_INDEX::END); i++)
				{
					if (strides[i][FLAG[i]] != -1)
					{
						strides_[count] = strides[i][FLAG[i]];
						count++;
					}
				}
			};

			// バーテックスバッファセットする種類を決める
			{
				if (buffernum <= 0 || buffernum > static_cast<size_t>(VBUFFER_TYPE_INDEX::END))
				{
					assert(!"頂点バッファセットしなかった");
					return false;
				}
				UINT* offset = new UINT[buffernum];
				UINT* stridesIn = new UINT[buffernum];
				for (int i = 0; i < buffernum; i++)
				{
					offset[i] = 0;
				}
				SetStride(stridesIn);
				GetDeviceContext->IASetVertexBuffers(0, buffernum, p_BufferVertex, stridesIn, offset);
				delete[] offset;
				delete[] stridesIn;
			}
			return true;
			// newしないやりかたでバーテックスバッファセットする種類を決める
			/*switch (buffernum)
			{
			default:
				break;
			case 1:
			{
				UINT offset[1] = { 0 };
				UINT strides_[1];
				SetStride(strides_);
				GetDeviceContext->IASetVertexBuffers(0, 1, p_BufferVertex, strides_, offset);
				return true;
			}
			case 2:
			{
				UINT offset[2] = { 0,0 };
				UINT strides_[2];
				SetStride(strides_);
				GetDeviceContext->IASetVertexBuffers(0, 2, p_BufferVertex, strides_, offset);
				return true;
			}
			case 3:
			{
				UINT offset[3] = { 0,0,0 };
				UINT strides_[3];
				SetStride(strides_);
				GetDeviceContext->IASetVertexBuffers(0, 3, p_BufferVertex, strides_, offset);
				return true;
			}
			case 4:
			{
				UINT offset[4] = { 0,0,0,0 };
				UINT strides_[4];
				SetStride(strides_);
				GetDeviceContext->IASetVertexBuffers(0, 4, p_BufferVertex, strides_, offset);
				return true;
			}
			case 5:
			{
				UINT offset[5] = { 0,0,0,0,0 };
				UINT strides_[5];
				SetStride(strides_);
				GetDeviceContext->IASetVertexBuffers(0, 5, p_BufferVertex, strides_, offset);
				return true;
			}
			}
			assert(!"頂点バッファセットしなかった");
			return false;*/
		}
}//shaderInfo
}