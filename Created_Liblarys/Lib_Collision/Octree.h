#pragma once
#include <Windows.h>
#include <vector>
#include "../Lib_3D/vector.h"
#include "PrimitivClass.h"
#include <memory>
#include "../Lib_Vassel/LocationManager.h"
#include "../Lib_Base/Common.h"
#include "../Lib_Base/Property.h"
#include "CollitonDitection.h"
#include "../Lib_Base/Property.h"
#include "..//Lib_Base/my_util.h"
#include <set>
#include <stack>
#include <cstdio>

namespace Lib_Collition
{
	//! 8分木管理クラス
	class OctTreeManager
	{
	private:
		//! べき乗数値配列
		template<std::size_t ...Ints>
		constexpr static const std::array<std::size_t, sizeof...(Ints)> fold_pow(std::size_t base, std::index_sequence<Ints...>)
		{
			return { ((pow_(base, Ints + 1) - 1) / 7)... };
		}
		constexpr static const std::size_t MAXLEVEL = 7;//! 最大レベル
		constexpr static inline const std::array<std::size_t, MAXLEVEL + 1> EACH_LEBEL_SPACE_NUM = fold_pow(8, std::make_index_sequence<MAXLEVEL + 1>{});//! それぞれのレベルでの空間数
	private:

		struct OctCell
		{
			using aOctCell = std::map<std::size_t, OctCell>;
			using aOctHere = std::vector<LeterForCollisonObject>;

			aOctCell next;//この先の空間S
			aOctHere mapNow;//ここに所属するleter
			int objnum = 0;//この空間に登録されているオブジェクトの数
		};

		std::set<size_t> mortonNumMemory;//! keyの番号を保持
		OctCell mipp0;//８分木空間

	public:
		OctTreeManager() = default;
		~OctTreeManager() = default;
		DEFAULT_COPY_MOVE_CONSTRACTOR(OctTreeManager)


		//! param[in] n : octtree number
		//! colLetter 当たり判定入れるもの (ltter)
		void Set(LeterForCollisonObject& colLetter);
		

		//! この空間からはずす
		//! colLetter 当たり判定入れるもの (ltter)
		void Remove(LeterForCollisonObject& colLetter);
		
		//! すべての当たり判定を計算
		void CompareAllCollision();
		

		
	private:
		//! 比較してフラグを入れる
		//! このクラスのmipp0とmortonNumMemoryを使う
		//! (空間番号を保持するコンテナがソートされている、std::setだからこれを使う)
		//! param[in] mortonNumMemory モートン番号記憶した配列
		//! param[in] mipp0 レッターを入れたマップ
		static void CompareAll(const std::set<size_t>& mortonNumMemory, OctCell& mipp0);

		//! 含有される階層レベルまで下る
		//! @param[in] hierlkyNum 含有される空間番号
		//! @param[in] startMap サーチ開始map 
		//! @param[in] startNum サーチ開始番号(mipp0から調べるときは必要ない、startMapより浅い階層がわかるときこれを指定)
		//! @return 含有される階層のmap
		[[nodiscard]] static OctCell* DownHieraluky(int hierlkyNum, _In_ OctCell* startMap, const int startNum = 0);
		


		//! set時、含有される階層レベルまで下る(下るときmapがない場合、新しくmapを作る)
		//! @param[in] hierlkyNum 含有される空間番号
		//! @param[in] startMap サーチ開始map 
		//! @return 含有される階層のmap
		[[nodiscard]] static OctCell* DownHieralukyWhenSet(int hierlkyNum, OctCell& startMap);
		

		//! ふくゆう番号から所属するレベルを調べる
		[[nodiscard]] constexpr static const int SerchLevel(int num);
		
		
		//! モートン順序にならんだ空間の上位空間の情報
		//! @return ルート空間からの入るmap
		[[nodiscard]] static const std::vector<int> SerchHieraluky(const int hielarNum);
		
		
	};

	

	//! ロケーションマネージャーにlet
	class LeterForBaseObject final
	{
	private:
		Lib_Vassel::LocationManager* const _pLom;
		Lib_Vassel::BaseObject* const _pBase;
	public:
		LeterForBaseObject() = delete;
		LeterForBaseObject(_In_ Lib_Vassel::LocationManager* const pLom, _In_  Lib_Vassel::BaseObject* const base)
			: _pLom(pLom)
			, _pBase(base)
		{}
		~LeterForBaseObject() = default;

		void Let(USER::LABEL& label)
		{

		}

	private:
	};


	//class OBJECT_FOR_TREE;
	//! 衝突判定オブジェletter
	class LeterForCollisonObject final
	{
		//! AABB
		_PROPERTY_READABLE_REFERENCE(AABB, aabb)
		//! 当たり判定形
		_PROPERTY_READABLE_REFERENCE(PrimitiveVariant, PrimitiveV)
		//! この当たり判定が存在する空間レベル
		//_PROPERTY_READABLE(std::size_t, ThisLevel)
		//! モートン順序番号
		_PROPERTY_READABLE(std::size_t, MortonNum)
		//! hitMessage
		_PROPERTY_WRITEABLE(std::vector<std::string>, Msg)
		//! mapのイテレーター入れた番号
		_PROPERTY_WRITEABLE(int, Iterator)
		//! サイズ
		//_PROPERTY_READABLE(int ,HierNum)
		//! 識別用ラベル
		_PROPERTY_READABLE( std::string, label)
	private:
		bool isThereAABB = true;//aabbがある
		int prevNum = 0;
	public:
		LeterForCollisonObject() = delete;
		constexpr explicit LeterForCollisonObject(const Lib_Collition::PrimitiveVariant& collisionPrimitive)
		{
			_aabb = std::visit(Lib_Collition::VisitorGetAABB{}, collisionPrimitive);
			if (_aabb._MaxPos.p.x == INFINITE)
				isThereAABB = false;
			_Msg.reserve(sizeof(std::string) * 10);
		};
		DEFAULT_COPY_MOVE_CONSTRACTOR(LeterForCollisonObject)
		~LeterForCollisonObject() = default;

		//! 衝突判定８分木から除く
		virtual bool Remove() {}

		//! 衝突判定８分木に登録
		virtual void Let(const USER::LABEL& label, OBJECT_FOR_TREE cl);

		//! 入っているメッセっージチェック
		void MsgCheck()
		{
			//重複メッセージの消去
			std::sort(this->_Msg.begin(), this->_Msg.end());
			this->Msg.erase(std::unique(this->_Msg.begin(), this->_Msg.end()), this->_Msg.end());
		}
	};

	//constexpr int x = OctTreeManager::SerchLevel(1);
	////! 静的なポリモーフィズム
	////! Let
	////! class T用のleter
	//template <class T>
	//class Leter
	//{
	//public:
	//	virtual bool Remove() = 0;
	//	virtual void Let(const USER::LABEL& label, const T& cl) 
	//	{
	//		cl.Let();
	//	}
	//};

	//class OctTree;
	////! 線形8分木空間
	//class OctTree
	//{
	//	_PROPERTY_READABLE_REFERENCE(VECTOR3, RgnMin)
	//		_PROPERTY_READABLE_REFERENCE(VECTOR3, Unit)
	//private:
	//	//! 0~
	//	template<std::size_t ...Ints>
	//	//! べき乗数値配列
	//	//! param[in] x base
	//	constexpr static const std::array<std::size_t, sizeof...(Ints)> fold_pow(std::size_t x, std::index_sequence<Ints...>)
	//	{
	//		return { pow_(x , Ints)... };
	//	}
	//	constexpr static const std::size_t MAXLEVEL = 7;//! 最大レベル
	//	constexpr static inline const std::array<std::size_t, MAXLEVEL + 1> EACH_LEBEL_SPACE_NUM = fold_pow(8, std::make_index_sequence<MAXLEVEL + 1>{});//! それぞれのレベルでの空間数
	//private:

	//	uint64_t _spaceNum;	//! 空間の数
	//	size_t _level;	//! ８分木分割レベル
	//	std::vector<Leter<Collision>*> mortonSpace;//! モートン順序線形リスト

	//	 //! 衝突判定マネージャー
	//	template <class T>//! 衝突判定マネージャー
	//	std::vector<CollitonDitection<T>> _mng;//! 衝突判定マネージャー



	//public:
	//	OctTree()
	//	{}
	//	~OctTree() {}
	//	//! @param[in] Level ８分木分割レベル
	//	bool init(size_t Level_, const VECTOR3& Min, const VECTOR3& Max)
	//	{
	//		_level = Level_;
	//		mortonSpace.resize(_level);

	//		//_mng.resize(_level);
	//	}

	//	//! @param[in]
	//	template<typename T1, typename T2>
	//	void Compare(T1 x, T2 y)
	//	{
	//		_mng<T1>[0];
	//		if (!IsKeyExist(x, y))
	//			return;
	//		CollisionSwitch(x, y);
	//	}

	//	//! 座標→線形8分木要素番号変換関数
	//	//! @param[in] ８分木に入れる座標
	//	//! @return モートン番号を返す
	//	uint64_t GetMortonNumber(const Point& Min);

	//	//! 座標から空間番号を算出
	//	//! @param[in] Min AABBの小さい部分
	//	//! @param[in] Max AABBの大きい部分
	//	//! @return AABBが入っている空間番号
	//	uint64_t GetInSpaceNum(const AABB& aabb);

	//	//! いる空間と下位空間を探索させる
	//	//! @param[in] num 今いる空間番号
	//	void SerchIncludSpace(const int num, const int level);
	//};

	///////////////////////////////////////
	////! 分木登録オブジェクト(OFT)
	////////////////////////////////////
	//class OBJECT_FOR_TREE : public Leter<const Lib_Vassel::BaseObject*>
	//{
	//private:
	//	std::vector<CCell<T>>* const m_pCell;// 登録空間のアドレス
	//	T* m_pObject;				// 判定対象オブジェクト
	//	std::unique_ptr<OBJECT_FOR_TREE<T>> m_spPre;	// 前のOBJECT_FOR_TREE構造体
	//	std::unique_ptr<OBJECT_FOR_TREE<T>> m_spNext;	// 次のOBJECT_FOR_TREE構造体
	//	OctTree octTree;

	//	Lib_Collition::AABB& _AABB;
	//	Lib_Vassel::LocationManager* _pLom;

	//	//! ラベルの数だけ当たり判定オブジェクトをnew
	//	std::unique_ptr<std::vector<Collision>[]> colLabelList;

	//public:

	//public:
	//	//! @param[in] p AABB中心点
	//	//! @param[in] hl AABB各軸の辺の長さの半分
	//	//! @param[in] 当たり判定用情報
	//	OBJECT_FOR_TREE(const Lib_Collition::Point& p, const VECTOR3& hl, const Collision& col, int n)
	//		: _AABB{ p, hl }
	//		, colLabelList(Collision[n])
	//	{
	//		m_pCell = nullptr;
	//		m_pObject = nullptr;
	//	}
	//	virtual ~OBJECT_FOR_TREE() {}
	//public:
	//	//! 自らリストから外れる
	//	// 自らリストから外れる
	//	bool Remove() override
	//	{
	//		// すでに逸脱している時は処理終了
	//		if (!m_pCell)
	//			return false;

	//		// 自分を登録している空間に自身を通知
	//		if (!m_pCell->OnRemove(this))
	//			return false;

	//		// 逸脱処理
	//		// 前後のオブジェクトを結びつける
	//		if (m_spPre.GetPtr() != NULL)
	//		{
	//			m_spPre->m_spNext = m_spNext;
	//			m_spPre.SetPtr(NULL);
	//		}
	//		if (m_spNext.GetPtr() != NULL)
	//		{
	//			m_spNext->m_spPre = m_spPre;
	//			m_spNext.SetPtr(NULL);
	//		}
	//		m_pCell = NULL;
	//		return true;
	//	}

	//	//! 空間を登録
	//	void RegistCell(CCell<T>* pCell)
	//	{
	//		Octree<int> oct;
	//		oct.Regist(_AABB.GetMinPos().);
	//		oct.Regist(_AABB.);
	//		m_pCell = pCell;
	//	}

	//	//! Msg Locationにletする
	//	template<typename Primitive>
	//	void Let(const USER::LABEL& label, const Lib_Vassel::BaseObject* object) override const;


	//private:
	//};





	///////////////////////////////////////
	////! 線形8分木空間管理クラス
	////////////////////////////////////
	//class Octree
	//{
	//private:
	//	// べき乗数値配列
	//	template<std::size_t ...Ints>
	//	constexpr static const std::array<std::size_t, sizeof...(Ints)> fold_pow(std::size_t x, std::index_sequence<Ints...>)
	//	{
	//		return { pow_(x , Ints)... };
	//	}
	//	constexpr static const std::size_t MAXLEVEL = 7;//最大レベル
	//	constexpr static inline const std::array<std::size_t, MAXLEVEL + 1> EACH_LEBEL_SPACE_NUM = fold_pow(8, std::make_index_sequence<MAXLEVEL + 1>{});//それぞれのレベルでの空間数
	//	//friend class ConstexprInit
	//private:

	//	size_t m_uiDim;
	//	CCell<T>** ppCellAry;	// 線形空間ポインタ配列


	//	VECTOR3 m_W;	// 領域の幅
	//	VECTOR3 m_RgnMin;	// 領域の最小値
	//	VECTOR3 m_RgnMax;	// 領域の最大値
	//	VECTOR3 m_Unit;		// 最小領域の辺の長さ

	//	//uint64_t m_dwCellNum;		// 空間の数
	//	//size_t m_uiLevel;			// 最下位レベル

	//	uint64_t _spaceNum;	//! 空間の数
	//	size_t _level;	//! ８分木分割レベル

	//public:
	//	Octree();
	//	~Octree();
	//	// 線形8分木配列を構築する
	//	bool init(size_t Level, const VECTOR3& Min, const VECTOR3& Max)
	//	{
	//		int x = 0;
	//		auto n = fold_pow(8, b)[x];

	//		// 設定最高レベル以上の空間は作れない
	//		if (Level >= MAXLEVEL)
	//			return false;

	//		// 各レベルでの空間数を算出
	//		int i;
	//		m_iPow[0] = 1;
	//		for (i = 1; i < CLINER8TREEMANAGER_MAXLEVEL + 1; i++)
	//			m_iPow[i] = m_iPow[i - 1] * 8;

	//		// Levelレベル（0基点）の配列作成
	//		_spaceNum = (pow(8, Level) - 1) / 7;
	//		//_spaceNum = (m_iPow[Level + 1] - 1) / 7;
	//		ppCellAry = new CCell<T> * [m_dwCellNum];
	//		SecureZeroMemory(ppCellAry, sizeof(CCell<T>*) * m_dwCellNum);

	//		// 領域を登録
	//		m_RgnMin = Min;
	//		m_RgnMax = Max;
	//		m_W = m_RgnMax - m_RgnMin;
	//		m_Unit = m_W / ((float)(1 << Level));

	//		this->_level = Level;

	//		return true;
	//	}

	//	// オブジェクトを登録する
	//	bool Regist(float left, float top, float right, float bottom, std::unique_ptr<OBJECT_FOR_TREE<T>>& spOFT)
	//	{
	//		// オブジェクトの境界範囲から登録モートン番号を算出
	//		uint64_t Elem = GetMortonNumber(left, top, right, bottom);
	//		if (Elem < m_dwCellNum)
	//		{
	//			// 空間が無い場合は新規作成
	//			if (!ppCellAry[Elem])
	//				CreateNewCell(Elem);
	//			return ppCellAry[Elem]->Push(spOFT);
	//		}
	//		return false;	// 登録失敗
	//	}

	//	//! 衝突判定リストを作成
	//	unsigned long EngageAllColisionList()
	//	{

	//	}

	//private:
	//	//! 空間内で衝突リストを作成する
	//	bool EngageCollsion()
	//	{

	//	}

	//	//! 座標→線形8分木要素番号変換関数
	//	//! @param[in] ８分木に入れる座標
	//	//! @return モートン番号を返す
	//	uint64_t GetMortonNumber(const Point& pos);


	//	//! 座標から空間番号を算出
	//	//! @param[in] Min AABBの小さい部分
	//	//! @param[in] Max AABBの大きい部分
	//	//! @return AABBが入っている空間番号
	//	uint64_t GetInSpaceNum(const AABB& aabb);

	//	//! いる空間と下位空間を探索させる
	//	//! @param[in] num 今いる空間番号
	//	void SerchIncludSpace(const int num, const int level);

	//};

	///////////////////////////////////////
	////! 空間クラス
	////////////////////////////////////
	//template <class T>
	//class CCell
	//{
	//protected:
	//	OBJECT_FOR_TREE<T>* m_spLatest;
	//public:
	//	// コンストラクタ
	//	CCell()
	//	{}

	//	// デストラクタ
	//	virtual ~CCell()
	//	{
	//		if (m_spLatest != nullptr)
	//			ResetLink(m_spLatest);
	//	}
	//	// リンクを全てリセットする
	//	void ResetLink(sp<OBJECT_FOR_TREE<T> >& spOFT)
	//	{
	//		if (spOFT->m_spNext.GetPtr() != nullptr)
	//			ResetLink(spOFT->m_spNext);
	//		spOFT.SetPtr(nullptr);		// スマートポインタリセット
	//	}

	//	// 削除されるオブジェクトをチェック
	//	bool OnRemove(OBJECT_FOR_TREE<T>* pRemoveObj)
	//	{
	//		if (m_spLatest.GetPtr() == pRemoveObj)
	//		{
	//			// 次のオブジェクトに挿げ替え
	//			if (m_spLatest.GetPtr() != nullptr)
	//				m_spLatest = m_spLatest->GetNextObj();
	//		}
	//		return true;
	//	}

	//};

}//Lib_Collition