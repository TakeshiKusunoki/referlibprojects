#include "Octree.h"
//#include <map>
//#include <typeinfo>
//#include <functional>



namespace {
	// ビット分割関数 //3バイトごとに間隔を開ける関数(Level8まで)
	constexpr uint64_t BitSeparateFor3D(uint8_t n) noexcept
	{
		uint64_t s = n;
		s = (s | s << 8) & 0x0000f00f;
		s = (s | s << 4) & 0x000c30c3;
		s = (s | s << 2) & 0x00249249;
		return s;
	}

	// 3Dモートン空間番号算出関数
	constexpr uint64_t Get3DMortonOrder(uint8_t x, uint8_t y, uint8_t z) noexcept
	{
		return BitSeparateFor3D(x) | BitSeparateFor3D(y) << 1 | BitSeparateFor3D(z) << 2;
	}


	//unit.xが0の時例外
	constexpr uint64_t GetMortonNumber_(const VECTOR3& v, const VECTOR3& region, const VECTOR3& unit)noexcept
	{
		std::is_lvalue_reference<VECTOR3>::type;
		//static_assert(v," ");
		DirectX::XMFLOAT3;
		return Get3DMortonOrder(
			static_cast<uint8_t>((v.x - region.x) / unit.x),// 領域の最小値/ 領域の最大値
			static_cast<uint8_t>((v.y - region.y) / unit.y),
			static_cast<uint8_t>((v.z - region.z) / unit.z)
		);
	}

	//! @param[in]
	[[nodiscard]] bool Compare(Lib_Collition::PrimitiveVariant x, Lib_Collition::PrimitiveVariant y)
	{
		if (!IsKeyExist(x, y))
			return;
		return CollisionSwitch(x, y);
	}
	

}	// namespace 


namespace Lib_Collition
{
	void OctTreeManager::Set(LeterForCollisonObject& colLetter)
	{
		int hierNum = colLetter.GetMortonNum();//空間番号
		auto pm = DownHieralukyWhenSet(hierNum, this->mipp0);
		
		//含有される階層レベルに当たり判定オブジェクトを入れる
		colLetter.Iterator = pm->mapNow.size();//このプリミティブが入るイテレーター番号を確保
		pm->mapNow.emplace_back(colLetter);//８分木の正しい位置にコリジョン挿入

		if (pm->objnum == 0)
			this->mortonNumMemory.emplace(hierNum);//あとでmapを探る用のkey要素を保持
		pm->objnum++;
	}

	void OctTreeManager::Remove(LeterForCollisonObject& colLetter)
	{
		int hierNum = colLetter.GetMortonNum();//空間番号
		auto pm = DownHieralukyWhenSet(hierNum, this->mipp0);
		//含有される階層レベルに当たり判定オブジェクトを入れる
		pm->mapNow.erase(std::next(pm->mapNow.begin(), colLetter.Iterator));

		pm->objnum--;
		if (pm->objnum <= 0)
			this->mortonNumMemory.erase(hierNum);//要素nを削除
	}

	void OctTreeManager::CompareAllCollision()
	{
		CompareAll(mortonNumMemory, mipp0);
	}

	void OctTreeManager::CompareAll(const std::set<size_t>& mortonNumMemory, OctCell& mipp0)
	{
		//存在するkey毎
		//itnum :番号
		for (auto& itnum = mortonNumMemory.begin(), ends = mortonNumMemory.end(); itnum != ends; ++itnum)
		{
			const auto HIERNUM = *itnum;//そのAABBが含有される空間の番号
			const auto LV = SerchLevel(HIERNUM);//そのAAABBが含有される空間のれべる
			auto pm = DownHieraluky(HIERNUM, &mipp0);//その空間番号の時の要素群1

			//同じ空間レベルのkeyが出るまでiteratorを進める
			auto& itnumne_ = mortonNumMemory.begin();//下のkeyがでた番号
			if (LV != 0)
			{
				for (const auto& endl = mortonNumMemory.end(); itnumne_ != endl; ++itnumne_)
				{
					if (*itnumne_ > OctTreeManager::EACH_LEBEL_SPACE_NUM[LV - 1])
						break;
				}
			}
			//その空間番号の時の要素群に対して
			//it :primitive
			for (auto& it = pm->mapNow.begin(), end = pm->mapNow.end(); it != end; ++it)
			{
				//自身の空間レベル以下の空間を調べる
				for (auto& itnumne = itnumne_, endl = mortonNumMemory.end(); itnumne != endl; ++itnumne)
				{
					const auto HIERNUM_ = *itnumne;
					auto pmTai = DownHieraluky(HIERNUM_, pm, LV);//そのkeyの時の要素群2
					//下空間の要素
					for (auto& it0 = pmTai->mapNow.begin(), end0 = pmTai->mapNow.end(); it0 != end0; ++it0)
					{
						if (it == it0)//同じものは比較しない
							continue;
						bool hit = Compare(it->GetPrimitiveV(), it0->GetPrimitiveV());
						//msgを追加
						if (!hit)
							continue;
						char* c = "";
						std::sprintf(c, "%d", it->GetMortonNum());
						it->Msg.emplace_back("Hit");
						it->Msg[it->Msg.size() - 1] += ' ';
						it->Msg[it->Msg.size() - 1] += c;
						it->Msg[it->Msg.size() - 1] += ' ';
						it->Msg[it->Msg.size() - 1] += it->Getlabel();
					}
				}
			}
		}
	}

	OctTreeManager::OctCell* OctTreeManager::DownHieraluky(int hierlkyNum, OctCell* startMap, const int startNum)
	{
		auto* pmnex = &startMap->next;//next
		auto pm = startMap;//cell
		const auto LV_ = SerchLevel(hierlkyNum);
		const auto HIER_NUMS = SerchHieraluky(hierlkyNum);
		//含有される階層レベルまで下る
		for (size_t i = startNum; i < LV_; i++)//label==0の時飛ばす
		{
			// 次のオブジェクトへ移行//extractにkeyを入れる
			pm = &pmnex->extract(HIER_NUMS[i]).mapped();//nextのpmnowへ
			pmnex = &pm->next;//nextを下る
		}
		return pm;
	}

	OctTreeManager::OctCell* OctTreeManager::DownHieralukyWhenSet(int hierlkyNum, OctCell& startMap)
	{
		auto* pmnex = &startMap.next;//next
		auto* pm = &startMap;//cell
		const auto LV_ = SerchLevel(hierlkyNum);
		const auto HIER_NUMS = SerchHieraluky(hierlkyNum);
		//含有される階層レベルまで下る
		for (size_t i = 0; i < LV_; i++)//label==0の時飛ばす
		{
			// mapがないなら新しく生成
			if (pmnex->count(HIER_NUMS[i]) == 0)
				pmnex->emplace(HIER_NUMS[i], OctCell{});

			// 次のオブジェクトへ移行//extractにkeyを入れる
			pm = &pmnex->extract(HIER_NUMS[i]).mapped();//nextのpmnowへ
			pmnex = &pm->next;//nextを下る
		}
		return pm;//pmnow
	}

	constexpr const int OctTreeManager::SerchLevel(int num)
	{
		int i = 0;
		for (; EACH_LEBEL_SPACE_NUM[i] > num; i++) {}
		return i - 1;
	}

	const std::vector<int> OctTreeManager::SerchHieraluky(const int hielarNum)
	{
		const unsigned int LEVEL = SerchLevel(hielarNum);
		std::stack<int> lifo;
		for (int i = 0; i < LEVEL; i++)
		{
			const uint64_t CHECK = (hielarNum >> (i * 3)) & 0x7;//3bitずつ取り出す
			lifo.emplace(CHECK);

		}
		//vectorに入れる
		std::vector<int> list;
		list.reserve(LEVEL);
		for (size_t i = 0; i < LEVEL; i++)
		{
			list[i] = lifo.top();
			lifo.pop();
		}
		return list;
	}




}	// LibCollision



//		//! 含有される階層レベルまで下る(mapが存在しないならフラグにfalseを格納する)
//		//! @param[in] hierlkyNum 含有される空間番号
//		//! @param[in] startMap サーチ開始map 
//		//! @param[in] existMap mapがあるか
//		//! @return 含有される階層のmap
//[[nodiscard]] [[deprecated("reason std::set certainly is contained information exist key.")]]
//static OctCell* DownHieralukyIsExistMap(int hierlkyNum, OctCell& startMap, _Outptr_ bool existMap)
//{
//	existMap = true;
//	auto* pmnex = &startMap.next;//next
//	auto* pm = &startMap;//cell
//	const auto LV_ = SerchLevel(hierlkyNum);
//	const auto HIER_NUMS = SerchHieraluky(hierlkyNum);
//	//含有される階層レベルまで下る
//	for (size_t i = 0; i < LV_; i++)//label==0の時飛ばす
//	{
//		// mapがない
//		if (pmnex->count(HIER_NUMS[i]) == 0)
//		{
//			existMap = false;
//			return nullptr;
//		}
//		// 次のオブジェクトへ移行//extractにkeyを入れる
//		pm = &pmnex->extract(HIER_NUMS[i]).mapped();//nextのpmnowへ
//		pmnex = &pm->next;//nextを下る
//	}
//	return pm;
//}