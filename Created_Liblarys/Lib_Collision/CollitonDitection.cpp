#include "CollitonDitection.h"
#include <map>
#include <typeinfo>
#include "CollisionFunc.h"

#define PRIMITIV_ARRAY(Type) Lib_Collition::PrimitiveVariantIndex::index_of_t<Lib_Collition::Type>::value

namespace Lib_Collition {
	namespace {
		//! 当たり判定関数関数ポインタ
		using Collision_ = bool(*)(const PrimitiveVariant&, const PrimitiveVariant&);

		//! def MapArray
		// Type1		衝突判定関数の引数１の型	//key
		// Type2		衝突判定関数の引数2の型	//key
#define MapArray(Type1, Type2) std::make_pair(PRIMITIV_ARRAY(Type1), PRIMITIV_ARRAY(Type2))

		//! key(クラス型名)から当たり判定を選ぶmap配列
		static const std::map<std::pair<const int, const int>, Collision_> mp{
			{ MapArray(Line, Plane)		,CollisionLinePlane },
			{ MapArray(Plane, Line)		,CollisionLinePlaneRe },
			{ MapArray(Capsule, Capsule)	,CollisionCapsuleCapsule},
			{ MapArray(Plane, AABB)		,CollisionPlaneAABB },
			{ MapArray(Point, Triangle)	,CollisionPointTryangle },
			{ MapArray(Segment, Plane)	,CollisionSegmentPlane },
			{ MapArray(Segment, Square)	,CollisionSegmentSquare },
		};
	}

	//! マップがキーを持っているならtrue
	bool IsKeyExist(const PrimitiveVariant& cl1, const PrimitiveVariant& cl2)
	{
		if (mp.count(std::make_pair(cl1.index(), cl2.index())) == 1)
			return true;
		return false;
	}
	//! 当たり判定の種類ごとに当たり判定をさせる
	//! @param[in] cl1 当たり判定オブジェクト1
	//! @param[in] cl2 当たり判定オブジェクト2
	[[nodiscard]] bool CollisionSwitch(const PrimitiveVariant&  cl1, const PrimitiveVariant&  cl2)
	{
		const auto& func = mp.at(std::make_pair(cl1.index(), cl2.index()));
		return func(cl1, cl2);
	}


}