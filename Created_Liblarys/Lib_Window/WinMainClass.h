#pragma once
/**
* @file WinMainClass.h
* @brief mainウインドウの隠蔽クラスが記述されている
* @details ウインドウ定義も書かれている
*/
#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
#include <Windows.h>
//#include "WindowCleate.h"
#include "high_resolution_timer.h"
#define WINMAIN_CLASS_NAME ("MainWindow")//メインウインドウにつけるウインドウクラス名

//! @namespace Lib_Base
//! ベースライブラリ
namespace Lib_Window
{

	/**
	* @brief mainウインドウの隠蔽クラス
	* @par 詳細
	* winMainがある<br>
	* このクラスを継承したクラスをnewすることで、そのクラスのメインループが行われる<br>
	* my_instanceにＮＵＬＬを入れると終了する
	*/
	class WinMainClass
	{
	private:
		static WinMainClass* _my_previnstance;//! 前フレームのループインスタンス
		static bool _restartFlag;//! リスタートフラグ(trueでリスタート)
		static bool _fpsLockFlag;//! fpsロックするかのフラグ(trueで60fps固定)
		static high_resolution_timer _Timer;//! フレームに影響されないタイマー
	public:
		static HINSTANCE _hInstance;//! window var
		static HINSTANCE _prev_instance_;//! window var
		static PSTR _cmd_lin_;//! window var
		static int _nShowCmd;//! window var
		static WinMainClass* _my_instance;//! 現在のメインループ(これを外で呼ばない!)
	public:
		WinMainClass();
		virtual ~WinMainClass() {}
		//ここから呼ばれる。ここが回る(継承クラスで呼ばない)
		int Boot();

		//! @brief このフレームで、ループを終了するフラグを立てる(MainLoopで呼ぶ)
		static void endLoop()
		{
			_my_instance = nullptr;
		}
		//! @brief 違うメインループへ移行する(MainLoopで呼ぶ)
		static void SetMain(WinMainClass* main)
		{
			_my_instance = main;
		}
		//! @brief ループを最初から始める(MainLoopで呼ぶ)
		//! *ループの初期化前にリスタートフラグはfalseになる
		static void RestartFlagSet()
		{
			_restartFlag = true;
		}
		static float GetTimer()
		{
			return _Timer.time_interval();
		}
	protected:
		//! @brief fpsロックするか?
		//! @param[in] flag (trueで60fps固定)(falseでぶん回し)
		static void SetFpsLockFlag(bool flag) {
			_fpsLockFlag = flag; 
		}
		//! @brief ウインドウにfps表示
		//! @param[in] windowName fpsを表示したいウインドウ名
		//! 1つのウインドウしか表示されない
		//! *ウインドウがなければreturnする
		static void ShowFps(const char* windowName);
	private:
		virtual void MainLoop() {}
		virtual bool Init() = 0;
		virtual bool UnInit() = 0;
		//! @brief メッセージループ
		void MsgLoop();

		//! @brief メインループ(メッセージループで呼ばれる)
		//! return bool trueでこのループを最初からやり直す
		bool MainLoop_()
		{
			MainLoop();
			return _restartFlag;
		}


	};

	namespace MainWindow {
		BOOL InitWindow(HWND& hWnd, const char* className, int left, int top, size_t width, size_t height);
	}
}
