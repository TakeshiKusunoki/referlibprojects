#include "WinMainClass.h"
#include "WindowCleate.h"
#include <sstream>
//! ベンチマークするフラグ
#define BENCH_MARK true

namespace
{
#if _DEBUG
	using WINCHAR = LPCSTR;
#else 
	using WINCHAR = LPCSTR;
#endif
}

namespace Lib_Window
{
	//+ WinMainClass---------------------------------------------------------
	WinMainClass* WinMainClass::_my_instance = NULL;
	WinMainClass* WinMainClass::_my_previnstance = NULL;
	bool WinMainClass::_restartFlag = false;
	bool WinMainClass::_fpsLockFlag = false;

	PSTR   WinMainClass::_cmd_lin_ = nullptr;
	HINSTANCE  WinMainClass::_hInstance = nullptr;
	int WinMainClass::_nShowCmd = 0;
	HINSTANCE WinMainClass::_prev_instance_ = nullptr;
	high_resolution_timer WinMainClass::_Timer{};

	WinMainClass::WinMainClass()
	{
		_my_instance = this;
		_my_previnstance = nullptr;

	}


	int WinMainClass::Boot()
	{
		//WinMainClass();
		_restartFlag = false;

		//初期化
		bool initSafe = Init();
		
		//メインループ
		if (initSafe)
			MsgLoop();
		//終了処理
		bool unInitSafe = UnInit();

		

		//! リスタートフラグが立ってない、またはエラーならループ終了
		if (!_restartFlag || !initSafe || !unInitSafe)
			endLoop();

		//メインループ切り替え//my_instanceがＮＵＬＬなら終了
		if (_my_instance)
			WinMainClass::_my_instance->Boot();

		return 0;
	}

	

	void WinMainClass::MsgLoop()
	{
		LONGLONG CountsPerSecond;
		LONGLONG CountsPerFrame;//1/60秒
		LONGLONG NextCounts;
		LARGE_INTEGER LargeInteger;

		QueryPerformanceFrequency(&LargeInteger);
		CountsPerSecond = LargeInteger.QuadPart;
		CountsPerFrame = CountsPerSecond / 60;

		// 時間計測開始
		QueryPerformanceCounter(&LargeInteger);
		NextCounts = LargeInteger.QuadPart;

		MSG msg;
		do
		{
			Sleep(1);
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) { DispatchMessage(&msg); }
			else
			{
				// フレーム制御
				LARGE_INTEGER count;
				QueryPerformanceCounter(&count);
//#if BENCH_MARK
				//fps固定されているかどうか
				if (this->_fpsLockFlag)
					if (NextCounts > count.QuadPart)
					{
						DWORD t = (DWORD)((NextCounts - count.QuadPart) * 1000 / CountsPerSecond);
						Sleep(t);
						continue;
					}
//#endif
				_Timer.tick();

				//過去のインスタンス取得
				_my_previnstance = _my_instance;

				//メインループが切り替わる、または終わったらループを抜ける
				if (MainLoop_() || _my_instance != _my_previnstance )
				{
					msg.message = WM_QUIT;
				}


				//次の活動予定時間
				NextCounts += CountsPerFrame;
			}
		} while (msg.message != WM_QUIT);//終了メッセージを受け取ったら終了
	}

	void WinMainClass::ShowFps(const char* windowName)
	{
		//ウインドウがあるか
		const Lib_Window::WindowCleate* WINC = pWindowCreateManager->GetWindowWithClassName(windowName);
		if (WINC == nullptr)//error処理
			return;

		// Code computes the average frames per second, and also the
		// average time it takes to render one frame.  These stats
		// are appended to the window caption bar.
		static int frames = { 0 };
		static float time_tlapsed = { 0.0f };
		frames++;

		// Compute averages over one second period.
		if ((_Timer.time_stamp() - time_tlapsed) >= 1.0f)
		{
			float fps = static_cast<float>(frames); // fps = frameCnt / 1
			float mspf = 1000.0f / fps;

			std::ostringstream outs;
			outs.precision(6);
			outs << "FPS : " << fps << " / " << "Frame Time : " << mspf << " (ms)";

			
			HWND hwnd = WINC->GetHWnd();//メインウインドウのウインドウハンドル
			

			SetWindowTextA(hwnd, outs.str().c_str());

			// Reset for next average.
				frames = 0;
				time_tlapsed += 1.0f;
		}
	}





	//+ ここからウインドウの定義------------------------------------------------

	/**
	* @namespace MainWindow
	* @brief メインウインドウ
	* @details 詳細な説明
	*/
	namespace MainWindow {


		LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
		{
			switch (msg)
			{
				case WM_DESTROY:
					PostQuitMessage(0);
					return 0;
				case WM_CLOSE:
				{
					int messageResult = MessageBox(
						hwnd, TEXT("ウィンドウを閉じます。\nよろしいですか？"), TEXT("確認"),
						MB_OKCANCEL | MB_ICONWARNING);
					
					if (messageResult != IDOK) 
						return 0;
					else
					{
						WinMainClass::endLoop();
						DefWindowProc(hwnd, msg, wp, lp);
					}
						
				}
			}
			return DefWindowProc(hwnd, msg, wp, lp);
		}

		// ウインドウ定義
		BOOL InitWindow(HWND& hWnd, const char* className, int left, int top, size_t width, size_t height)
		{
			WNDCLASS winc;

			winc.style = CS_HREDRAW | CS_VREDRAW;
			winc.lpfnWndProc = WndProc;
			winc.cbClsExtra = winc.cbWndExtra = 0;
			winc.hInstance = WinMainClass::_hInstance;
			winc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
			winc.hCursor = LoadCursor(NULL, IDC_ARROW);
			winc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
			winc.lpszMenuName = NULL;
			winc.lpszClassName = (WINCHAR)className;

			if (!RegisterClass(&winc)) return 0;

			hWnd = CreateWindow(
				(WINCHAR)className, TEXT("☆main "),
				WS_OVERLAPPEDWINDOW,
				left, top, (int)width, (int)height, NULL, NULL,
				WinMainClass::_hInstance, NULL
			);

			if (hWnd == NULL) return 0;

			ShowWindow(hWnd, SW_SHOW);

			return (TRUE);
		}
#undef WINMAIN_CLASS_NAME
	}





}
//+	WinMain---------------------------------------------------------
//	windowsアプリが始まると最初にここへ入ります。
int WINAPI WinMain(_In_ HINSTANCE hInstance_,_In_opt_ HINSTANCE prev_instance,_In_ PSTR cmd_lin,_In_ int cmd_show)
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);//メモリリーク検知
#endif // DEBUG
	Lib_Window::WinMainClass::_hInstance = hInstance_;
	Lib_Window::WinMainClass::_prev_instance_ = prev_instance;
	Lib_Window::WinMainClass::_cmd_lin_ = cmd_lin;
	Lib_Window::WinMainClass::_nShowCmd = cmd_show;
	return Lib_Window::WinMainClass::_my_instance->Boot();
}
// You should use this macro when you use new.
//#ifdef _DEBUG
//#define new  ::new(_NORMAL_BLOCK, __FILE__, __LINE__)
//#endif // DEBUG