#pragma once


#include "WinMainClass.h"

namespace Lib_Window
{
	class WindowCleate;
	//! @brief このクラスをウインドウ生成マネージャーから呼んでやる
	class WindowEatherProcess
	{
	public:
		WindowEatherProcess() = default;
		//! @brief ウインドウごとにさせたい処理
		//! @param[in] ウインドウ情報を引数でもらえる
		virtual void WinEatherProcess(const WindowCleate* win) = 0;
		virtual ~WindowEatherProcess(){}
	};


}

