struct VSInput
{
    float4 position : POSITION;
    float4 normal : NORMAL;
    float2 texcoord : TEXCOORD;
    float4 color : COLOR;
    float4 bone_weights : WEIGHTS;
    uint4 bone_indices : BONES;
};


struct VS_OUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float2 texcoord : TEXCOORD;
	// UNIT.20
	//float4 bone_weights : WEIGHTS;
	//uint4 bone_indices : BONES;
    float4 ScreenPos : TEXCOORD1;
};

//// UNIT.20
//struct PS_OUT
//{
//	float4 position : SV_POSITION;
//	float4 color : COLOR;
//	float2 texcoord : TEXCOORD;
//};

#define MAX_BONES 72
cbuffer CONSTANT_BUFFER : register(b0)
{
	row_major float4x4 WVP : packoffset(c0);//wvp
    row_major float4x4 WORLD : packoffset(c4); //ワールド座標
	//float4 MATRIAL_COLOR;	//物体の色
    float4 LIGHT_DIRECTION : packoffset(c8); //ライト進行方向
    float4 EYE_POS : packoffset(c9); //視点座標
    float4 LIGHT_COLOR : packoffset(c10); //光源の色
    float4 NYUTORAL_LIGHT : packoffset(c11); //環境光
    column_major float4x3 BONE_TRANSFORM[MAX_BONES] : packoffset(c12); //ボーン行列
	
   
};