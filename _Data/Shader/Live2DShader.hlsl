cbuffer ConstantBuffer : register(b1)
{
    float4x4 projectMatrix : packoffset(c0);
    float4x4 clipMatrix : packoffset(c4);
    float4 baseColor : packoffset(c8);
    float4 channelFlag : packoffset(c9);
}

Texture2D mainTexture : register(t0);
SamplerState mainSampler : register(s0);
struct VS_IN
{
    float2 pos : POSITION0;
    float2 uv : TEXCOORD0;
};
struct VS_OUT
{
    float4 Position : SV_POSITION;
    float2 uv : TEXCOORD0;
    float4 clipPosition : TEXCOORD1;
};
		
		/* Vertex Shader */
		/* normal */
VS_OUT VertNormal(VS_IN In)
{
    VS_OUT Out = (VS_OUT) 0;
    Out.Position = mul(float4(In.pos, 0.0f, 1.0f), projectMatrix);
    Out.uv.x = In.uv.x;
    Out.uv.y = 1.0 - +In.uv.y;
    return Out;
}
		
		/* Pixel Shader */
		/* normal */
float4 PixelNormal(VS_OUT In) : SV_Target
{
    float4 color = mainTexture.Sample(mainSampler, In.uv) * baseColor;
    return color;
}