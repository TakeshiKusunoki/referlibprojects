#include "skinned_mesh.hlsli"



VS_OUT main(VSInput input)// UNIT.20
{
    VS_OUT vout = (VS_OUT)0;
	// UNIT.20--------------------------------------------------
    /*
    float4 influence = { 0, 0, 0, 1 };
    for (int i = 0; i < 4; i++)
    {
        float weight = input.bone_weights[i];
        if (weight > 0.0f)
        {
            switch (input.bone_indices[i])
            {
                case 0:
                    influence.r = weight;
                    break;
                case 1:
                    influence.g = weight;
                    break;
                case 2:
                    influence.b = weight;
                    break;
            }
        }
    }
    vout.color = influence;
    */
	//--------------------------------------------------------


	// UNIT.21-----------
	float3 p = { 0, 0, 0 };
	float3 n = { 0, 0, 0 };
    //���W���e���͕��ړ�
    for (int i2 = 0; i2 < 4; i2++)
    {
        if (input.bone_weights[i2] == 0)
            continue;
       //p += (input.bone_weights[i2] * mul(input.position, BONE_TRANSFORM[input.bone_indices[i2]])).xyz;
       /*
        p.x += (input.bone_weights[i2] * (
        (input.position.x * BONE_TRANSFORM[input.bone_indices[i2]]._11) +
        (input.position.y * BONE_TRANSFORM[input.bone_indices[i2]]._21) +
        (input.position.z * BONE_TRANSFORM[input.bone_indices[i2]]._31) +
        (1 * BONE_TRANSFORM[input.bone_indices[i2]]._41)));
        p.y += (input.bone_weights[i2] * (
        (input.position.x * BONE_TRANSFORM[input.bone_indices[i2]]._12) +
        (input.position.y * BONE_TRANSFORM[input.bone_indices[i2]]._22) +
        (input.position.z * BONE_TRANSFORM[input.bone_indices[i2]]._32) +
        (1 * BONE_TRANSFORM[input.bone_indices[i2]]._42)));
        p.z += (input.bone_weights[i2] * (
        (input.position.x * BONE_TRANSFORM[input.bone_indices[i2]]._13) +
        (input.position.y * BONE_TRANSFORM[input.bone_indices[i2]]._23) +
        (input.position.z * BONE_TRANSFORM[input.bone_indices[i2]]._33) +
        (1 * BONE_TRANSFORM[input.bone_indices[i2]]._43)));
        */
        p.x += (input.bone_weights[i2] * (
        (input.position.x * BONE_TRANSFORM[input.bone_indices[i2]]._11) +
        (input.position.y * BONE_TRANSFORM[input.bone_indices[i2]]._21) +
        (input.position.z * BONE_TRANSFORM[input.bone_indices[i2]]._31) +
        (1 * BONE_TRANSFORM[input.bone_indices[i2]]._41)));
        p.y += (input.bone_weights[i2] * (
        (input.position.x * BONE_TRANSFORM[input.bone_indices[i2]]._12) +
        (input.position.y * BONE_TRANSFORM[input.bone_indices[i2]]._22) +
        (input.position.z * BONE_TRANSFORM[input.bone_indices[i2]]._32) +
        (1 * BONE_TRANSFORM[input.bone_indices[i2]]._42)));
        p.z += (input.bone_weights[i2] * (
        (input.position.x * BONE_TRANSFORM[input.bone_indices[i2]]._13) +
        (input.position.y * BONE_TRANSFORM[input.bone_indices[i2]]._23) +
        (input.position.z * BONE_TRANSFORM[input.bone_indices[i2]]._33) +
        (1 * BONE_TRANSFORM[input.bone_indices[i2]]._43)));
        n += (input.bone_weights[i2] * mul(float4(input.normal.xyz, 0), BONE_TRANSFORM[input.bone_indices[i2]])).xyz;
    }
    

   /*
    for (int i2 = 0; i2 < 4; i2++)
    {
        if (input.bone_weights[i2] == 0)
            continue;
           
        
        p.x += (bone_weights[i2] * (
        (position.x * BONE_TRANSFORM[bone_indices[i2]]._11) +
        (position.y * BONE_TRANSFORM[bone_indices[i2]]._21) +
        (position.z * BONE_TRANSFORM[bone_indices[i2]]._31) +
        (1 * BONE_TRANSFORM[bone_indices[i2]]._41)));
        p.y += (bone_weights[i2] * (
        (position.x * BONE_TRANSFORM[bone_indices[i2]]._12) +
        (position.y * BONE_TRANSFORM[bone_indices[i2]]._22) +
        (position.z * BONE_TRANSFORM[bone_indices[i2]]._32) +
        (1 * BONE_TRANSFORM[bone_indices[i2]]._42)));
        p.z += (bone_weights[i2] * (
        (position.x * BONE_TRANSFORM[bone_indices[i2]]._13) +
        (position.y * BONE_TRANSFORM[bone_indices[i2]]._23) +
        (position.z * BONE_TRANSFORM[bone_indices[i2]]._33) +
        (1 * BONE_TRANSFORM[bone_indices[i2]]._43)));
        
            //p += (bone_weights[i2] * mul(position, BONE_TRANSFORM[bone_indices[i2]])).xyz;
        
        n.x += (bone_weights[i2] * (
        (normal.x * BONE_TRANSFORM[bone_indices[i2]]._11) +
        (normal.y * BONE_TRANSFORM[bone_indices[i2]]._21) +
        (normal.z * BONE_TRANSFORM[bone_indices[i2]]._31) +
        (1 * BONE_TRANSFORM[bone_indices[i2]]._41)));
        n.y += (bone_weights[i2] * (
        (normal.x * BONE_TRANSFORM[bone_indices[i2]]._12) +
        (normal.y * BONE_TRANSFORM[bone_indices[i2]]._22) +
        (normal.z * BONE_TRANSFORM[bone_indices[i2]]._32) +
        (1 * BONE_TRANSFORM[bone_indices[i2]]._42)));
        n.z += (bone_weights[i2] * (
        (normal.x * BONE_TRANSFORM[bone_indices[i2]]._13) +
        (normal.y * BONE_TRANSFORM[bone_indices[i2]]._23) +
        (normal.z * BONE_TRANSFORM[bone_indices[i2]]._33) +
        (1 * BONE_TRANSFORM[bone_indices[i2]]._43)));
        
    }
    */
    float4 position = float4(p, 1.0f);
    float4 normal = float4(n, 0.0f);
	//normal.w = 0;
	//-----------------------

    vout.position = mul(position, WVP);
	vout.texcoord = input.texcoord;
	float4 N = normalize(mul(normal, WORLD));

	float4 L = normalize(-LIGHT_DIRECTION);
    vout.ScreenPos =
		position / position.w;

	//���ʔ���
	/*float3 l = normalize(L.xyz - position.xyz);
	float3 no = normalize(normal.xyz);
	float3 r = 2.0 * no * dot(no, l) - l;
	float3 v = normalize(EYE_POS.xyz - position.xyz);
	float  i = pow(saturate(dot(r, v)), MATRIAL_COLOR.w);*/

	//�n�[�t�x�N�g�����ʔ���
    float3 l = normalize(LIGHT_DIRECTION.xyz - position.xyz);
    float3 no = normalize(normal.xyz);
    float3 v = normalize(EYE_POS.xyz - position.xyz);
    float3 h = normalize(l + v);
    float i = pow(saturate(dot(h, v)), input.color.w);

    vout.color = float4(i * input.color.xyz * LIGHT_COLOR.xyz, input.color.a);


	//�����łڂ���
	//vout.color *= max(0, dot((L.xyz*(EYE_POS.xyz-position.xyz)), N));
	//vout.color *= max(0, dot(L, N));
	//���̉e
	vout.color *= max(0, dot(L, N));
	//����
	vout.color += NYUTORAL_LIGHT;
	vout.color.a = 1;
    
	return vout;
}