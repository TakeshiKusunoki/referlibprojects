


struct GSSceneIn
{
    bool whereSide; //この頂点は切り離されるときどちら側か
    bool isSeparate;//この頂点は切り離されるか?
     //切り離されるなら切り離されるフラグでない隣接頂点と新しい頂点で生成
};
//  ・    1
//・　・0  2
struct GSOutput
{
  float4 pos : SV_POSITION;
  float4 color : COLOR0;
};
cbuffer CONSTANT_BUFFER : register(b0)
{
    //float3 newpos; //新頂点の位置情報
    float timer;
}
    static const float4 trianglePos[3] = {
  float4(0.f, 0.5f, 0.f, 0.f),
  float4(0.5f, -0.5f, 0.f, 0.f),
  float4(-0.5f, -0.5f, 0.f, 0.f),
};

//maxvertexcount属性：最大3つの頂点を生成することを宣言している
[maxvertexcount(3)]
void main(
  point GSSceneIn input[3] : POSITION,
  inout TriangleStream<GSOutput> output //三角形を生成するときに使うもの
)
{
  //点から三角形を生成するシェーダ
  //三角形を構成する頂点の生成するループ
  [unroll]
    for (uint i = 0; i < 3; i++)
    {
        GSOutput element;
        element.pos = input[0] + trianglePos[i];
        element.color = float4(1.0f, 1.0f, 0.3f, 1.f);
        
        output.Append(element); //ここで頂点を生成している
    }
    output.RestartStrip(); //Append関数で生成した頂点を三角形として構成している
}