// Assets/base.fx
cbuffer CBPerFrame : register( b0 )
{
	matrix  World;
	matrix  View;
	matrix  Proj;
	matrix  WVP;
    float timer;
};

Texture2D Diffuse : register(t0); //textureスロット0番をDiffuseとゆう名前で使う
SamplerState Decale : register(s0);
struct VSInput
{
	float3 Position : POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
	float4 Color : COLOR;
};

struct GSInput
{
    float3 Position : POSITION;
    float3 Normal : NORMAL;
    float2 Tex : TEXCOORD;
    float4 Color : COLOR;
};

struct PSInput
{
	float4 Position : SV_POSITION;  //!< 位置座標です.
	float2 Tex : TEXCOORD;
	float4 Color : COLOR;
	float4 ScreenPos : TEXCOORD1;
};


static const float3 PlaneNormal = float3(0.5f, -1.f, 1.f);
static const float PlaneHeight = 10.0f;

VSInput VSMain(VSInput input)
{
    VSInput output = (VSInput) 0;

    float3 LinePos = { 0, 0, 0 };
    float3 LineDirection = { 0, 0, 0 };
    float y = 10000;
    float kousaFlag = 0;
   //平面より上にあるか ?
    LinePos = input.Position.xyz;
    LineDirection = float3(0, 1, 0);
        {
        //平面原点からのベクトル
        float l = dot(LinePos, normalize(PlaneNormal));
        float l2 = dot(float3(0, PlaneHeight, 0), normalize(PlaneNormal)) - l;
        float3 vec = normalize(PlaneNormal) * l2;
            //線の方向と平面原点からのベクトルがなすcosθ
        float cos = dot(normalize(LinePos), normalize(vec));
            //if (cos <= 0) //cos== 0にならないようにする
                
        float naname = length(vec) / cos;
        float3 PlanePosY = LinePos + normalize(LineDirection) * naname;
        y = PlanePosY.y;
        
    }
    float4 P = float4(input.Position.xyz, 1.0);
    if (input.Position.y > y)
        kousaFlag = 1;
    if(y < 0)
        kousaFlag = 0;
    //代入
    output.Color = input.Color;
    output.Color.b = y;
    output.Color.r = kousaFlag;
    output.Normal = input.Normal;
    output.Tex = input.Tex;
    output.Position = input.Position;
    output.Position.y = input.Position.y;
    return output;
}



[maxvertexcount(3)]
void GSMain(triangle VSInput input[3], inout TriangleStream<PSInput> outStream)
{
    //結ぶ線の傾き
    float3 Line0 = input[0].Position - input[1].Position;
    float3 Line1 = input[1].Position - input[2].Position;
    float3 Line2 = input[2].Position - input[0].Position;
    
    //平面上の点
    float3 PlaneP = PlaneHeight * PlaneNormal;
    float3 P0 = PlaneP - input[0].Position;
    float3 P1 = PlaneP - input[1].Position;
    float3 P2 = PlaneP - input[2].Position;
    //平面と内積
    float P0Dot = dot(P0, normalize(PlaneNormal));
    float P1Dot = dot(P1, normalize(PlaneNormal));
    float P2Dot = dot(P2, normalize(PlaneNormal));
    
    float3 LinePos = { 0, 0, 0 };
    float3 LineDirection = { 0, 0, 0 };
    float y = 0;
    float kousaFlag = input[0].Color.r;
    if (input[1].Color.r == 1)
    {
        kousaFlag = 1;

    }
    if (input[2].Color.r == 1)
    {
        kousaFlag = 1;

    }
 //   outStream.RestartStrip();
    bool kousa = false;
    [unroll]
    for (int i = 0; i < 3; i++)
    {
        PSInput output = (PSInput)0;
        
        float4 P = float4(input[i].Position.xyz, 1.0);
        
        //もし交差しているならポリゴンを作り替える
       
       if (kousaFlag == 1)
       // if (input[i].Color.b < P.y)
        {
           // P.z = (input[i].Position.z +input[i].Position.z /5) / input[i].Position.z ;
            //if (P.y >= 5)
            {
                if (timer < 30)
                {
                    P.x = input[i].Position.x + 0.3f * timer;
                    P.y = input[i].Position.y + 0.3f * timer;
                }
                else
                {
                    P.x = input[i].Position.x + 0.3f * (timer - (timer - 30));
                    P.y = input[i].Position.y + 0.3f * (timer - (timer - 30));
                    P.x += 0.3f * (timer - 30);
                    P.y -= 0.3f * (timer - 30);
                }
                
            }
            
        }
        
        
        
        float4 projPos = mul(WVP, P);

	// 出力値設定.
        output.Position = projPos;
        output.Color = 1; // input.Color;
        output.Tex = input[i].Tex;
        output.ScreenPos =
		projPos / projPos.w;
        
        outStream.Append(output);
    }
    outStream.RestartStrip();

}

// ピクセルシェーダー
// 画面にピクセルを打つときのプログラム
float4 PSMain( PSInput input ) : SV_TARGET0
{
	float4 color;
    color = Diffuse.Sample(Decale, input.Tex);
	return color;

}

