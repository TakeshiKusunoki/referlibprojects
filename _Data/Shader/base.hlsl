// Assets/base.fx
cbuffer CBPerFrame : register( b0 )
{
	matrix  World;
	matrix  View;
	matrix  Proj;
	matrix  WVP;
};

Texture2D Diffuse : register(t0); //textureスロット0番をDiffuseとゆう名前で使う
SamplerState Decale : register(s0);
struct VSInput
{
	float3 Position : POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
	float4 Color : COLOR;
};

struct PSInput
{
	float4 Position : SV_POSITION;  //!< 位置座標です.
	float2 Tex : TEXCOORD;
	float4 Color : COLOR;
	float4 ScreenPos : TEXCOORD1;
};

PSInput VSMain( VSInput input )
{
	PSInput output = (PSInput)0;

	float4 P = float4(input.Position, 1.0);
	float4 projPos = mul(WVP,P);

	// 出力値設定.
	output.Position = projPos;
	output.Color = 1;// input.Color;
	output.Tex = input.Tex;
	output.ScreenPos =
		projPos / projPos.w;
    
	return output;
}

// ピクセルシェーダー
// 画面にピクセルを打つときのプログラム
float4 PSMain( PSInput input ) : SV_TARGET0
{
	float4 color;
    color = Diffuse.Sample(Decale, input.Tex);
	return color;

}

