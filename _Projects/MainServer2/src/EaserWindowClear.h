#pragma once
#include <Lib_Window/WindowEatherProcess.h>
#include <Lib_DirectX11/DirectX11Init.h>
#include <unordered_map>
//#include <Lib_DirectX11/Renderer/RenderTarget.h>
#include <Lib_Base/Flag.h>
#include <Lib_DirectX11\Renderer\Render.h>
namespace WinFunc
{
	
	//! それぞれのウインドウの画面をクリア
	class EaserWindowClear final : public Lib_Window::WindowEatherProcess
	{
		Lib_3D::DirectX11ComInit* com;//! このウインドウの時のcom
		const std::unordered_map<const Lib_Window::WindowCleate*, Lib_3D::DirectX11ComInit*>& commap_;//! DirectXcom map
		
	public:
		EaserWindowClear() = default;
		~EaserWindowClear() = default;

		//! ウインドウをクリア
		void WinEatherProcess(const Lib_Window::WindowCleate* win)override
		{
			com = commap_.at(win);
			com->ClearRenderTarget(255, 255, 255, 0);//レンダーターゲットだけをクリア
			//ビューポートごとに描画
			for (size_t i = 0; i < com->GetViewNum(); i++)
			{
				com->target[i].RenderTargetClear(255, 255, 255, 0);//ビューポートレンダーターゲットをクリア
			}
		}
	};

	//! それぞれのウインドウに描画
	class EaserWindowRender final : public Lib_Window::WindowEatherProcess
	{
		bool _standByMode = false;//! スタンバイモード
		//! 描画
		void Draw(const Lib_Window::WindowCleate*);
		//! このウインドウの画面ピクセルを取得
		void CupturePixels(const Lib_Window::WindowCleate*);
		const std::unordered_map<const Lib_Window::WindowCleate*, Lib_3D::DirectX11ComInit*>& commap_;//! DirectXcom map
		Lib_3D::DirectX11ComInit* com;//! このウインドウの時のcom
		//Lib_3D::Lib_Render::RenderTarget renderTarget[8];
		static Lib_Base::Utility::FlagForOneTimeCall oneTimeCallFlag;
		static Lib_Base::Utility::DecideSupport::RecognizeFlagOn<Lib_Base::Utility::FlagForOneTimeCall> recongyOneTimeCall;

		static Lib_3D::Lib_Render::FBXOBJ fbxObj;
		static Lib_3D::Lib_Render::LIVE2D live2D;
		static Lib_3D::Lib_Render::OBJ3D obj3d;
		static Lib_3D::Lib_Render::OBJ4D obj4d;
		static Lib_3D::Lib_Render::OBJAT objat;
		static Lib_3D::Lib_Render::XVERTEX xvertex;
		static Lib_3D::ShaderInfo::RENDER_TYPE renderType;
		//static Lib_3D::Lib_Render::OBJ* obj;//描画するレンダーオブジェのポリモーフィズム
		std::unordered_map <Lib_3D::ShaderInfo::RENDER_TYPE, Lib_3D::Lib_Render::OBJ*> objInsMap{
			std::make_pair(Lib_3D::ShaderInfo::RENDER_TYPE::FBXOBJ, &fbxObj),
			std::make_pair(Lib_3D::ShaderInfo::RENDER_TYPE::LIVE2D,&live2D),
			std::make_pair(Lib_3D::ShaderInfo::RENDER_TYPE::OBJ3D,&obj3d),
			std::make_pair(Lib_3D::ShaderInfo::RENDER_TYPE::OBJ4D,&obj4d),
			std::make_pair(Lib_3D::ShaderInfo::RENDER_TYPE::OBJAT,&objat),
			std::make_pair(Lib_3D::ShaderInfo::RENDER_TYPE::XVERTEX,&xvertex)
		};
	public:
		EaserWindowRender(std::unordered_map<const Lib_Window::WindowCleate*, Lib_3D::DirectX11ComInit*>& commap_) :commap_(commap_), com(nullptr) {};
		~EaserWindowRender() = default;
		//! @brief ウインドウの数だけ呼び出される
		//! レンダー
		void WinEatherProcess(const Lib_Window::WindowCleate* win)override
		{
			com = commap_.at(win);
			//ビューポートごとに描画
			Draw(win);
		}
		static void Render();
	private:
		static void RenderSwitchModelUse(const Lib_3D::Lib_Render::Model* model, int shaderNum);
		
	};

	//! それぞれのウインドウを絵を見せる
	class EaserWindowFlip final : public Lib_Window::WindowEatherProcess
	{
		const std::unordered_map<const Lib_Window::WindowCleate*, Lib_3D::DirectX11ComInit*>& commap_;//! DirectXcom map
		Lib_3D::DirectX11ComInit* com;//! このウインドウの時のcom
		
	public:
		EaserWindowFlip() = default;
		~EaserWindowFlip() = default;

		 //! フリップ
		void WinEatherProcess(const Lib_Window::WindowCleate* win)override
		{
			com = commap_.at(win);
			//このウインドウに描画
			com->RendetTargetSet();//描画するレンダーターゲットをスクリーンに変える
			//ビューポートごとに描画
			for (size_t i = 0; i < com->GetViewNum(); i++)
			{
				com->target[i].SetView();//ビューポートセット
				com->target[i].RenderPostEffect();//テクスチャをウインドウに描画
			}
			com->Flip0();//この画面に描画(コマンドが送信される)
		}
	};
	
}

