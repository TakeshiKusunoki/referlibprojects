//#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
//#include <windows.h>
//#include <assert.h>



// すべての静的ライブラリ
//DirectX
#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"dxgi.lib")
#pragma comment (lib,"d3dcompiler.lib")
#pragma comment(lib, "ws2_32.lib")

#ifdef _DEBUG
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MTd.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerSoundXAudio2.lib")
#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/debug/libfbxsdk-mt.lib")
#pragma comment ( lib, "../../Exterior_Liblarys/DirectXTex/Debug/DirectXTexT.lib")
#else
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MT.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerSoundXAudio2.lib")
#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/release/libfbxsdk-mt.lib")
#pragma comment ( lib, "../../Exterior_Liblarys/DirectXTex/Release/DirectXTexT.lib")
#endif

#include <Lib_Window/WinMainClass.h>
#include <Lib_Window/WindowCleate.h>
#include "src/Server.h"
//! @brief メイン関数で呼ばれる関数
//! par 詳細
//! Lib_Base::からウインドウの定義を呼べる
//! @details main.cppを変えることでwinMain関数の中身を変えることができる
class Main final : public Lib_Window::WinMainClass
{
public:
	//! @brief 最初に１度だけ呼ばれる
	Main(void) : Lib_Window::WinMainClass::WinMainClass() {};
	Main(const Main&) = default;//メインループ切り替えのため
	~Main(void) = default;
	ConnectCl::Server server;
private:
	//! @brief ループ前初期化
	//! @return falseを返すとループを終了する(その時、MainLoopの処理は行われない。)
	bool Init() override;
	//! @brief メインループ
	void MainLoop() override;
	//! @brief ループ後終了処理
	//! @return falseを返すとループを終了する
	bool UnInit() override;

};

//! @def IS_SAFE
//! @brief エラーが起きなければセーフを返す(falseを返すとループを終了する)
#define IS_SAFE true;
Main main_app{};//メインループの実態

#include <Lib_DirectX11/DirectX11Init.h>
#include <ImGUI/ImGUI/imgui.h>
#include <ImGUI/ImGUI/example/imgui_impl_win32.h>
#include <ImGUI/ImGUI/example/imgui_impl_dx11.h>
#include <ImGUI\ImGUI\imgui_ja_gryph_ranges.cpp>
#include "src/WinFuncs.h"
//#include "src/EaserWindowRender.h"
#include <vector>
#include <map>
#include <unordered_map>
#include <Lib_Base/Flag.h>
#include <Lib_DirectX11/Renderer/Fbx/FbxLoad.h>
#include <Lib_DirectX11/Renderer/Render.h>
#include <Lib_DirectX11/DirectX11Init.h>
#include <Lib_DirectX11/StencilAndRasterState.h>
#include "src/Camera.h"
#include "src/Light.h"
#include "Lib_Window/winMainFunc.h"
#include <thread>
#include <Lib_Template/DimensGrap.h>
#include <Lib_Template/Template.h>
#include "src\EaserWindowClear.h"


namespace
{
	//! @namespace call_externaly
	//! @brief 外のソースコードから呼んでいるもの(このファイル専用に作ったソースではないもの)
	namespace call_externaly
	{
#define GetDevice Lib_3D::DirectX11InitDevice::GetDevice()
#define GetDeviceContext Lib_3D::DirectX11InitDevice::GetImidiateContext()
	}
}


std::vector<Lib_3D::DirectX11ComInit> coms;
std::unordered_map<const Lib_Window::WindowCleate*, Lib_3D::DirectX11ComInit*>commap;
//ウインドウからカメラを見つける
std::unordered_map <const Lib_Window::WindowCleate* ,const Draw::Camera*>cameraWinMap;
Lib_3D::Lib_Render::LoadFbx load;
Lib_3D::Lib_Render::LoadFbx load1;
Lib_3D::Lib_Render::LoadXVertex loadxver;






struct EASY_MODEL final: public Lib_3D::Lib_Render::FBXOBJ
{

};

struct EASY_XVERTEX final : public Lib_3D::Lib_Render::XVERTEX
{
};

struct NNMDEL final: public Lib_3D::Lib_Render::Model
{
	const char* modelname;//モデル名
	unsigned int shaderTypeNum = 0;
private:
	void SetRenderParam(Lib_3D::Lib_Render::FBXOBJ* renderParam)const override
	{
		renderParam->angle = { 0,6.4416f,0 };
		renderParam->position = { 0, 0, -12 };
		renderParam->scale = { 0.1f, 0.1f, 0.1f };
		renderParam->modelname = modelname;
		renderParam->shaderTypeNum = shaderTypeNum;
	}
	void SetRenderParam(Lib_3D::Lib_Render::OBJ3D* renderParam)const  override{}
	void SetRenderParam(Lib_3D::Lib_Render::XVERTEX* renderParam)const override {}
	void SetRenderParam(Lib_3D::Lib_Render::OBJAT* renderParam)const  override {}
	void SetRenderParam(Lib_3D::Lib_Render::LIVE2D* renderParam)const override{}
	void SetRenderParam(Lib_3D::Lib_Render::OBJ4D* renderParam)const  override {}
};
NNMDEL nmodel;



EASY_MODEL model0{};
EASY_MODEL model1{};
EASY_XVERTEX xvertex{};

ID3D11RasterizerState* pRasterizerState;
ID3D11DepthStencilState* pDepthStencilState;

Draw::Camera camera;
Draw::Camera camera1;
Draw::Light light;

//初期化 (restat時などにも呼ばれる)
bool Main::Init()
{
	//サーバ用TCP/IPソケットの生成
	Lib_Window::Console();
	//this->server.listen();;
	std::thread th;
	//{

	coms.reserve(2);
	//mainwindow(0番目のウインドウを作成)
	Lib_Window::WindowCreateManager::GetInstance()->AddWindow(Lib_Window::MainWindow::InitWindow, WINMAIN_CLASS_NAME, 800, 0, 1000, 1000);
	Lib_Window::WindowCreateManager::GetInstance()->AddWindow(WinFunc::InitWindowImGui, "imguiWin", 0, 400, 640,480);


	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	//日本語用フォントの設定
	io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\meiryo.ttc", 13.0f, nullptr, glyphRangesJapanese);


	{
		auto winC = Lib_Window::WindowCreateManager::GetInstance()->GetWindowWithClassName(WINMAIN_CLASS_NAME);
		//DirectX com追加
		coms.emplace_back(winC->GetHWnd(), winC->GetWindowWidth(), winC->GetWindowHeight());
		commap[winC] = &coms[0];
		cameraWinMap[winC] = &camera;
		winC = Lib_Window::WindowCreateManager::GetInstance()->GetWindowWithClassName("imguiWin");
		coms.emplace_back(winC->GetHWnd(), winC->GetWindowWidth(), winC->GetWindowHeight());
		commap[winC] = &coms[1];
		cameraWinMap[winC] = &camera1;
		WinFunc::Wndresizebyclient(winC->GetHWnd(), winC->GetWindowWidth(), winC->GetWindowHeight());
		// Setup Platform/Renderer bindings
		ImGui_ImplWin32_Init(winC->GetHWnd());
		ImGui_ImplDX11_Init(GetDevice, GetDeviceContext);
		// Setup Dear ImGui style
		ImGui::StyleColorsClassic();
	}



	//コンスタントバッファ作成
	Lib_3D::ShaderInfo::CBufferSelector::InitConstantBuffer();
	//シェーダをロード
	bool f = Lib_3D::ShaderInfo::ShaderCorrespondsShaderLoad::ShaderCreate();

	//描画ステートを作成
	Lib_3D::StencilAndRasterState().CreateDefaultRasutrizerState(&pRasterizerState);
	Lib_3D::StencilAndRasterState().CreateDefaultStencilState(&pDepthStencilState);
	//モデルをロード
	printf("モデルをロード\n");
	load1.LoadFbxFile("../../_Data/FBX/danbo_fbx/danbo_taiki.fbx");
	load1.AddMotion("neutral", "../../_Data/FBX/danbo_fbx/danbo_atk.fbx");
	load.LoadFbxFile("../../_Data/FBX/iga/iga.fbx");
	//load.LoadFbxFile("../../_Data/FBX/layerd/JNT_00480_ion_H.fbx");
	load.AddMotion("neutral", "../../_Data/FBX/iga/iga@01wait.fbx");

	loadxver.Load("../../_Data/xvertex/uv_checker.png", { 1,1 }, { 1,1 }, 6);

	printf("モデルロード終了。モデルをセット");
	Lib_3D::Lib_Render::ModelManager::SetData("iga", &load);
	Lib_3D::Lib_Render::ModelManager::SetData("danbo", &load1);
	Lib_3D::Lib_Render::ModelManager::SetData("xv", &loadxver);

	//初期化
	model0.angle = { 0,6.4416f,0 };
	model0.position = { 0, 0, -12 };
	model0.scale = { 0.2f, 0.2f, 0.2f };
	model0.shaderTypeNum = 1;
	model0.modelname = "danbo";
	//model0.modelname = "iga";
	model0.motionname = "neutral";

	model1.angle = { 0,6.4416f,0 };
	model1.position = { 3, 0, -12 };
	model1.scale = { 0.5f,0.5f,0.5f };
	model1.shaderTypeNum = 2;
	model1.modelname = "iga";

	xvertex.angle = {0,0,0};
	xvertex.modelname = "xv";
	xvertex.shaderTypeNum = 1;
	xvertex.scale = { 5,5,5 };
	xvertex.position = { 0,0,-12 };



	camera.camera.position = { 0.0f, 1.2f, 20.0f };
	camera.camera.target = { 0.0f, 1.2f, -12.0f };
	camera.camera.up = { 0.0f, 1.0f, 0.0f };
	camera.camera.aspect = (float)coms[0].GetScreenWidth() / coms[0].GetScreenHeight();
	camera.camera.znear = 0.1f;
	camera.camera.zfar = 1000.f;
	camera.camera.fovY = 0.7f;

	camera1.camera.position = { 0.0f, 1.2f, 1.0f };
	camera1.camera.target = { 0.0f, 1.2f, -12.0f };
	camera1.camera.up = { 0.0f, 1.0f, 0.0f };
	camera1.camera.aspect = (float)coms[1].GetScreenWidth() / coms[1].GetScreenHeight();
	camera1.camera.znear = 0.1f;
	camera1.camera.zfar = 1000.f;
	camera1.camera.fovY = 0.7f;

	light.light.color.var = { 0.9f,0.9f,0.9f,0.9f };
	light.light.lightDirection.var = { 0,-1,-1, 0 };
	light.light.nyutralColor.var = { 0.2f,0.2f,0.2f,1 };



	this->SetFpsLockFlag(true);

	return IS_SAFE;
}
//コマンドで別のゲームループへ
void Main::MainLoop()
{
	// Start the Dear ImGui frame
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();

	this->ShowFps(WINMAIN_CLASS_NAME);

	static bool f_open = true;
	// Our state
	bool show_demo_window = false;
	static bool show_another_window = true;
	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
	// 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
	if (show_demo_window)
		ImGui::ShowDemoWindow(&show_demo_window);
	static float f = 0.0f;
	static int counter = 0;
	static int vertexnum = 1;
	static bool devide = true;
	// 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
	{
		ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.

		ImGui::Text("Paramator");               // Display some text (you can use a format strings too)
		ImGui::Checkbox("Another Window", &show_another_window);

		ImGui::SliderFloat("model0 x", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
		//ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

		//シェーダー名
		static Lib_3D::ShaderInfo::GetShaderInfo::GET_SHADER_NAME name;
		bool flag[Lib_3D::ShaderInfo::GetShaderInfo::SHADER_TORTAL_NUM] = { false };
		for (size_t i = 0; i < Lib_3D::ShaderInfo::GetShaderInfo::SHADER_TORTAL_NUM; i++)
		{
			ImGui::Checkbox("shader", &flag[i]);
			if (flag[i])
			{
				nmodel.shaderTypeNum = i;
			}
			ImGui::SameLine();
			ImGui::Text("%s\n", name.FuncExecute(i));
		}

		if (ImGui::Button("shaderType "))                            // Buttons return true when clicked (most widgets return true when edited/activated)
			counter++;
		const char* renderStr;//レンダータイプ名
		{
			Lib_Template::Utility::ArrayDimensGrap<const char*> in;
			using namespace Lib_3D::ShaderInfo;


			in.InElement(static_cast<int>(RENDER_TYPE::FBXOBJ), TOSTRING(RENDER_TYPE::FBXOBJ));
			in.InElement(static_cast<int>(RENDER_TYPE::LIVE2D), TOSTRING(RENDER_TYPE::LIVE2D));
			in.InElement(static_cast<int>(RENDER_TYPE::OBJ3D), TOSTRING(RENDER_TYPE::OBJ3D));
			in.InElement(static_cast<int>(RENDER_TYPE::OBJ4D), TOSTRING(RENDER_TYPE::OBJ4D));
			in.InElement(static_cast<int>(RENDER_TYPE::OBJAT), TOSTRING(RENDER_TYPE::OBJAT));
			in.InElement(static_cast<int>(RENDER_TYPE::XVERTEX), TOSTRING(RENDER_TYPE::XVERTEX));
			static Lib_3D::ShaderInfo::GetShaderInfo::GET_RENDER_TYPE get;

			if (counter >= Lib_3D::ShaderInfo::GetShaderInfo::SHADER_TORTAL_NUM)
				counter = 0;
			for (size_t i = 0; i < in.arr.size(); i++)
			{
				if (in.arr[i].index == static_cast<int>(get.FuncExecute(counter)))
				{
					renderStr = in.arr[i].var;
					continue;
				}
			}


		}
		ImGui::SameLine();
		ImGui::Text("shaderType = %d %s", counter, renderStr);


		if (ImGui::Button("vertexNum "))                            // Buttons return true when clicked (most widgets return true when edited/activated)
			vertexnum++;
		ImGui::SameLine();
		ImGui::Text("vertexNum = %d", vertexnum);

		if (ImGui::Button("devide "))
			devide = true;
		ImGui::SameLine();
		ImGui::Text("devide = %f", devide);

		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::End();
	}

	// 3. Show another simple window.
	if (show_another_window)
	{
		ImGui::Begin("Model change Window", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
		ImGui::Text("It is model change");
		if (ImGui::Button("iga"))
			model0.modelname = "iga";
		if (ImGui::Button("danbo"))
			model0.modelname = "danbo";
		ImGui::End();
	}
	//頂点数切り替え
	//loadxver.ChangeShape(vertexnum);

	float deltaTime = 1.0f / 5.0f;
	static float flame = 0;
	static float flame1 = 0;

	light.light.nyutralColor.var.x = (float)counter/10;
	model0.shaderTypeNum = counter;

	if (GetAsyncKeyState(VK_LSHIFT) != 0)
		model0.position.z+=0.1f;
	model0.position.x = f;
	//camera1.camera.position = model0.position;
	model1.motionname = "neutral";
	model0.Update();
	model1.Update();


	//camera1.camera.target = model0.angle;
	xvertex.Update();
	xvertex.devideFlag = devide;
	Lib_Math::Primitive::Plane plane{ 0.2f, {1,1,0} };
	xvertex.devidePlane = plane;
	//ほんとうはよくないが・・分割初期化
	if (xvertex.parts[0].get() != nullptr)
	{
		xvertex.parts[0]->position = { 2,2,-12 };
		xvertex.parts[0]->angle = { 0,0,0 };
		xvertex.parts[0]->scale = { 5,5,5 };
		xvertex.parts[0]->shaderTypeNum = 1;
		//xvertex.parts[0]->renderType = Lib_3D::ShaderInfo::RENDER_TYPE::XVERTEX;
		xvertex.parts[0]->Update();
	}
	if (xvertex.parts[1].get() != nullptr)
	{
		xvertex.parts[1]->position = { -2,-2,-12 };
		xvertex.parts[1]->angle = { 0,0,0 };
		xvertex.parts[1]->scale = { 5,5,5 };
		xvertex.parts[1]->shaderTypeNum = 1;
		//xvertex.parts[0]->renderType = Lib_3D::ShaderInfo::RENDER_TYPE::XVERTEX;
		xvertex.parts[1]->Update();
	}

	//カメラ座標変換
	camera.camera.ConvertViewProjection();
	camera1.camera.ConvertViewProjection();


	//画面更新
	Lib_3D::StencilAndRasterState().SetRastarizerState(pRasterizerState);
	Lib_3D::StencilAndRasterState().SetDepthStencilState(pDepthStencilState);

	// レンダー
	WinFunc::EaserWindowRender::Render();


	devide = false;
	return;
}

WinFunc::EaserWindowClear c;//rendertarget clear
WinFunc::EaserWindowFlip r;//present
void WinFunc::EaserWindowRender::Render()
{
	//それぞれのウインドウとビューポートのレンダーターゲットをクリア
	Lib_Window::WindowCreateManager::GetInstance()->ProcessExectuteEachWindow(&c);

	//モデルごとに決めたウインドウのあるビューポート（レンダーターゲット）に描画
	//for (size_t i = 0; i < 10; i++)//コントローラー毎
	{
		// レンダーターゲットへ描画
		WinFunc::EaserWindowRender::RenderSwitchModelUse(&nmodel, nmodel.shaderTypeNum);
	}
	// バックバッファへ描画
	
	Lib_Window::WindowCreateManager::GetInstance()->ProcessExectuteEachWindow(&r);

}



//! RenderクラスにRoleのセッターを適用
template<typename RenderT>//ROLE 役割クラス Render renderクラス
class Adapter
{
public:
	Adapter() = delete;
	Adapter(Adapter&) = delete;
	Adapter(Adapter&&) = delete;
	~Adapter() = default;
	//! モデルの持つoverrideした関数に応じてレンダーに値を設定
	//! param[in] role
	//! param[out] render
	template<typename RenderT>
	static void SetterRenderParam(const Lib_3D::Lib_Render::Model* role, RenderT* render)
	{
		role->SetRenderParam(render);
	}
};


void WinFunc::EaserWindowRender::RenderSwitchModelUse(const Lib_3D::Lib_Render::Model* model, int shaderNum)
{
	WinFunc::EaserWindowRender r{ commap };
	static Lib_3D::ShaderInfo::GetShaderInfo::GET_RENDER_TYPE get;//シェーダーのレンダータイプ取得
	//シェーダーの持つレンダータイプで描画するモデルも変える
	renderType = get.FuncExecute(shaderNum);

	switch (renderType)
	{
	default:
		break;
	case Lib_3D::ShaderInfo::RENDER_TYPE::FBXOBJ:
	{
		//このコードの解説
		//この中でモデル名を変えたりする
		Adapter<Lib_3D::Lib_Render::FBXOBJ>::SetterRenderParam(model, &fbxObj);
		//数値を入れた後、オブジェクトをアップデート
		fbxObj.Update();
		//それぞれのビューへレンダーする
		Lib_Window::WindowCreateManager::GetInstance()->ProcessExectuteEachWindow(&r);
		break;
	}
	case Lib_3D::ShaderInfo::RENDER_TYPE::OBJ3D:
	{
		Adapter<Lib_3D::Lib_Render::OBJ3D>::SetterRenderParam(model, &obj3d);
		obj3d.Update();
		Lib_Window::WindowCreateManager::GetInstance()->ProcessExectuteEachWindow(&r);
		break;
	}
	case Lib_3D::ShaderInfo::RENDER_TYPE::XVERTEX:
	{
		Adapter<Lib_3D::Lib_Render::XVERTEX>::SetterRenderParam(model, &xvertex);
		xvertex.Update();
		Lib_Window::WindowCreateManager::GetInstance()->ProcessExectuteEachWindow(&r);
		break;
	}
	case Lib_3D::ShaderInfo::RENDER_TYPE::OBJAT:
	{
		Adapter<Lib_3D::Lib_Render::OBJAT>::SetterRenderParam(model, &objat);
		objat.Update();
		Lib_Window::WindowCreateManager::GetInstance()->ProcessExectuteEachWindow(&r);
		break;
	}
	case Lib_3D::ShaderInfo::RENDER_TYPE::LIVE2D:
	{
		Adapter<Lib_3D::Lib_Render::LIVE2D>::SetterRenderParam(model, &live2D);
		live2D.Update();
		Lib_Window::WindowCreateManager::GetInstance()->ProcessExectuteEachWindow(&r);
		break;
	}
	case Lib_3D::ShaderInfo::RENDER_TYPE::OBJ4D:
	{
		Adapter<Lib_3D::Lib_Render::OBJ4D>::SetterRenderParam(model, &obj4d);
		obj4d.Update();
		Lib_Window::WindowCreateManager::GetInstance()->ProcessExectuteEachWindow(&r);
		break;
	}
	}
}

void WinFunc::EaserWindowRender::Draw(const Lib_Window::WindowCleate* winC)
{
	auto obj = objInsMap[this->renderType];//オブジェクトを選定
	//ウインドウ毎
	if (winC->GetClassName_() == "imguiWin")
	{
		// Rendering
		ImGui::Render();
		ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
	}

	if (winC->GetClassName_() == WINMAIN_CLASS_NAME)
	{
		light.light.SetLightVariable(obj->shaderTypeNum);
	}

	//! このウインドウに描画するか？
	for (size_t i = 0; i < obj->SAME_DRAW_WINDOW_MAX; i++)
	{
		if (winC->GetClassName_() != obj->windowName[i])
			return;
	}
	
	
	//! ビューポートの数だけレンダーターゲットへセット
	// 描画ターゲット・ビューの設定
	//! そのウインドウ特有の処理
	//ビューポートの数だけレンダーターゲットセット
	for (size_t i = 0, viewnum = this->com->GetViewNum(); i < viewnum; i++)
	{
		if (!obj->usingViewFlag.IsRightBitFlag(i))
			continue;
		//レンダーターゲットセット
		this->com->target[i].SetView();
		this->com->target[i].SetRenderTarget();//レンダーターゲットを四角いテクスチャにセット

		//そのビューに対する処理
		this->com->target[i].camera->SetWVPVariable(obj->w, obj->shaderTypeNum);

		//レンダーターゲットへ描画
		Lib_3D::Lib_Render::Renderer::RenderSwitch(obj);
	}


	
}

constexpr UINT SCREEN_WIDTH_MAX = 1960;
constexpr UINT SCREEN_HEIGHT_MAX = 1080;
BYTE screenPixcels[SCREEN_WIDTH_MAX * SCREEN_HEIGHT_MAX * 4];
void WinFunc::EaserWindowRender::CupturePixels(const Lib_Window::WindowCleate* winC)
{
	DirectX::ScratchImage image;
	ID3D11Resource* source = nullptr;
	//ShaderResourceView->GetResource(&source);
	HRESULT hr = CaptureTexture(GetDevice, GetDeviceContext, source, image);
	if (FAILED(hr))
	{
		assert(!"CaptureTexture error");
		return;
	}
	uint8_t* ptr = image.GetImage(0, 0, 0)->pixels;

	screenPixcels[winC->GetWindowWidth() * winC->GetWindowHeight()];
	CopyMemory(screenPixcels, ptr, winC->GetWindowWidth() * winC->GetWindowHeight() * 4);
}

//restary時やthis->_my_instance = nullptr;になったとき
bool Main::UnInit()
{
	Lib_Window::WindowCreateManager::GetInstance()->DestoroyAllWindow();
	// Cleanup
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();

	return IS_SAFE;
}
