#pragma once
#include <Lib_Window/WindowEatherProcess.h>
#include <Lib_DirectX11/DirectX11Init.h>
#include <unordered_map>
namespace WinFunc
{
	
	//! それぞれのウインドウの画面をクリア
	class EaserWindowClear final : public Lib_Window::WindowEatherProcess
	{
		bool _standByMode = false;//! スタンバイモード
		class EaserDraw
		{
		public:
			void Draw(const Lib_Window::WindowCleate*);
		};
		EaserDraw draw;
		const std::unordered_map<const Lib_Window::WindowCleate*, Lib_3D::DirectX11ComInit*>&commap;//! DirectXcom map
	public:
		EaserWindowClear(std::unordered_map<const Lib_Window::WindowCleate*, Lib_3D::DirectX11ComInit*>& commap_) :commap(commap_) {};
		~EaserWindowClear() = default;
		//! @brief ウインドウの数だけ呼び出される
		void WinEatherProcess(const Lib_Window::WindowCleate* win)override;

	};
}

