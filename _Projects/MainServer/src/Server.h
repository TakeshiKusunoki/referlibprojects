#pragma once
#include <WinSock2.h>
#include <iostream>
#include <string>
#include <thread>
#include <vector>


namespace ConnectCl
{


	//! クライアント情報クラス
	class Client final
	{
	public:
		SOCKET             sock = INVALID_SOCKET;
		struct sockaddr_in addr;
		std::string        nickname;
	public:
		Client();
		~Client();
		bool isActive();
	};

	//! サーバークラス
	class Server final
	{
	public:
		static const int CLIENT_MAX = 10;//! クライアント総数
	protected:
		SOCKET sock = INVALID_SOCKET;
		struct sockaddr_in addr;
		Client clients[CLIENT_MAX];//! クライアントの情報

	private:
		int active_flag = 0;
	public:
		Server() :Server(9000) {}

		//! コンストラクタ(サーバーソケットを作成)
		//! @param[in] port ポート番号
		Server(int port);
		~Server();

		void listen(int backlog = 10);


		//指定されたバイト数を受信する関数
		//受信バイト数が引数で指定された数を下回る場合
		//受信が完了するまで待機する
		void recvAll(char* dest, int len);

	private:
		//! クライアントを受け入れ出来たらtrue
		bool accept(Client* client);
		//! 使ってないクライアントを調べる
		//! @return 使っていないクライアント番号、全部使っているなら-1
		int getUnusedClientIndex();
	};

}