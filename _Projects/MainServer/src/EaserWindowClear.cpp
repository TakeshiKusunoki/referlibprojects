#include "EaserWindowClear.h"
namespace
{
	//! @namespace call_externaly
	//! @brief 外のソースコードから呼んでいるもの(このファイル専用に作ったソースではないもの)
	namespace call_externaly
	{
#define GetDevice Lib_3D::DirectX11InitDevice::GetDevice()
#define GetDeviceContext Lib_3D::DirectX11InitDevice::GetImidiateContext()
	}
}
namespace WinFunc
{
	void EaserWindowClear::WinEatherProcess(const Lib_Window::WindowCleate* win)
	{
		auto com = this->commap.at(win);

		HRESULT hr = S_OK;
		HWND hwnd = com->GetHwnd();
		// スタンバイモード
		if (_standByMode)
		{
			
			hr = com->GetSwapChain()->Present(0, DXGI_PRESENT_TEST);
			if (hr == DXGI_STATUS_OCCLUDED)// 描画する必要がない(表示領域がない)
				return;

			_standByMode = false;//スタンバイ・モードを解除
		}

		{
#define VIEW_NUM 1
			// 画面描画ターゲットの領域の設定		viewport(ビューポートの寸法を定義します。)ビューポートは -1.0〜1.0 の範囲で作られたワールド座標をスクリーン座標（表示するウインドウのサイズ）に変換するための情報)//
			D3D11_VIEWPORT Viewport[VIEW_NUM];
			Viewport[0].TopLeftX = 0;
			Viewport[0].TopLeftY = 0;
			Viewport[0].Width = static_cast<FLOAT>(com->GetScreenWidth());
			Viewport[0].Height = static_cast<FLOAT>(com->GetScreenHeight());//1lo0O10O0O8sSBloO
			Viewport[0].MinDepth = 0.0f;
			Viewport[0].MaxDepth = 1.0f;

			// 描画ターゲット・ビューの設定
			GetDeviceContext->OMSetRenderTargets(VIEW_NUM, com->GetRenderTargetViewA(), com->GetDepthStencilView());
			GetDeviceContext->RSSetViewports(VIEW_NUM, Viewport);
#undef VIEW_NUM
			// 描画ターゲットのクリア
			float ClearColor[4] = { 0.0f,0.0f,255.0f,0.0f };//クリアする値
			GetDeviceContext->ClearRenderTargetView(com->GetRenderTargetView(), ClearColor);
			// 深度ステンシル リソースをクリアします。
			GetDeviceContext->ClearDepthStencilView(com->GetDepthStencilView(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
			
			//描画タスク
			draw.Draw(win);

			//レンダリングされたイメージをユーザーに表示。
			com->GetSwapChain()->Present(0, 0);

			if (hr == DXGI_STATUS_OCCLUDED)// 描画する必要がない(表示領域がない)
			{
				_standByMode = true;//スタンバイ・モードに入る
			}
		}
		return;
	}
	
}