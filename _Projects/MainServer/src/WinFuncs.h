#pragma once

#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
#include <windows.h>
namespace WinFunc
{
	BOOL InitWindowImGui(HWND& hWnd, const char* className, int left, int top, size_t width, size_t height);
	//! imGuiのためにウインドウをクライアントの大きさに合わせる
	void Wndresizebyclient(HWND hWnd, int x, int y);
	
}//WinFunc
