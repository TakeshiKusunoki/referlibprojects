#include "Server.h"


#include <sstream>
#include <iomanip>
namespace
{
	void debug_log(std::string& message)
	{
#ifdef _DEBUG
		std::cout << "debug:" << message << std::endl;
#endif
	}
	void debug_log(const char* message)
	{
		debug_log(std::string(message));
	}

	void info_log(std::string& message)
	{
		std::cout << "info:" << message << std::endl;
	}

	void info_log(const char* message)
	{
		info_log(std::string(message));
	}
}


namespace ConnectCl
{
	Client::Client()
	{
		debug_log("debug:Client new");
	}
	Client::~Client()
	{
		debug_log("Client delete");
		debug_log("クライアントのソケットを閉じます");
		if (sock != INVALID_SOCKET)
		{
			closesocket(sock);
			sock = INVALID_SOCKET;
		}
	}

	bool Client::isActive()
	{
		return (sock != INVALID_SOCKET);
	}




	Server::Server(int port)
	{
		//WSAの初期化
		WSADATA wsaData;
		auto safe = WSAStartup(MAKEWORD(2, 2), &wsaData);

		//サーバ用TCP/IPソケットの生成
		sock = socket(AF_INET, SOCK_STREAM, 0);

		//接続を受け入れるポート番号とIPアドレスを設定
		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		addr.sin_addr.S_un.S_addr = INADDR_ANY;

		//サーバ用ソケットと接続受け入れ情報を関連付け
		bind(sock, (struct sockaddr*) & addr, sizeof(struct sockaddr_in));
	}

	Server::~Server()
	{
		if (sock != INVALID_SOCKET)
		{
			closesocket(sock);
			sock = INVALID_SOCKET;
		}

		//WSAの終了
		WSACleanup();
	}

	void Server::listen(int backlog)
	{
		//クライアントからの接続を待機する（キュー数は10)
		::listen(sock, backlog);
		//	ノンブロッキング設定
		u_long	val = 1;
		ioctlsocket(sock, FIONBIO, &val);

		active_flag = 1;

		//サーバー終了コマンドの受付スレッド生成と開始
		{
			std::thread th([this]
				{
					int cmd = 0;
					while (1)
					{
						std::cin >> cmd;
						if (cmd == -1) { this->active_flag = 0; }
					}
				});
			th.detach();
		}

		int clientIndex = getUnusedClientIndex();
		while (active_flag)
		{
			//新規クライアントの受け入れ処理
			
			if (this->accept(&clients[clientIndex]))
			{
				std::cout << "クライアントが接続しました" << std::endl;
			}

			//クライアントからのデータ受信

			for (Client& client : clients)
			{
				//未使用のクライアントはスキップ
				if (!client.isActive()) { continue; }
				char com = -1;
				int len = recv(client.sock, &com, 1, 0);
				//コマンド受信
				if (len > 0)
				{

				}
				//エラー
				else
				{
					//戻り値がSOCKET_ERRORでかつエラーコードがWSAEWOULDBLOCK以外は復帰不能
					if (!(len == SOCKET_ERROR && WSAGetLastError() == WSAEWOULDBLOCK))
					{
						client.sock = INVALID_SOCKET;
						std::ostringstream oss;
						oss << "client:" << client.nickname << "の接続に異常が発生した為、強制切断";

					}
				}


			}
		}
	}
	void Server::recvAll(char* dest, int len)
	{}

	bool Server::accept(Client* client)
	{
		int addr_size = sizeof(struct sockaddr_in);
		client->sock = ::accept(this->sock, (struct sockaddr*) & client->addr, &addr_size);

		if (client->sock == INVALID_SOCKET)
			return false;
		//257 = 先頭(1byte) + メッセージ長(1byte) + メッセージの最大長(255)
		char message[257];
		message[0] = 0; //接続時のデータの種類は0
		message[1] = 19;
		strcpy_s(&message[2], 19, "サーバーへようこそ");
		send(client->sock, message, 21, 0);
		//クライアント情報を取得

		return true;
	}
	int Server::getUnusedClientIndex()
	{
		for (int i = 0; i < CLIENT_MAX; i++)
		{
			if (clients[i].sock == INVALID_SOCKET) 
				return i;
		}
		return -1;
	}
}//connect