#include "WinFuncs.h"

#include <Lib_Window/WinMainClass.h>
#include <ImGUI/ImGUI/imgui.h>
#include <ImGUI/ImGUI/example/imgui_impl_win32.h>
#include <ImGUI/ImGUI/example/imgui_impl_dx11.h>
namespace
{
#if _DEBUG
	using WINCHAR = LPCSTR;
#else 
	using WINCHAR = LPCSTR;
#endif
}
// Win32 message handler
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
namespace WinFunc
{
	LRESULT CALLBACK WinProcImGui(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
	{
		if (ImGui_ImplWin32_WndProcHandler(hwnd, msg, wp, lp))
			return true;
		switch (msg)
		{
		case WM_CREATE:
			SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_LAYERED);
			SetLayeredWindowAttributes(hwnd, /*GetSysColor(COLOR_BTNFACE)*/RGB(0, 0, 255), 0, LWA_COLORKEY);
			
			return 0;
		case WM_SYSCOMMAND:
			if ((wp & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
				return 0;
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;
		}
		return DefWindowProc(hwnd, msg, wp, lp);
	}

	BOOL InitWindowImGui(HWND& hWnd, const char* className, int left, int top, size_t width, size_t height)
	{
		// ウインドウクラスを定義する
		WNDCLASSEX WindowClass;
		WindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS | CS_CLASSDC/*|CS_GLOBALCLASS| CS_DBLCLKS*/;//このクラスのウインドウ同士で１つのデバイスコンテキストを共有する
		WindowClass.lpfnWndProc = WinProcImGui;								  //ウィンドウプロシージャ関数
		WindowClass.cbSize = sizeof(WNDCLASSEX);
		WindowClass.cbClsExtra = 0;									  //エキストラ（なしに設定）
		WindowClass.cbWndExtra = 0;									  //必要な情報（なしに設定）
		WindowClass.hInstance = Lib_Window::WinMainClass::_hInstance;								  //このインスタンスへのハンドル
		WindowClass.hIcon = LoadIcon(Lib_Window::WinMainClass::_hInstance, IDI_APPLICATION);		  //ラージアイコン
		WindowClass.hIconSm = LoadIcon(Lib_Window::WinMainClass::_hInstance, IDI_WINLOGO);			 //スモールアイコン
		WindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);				  //カーソルスタイル
		WindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);	  //ウィンドウの背景（黒に設定）
		WindowClass.lpszMenuName = NULL;								  //メニュー（なしに設定）
		WindowClass.lpszClassName = (WINCHAR)className;					 //このウインドウクラスにつける名前

																 // ウインドウクラスを登録する
		if (!RegisterClassEx(&WindowClass))
		{
			OutputDebugString(TEXT("Error: ウィンドウクラスの登録ができません。\n"));
			return false;
		}
		// ウインドウクラスの登録ができたので、ウィンドウを生成する。

		hWnd = CreateWindowEx(
			WS_EX_ACCEPTFILES | WS_EX_TOPMOST,			   //拡張ウィンドウスタイル
			(WINCHAR)className,								//ウィンドウクラスの名前
			className,							//ウィンドウタイトル
			WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_SYSMENU,	   //ウィンドウスタイル
			left,												//
			top,												//
			(int)width,									   //
			(int)height,									   //
			NULL,											//親ウィンドウ（なし）
			NULL,											//メニュー（なし）
			Lib_Window::WinMainClass::_hInstance,										//このプログラムのインスタンスのハンドル
			NULL											   //追加引数（なし）
		);
		
		if (!hWnd)
		{
			OutputDebugString(TEXT("Error: ウィンドウが作成できません。\n"));
			return false;
		}

		ShowWindow(hWnd, Lib_Window::WinMainClass::_nShowCmd); // ウィンドウを表示する
		UpdateWindow(hWnd);

		return (TRUE);

	}
	void Wndresizebyclient(HWND hWnd, int x, int y)
	{
		{
			RECT size;
			RECT wndsize;
			GetClientRect(hWnd, &size);
			GetWindowRect(hWnd, &wndsize);
			wndsize.right = wndsize.right - wndsize.left;
			wndsize.bottom = wndsize.bottom - wndsize.top;
			SetWindowPos(hWnd, NULL, 0, 0, x + wndsize.right - size.right, y + wndsize.bottom - size.bottom, SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
		}
	}
}//WinFunc
