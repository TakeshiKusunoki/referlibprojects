//#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
//#include <windows.h>
//#include <assert.h>



// すべての静的ライブラリ
//DirectX
#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"dxgi.lib")
#pragma comment (lib,"d3dcompiler.lib")
#pragma comment(lib, "ws2_32.lib")

#ifdef _DEBUG
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MTd.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerSoundXAudio2.lib")
#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/debug/libfbxsdk-mt.lib")
#pragma comment ( lib, "../../Exterior_Liblarys/DirectXTex/Debug/DirectXTexT.lib")
#else
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MT.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerSoundXAudio2.lib")
#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/release/libfbxsdk-mt.lib")
#pragma comment ( lib, "../../Exterior_Liblarys/DirectXTex/Release/DirectXTexT.lib")
#endif

#include <Lib_Window/WinMainClass.h>
#include <Lib_Window/WindowCleate.h>
#include "src/Server.h"
//! @brief メイン関数で呼ばれる関数
//! par 詳細
//! Lib_Base::からウインドウの定義を呼べる
//! @details main.cppを変えることでwinMain関数の中身を変えることができる
class Main final : public Lib_Window::WinMainClass
{
public:
	//! @brief 最初に１度だけ呼ばれる
	Main(void) : Lib_Window::WinMainClass::WinMainClass() {};
	Main(const Main&) = default;//メインループ切り替えのため
	~Main(void) = default;
	ConnectCl::Server server;
private:
	//! @brief ループ前初期化
	//! @return falseを返すとループを終了する(その時、MainLoopの処理は行われない。)
	bool Init() override;
	//! @brief メインループ
	void MainLoop() override;
	//! @brief ループ後終了処理
	//! @return falseを返すとループを終了する
	bool UnInit() override;

};

//! @def IS_SAFE
//! @brief エラーが起きなければセーフを返す(falseを返すとループを終了する)
#define IS_SAFE true;
Main main_app{};//メインループの実態

#include <Lib_DirectX11/DirectX11Init.h>
#include <ImGUI/ImGUI/imgui.h>
#include <ImGUI/ImGUI/example/imgui_impl_win32.h>
#include <ImGUI/ImGUI/example/imgui_impl_dx11.h>
#include <ImGUI\ImGUI\imgui_ja_gryph_ranges.cpp>
#include "src/WinFuncs.h"
#include "src/EaserWindowClear.h"
#include <vector>
#include <map>
#include <unordered_map>
#include <Lib_Base/Flag.h>
#include <Lib_DirectX11/Renderer/Fbx/FbxLoad.h>
#include <Lib_DirectX11/Renderer/Render.h>
#include <Lib_DirectX11/DirectX11Init.h>
#include <Lib_DirectX11/StencilAndRasterState.h>
#include "src/Camera.h"
#include "src/Light.h"
#include "Lib_Window/winMainFunc.h"
#include <thread>


namespace
{
	//! @namespace call_externaly
	//! @brief 外のソースコードから呼んでいるもの(このファイル専用に作ったソースではないもの)
	namespace call_externaly
	{
#define GetDevice Lib_3D::DirectX11InitDevice::GetDevice()
#define GetDeviceContext Lib_3D::DirectX11InitDevice::GetImidiateContext()
	}
}


std::vector<Lib_3D::DirectX11ComInit> coms;
std::unordered_map<const Lib_Window::WindowCleate*, Lib_3D::DirectX11ComInit*>commap;
//ウインドウからカメラを見つける
std::unordered_map <const Lib_Window::WindowCleate* ,const Draw::Camera*>cameraWinMap;
Lib_3D::Lib_Render::LoadFbx load;
Lib_3D::Lib_Render::LoadFbx load1;

//! レンダーを使えるクラス
class Model
{
public:
	Model() = default;
	virtual ~Model() {}
protected://Adapterクラスに公開
public:
#define DEFINE_SetRenderParam SetRenderParam
	virtual void DEFINE_SetRenderParam(Lib_3D::Lib_Render::FBXOBJ* renderParam)const = 0;
	virtual void DEFINE_SetRenderParam(Lib_3D::Lib_Render::OBJ3D* renderParam)const = 0;
	virtual void DEFINE_SetRenderParam(Lib_3D::Lib_Render::XVERTEX* renderParam)const = 0;
	virtual void DEFINE_SetRenderParam(Lib_3D::Lib_Render::OBJAT* renderParam)const = 0;
	virtual void DEFINE_SetRenderParam(Lib_3D::Lib_Render::LIVE2D* renderParam)const = 0;
	virtual void DEFINE_SetRenderParam(Lib_3D::Lib_Render::OBJ4D* renderParam)const = 0;
};

//! RenderクラスにRoleのセッターを適用
template<typename Render>//ROLE 役割クラス Render renderクラス
class Adapter
{
public:
	Adapter() = delete;
	Adapter(Adapter&) = delete;
	Adapter(Adapter&&) = delete;
	~Adapter() = default;
	//! レンダーに値を設定
	//! param[in] role
	//! param[out] render
	template<typename Render>
	static void SetterRenderParam(const Model* role, Render* render)
	{
		role->DEFINE_SetRenderParam(render);
	}
};


struct EASY_MODEL final: public Lib_3D::Lib_Render::FBXOBJ
{

};

struct NNMDEL final: public Model
{
	int switchVar = 0;//シェーダー変更スイッチ
	int shaderTypeNum;//シェーダータイプ番号
	const char* modelname;//モデル名
private:
	void SetRenderParam(Lib_3D::Lib_Render::FBXOBJ* renderParam)const override
	{
		renderParam->angle = { 0,6.4416f,0 };
		renderParam->position = { 0, 0, -12 };
		renderParam->scale = { 0.1f, 0.1f, 0.1f };
		renderParam->shaderTypeNum = switchVar;
	}
	void SetRenderParam(Lib_3D::Lib_Render::OBJ3D* renderParam)const  override{}
	void SetRenderParam(Lib_3D::Lib_Render::XVERTEX* renderParam)const override {}
	void SetRenderParam(Lib_3D::Lib_Render::OBJAT* renderParam)const  override {}
	void SetRenderParam(Lib_3D::Lib_Render::LIVE2D* renderParam)const override{}
	void SetRenderParam(Lib_3D::Lib_Render::OBJ4D* renderParam)const  override {}
};
NNMDEL nmodel;

//! @param[in] renderType シェーダーのレンダータイプ
void RenderSwitcher(const Model* model, Lib_3D::ShaderInfo::RENDER_TYPE renderType)
{
	static Lib_3D::Lib_Render::FBXOBJ obj;//すべての値を書き換えること
	switch (renderType)
	{
	default:
		break;
	case Lib_3D::ShaderInfo::RENDER_TYPE::FBXOBJ:
	{
		Adapter< Lib_3D::Lib_Render::FBXOBJ>::SetterRenderParam(model, &obj);//この中でモデル名を変えたりする
		obj.Update();
		Lib_3D::Lib_Render::Renderer::RenderSwitch(&obj);
		break;
	}
	case Lib_3D::ShaderInfo::RENDER_TYPE::OBJ3D:
	{
		Lib_3D::Lib_Render::OBJ3D obj_;
		Adapter<Lib_3D::Lib_Render::OBJ3D>::SetterRenderParam(model, &obj_);
		obj_.Update(); 
		Lib_3D::Lib_Render::Renderer::RenderSwitch(&obj_);
		break;
	}
	case Lib_3D::ShaderInfo::RENDER_TYPE::XVERTEX:
	{
		Lib_3D::Lib_Render::XVERTEX obj_;
		Adapter<Lib_3D::Lib_Render::XVERTEX>::SetterRenderParam(model, &obj_);
		obj_.Update();
		Lib_3D::Lib_Render::Renderer::RenderSwitch(&obj_);
		break;
	}
	case Lib_3D::ShaderInfo::RENDER_TYPE::OBJAT:
	{
		Lib_3D::Lib_Render::OBJAT obj_;
		Adapter<Lib_3D::Lib_Render::OBJAT>::SetterRenderParam(model, &obj_);
		obj_.Update();
		Lib_3D::Lib_Render::Renderer::RenderSwitch(&obj_);
		break;
	}
	case Lib_3D::ShaderInfo::RENDER_TYPE::LIVE2D:
	{
		Lib_3D::Lib_Render::LIVE2D obj_;
		Adapter<Lib_3D::Lib_Render::LIVE2D>::SetterRenderParam(model, &obj_);
		obj_.Update();
		Lib_3D::Lib_Render::Renderer::RenderSwitch(&obj_);
		break;
	}
	case Lib_3D::ShaderInfo::RENDER_TYPE::OBJ4D:
	{
		Lib_3D::Lib_Render::OBJ4D obj_;
		Adapter<Lib_3D::Lib_Render::OBJ4D>::SetterRenderParam(model, &obj_);
		obj_.Update();
		Lib_3D::Lib_Render::Renderer::RenderSwitch(&obj_);
		break;
	}
	}
}

EASY_MODEL model0{};
EASY_MODEL model1{};
ID3D11RasterizerState* pRasterizerState;
ID3D11DepthStencilState* pDepthStencilState;

Draw::Camera camera;
Draw::Camera camera1;
Draw::Light light;

//初期化 (restat時などにも呼ばれる)
bool Main::Init()
{	
	//サーバ用TCP/IPソケットの生成
	Lib_Window::Console();
	//this->server.listen();;
	std::thread th;
	//for (size_t i = 0; i < 10; i++)
	//{
	//	auto r = Lib_3D::ShaderInfo::GET_SHADER_INFO::INIT::GET_RENDER_TYPE().FuncExecute(i);
	//}
	//int x ;
	//Lib_3D::ShaderInfo::RENDER_TYPE r_ ;
	//Lib_3D::ShaderInfo::GET_SHADER_INFO::SETTER::MakeSequence_(0, &r_, 0);
	//Lib_3D::ShaderInfo::GET_SHADER_INFO::SETTER::MakeSequence_(0, &r_, 1);
	//Lib_3D::ShaderInfo::GET_SHADER_INFO::SETTER::MakeSequence_(0, &x, 0);
	
	coms.reserve(2);
	//mainwindow(0番目のウインドウを作成)
	Lib_Window::WindowCreateManager::GetInstance()->AddWindow(Lib_Window::MainWindow::InitWindow, WINMAIN_CLASS_NAME, 800, 0, 1000, 1000);
	Lib_Window::WindowCreateManager::GetInstance()->AddWindow(WinFunc::InitWindowImGui, "imguiWin", 0, 400, 640,480);
	
	
	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	//日本語用フォントの設定
	io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\meiryo.ttc", 13.0f, nullptr, glyphRangesJapanese);

	
	{
		auto winC = Lib_Window::WindowCreateManager::GetInstance()->GetWindowWithClassName(WINMAIN_CLASS_NAME);
		//DirectX com追加
		coms.emplace_back(winC->GetHWnd(), winC->GetWindowWidth(), winC->GetWindowHeight());
		commap[winC] = &coms[0];
		cameraWinMap[winC] = &camera;
		winC = Lib_Window::WindowCreateManager::GetInstance()->GetWindowWithClassName("imguiWin");
		coms.emplace_back(winC->GetHWnd(), winC->GetWindowWidth(), winC->GetWindowHeight());
		commap[winC] = &coms[1];
		cameraWinMap[winC] = &camera1;
		WinFunc::Wndresizebyclient(winC->GetHWnd(), winC->GetWindowWidth(), winC->GetWindowHeight());
		// Setup Platform/Renderer bindings
		ImGui_ImplWin32_Init(winC->GetHWnd());
		ImGui_ImplDX11_Init(GetDevice, GetDeviceContext);
		// Setup Dear ImGui style
		ImGui::StyleColorsClassic();
	}



	//コンスタントバッファ作成
	Lib_3D::ShaderInfo::CBufferSelector::InitConstantBuffer();
	//シェーダをロード
	bool f = Lib_3D::ShaderInfo::ShaderCorrespondsShaderLoad::ShaderCreate();

	//描画ステートを作成
	Lib_3D::StencilAndRasterState().CreateDefaultRasutrizerState(&pRasterizerState);
	Lib_3D::StencilAndRasterState().CreateDefaultStencilState(&pDepthStencilState);
	//モデルをロード
	printf("モデルをロード\n");
	load1.LoadFbxFile("../../_Data/FBX/danbo_fbx/danbo_taiki.fbx");
	load1.AddMotion("neutral", "../../_Data/FBX/danbo_fbx/danbo_atk.fbx");
	load.LoadFbxFile("../../_Data/FBX/iga/iga.fbx");
	//load.LoadFbxFile("../../_Data/FBX/layerd/JNT_00480_ion_H.fbx");
	load.AddMotion("neutral", "../../_Data/FBX/iga/iga@01wait.fbx");
	printf("モデルロード終了。モデルをセット");
	Lib_3D::Lib_Render::ModelManager::SetData("iga", &load);
	Lib_3D::Lib_Render::ModelManager::SetData("danbo", &load1);

	//初期化
	model0.angle = { 0,6.4416f,0 };
	model0.position = { 0, 0, -12 };
	model0.scale = { 0.2f, 0.2f, 0.2f };
	model0.shaderTypeNum = 1;
	model0.modelname = "danbo";
	//model0.modelname = "iga";
	model0.motionname = "neutral";

	model1.angle = { 0,6.4416f,0 };
	model1.position = { 3, 0, -12 };
	model1.scale = { 0.5f,0.5f,0.5f };
	model1.shaderTypeNum = 2;
	model1.modelname = "iga";

	camera.camera.position = { 0.0f, 1.2f, 20.0f };
	camera.camera.target = { 0.0f, 1.2f, -12.0f };
	camera.camera.up = { 0.0f, 1.0f, 0.0f };
	camera.camera.aspect = (float)coms[0].GetScreenWidth() / coms[0].GetScreenHeight();
	camera.camera.znear = 0.1f;
	camera.camera.zfar = 1000.f;
	camera.camera.fovY = 0.7f;

	camera1.camera.position = { 0.0f, 1.2f, 1.0f };
	camera1.camera.target = { 0.0f, 1.2f, -12.0f };
	camera1.camera.up = { 0.0f, 1.0f, 0.0f };
	camera1.camera.aspect = (float)coms[1].GetScreenWidth() / coms[1].GetScreenHeight();
	camera1.camera.znear = 0.1f;
	camera1.camera.zfar = 1000.f;
	camera1.camera.fovY = 0.7f;

	light.light.color.var = { 0.9f,0.9f,0.9f,0.9f };
	light.light.lightDirection.var = { 0,-1,-1, 0 };
	light.light.nyutralColor.var = { 0.2f,0.2f,0.2f,1 };





	this->SetFpsLockFlag(true);

	

	
	return IS_SAFE;
}
//コマンドで別のゲームループへ
void Main::MainLoop()
{
	// Start the Dear ImGui frame
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();

	this->ShowFps(WINMAIN_CLASS_NAME);
	

	static bool f_open = true;
	// Our state
	bool show_demo_window = false;
	static bool show_another_window = true;
	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
	// 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
	if (show_demo_window)
		ImGui::ShowDemoWindow(&show_demo_window);
	static float f = 0.0f;
	static int counter = 0;
	// 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
	{
		ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.

		ImGui::Text("Paramator");               // Display some text (you can use a format strings too)
		ImGui::Checkbox("Another Window", &show_another_window);

		ImGui::SliderFloat("model0 x", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
		//ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

		if (ImGui::Button("shaderType "))                            // Buttons return true when clicked (most widgets return true when edited/activated)
			counter++;

		ImGui::SameLine();
		ImGui::Text("shaderType = %d", counter);
		if (counter >= Lib_3D::ShaderInfo::GET_SHADER_INFO::SHADER_TORTAL_NUM)
			counter = 0;

		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::End();
	}

	// 3. Show another simple window.
	if (show_another_window)
	{
		ImGui::Begin("Model change Window", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
		ImGui::Text("It is model change");
		if (ImGui::Button("iga"))
			model0.modelname = "iga";
		if (ImGui::Button("danbo"))
			model0.modelname = "danbo";
		ImGui::End();
	}

	float deltaTime = 1.0f / 5.0f;
	static float flame = 0;
	static float flame1 = 0;

	light.light.nyutralColor.var.x = (float)counter/10;
	model0.shaderTypeNum = counter;

	if (GetAsyncKeyState(VK_LSHIFT) != 0)
		model0.position.z+=0.1f;
	model0.position.x = f;
	//camera1.camera.position = model0.position;
	model1.motionname = "neutral";
	model0.Update();
	model1.Update();

	
	//camera1.camera.target = model0.angle;


	//カメラ座標変換
	camera.camera.ConvertViewProjection();
	camera1.camera.ConvertViewProjection();
	

	//画面更新
	Lib_3D::StencilAndRasterState().SetRastarizerState(pRasterizerState);
	Lib_3D::StencilAndRasterState().SetDepthStencilState(pDepthStencilState);
	WinFunc::EaserWindowClear r{ commap };
	Lib_Window::WindowCreateManager::GetInstance()->ProcessExectuteEachWindow(&r);
	
	
	
	return;
}

void WinFunc::EaserWindowClear::EaserDraw::Draw(const Lib_Window::WindowCleate* winC)
{
	light.light.SetLightVariable(model0.shaderTypeNum);
	//カメラ切り替え
	if (winC->GetClassName_() == WINMAIN_CLASS_NAME)
	{
		camera.camera.SetWVPVariable(model0.w, model0.shaderTypeNum);
	}
	if (winC->GetClassName_() == "imguiWin")
	{	
		//camera1.camera.SetWVPVariable(model0.w, model0.shaderTypeNum);
	}
	Lib_3D::Lib_Render::Renderer::RenderSwitch(&model0);

	light.light.SetLightVariable(model1.shaderTypeNum);
	if (winC->GetClassName_() == WINMAIN_CLASS_NAME)
	{
		camera.camera.SetWVPVariable(model1.w, model1.shaderTypeNum);
	}
	if (winC->GetClassName_() == "imguiWin")
	{
		camera1.camera.SetWVPVariable(model1.w, model1.shaderTypeNum);
	}
	
	Lib_3D::Lib_Render::Renderer::RenderSwitch(&model1);

	//RenderSwitcher(&nmodel, );

	if (winC->GetClassName_() == "imguiWin")
	{
		// Rendering
		ImGui::Render();
		ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
	}
}

//restary時やthis->_my_instance = nullptr;になったとき
bool Main::UnInit()
{
	Lib_Window::WindowCreateManager::GetInstance()->DestoroyAllWindow();
	// Cleanup
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();

	return IS_SAFE;
}
