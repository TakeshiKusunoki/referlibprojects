//#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
//#include <windows.h>
#include <assert.h>



// すべての静的ライブラリ
//DirectX
#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"dxgi.lib")
#pragma comment (lib,"d3dcompiler.lib")


#ifdef _DEBUG
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MTd.lib")
#pragma comment (lib, "../../Exterior_Liblarys/DirectXTex/DirectXTexD.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerSoundXAudio2.lib")
#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/debug/libfbxsdk.lib")
#else
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MT.lib")
#pragma comment (lib, "../../Exterior_Liblarys/DirectXTex/DirectXTexD.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerSoundXAudio2.lib")
#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/release/libfbxsdk.lib")
#endif

#include <Lib_Window/WinMainClass.h>
#include <Lib_Window/WindowCleate.h>

//! @brief メイン関数で呼ばれる関数
//! par 詳細
//! Lib_Base::からウインドウの定義を呼べる
//! @details main.cppを変えることでwinMain関数の中身を変えることができる
class Main final : public Lib_Window::WinMainClass
{
public:
	Main(void);
	Main(const Main&) {}
	~Main(void);
private:
	//! @brief ループ前初期化
	bool Init();
	//! @brief メインループ
	void MainLoop();
	//! @brief ループ後終了処理
	bool UnInit();

};










//--------------------------------------------------------------------------------------------------------
#include <memory>
#include "Lib_DirectX11/DirectX11Init.h"
#include "src/CubizmLoadRender.h"
#include "src/CubizmInitialize.h"

Main main_app;//メインループの実態


std::unique_ptr<Lib_3D::DirectX11ComInit> directX11Com;//DirectX11Com初期化
//Lib_3D::DirectX11ComInit* directX11Com;
//最初に１度だけ呼ばれる
Main::Main(void) : Lib_Window::WinMainClass::WinMainClass()
{
	//mainWindow関数を自由に設定
	CubizmInitializer::Initialize();
}

//初期化 (restat時などにも呼ばれる)
bool Main::Init()
{
	//mainwindow(0番目のウインドウを作成)
	Lib_Window::WindowCreateManager::GetInstance()->AddWindow(Lib_Window::MainWindow::InitWindow, WINMAIN_CLASS_NAME, 0, 0, 1000, 1000);
	this->SetFpsLockFlag(true);
	
	//DirectX11Com初期化--------------
	const auto* winC = Lib_Window::WindowCreateManager::GetInstance()->GetWindowWithClassName(WINMAIN_CLASS_NAME);
	
	if (!directX11Com)
		directX11Com = std::make_unique<Lib_3D::DirectX11ComInit>(winC->GetHWnd(), winC->GetWindowWidth(), winC->GetWindowHeight());
	//directX11Com = new Lib_3D::DirectX11ComInit(winC->GetHWnd(), winC->GetWindowWidth(), winC->GetWindowHeight());

	//LoadMocFile
	CubizmTest4::getInstance()->InitializeCubism(Lib_3D::DirectX11InitDevice::GetDevice());
	CubizmTest4::getInstance()->Load("../../_Data/Live2D/Haru/", "Haru.model3.json");
	
	return true;
}

//コマンドで別のゲームループへ
void Main::MainLoop()
{
	this->ShowFps(WINMAIN_CLASS_NAME);
	

	CubizmTest4::getInstance()->Update();


	Lib_Window::WindowCreateManager::GetInstance()->UpdateAllCursorPos();






	static ID3D11RenderTargetView* p_RenderTargetView = directX11Com->GetRenderTargetView();//フレームワークの数まで
	static ID3D11DepthStencilView* p_DepthStencilView = directX11Com->GetDepthStencilView();//フレームワークの数まで
	// 画面の更新
	{
#define VIEW_NUM 1
		// 画面描画ターゲットの領域の設定		viewport(ビューポートの寸法を定義します。)ビューポートは -1.0〜1.0 の範囲で作られたワールド座標をスクリーン座標（表示するウインドウのサイズ）に変換するための情報)//
		D3D11_VIEWPORT Viewport[VIEW_NUM];
		Viewport[0].TopLeftX = 0;
		Viewport[0].TopLeftY = 0;
		Viewport[0].Width = static_cast<FLOAT>(directX11Com->GetScreenWidth());
		Viewport[0].Height = static_cast<FLOAT>(directX11Com->GetScreenHeight());//1lo0O10O0O8sSBloO
		Viewport[0].MinDepth = 0.0f;
		Viewport[0].MaxDepth = 1.0f;

		// 描画ターゲット・ビューの設定
		Lib_3D::DirectX11InitDevice::GetImidiateContext()->OMSetRenderTargets(VIEW_NUM, &p_RenderTargetView, p_DepthStencilView);
		Lib_3D::DirectX11InitDevice::GetImidiateContext()->RSSetViewports(VIEW_NUM, Viewport);



#undef VIEW_NUM
		// 描画ターゲットのクリア
		float ClearColor[4] = { 8.0f,8.125f,8.3f,1.0f };//クリアする値
		//if (i == Enum::WINDOW_2)
		//{
		//	ClearColor[0] = { 0.0f };//クリアする値
		//	ClearColor[1] = { 7.125f };//クリアする値
		//	ClearColor[2] = { 6.3f};//クリアする値
		//	ClearColor[3] = { 1.0f };//クリアする値
		//}
		Lib_3D::DirectX11InitDevice::GetImidiateContext()->ClearRenderTargetView(p_RenderTargetView, ClearColor);
		// 深度ステンシル リソースをクリアします。
		Lib_3D::DirectX11InitDevice::GetImidiateContext()->ClearDepthStencilView(p_DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	}
	CubizmTest4::getInstance()->Draw( Lib_3D::DirectX11InitDevice::GetDevice(), Lib_3D::DirectX11InitDevice::GetImidiateContext(), directX11Com->GetScreenWidth(), directX11Com->GetScreenHeight());

	directX11Com->GetSwapChain()->Present(0, 0);
}

//restary時やthis->_my_instance = nullptr;になったとき
bool Main::UnInit()
{
	Lib_Window::WindowCreateManager::GetInstance()->DestoroyAllWindow();

	CubizmTest4::getInstance()->Release();
	//if(directX11Com)
	//delete directX11Com;
	directX11Com.release();

	CubizmInitializer::Delete();

	return true;
}

//終わりに１度だけ呼ばれる
Main::~Main(void)
{
}
