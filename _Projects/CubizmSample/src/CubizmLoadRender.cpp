#include "CubizmLoadRender.h"
//#include "Lib_3D\Wrapped.h"
#include <Cubizm/Demo/LAppPal.hpp>
//#include "Lib_Live2D\Demo\LAppDelegate.hpp"
#include <Cubizm/src/Rendering/D3D11/CubismRenderer_D3D11.hpp>
#include <Cubizm/src/CubismModelSettingJson.hpp>
#include <Cubizm/src/CubismDefaultParameterId.hpp>
#include <Cubizm/src/Id/CubismIdManager.hpp>
#include <Cubizm/src/Model/CubismModelUserData.hpp>
#include <d3dcompiler.h>

#include <Lib_Window/WindowCleate.h>
#include <Lib_DirectX11/DirectX11Init.h>


namespace {
	constexpr Csm::csmBool DebugLogEnable = true;
	Csm::csmByte* CreateBuffer(const Csm::csmChar* path, Csm::csmSizeInt* size)
	{
		if (DebugLogEnable)
		{
			LAppPal::PrintLog("[APP]create buffer: %s ", path);
		}
		return LAppPal::LoadFileAsBytes(path, size);
	}

	void DeleteBuffer(Csm::csmByte* buffer, const Csm::csmChar* path = "")
	{
		if (DebugLogEnable)
		{
			LAppPal::PrintLog("[APP]delete buffer: %s", path);
		}
		LAppPal::ReleaseBytes(buffer);
	}
}




CubizmTest3::CubizmTest3() : CubismUserModel()
{
	// デバイス作成の後
	this->_textureManager = new LAppTextureManager();
	if (DebugLogEnable)
	{
		this->_debugMode = true;
	}
	this->_idParamAngleX = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamAngleX);
	this->_idParamAngleY = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamAngleY);
	this->_idParamAngleZ = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamAngleZ);
	this->_idParamBodyAngleX = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamBodyAngleX);
	this->_idParamBodyAngleY = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamBodyAngleY);
	this->_idParamBodyAngleZ = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamBodyAngleZ);
	this->_idParamEyeBallX = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamEyeBallX);
	this->_idParamEyeBallY = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamEyeBallY);
	this->_idParamArmLA = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamArmLA);
	this->_idParamArmRA = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamArmRA);
	this->_idParamArmLB = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamArmLB);
	this->_idParamArmRB = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamArmRB);
	this->_idParamHandL = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamHandL);
	this->_idParamHandR = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamHandR);
	this->_idParamBrowLY = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamBrowLY);
	this->_idParamBrowRY = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamBrowRY);
	this->_idParamBrowLX = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamBrowLX);
	this->_idParamBrowRX = Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::ParamBrowRX);

	//Csm::CubismModelUserData::Create();
}


void CubizmTest3::ReadModel3json(const Csm::csmChar* dir, const Csm::csmChar* fileName)
{
	const Csm::csmString jsonpath = static_cast<Csm::csmString>(dir) + fileName;
	this->_modelHomeDir = dir;
	// model3.jsonを読み込み。
	Csm::csmSizeInt size;
	Csm::csmByte* buffer = CreateBuffer(jsonpath.GetRawString(), &size);
	Csm::ICubismModelSetting* setting = new Csm::CubismModelSettingJson(buffer, size);
	DeleteBuffer(buffer, jsonpath.GetRawString());

	{
		Csm::csmString path = setting->GetUserDataFile();
		path = _modelHomeDir + path;

		buffer = CreateBuffer(path.GetRawString(), &size);

		Csm::CubismModelUserData* _modelUserData = Csm::CubismModelUserData::Create(buffer, size);

		DeleteBuffer(buffer, path.GetRawString());

		{
			const Csm::csmVector<const Csm::CubismModelUserData::CubismModelUserDataNode*>& ans =
				_modelUserData->GetArtMeshUserDatas();

			for (Csm::csmUint32 i = 0; i < ans.GetSize(); ++i)
			{
				Csm::CubismIdHandle handle = ans[i]->TargetId;

					if (DebugLogEnable)
					{
						LAppPal::PrintLog("[APP]value: %s", ans[i]->Value.GetRawString());
					}
			}
		}
	}


	//Cubism Model
	if (strcmp(setting->GetModelFileName(), "") != 0)
	{
		Csm::csmString path = setting->GetModelFileName();//独特なパス結合
		path = static_cast<Csm::csmString>(dir) + path;

		if (this->_debugMode)
			LAppPal::PrintLog("[APP]create model: %s", setting->GetModelFileName());

		Csm::csmByte* buffer2 = CreateBuffer(path.GetRawString(), &size);
		//（.moc3ファイルの読み込み)(表示位置と大きさの指定)など)
		LoadModel(buffer2, size);
		DeleteBuffer(buffer2, path.GetRawString());

	}
	this->_modelSetting = setting;

	//グラフィック環境の関連付け
	this->CreateRenderer();

	this->SetupTextures();
}

void CubizmTest3::SetupTextures()
{
#ifdef PREMULTIPLIED_ALPHA_ENABLE
	const bool isPreMult = true;
	// αが合成されていないテクスチャを無理矢理ここで合成する実験を行う場合はtrueにする
	const bool isTextureMult = false;
#else
	const bool isPreMult = false;
	const bool isTextureMult = false;
#endif
	this->_bindTextureId.Clear();

	for (Csm::csmInt32 modelTextureNumber = 0; modelTextureNumber < this->_modelSetting->GetTextureCount(); modelTextureNumber++)
	{
		// テクスチャ名が空文字だった場合はロード・バインド処理をスキップ
		if (strcmp(this->_modelSetting->GetTextureFileName(modelTextureNumber), "") == 0)
		{
			continue;
		}

		//テクスチャをロードする
		Csm::csmString texturePath = this->_modelSetting->GetTextureFileName(modelTextureNumber);
		texturePath = this->_modelHomeDir + texturePath;

		LAppTextureManager::TextureInfo* texture = this->_textureManager->CreateTextureFromPngFile(texturePath.GetRawString(), isTextureMult, 0, Lib_3D::DirectX11InitDevice::GetDevice(), Lib_3D::DirectX11InitDevice::GetImidiateContext());

		//
		if (texture)
		{
			const Csm::csmUint64 textureManageId = texture->id;

			ID3D11ShaderResourceView* textureView = NULL;
			if (this->_textureManager->GetTexture(textureManageId, textureView))
			{
				this->GetRenderer<Csm::Rendering::CubismRenderer_D3D11>()->BindTexture(modelTextureNumber, textureView);
				this->_bindTextureId.PushBack(textureManageId);
			}
		}
	}

	// premultであるなら設定
	this->GetRenderer<Csm::Rendering::CubismRenderer_D3D11>()->IsPremultipliedAlpha(isPreMult);
}

void CubizmTest3::Update()
{
	//WinFunc::UpdateCursorPos(0);

	this->_model->Update();

	//this->_model->MultiplyParameterValue(this->_idParamBodyAngleX, 2.0f, 1.0f);
	 //顔パーツの透明度を0.5に設定する
	//_model->SetPartOpacity(Csm::CubismFramework::GetIdManager()->GetId(Csm::DefaultParameterId::PartsArmPrefix), 0.5f);


	//Csm::CubismUserModel model;
	Csm::CubismIdHandle drawId = Csm::CubismFramework::GetIdManager()->GetId("HitArea");
	Csm::CubismIdHandle drawId2 = Csm::CubismFramework::GetIdManager()->GetId("HitArea2");
	const auto* winC = Lib_Window::WindowCreateManager::GetInstance()->GetWindowWithClassName(WINMAIN_CLASS_NAME);
	int w = winC->GetWindowWidth();
	int h = winC->GetWindowHeight();
	float x = (float)(winC->GetCursorPos_().x - w / 2) / w;
	float y = (float)(winC->GetCursorPos_().y - h / 2) / h;
	//x = (float)winC->GetCursorPos_().x / w;
	y = (float)winC->GetCursorPos_().y / h;

	if (this->IsHit(drawId2, (Csm::csmFloat32)x, (Csm::csmFloat32)y)
		/*|| this->IsHit(drawId2, (Csm::csmFloat32)x, (Csm::csmFloat32)y)*/)
	{
		this->_model->AddParameterValue(this->_idParamAngleX, 0.05f, 0.05f);
		this->_model->AddParameterValue(this->_idParamBodyAngleX, 0.05f, 0.05f);

		this->_model->AddParameterValue(this->_idParamArmLA, -0.01f, -0.1f);
		this->_model->AddParameterValue(this->_idParamArmLB, -0.05f, -0.05f);
		this->_model->AddParameterValue(this->_idParamArmRA, 0.05f, 0.05f);
		this->_model->AddParameterValue(this->_idParamArmRB, 0.05f, 0.05f);
		this->_model->AddParameterValue(this->_idParamHandL, 0.05f, 0.05f);
		this->_model->AddParameterValue(this->_idParamHandR, -0.05f, -0.05f);
		this->_model->AddParameterValue(this->_idParamBodyAngleZ, 0.05f, 0.05f);
		this->_model->AddParameterValue(this->_idParamBrowLX, 0.05f, 0.05f);
		this->_model->AddParameterValue(this->_idParamBrowLY, 0.05f, 0.05f);
		this->_model->AddParameterValue(this->_idParamEyeBallX, 0.1f, 0.1f);

	}

	static int time = 0;
	time++;
	//output: value = 0
	if (time % 300 == 0)
	{
		//printf("me value = %f", _model->GetParameterValue(this->_idParamEyeBallX));
		//printf("arm value = %f\n", _model->GetParameterValue(this->_idParamArmLA));
		//printf("x%d,y%d", winC->GetCursorPos_().x, winC->GetCursorPos_().y);
	}

}

void CubizmTest3::Draw(Csm::CubismMatrix44& matrix)
{
	Csm::Rendering::CubismRenderer_D3D11* renderer = this->GetRenderer<Csm::Rendering::CubismRenderer_D3D11>();
	if (this->_model == NULL || renderer == NULL)
	{
		return;
	}
	// 投影行列と乗算
	matrix.MultiplyByMatrix(this->_modelMatrix);

	renderer->SetMvpMatrix(&matrix);
	renderer->DrawModel();
}

void CubizmTest3::ReloadRenderer()
{
	this->DeleteRenderer();

	this->CreateRenderer();

	this->SetupTextures();
}

void CubizmTest3::Release()
{
	//Csm::CubismModelUserData::Delete(_modelUserData);

	if (_textureManager == nullptr)
		delete _textureManager;
	_textureManager = nullptr;
}






















void CubizmTest4::InitializeCubism(ID3D11Device* device)
{

	// モデルロード前に必ず呼び出す必要がある
	Csm::Rendering::CubismRenderer_D3D11::InitializeConstantSettings(1, device);

	// ラスタライザ
	D3D11_RASTERIZER_DESC rasterDesc;
	memset(&rasterDesc, 0, sizeof(rasterDesc));
	rasterDesc.FillMode = D3D11_FILL_MODE::D3D11_FILL_SOLID;
	rasterDesc.CullMode = D3D11_CULL_MODE::D3D11_CULL_BACK; // 裏面を切る
	rasterDesc.FrontCounterClockwise = TRUE; // CCWを表面にする
	rasterDesc.DepthClipEnable = FALSE;
	rasterDesc.MultisampleEnable = FALSE;
	rasterDesc.DepthBiasClamp = 0;
	rasterDesc.SlopeScaledDepthBias = 0;
	HRESULT result = device->CreateRasterizerState(&rasterDesc, &this->_rasterizer);
	if (FAILED(result))
	{
		LAppPal::PrintLog("Fail Create Rasterizer 0x%x", result);
		return;
	}

	// テクスチャサンプラーステート
	D3D11_SAMPLER_DESC samplerDesc;
	memset(&samplerDesc, 0, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.MinLOD = -D3D11_FLOAT32_MAX;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	HRESULT hr = device->CreateSamplerState(&samplerDesc, &this->_samplerState);
	if (FAILED(hr))
	{
		LAppPal::PrintLog("Fail Create Sampler 0x%x", hr);
		return;
	}

	this->CreateShader(device);
}

void CubizmTest4::Load(const Csm::csmChar * dir, const Csm::csmChar * fileName)
{
	//please put data in live2d_animation folder
	this->live2d.ReadModel3json(dir, fileName);
}

void CubizmTest4::Draw( ID3D11Device* device, ID3D11DeviceContext* context, int windowWidth, int windowHeight)
{
	DirectX::XMMATRIX world;
	DirectX::XMMATRIX S, R, Rx, Ry, Rz, T;

	

	//	初期化
	world = DirectX::XMMatrixIdentity();


	////	拡大・縮小
	//S = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
	////	回転
	//Rx = DirectX::XMMatrixRotationX(angle.x);
	//Ry = DirectX::XMMatrixRotationY(angle.y);
	//Rz = DirectX::XMMatrixRotationZ(angle.z);
	//R = Rz * Ry * Rx;
	////	平行移動
	//T = DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);

	////	ワールド変換行列
	//world = S * R * T;

	//	ワールド変換行列取得
	//DirectX::XMMATRIX worldM = world;

	////	Matrix -> Float4x4 変換
	//DirectX::XMFLOAT4X4 world_view_projection;
	//DirectX::XMFLOAT4X4 world_;
	//DirectX::XMStoreFloat4x4(&world_view_projection, worldM * view * projection);
	//DirectX::XMStoreFloat4x4(&world_, worldM);

	//

	//Csm::csmFloat32 x[16];
	//for (size_t i = 0; i < 4; i++)
	//{
	//	for (size_t i2 = 0; i2 < 4; i2++)
	//	{
	//		x[i] = world_view_projection.m[i][i2];
	//	}
	//}
	// 投影用マトリックス
	Csm::CubismMatrix44 projection_ = Csm::CubismMatrix44();
	if (windowWidth != 0 && windowHeight != 0)
	{
		projection_.Scale(1.0f, static_cast<float>(windowWidth) / static_cast<float>(windowHeight));
	}
	Csm::CubismMatrix44 matrix = Csm::CubismMatrix44();
	matrix.SetMatrix(projection_.GetArray());

	//matrix.TranslateRelative(timer/600, timer / 600);
	//シェーダーセット
	this->SetupShader(device, context, windowWidth, windowHeight);
	// D3D11 フレーム先頭処理
	// 各フレームでの、Cubismの処理前にコール
	Csm::Rendering::CubismRenderer_D3D11::StartFrame(device, context, windowWidth, windowHeight);


	this->live2d.Draw(matrix);

	// D3D11 フレーム終了処理
	// 各フレームでの、Cubismの処理後にコール
	Csm::Rendering::CubismRenderer_D3D11::EndFrame(device);
}

void CubizmTest4::Release()
{
	if (_samplerState)
	{
		_samplerState->Release();
		_samplerState = NULL;
	}
	if (_rasterizer)
	{
		_rasterizer->Release();
		_rasterizer = NULL;
	}
	this->ReleaseShader();
	this->live2d.Release();
}

bool CubizmTest4::CreateShader(ID3D11Device* device)
{
	// 一旦削除する
	this->ReleaseShader();

	// スプライト描画用シェーダ
	static const Csm::csmChar* SpriteShaderEffectSrc =
		"cbuffer ConstantBuffer {"\
		"float4x4 projectMatrix;"\
		"float4x4 clipMatrix;"\
		"float4 baseColor;"\
		"float4 channelFlag;"\
		"}"\
		\
		"Texture2D mainTexture : register(t0);"\
		"SamplerState mainSampler : register(s0);"\
		"struct VS_IN {"\
		"float2 pos : POSITION0;"\
		"float2 uv : TEXCOORD0;"\
		"};"\
		"struct VS_OUT {"\
		"float4 Position : SV_POSITION;"\
		"float2 uv : TEXCOORD0;"\
		"float4 clipPosition : TEXCOORD1;"\
		"};"\
		\
		"/* Vertex Shader */"\
		"/* normal */"\
		"VS_OUT VertNormal(VS_IN In) {"\
		"VS_OUT Out = (VS_OUT)0;"\
		"Out.Position = mul(float4(In.pos, 0.0f, 1.0f), projectMatrix);"\
		"Out.uv.x = In.uv.x;"\
		"Out.uv.y = 1.0 - +In.uv.y;"\
		"return Out;"\
		"}"\
		\
		"/* Pixel Shader */"\
		"/* normal */"\
		"float4 PixelNormal(VS_OUT In) : SV_Target {"\
		"float4 color = mainTexture.Sample(mainSampler, In.uv) * baseColor;"\
		"return color;"\
		"}";

	ID3DBlob* vertexError = NULL;
	ID3DBlob* pixelError = NULL;

	ID3DBlob* vertexBlob = NULL;   //< スプライト描画用シェーダ
	ID3DBlob* pixelBlob = NULL;     //< スプライト描画用シェーダ

	HRESULT hr = S_OK;
	do
	{
		UINT compileFlag = 0;

		hr = D3DCompile(
			SpriteShaderEffectSrc,              // メモリー内のシェーダーへのポインターです
			strlen(SpriteShaderEffectSrc),      // メモリー内のシェーダーのサイズです
			NULL,                               // シェーダー コードが格納されているファイルの名前
			NULL,                               // マクロ定義の配列へのポインター
			NULL,                               // インクルード ファイルを扱うインターフェイスへのポインター
			"VertNormal",                       // シェーダーの実行が開始されるシェーダー エントリポイント関数の名前
			"vs_4_0",                           // シェーダー モデルを指定する文字列。
			compileFlag,                        // シェーダーコンパイルフラグ
			0,                                  // シェーダーコンパイルフラグ
			&vertexBlob,
			&vertexError);                              // エラーが出る場合はここで
		if (FAILED(hr))
		{
			LAppPal::PrintLog("Fail Compile Vertex Shader");
			break;
		}
		hr = device->CreateVertexShader(vertexBlob->GetBufferPointer(), vertexBlob->GetBufferSize(), NULL, &this->_vertexShader);
		if (FAILED(hr))
		{
			LAppPal::PrintLog("Fail Create Vertex Shader");
			break;
		}

		hr = D3DCompile(
			SpriteShaderEffectSrc,              // メモリー内のシェーダーへのポインターです
			strlen(SpriteShaderEffectSrc),      // メモリー内のシェーダーのサイズです
			NULL,                               // シェーダー コードが格納されているファイルの名前
			NULL,                               // マクロ定義の配列へのポインター
			NULL,                               // インクルード ファイルを扱うインターフェイスへのポインター
			"PixelNormal",                      // シェーダーの実行が開始されるシェーダー エントリポイント関数の名前
			"ps_4_0",                           // シェーダー モデルを指定する文字列
			compileFlag,                        // シェーダーコンパイルフラグ
			0,                                  // シェーダーコンパイルフラグ
			&pixelBlob,
			&pixelError);                       // エラーが出る場合はここで
		if (FAILED(hr))
		{
			LAppPal::PrintLog("Fail Compile Pixel Shader");
			break;
		}

		hr = device->CreatePixelShader(pixelBlob->GetBufferPointer(), pixelBlob->GetBufferSize(), NULL, &this->_pixelShader);
		if (FAILED(hr))
		{
			LAppPal::PrintLog("Fail Create Pixel Shader");
			break;
		}

		// この描画で使用する頂点フォーマット
		D3D11_INPUT_ELEMENT_DESC elems[] = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		hr = device->CreateInputLayout(elems, ARRAYSIZE(elems), vertexBlob->GetBufferPointer(), vertexBlob->GetBufferSize(), &this->_vertexFormat);

		if (FAILED(hr))
		{
			LAppPal::PrintLog("CreateVertexDeclaration failed");
			break;
		}

	} while (0);

	if (pixelError)
	{
		pixelError->Release();
		pixelError = NULL;
	}
	if (vertexError)
	{
		vertexError->Release();
		vertexError = NULL;
	}

	// blobはもうここで不要
	if (pixelBlob)
	{
		pixelBlob->Release();
		pixelBlob = NULL;
	}
	if (vertexBlob)
	{
		vertexBlob->Release();
		vertexBlob = NULL;
	}

	if (FAILED(hr))
	{
		return false;
	}

	// レンダリングステートオブジェクト
	D3D11_BLEND_DESC blendDesc;
	memset(&blendDesc, 0, sizeof(blendDesc));
	blendDesc.AlphaToCoverageEnable = FALSE;
	blendDesc.IndependentBlendEnable = FALSE;   // falseの場合はRenderTarget[0]しか使用しなくなる
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	device->CreateBlendState(&blendDesc, &this->_blendState);

	return true;
}

void CubizmTest4::SetupShader(ID3D11Device* device, ID3D11DeviceContext* context, int windowWidth, int windowHeight)
{
	if (device == NULL || this->_vertexFormat == NULL || this->_vertexShader == NULL || this->_pixelShader == NULL)
	{
		return;
	}

	// 現在のウィンドウサイズ

	// スプライト描画用の設定をし、シェーダセット
	float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	context->OMSetBlendState(this->_blendState, blendFactor, 0xffffffff);
	context->IASetInputLayout(this->_vertexFormat);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetInputLayout(this->_vertexFormat);

	D3D11_VIEWPORT viewport;
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = static_cast<FLOAT>(windowWidth);
	viewport.Height = static_cast<FLOAT>(windowHeight);
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;

	context->RSSetViewports(1, &viewport);
	context->RSSetState(this->_rasterizer);
	context->VSSetShader(this->_vertexShader, NULL, 0);
	context->PSSetShader(this->_pixelShader, NULL, 0);
	context->PSSetSamplers(0, 1, &this->_samplerState);
}

void CubizmTest4::ReleaseShader()
{
	if (this->_blendState)
	{
		this->_blendState->Release();
		this->_blendState = NULL;
	}
	if (this->_vertexFormat)
	{
		this->_vertexFormat->Release();
		this->_vertexFormat = NULL;
	}
	if (this->_pixelShader)
	{
		this->_pixelShader->Release();
		this->_pixelShader = NULL;
	}
	if (this->_vertexShader)
	{
		this->_vertexShader->Release();
		this->_vertexShader = NULL;
	}
}

//void CubizmTest4::StartFrame()
//{///*
//	//アプリのフレーム先頭処理 他の描画物がある体での各種設定、
//	//レンダーターゲットクリアなど
//	//*/
//
//	//// デバイス未設定
//	//if (!GetDevice() || !GetDeviceContext())
//	//{
//	//	return;
//	//}
//
//	//// バックバッファのクリア
//	//float clearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
//	//GetDeviceContext()->OMSetRenderTargets(1, &_renderTargetView, _depthStencilView);
//	//GetDeviceContext()->ClearRenderTargetView(_renderTargetView, clearColor);
//	//GetDeviceContext()->ClearDepthStencilView(_depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
//
//	//// Z無効
//	//GetDeviceContext()->OMSetDepthStencilState(_depthState, 0);
//}
//
//void CubizmTest4::EndFrame()
//{// 画面反映
//	//HRESULT hr = _swapChain->Present(1, 0);
//	//if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
//	//{// デバイスロストチェック
//	//	_deviceStep = DeviceStep_Lost;
//	//}
//
//	//// ウィンドウサイズ変更対応
//	//if (_deviceStep == DeviceStep_Size)
//	//{
//	//	ResizeDevice();
//	//}
//
//	//if (_deviceStep == DeviceStep_Lost)
//	//{// ロストした
//	//	LAppPal::PrintLog("Device Lost Abort");
//	//	// アプリケーション終了
//	//	AppEnd();
//	//}
//
//	//// 遅延開放監視
//	//LAppLive2DManager::GetInstance()->EndFrame();
//}
