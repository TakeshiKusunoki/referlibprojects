#pragma once
#include <Cubizm/src/Model/CubismUserModel.hpp>
#include <Cubizm/src/ICubismModelSetting.hpp>
#include <d3d11.h>
#include <Cubizm/Demo/LAppTextureManager.hpp>
#include <Cubizm/src/Math/CubismMatrix44.hpp>

namespace Csm = Live2D::Cubism::Framework;
class CubizmTest3 final : public Csm::CubismUserModel
{
private:
	Csm::ICubismModelSetting* _modelSetting; //< モデルセッティング情報
	Csm::csmVector<Csm::csmUint64> _bindTextureId; //< テクスチャID
	Csm::csmString _modelHomeDir;
	LAppTextureManager* _textureManager = nullptr;

	const Csm::CubismId* _idParamAngleX; //< パラメータID: ParamAngleX
	const Csm::CubismId* _idParamAngleY; //< パラメータID: ParamAngleX
	const Csm::CubismId* _idParamAngleZ; //< パラメータID: ParamAngleX
	const Csm::CubismId* _idParamBodyAngleX; //< パラメータID: ParamBodyAngleX
	const Csm::CubismId* _idParamBodyAngleY;
	const Csm::CubismId* _idParamBodyAngleZ;
	const Csm::CubismId* _idParamEyeBallX; //< パラメータID: ParamEyeBallX
	const Csm::CubismId* _idParamEyeBallY; //< パラメータID: ParamEyeBallXY
	const Csm::CubismId* _idParamArmLA;
	const Csm::CubismId* _idParamArmRA;
	const Csm::CubismId* _idParamArmLB;
	const Csm::CubismId* _idParamArmRB;
	const Csm::CubismId* _idParamHandL;
	const Csm::CubismId* _idParamHandR;
	const Csm::CubismId* _idParamBrowLY;
	const Csm::CubismId* _idParamBrowRY;
	const Csm::CubismId* _idParamBrowLX;
	const Csm::CubismId* _idParamBrowRX;

public:
	CubizmTest3();
	virtual ~CubizmTest3() {}

	//! model3.jsonファイル(モデルに関するすべてのファイルの参照)読み込み
	void ReadModel3json(const Csm::csmChar* dir, const Csm::csmChar* fileName);
	//! moc3ファイル(パラメータに対する頂点などの動き)読み込み



	//! 頂点の更新
	void Update();

	//! 描画
	void Draw(Csm::CubismMatrix44& matrix);

	/**
	* @brief レンダラを再構築する
	*
	*/
	void ReloadRenderer();

	void Release();
private:
	//! テクスチャの関連付け
	//! @param[in]  _modelHomeDir モデルセッティングが置かれたディレクトリ
	void SetupTextures();
};



class CubizmTest4
{
private:
	CubizmTest3 live2d;
	LAppTextureManager* _textureManager = nullptr;

	ID3D11VertexShader*     _vertexShader;  //< スプライト描画シェーダ
	ID3D11PixelShader*      _pixelShader;   //< スプライト描画シェーダ
	ID3D11BlendState*       _blendState;    //< スプライト描画用ブレンドステート
	ID3D11InputLayout*      _vertexFormat;  //< スプライト描画用型宣言

	ID3D11RasterizerState*  _rasterizer;    //< スプライト描画用ラスタライザ
	ID3D11SamplerState*     _samplerState;  //< スプライト描画用サンプラーステート
private:

public:
	CubizmTest4() = default;
	~CubizmTest4() = default;

	static CubizmTest4* getInstance()
	{
		static CubizmTest4 instance;
		return &instance;
	}

	//! 初期化
	void InitializeCubism(ID3D11Device* device);

	//! @param[in] dir ディレクトリ名
	//! @param[in] fileName ファイル名
	void Load(const Csm::csmChar* dir, const Csm::csmChar* fileName);


	//! 頂点の更新
	void Update()
	{
		live2d.Update();
	}


	//! 描画
	void Draw(ID3D11Device* device, ID3D11DeviceContext* context, int windowWidth, int windowHeight);

	//! 破棄
	void Release();

	LAppTextureManager*  GetTextureManager()
	{
		return _textureManager;
	}

private:
	/**
	* @brief   スプライト描画用シェーダの作成と頂点宣言の作成を行う
	*/
	bool CreateShader(ID3D11Device* device);

	/**
	* @brief   シェーダをコンテキストにセット
	*/
	void SetupShader(ID3D11Device* device, ID3D11DeviceContext* context, int windowWidth, int windowHeight);

	/**
	* @brief   CreateShaderで確保したオブジェクトの開放
	*/
	void ReleaseShader();

	/**
	* @brief   フレーム最初の行動
	*/
	//void StartFrame();

	/**
	* @brief   フレーム最後の行動
	*/
	//void EndFrame();

};

