#pragma once
#include <Cubizm/src/CubismFramework.hpp>
#include <Cubizm/Demo/LAppAllocator.hpp>

namespace Csm = Live2D::Cubism::Framework;

class CubizmInitializer final
{
private:
	static Csm::CubismFramework::Option _cubismOption;  ///< Cubism3 Option
	static LAppAllocator _cubismAllocator;
public:
	CubizmInitializer()
	{}
	~CubizmInitializer()
	{}

	//! 初期化処理
	static void Initialize();

	//! モデル削除
	//! *デストラクタで呼ぶとエラーが起きた。
	static void Delete();

private:
};

