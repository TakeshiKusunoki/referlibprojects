#include "CubizmInitialize.h"

//#include "Lib_Live2D\Demo\LAppLive2DManager.hpp"
//#include "Lib_Live2D\Demo\LAppModel.hpp"
//#include "Lib_Live2D\Demo\LAppDelegate.hpp"

#include <Cubizm/src/Type/CubismBasicType.hpp>
#include <Cubizm/Demo/LAppPal.hpp>

namespace
{
	CubizmInitializer Init{};
}

Csm::CubismFramework::Option CubizmInitializer::_cubismOption{};  ///< Cubism3 Option
LAppAllocator CubizmInitializer::_cubismAllocator{};


void CubizmInitializer::Initialize()
{
	// ログ等のオプション設定。
	//Csm::CubismFramework::Option _cubismOption;
	// アロケータ。
	//LAppAllocator _cubismAllocator;

	// ログ出力のレベルを設定。LogLevel_Verboseの場合は詳細ログを出力させる。
	_cubismOption.LoggingLevel = Csm::CubismFramework::Option::LogLevel_Verbose;
	_cubismOption.LogFunction = LAppPal::PrintMessage;

	// CubismNativeFrameworkの初期化に必要なパラメータを設定する。
	Csm::CubismFramework::StartUp(&_cubismAllocator, &_cubismOption);

	// CubismFrameworkを初期化する。
	Csm::CubismFramework::Initialize();

}




void CubizmInitializer::Delete()
{
	Csm::CubismFramework::Dispose();
}



