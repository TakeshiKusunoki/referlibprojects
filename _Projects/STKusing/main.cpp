//#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
//#include <windows.h>
//#include <assert.h>



// すべての静的ライブラリ
//DirectX
#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"dxgi.lib")
#pragma comment (lib, "d3dcompiler.lib")
//stk
//#pragma comment (lib, "dsound.lib")
//#pragma comment (lib, "winmm.lib")
//#pragma comment (lib, "Wsock32.lib")


#ifdef _DEBUG
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MDd.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerSoundXAudio2.lib")
#else
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MD.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerSoundXAudio2.lib")
#endif

#include <Lib_Window/WinMainClass.h>
#include <Lib_Window/WindowCleate.h>


//! @brief メイン関数で呼ばれる関数
//! par 詳細
//! Lib_Base::からウインドウの定義を呼べる
//! @details main.cppを変えることでwinMain関数の中身を変えることができる
class Main final : public Lib_Window::WinMainClass
{
public:
	Main(void);
	Main(const Main&) {}
	~Main(void);
private:
	//! @brief ループ前初期化
	bool Init();
	//! @brief メインループ
	void MainLoop();
	//! @brief ループ後終了処理
	bool UnInit();

};


#include <Stk/include/RtAudio.h>
#include <Stk/include/BeeThree.h>
namespace{
	Main main_app;//メインループの実態

	// The TickData structure holds all the class instances and data that
// are shared by the various processing functions.
	struct TickData
	{
		stk::Instrmnt* instrument;
		stk::StkFloat frequency;
		stk::StkFloat scaler;
		long counter;
		bool done;

		// Default constructor.
		TickData()
			: instrument(0), frequency(0), scaler(1.0), counter(0), done(false)
		{}
	};

	// This tick() function handles sample computation only.  It will be
	// called automatically when the system needs a new buffer of audio
	// samples.
	int tick(void* outputBuffer, void* inputBuffer, unsigned int nBufferFrames,
		double streamTime, RtAudioStreamStatus status, void* userData)
	{
		TickData* data = (TickData*)userData;
		register stk::StkFloat* samples = (stk::StkFloat*)outputBuffer;

		for (unsigned int i = 0; i < nBufferFrames; i++)
		{
			*samples++ = data->instrument->tick();
			if (++data->counter % 2000 == 0)
			{
				data->scaler += 0.025;
				data->instrument->setFrequency(data->frequency * data->scaler);
			}
		}

		if (data->counter > 80000)
			data->done = true;

		return 0;
	}
	void STKSample()
	{
		// Set the global sample rate and rawwave path before creating class instances.
		stk::Stk::setSampleRate(44100.0);
		stk::Stk::setRawwavePath("../../_Data/rawwaves/");
		
		TickData data;
		RtAudio dac;

		// Figure out how many bytes in an StkFloat and setup the RtAudio stream.
		RtAudio::StreamParameters parameters;
		parameters.deviceId = dac.getDefaultOutputDevice();
		parameters.nChannels = 1;
		RtAudioFormat format = (sizeof(stk::StkFloat) == 8) ? RTAUDIO_FLOAT64 : RTAUDIO_FLOAT32;
		unsigned int bufferFrames = stk::RT_BUFFER_SIZE;
		try
		{
			dac.openStream(&parameters, NULL, format, (unsigned int)stk::Stk::sampleRate(), &bufferFrames, &tick, (void*)& data);
		}
		catch (RtAudioError& error)
		{
			error.printMessage();
			goto cleanup;
		}

		try
		{
			// Define and load the BeeThree instrument
			data.instrument = new stk::BeeThree();
		}
		catch (stk::StkError&)
		{
			goto cleanup;
		}

		data.frequency = 220.0;
		data.instrument->noteOn(data.frequency, 0.5);

		try
		{
			dac.startStream();
		}
		catch (RtAudioError& error)
		{
			error.printMessage();
			goto cleanup;
		}

		// Block waiting until callback signals done.
		while (!data.done)
			stk::Stk::sleep(100);

		// Shut down the callback and output stream.
		try
		{
			dac.closeStream();
		}
		catch (RtAudioError& error)
		{
			error.printMessage();
		}
	cleanup:
		delete data.instrument;

		return;
	}
}

#include <Stk/include/Noise.h>
#include <Lib_Window/winMainFunc.h>
#include <Stk/include/FileLoop.h>
#include <Stk/include/FileWvOut.h>
#include <Stk/include/Iir.h>
//最初に１度だけ呼ばれる
Main::Main(void) : Lib_Window::WinMainClass::WinMainClass()
{}

//初期化 (restat時などにも呼ばれる)
bool Main::Init()
{
	//mainwindow(0番目のウインドウを作成)
	Lib_Window::WindowCreateManager::GetInstance()->AddWindow(Lib_Window::MainWindow::InitWindow, WINMAIN_CLASS_NAME, 0, 0, 1000, 1000);
	this->SetFpsLockFlag(true);
	//STKSample();
	Lib_Window::Console();


	stk::StkFrames output(20, 1);   // initialize StkFrames to 20 frames and 1 channel (default: interleaved)
	output[0] = 1.0;
	std::vector<stk::StkFloat> numerator(5, 0.1); // create and initialize numerator coefficients
	std::vector<stk::StkFloat> denominator;         // create empty denominator coefficients
	denominator.push_back(1.0);              // populate our denomintor values
	denominator.push_back(0.3);
	denominator.push_back(-0.5);
	stk::Iir filter(numerator, denominator);
	filter.tick(output);
	for (unsigned int i = 0; i < output.size(); i++)
	{
		std::cout << "i = " << i << " : output = " << output[i] << std::endl;
	}

	return true;
}

//コマンドで別のゲームループへ
void Main::MainLoop()
{
	this->ShowFps(WINMAIN_CLASS_NAME);
	//this->endLoop();
}

//restary時やthis->_my_instance = nullptr;になったとき
bool Main::UnInit()
{
	Lib_Window::WindowCreateManager::GetInstance()->DestoroyAllWindow();

	return true;
}

//終わりに１度だけ呼ばれる
Main::~Main(void)
{
}
