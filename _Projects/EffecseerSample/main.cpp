#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
//#include <windows.h>
#include <assert.h>



// すべての静的ライブラリ
//DirectX
#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "xaudio2.lib" )
//stk
//#pragma comment (lib, "dsound.lib")
//#pragma comment (lib, "winmm.lib")
//#pragma comment (lib, "Wsock32.lib")


#ifdef _DEBUG
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MDd.lib")
#pragma comment (lib, "../../Exterior_Liblarys/DirectXTex/DirectXTexD.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerSoundXAudio2.lib")
#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/debug/libfbxsdk.lib")
#else
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MD.lib")
#pragma comment (lib, "../../Exterior_Liblarys/DirectXTex/DirectXTexD.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerSoundXAudio2.lib")
#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/release/libfbxsdk.lib")
#endif
#include <cstdio>

#include <string>

#include <d3d11.h>
#include <XAudio2.h>

#include <Lib_Window/WinMainClass.h>
#include <Lib_Window/WindowCleate.h>

#include <Effecseer/include/Effekseer.h>
#include <Effecseer/include/EffekseerRendererDX11.h>
#include <Effecseer/include/EffekseerSoundXAudio2.h>

//! @brief メイン関数で呼ばれる関数
//! par 詳細
//! Lib_Base::からウインドウの定義を呼べる
//! @details main.cppを変えることでwinMain関数の中身を変えることができる
class Main final : public Lib_Window::WinMainClass
{
public:
	Main(void);
	Main(const Main&) {}
	~Main(void);
private:
	//! @brief ループ前初期化
	void Init();
	//! @brief メインループ
	void MainLoop();
	//! @brief ループ後終了処理
	void UnInit();

};

namespace{
	

	static ::Effekseer::Manager* g_manager = NULL;
	static ::EffekseerRenderer::Renderer* g_renderer = NULL;
	static ::EffekseerSound::Sound* g_sound = NULL;
	static ::Effekseer::Effect* g_effect = NULL;
	static ::Effekseer::Handle				g_handle = -1;
	static ::Effekseer::Vector3D			g_position;

	static ID3D11Device* g_device = NULL;
	static ID3D11DeviceContext* g_context = NULL;
	static IDXGIDevice1* g_dxgiDevice = NULL;
	static IDXGIAdapter* g_adapter = NULL;
	static IDXGIFactory* g_dxgiFactory = NULL;
	static IDXGISwapChain* g_swapChain = NULL;
	static ID3D11Texture2D* g_backBuffer = NULL;
	static ID3D11Texture2D* g_depthBuffer = NULL;
	static ID3D11RenderTargetView* g_renderTargetView = NULL;
	static ID3D11DepthStencilView* g_depthStencilView = NULL;

	static IXAudio2* g_xa2 = NULL;
	static IXAudio2MasteringVoice* g_xa2_master = NULL;
}


void DirectX11Init(HWND hwnd, int windowWidth, int windowHeight)
{
	// COMの初期化
	CoInitializeEx(NULL, NULL);


	// DirectX11の初期化を行う
	UINT debugFlag = 0;
	//debugFlag = D3D11_CREATE_DEVICE_DEBUG;

	auto hr = D3D11CreateDevice(
		NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		debugFlag,
		NULL,
		0,
		D3D11_SDK_VERSION,
		&g_device,
		NULL,
		&g_context);

	if FAILED(hr)
	{
		goto End;
	}

	if (FAILED(g_device->QueryInterface(__uuidof(IDXGIDevice1), (void**)& g_dxgiDevice)))
	{
		goto End;
	}

	if (FAILED(g_dxgiDevice->GetAdapter(&g_adapter)))
	{
		goto End;
	}

	g_adapter->GetParent(__uuidof(IDXGIFactory), (void**)& g_dxgiFactory);
	if (g_dxgiFactory == NULL)
	{
		goto End;
	}

	DXGI_SWAP_CHAIN_DESC hDXGISwapChainDesc;
	hDXGISwapChainDesc.BufferDesc.Width = windowWidth;
	hDXGISwapChainDesc.BufferDesc.Height = windowHeight;
	hDXGISwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	hDXGISwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	hDXGISwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	hDXGISwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	hDXGISwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	hDXGISwapChainDesc.SampleDesc.Count = 1;
	hDXGISwapChainDesc.SampleDesc.Quality = 0;
	hDXGISwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	hDXGISwapChainDesc.BufferCount = 1;
	hDXGISwapChainDesc.OutputWindow = hwnd;
	hDXGISwapChainDesc.Windowed = TRUE;
	hDXGISwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	hDXGISwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	if (FAILED(g_dxgiFactory->CreateSwapChain(g_device, &hDXGISwapChainDesc, &g_swapChain)))
	{
		goto End;
	}

	if (FAILED(g_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)& g_backBuffer)))
	{
		goto End;
	}

	if (FAILED(g_device->CreateRenderTargetView(g_backBuffer, NULL, &g_renderTargetView)))
	{
		goto End;
	}

	D3D11_TEXTURE2D_DESC hTexture2dDesc;
	hTexture2dDesc.Width = hDXGISwapChainDesc.BufferDesc.Width;
	hTexture2dDesc.Height = hDXGISwapChainDesc.BufferDesc.Height;
	hTexture2dDesc.MipLevels = 1;
	hTexture2dDesc.ArraySize = 1;
	hTexture2dDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	hTexture2dDesc.SampleDesc = hDXGISwapChainDesc.SampleDesc;
	hTexture2dDesc.Usage = D3D11_USAGE_DEFAULT;
	hTexture2dDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	hTexture2dDesc.CPUAccessFlags = 0;
	hTexture2dDesc.MiscFlags = 0;
	if (FAILED(g_device->CreateTexture2D(&hTexture2dDesc, NULL, &g_depthBuffer)))
	{
		goto End;
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC hDepthStencilViewDesc;
	hDepthStencilViewDesc.Format = hTexture2dDesc.Format;
	hDepthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	hDepthStencilViewDesc.Flags = 0;
	if (FAILED(g_device->CreateDepthStencilView(g_depthBuffer, &hDepthStencilViewDesc, &g_depthStencilView)))
	{
		goto End;
	}

	g_context->OMSetRenderTargets(1, &g_renderTargetView, g_depthStencilView);

	D3D11_VIEWPORT vp;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	vp.Width = (float)windowWidth;
	vp.Height = (float)windowHeight;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	g_context->RSSetViewports(1, &vp);

	// XAudio2の初期化を行う
	XAudio2Create(&g_xa2);

	g_xa2->CreateMasteringVoice(&g_xa2_master);

	return;
End:
	ES_SAFE_RELEASE(g_renderTargetView);
	ES_SAFE_RELEASE(g_backBuffer);
	ES_SAFE_RELEASE(g_depthStencilView);
	ES_SAFE_RELEASE(g_depthBuffer);
	ES_SAFE_RELEASE(g_swapChain);
	ES_SAFE_RELEASE(g_dxgiFactory);
	ES_SAFE_RELEASE(g_adapter);
	ES_SAFE_RELEASE(g_dxgiDevice);
	ES_SAFE_RELEASE(g_context);
	ES_SAFE_RELEASE(g_device);
}



void DirectXUninit()
{
	// DirectXの解放
	ES_SAFE_RELEASE(g_renderTargetView);
	ES_SAFE_RELEASE(g_backBuffer);
	ES_SAFE_RELEASE(g_depthStencilView);
	ES_SAFE_RELEASE(g_depthBuffer);
	ES_SAFE_RELEASE(g_swapChain);
	ES_SAFE_RELEASE(g_dxgiFactory);
	ES_SAFE_RELEASE(g_adapter);
	ES_SAFE_RELEASE(g_dxgiDevice);
	ES_SAFE_RELEASE(g_context);
	ES_SAFE_RELEASE(g_device);

	// COMの終了処理
	CoUninitialize();
}

#if _WIN32
#include <Windows.h>
std::wstring ToWide(const char* pText);
void GetDirectoryName(char* dst, const char* src);

static std::wstring ToWide(const char* pText)
{
	int Len = ::MultiByteToWideChar(CP_ACP, 0, pText, -1, NULL, 0);

	wchar_t* pOut = new wchar_t[Len + 1];
	::MultiByteToWideChar(CP_ACP, 0, pText, -1, pOut, Len);
	std::wstring Out(pOut);
	delete[] pOut;

	return Out;
}

void GetDirectoryName(char* dst, const char* src)
{
	auto Src = std::string(src);
	int pos = 0;
	int last = 0;
	while (Src.c_str()[pos] != 0)
	{
		dst[pos] = Src.c_str()[pos];

		if (Src.c_str()[pos] == L'\\' || Src.c_str()[pos] == L'/')
		{
			last = pos;
		}

		pos++;
	}

	dst[pos] = 0;
	dst[last] = 0;
}
#endif

//!@param[in] filename エフェクトファイル名
void EffectInit(const wchar_t* filename,int windowWidth, int windowHeight)
{
	const char* d = "";
	const char** argv = &d;
#if _WIN32
	char current_path[MAX_PATH + 1];
	GetDirectoryName(current_path, argv[0]);
	SetCurrentDirectoryA(current_path);
#endif

	// 描画用インスタンスの生成
	g_renderer = ::EffekseerRendererDX11::Renderer::Create(g_device, g_context, 2000);

	// エフェクト管理用インスタンスの生成
	g_manager = ::Effekseer::Manager::Create(2000);

	// 描画用インスタンスから描画機能を設定
	g_manager->SetSpriteRenderer(g_renderer->CreateSpriteRenderer());
	g_manager->SetRibbonRenderer(g_renderer->CreateRibbonRenderer());
	g_manager->SetRingRenderer(g_renderer->CreateRingRenderer());
	g_manager->SetTrackRenderer(g_renderer->CreateTrackRenderer());
	g_manager->SetModelRenderer(g_renderer->CreateModelRenderer());

	// 描画用インスタンスからテクスチャの読込機能を設定
	// 独自拡張可能、現在はファイルから読み込んでいる。
	g_manager->SetTextureLoader(g_renderer->CreateTextureLoader());
	g_manager->SetModelLoader(g_renderer->CreateModelLoader());

	// 音再生用インスタンスの生成
	g_sound = ::EffekseerSound::Sound::Create(g_xa2, 16, 16);

	// 音再生用インスタンスから再生機能を指定
	g_manager->SetSoundPlayer(g_sound->CreateSoundPlayer());

	// 音再生用インスタンスからサウンドデータの読込機能を設定
	// 独自拡張可能、現在はファイルから読み込んでいる。
	g_manager->SetSoundLoader(g_sound->CreateSoundLoader());

	// 視点位置を確定
	g_position = ::Effekseer::Vector3D(10.0f, 5.0f, 20.0f);

	// 投影行列を設定
	g_renderer->SetProjectionMatrix(
		::Effekseer::Matrix44().PerspectiveFovRH(90.0f / 180.0f * 3.14f, (float)windowWidth / (float)windowHeight, 1.0f, 50.0f));

	// カメラ行列を設定
	g_renderer->SetCameraMatrix(
		::Effekseer::Matrix44().LookAtRH(g_position, ::Effekseer::Vector3D(0.0f, 0.0f, 0.0f), ::Effekseer::Vector3D(0.0f, 1.0f, 0.0f)));

	// エフェクトの読込
	g_effect = Effekseer::Effect::Create(g_manager, (const EFK_CHAR*)filename);
	if (g_effect == nullptr)
	{
		assert(!"effect read failed");
		return;
	}

	// エフェクトの再生
	g_handle = g_manager->Play(g_effect, 0, 0, 0);

}

void effectProssess()
{
	// エフェクトの移動処理を行う
	g_manager->AddLocation(g_handle, ::Effekseer::Vector3D(0.2f, 0.0f, 0.0f));

	// エフェクトの更新処理を行う
	g_manager->Update();

	float ClearColor[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	g_context->ClearRenderTargetView(g_renderTargetView, ClearColor);
	g_context->ClearDepthStencilView(g_depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	// エフェクトの描画開始処理を行う。
	g_renderer->BeginRendering();

	// エフェクトの描画を行う。
	g_manager->Draw();

	// エフェクトの描画終了処理を行う。
	g_renderer->EndRendering();


	g_swapChain->Present(1, 0);
}

void EffectUninit()
{
	// エフェクトの停止
	g_manager->StopEffect(g_handle);

	// エフェクトの破棄
	ES_SAFE_RELEASE(g_effect);

	// 先にエフェクト管理用インスタンスを破棄
	g_manager->Destroy();

	// 次に音再生用インスタンスを破棄
	g_sound->Destroy();

	// 次に描画用インスタンスを破棄
	g_renderer->Destroy();

	// XAudio2の解放
	if (g_xa2_master != NULL)
	{
		g_xa2_master->DestroyVoice();
		g_xa2_master = NULL;
	}
	ES_SAFE_RELEASE(g_xa2);

	
}


















//-------------------------------------------------------------------------------------------------------------------
Main main_app;//メインループの実態


//最初に１度だけ呼ばれる
Main::Main(void) : Lib_Window::WinMainClass::WinMainClass()
{
	//mainWindow関数を自由に設定
	
}

//初期化 (restat時などにも呼ばれる)
void Main::Init()
{
	Lib_Window::WindowCreateManager::GetInstance()->AddWindow(Lib_Window::MainWindow::InitWindow, WINMAIN_CLASS_NAME, 0, 0, 1000, 1000);
	auto windowC = Lib_Window::WindowCreateManager::GetInstance()->GetWindowWithClassName(WINMAIN_CLASS_NAME);
	
	windowC->GetHWnd();

	DirectX11Init(windowC->GetHWnd(), windowC->GetWindowWidth(), windowC->GetWindowHeight());
	EffectInit(L"../../_Data/effect/test.efk", windowC->GetWindowWidth(), windowC->GetWindowHeight());
	//mainwindow(0番目のウインドウを作成)
	
	this->SetFpsLockFlag(true);
	
	
}

//コマンドで別のゲームループへ
void Main::MainLoop()
{
	this->ShowFps(WINMAIN_CLASS_NAME);
	effectProssess();
}

//restary時やthis->_my_instance = nullptr;になったとき
void Main::UnInit()
{
	EffectUninit();
	DirectXUninit();
	Lib_Window::WindowCreateManager::GetInstance()->DestoroyAllWindow();
}

//終わりに１度だけ呼ばれる
Main::~Main(void)
{
}
