//#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
//#include <windows.h>
//#include <assert.h>



// すべての静的ライブラリ
//DirectX
#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"dxgi.lib")
#pragma comment (lib,"d3dcompiler.lib")


#ifdef _DEBUG
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MTd.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerSoundXAudio2.lib")
#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/debug/libfbxsdk-mt.lib")
#pragma comment ( lib, "../../Exterior_Liblarys/DirectXTex/Debug/DirectXTexT.lib")
#else
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MT.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerSoundXAudio2.lib")
#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/release/libfbxsdk-mt.lib")
#pragma comment ( lib, "../../Exterior_Liblarys/DirectXTex/Release/DirectXTexT.lib")
#endif

#include <Lib_Window/WinMainClass.h>
#include <Lib_Window/WindowCleate.h>

//! @brief メイン関数で呼ばれる関数
//! par 詳細
//! Lib_Base::からウインドウの定義を呼べる
//! @details main.cppを変えることでwinMain関数の中身を変えることができる
class Main final : public Lib_Window::WinMainClass
{
public:
	//! @brief 最初に１度だけ呼ばれる
	Main(void) : Lib_Window::WinMainClass::WinMainClass() {};
	Main(const Main&) = default;//メインループ切り替えのため
	~Main(void) = default;
private:
	//! @brief ループ前初期化
	//! @return falseを返すとループを終了する(その時、MainLoopの処理は行われない。)
	bool Init() override;
	//! @brief メインループ
	void MainLoop() override;
	//! @brief ループ後終了処理
	//! @return falseを返すとループを終了する
	bool UnInit() override;

};
//! @def IS_SAFE
//! @brief エラーが起きなければセーフを返す(falseを返すとループを終了する)
#define IS_SAFE true;
Main main_app{};//メインループの実態


//初期化 (restat時などにも呼ばれる)
bool Main::Init()
{
	//mainwindow(0番目のウインドウを作成)
	Lib_Window::WindowCreateManager::GetInstance()->AddWindow(Lib_Window::MainWindow::InitWindow, WINMAIN_CLASS_NAME, 0, 0, 1000, 1000);
	this->SetFpsLockFlag(true);

	return IS_SAFE;
}

//コマンドで別のゲームループへ
void Main::MainLoop()
{
	this->ShowFps(WINMAIN_CLASS_NAME);

	return;
}

//restary時やthis->_my_instance = nullptr;になったとき
bool Main::UnInit()
{
	Lib_Window::WindowCreateManager::GetInstance()->DestoroyAllWindow();

	return IS_SAFE;
}
