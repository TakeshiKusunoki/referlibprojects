//#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く
//#include <windows.h>
//#include <assert.h>


// すべての静的ライブラリ
//DirectX
#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"dxgi.lib")
#pragma comment (lib,"d3dcompiler.lib")

#if _DEBUG
#pragma comment ( lib, "../../Exterior_Liblarys/DirectXTex/Debug/DirectXTexT.lib")
#else
#pragma comment ( lib, "../../Exterior_Liblarys/DirectXTex/Release/DirectXTexT.lib")
#endif

#ifdef _DEBUG
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MDd.lib")
#pragma comment (lib, "../../Exterior_Liblarys/DirectXTex/DirectXTexD.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Debug/EffekseerSoundXAudio2.lib")
//#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/debug/libfbxsdk.lib")
#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/debug/libfbxsdk-mt.lib")
//#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/debug/libxml2-mt.lib")
//#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/debug/zlib-mt.lib")
#else
#pragma comment (lib, "../../Exterior_Liblarys/Cubizm/Core/lib/windows/x86/140/Live2DCubismCore_MD.lib")
#pragma comment (lib, "../../Exterior_Liblarys/DirectXTex/DirectXTexD.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/Effekseer.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerRendererDX11.lib")
#pragma comment (lib, "../../Exterior_Liblarys/Effecseer/lib/x86/Release/EffekseerSoundXAudio2.lib")
//#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/release/libfbxsdk.lib")
#pragma comment (lib, "../../Exterior_Liblarys/FBXSDK/lib/release/libfbxsdk-mt.lib")
#endif

#include <Lib_Window/WinMainClass.h>
#include <Lib_Window/WindowCleate.h>
#include <Lib_DirectX11/Renderer/Fbx/FbxLoad.h>
#include <Lib_DirectX11/Renderer/Render.h>
#include <Lib_DirectX11/DirectX11Init.h>

//! @brief メイン関数で呼ばれる関数
//! par 詳細
//! Lib_Base::からウインドウの定義を呼べる
//! @details main.cppを変えることでwinMain関数の中身を変えることができる
class Main final : public Lib_Window::WinMainClass
{
	//ここに宣言した変数はこのメインループでグローバルになります(なので、リソースの破棄にデストラクタを使うものは宣言しないで下さい。再度読み込まれる可能性があります。)
	//次のinitまでにインスタンスがデストラクタを呼ぶようにする(インスタンスなどを作りたいときはstageクラスなどをローカルで宣言してやるといい)
	
public:
	//! @brief 最初に１度だけ呼ばれる
	Main(void);
	Main(const Main&) {}
	~Main(void);
private:
	//! @brief ループ前初期化
	//! @return falseを返すとループを終了する(その時、MainLoopの処理は行われない。)
	bool Init();
	//! @brief メインループ
	void MainLoop();
	//! @brief ループ後終了処理
	//! @return falseを返すとループを終了する
	bool UnInit();

};
//! @def IS_SAFE
//! @brief エラーが起きなければセーフを返す(falseを返すとループを終了する)
#define IS_SAFE true;
Main main_app;//メインループの実態


//最初に１度だけ呼ばれる	[コンストラクタでは例外が起こらない処理を書く]
Main::Main(void) : Lib_Window::WinMainClass::WinMainClass()
{
	//mainWindow関数を自由に設定
	
	
}


namespace
{
	//! @namespace call_externaly
	//! @brief 外のソースコードから呼んでいるもの(このファイル専用に作ったソースではないもの)
	namespace call_externaly
	{
#define GetDevice Lib_3D::DirectX11InitDevice::GetDevice()
#define GetDeviceContext Lib_3D::DirectX11InitDevice::GetImidiateContext()
	}
}





Lib_3D::LoadFbx load;
Lib_3D::LoadFbx load1;
struct EASY_MODEL : public Lib_3D::Lib_Render::FBXOBJ
{

};
EASY_MODEL model0{};
EASY_MODEL model1{};
std::vector<Lib_3D::DirectX11ComInit> coms;
ID3D11RasterizerState* p_RasterizerStateLine = nullptr;
ID3D11RasterizerState* p_RasterizerStatePaint = nullptr;
ID3D11DepthStencilState* p_DepthStencilState = nullptr;
//初期化 (restat時などにも呼ばれる)
bool Main::Init()
{	
	////////////////////////////////////////////////////////
	// �Cラスタライザーステートオブジェクトの生成（線描画・塗りつぶし描画)
	////////////////////////////////////////////////////////
	HRESULT hr = S_OK;
	D3D11_RASTERIZER_DESC RasteriserDesk;
	ZeroMemory(&RasteriserDesk, sizeof(D3D11_RASTERIZER_DESC));
	//-線描画の場合
	RasteriserDesk.FillMode = D3D11_FILL_WIREFRAME;	//レンダリング時に使用する描画モードを決定します
	RasteriserDesk.CullMode = D3D11_CULL_NONE;		//特定の方向を向いている三角形の描画の有無を示します。
	RasteriserDesk.FrontCounterClockwise = FALSE;	//三角形が前向きか後ろ向きかを決定します。
	RasteriserDesk.DepthBias = 0;					//指定のピクセルに加算する深度値です。
	RasteriserDesk.DepthBiasClamp = 0;				//ピクセルの最大深度バイアスです。
	RasteriserDesk.SlopeScaledDepthBias = 0;		//指定のピクセルのスロープに対するスカラです。
	RasteriserDesk.DepthClipEnable = FALSE;			//距離に基づいてクリッピングを有効にします。
	RasteriserDesk.ScissorEnable = FALSE;			//シザーカリング
	RasteriserDesk.MultisampleEnable = FALSE;		//マルチサンプリングのアンチエイリアシング
	RasteriserDesk.AntialiasedLineEnable = TRUE;	//線のアンチエイリアシング(線を描画中で MultisampleEnable が false の場合にのみ)
	hr = GetDevice->CreateRasterizerState(&RasteriserDesk, &p_RasterizerStateLine);
	if (FAILED(hr))
	{
		assert(!"ラスタライザーステートオブジェクトの生成ができません");
		return !IS_SAFE;
	}

	//-塗りつぶし描画の場合
	RasteriserDesk.FillMode = D3D11_FILL_SOLID;		//レンダリング時に使用する描画モードを決定します
	RasteriserDesk.CullMode = /*D3D11_CULL_BACK*//*D3D11_CULL_FRONT*/D3D11_CULL_NONE;		//特定の方向を向いている三角形の描画の有無を示します。
	RasteriserDesk.MultisampleEnable = TRUE;		//マルチサンプリングのアンチエイリアシング
	RasteriserDesk.AntialiasedLineEnable = FALSE;	//線のアンチエイリアシング(線を描画中で MultisampleEnable が false の場合にのみ)
	hr = GetDevice->CreateRasterizerState(&RasteriserDesk, &p_RasterizerStatePaint);
	if (FAILED(hr))
	{
		assert(!"ラスタライザーステートオブジェクト(塗りつぶし描画)の生成ができません");
		return !IS_SAFE;
	}
	
	/////////////////////////////////////////////////
	// �D深度ステンシル ステート オブジェクトの生成
	/////////////////////////////////////////////////
	D3D11_DEPTH_STENCIL_DESC DepthDesc;
	ZeroMemory(&DepthDesc, sizeof(DepthDesc));
	DepthDesc.DepthEnable = TRUE;									//深度テストあり
	DepthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;			//書き込む
	DepthDesc.DepthFunc = D3D11_COMPARISON_LESS;					//手前の物体を描画
	DepthDesc.StencilEnable = FALSE;								//ステンシル テストなし
	DepthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;		//ステンシル読み込みマスク
	DepthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;	//ステンシル書き込みマスク
	//面が表を向いている場合のステンシルステートの設定
	DepthDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
	DepthDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
	DepthDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
	DepthDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
	 //面が裏を向いている場合のステンシルステートの設定
	DepthDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
	DepthDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
	DepthDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
	DepthDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
	hr = GetDevice->CreateDepthStencilState(&DepthDesc, &p_DepthStencilState);	//
	if (FAILED(hr))
	{
		throw std::runtime_error("深度ステンシル ステート オブジェクトの生成ができません");
	}
	//mainwindow(0番目のウインドウを作成)
	Lib_Window::WindowCreateManager::GetInstance()->AddWindow(Lib_Window::MainWindow::InitWindow, WINMAIN_CLASS_NAME, 0, 0, 1000, 1000);
	
	//コンスタントバッファ作成
	Lib_3D::ShaderInfo::CBufferSelector::InitConstantBuffer();

	auto winC = Lib_Window::WindowCreateManager::GetInstance()->GetWindowWithClassName(WINMAIN_CLASS_NAME);
	coms.emplace_back(winC->GetHWnd(), winC->GetWindowWidth(), winC->GetWindowHeight());

	load1.LoadFbxFile("../../_Data/FBX/danbo_fbx/danbo_taiki.fbx");
	load1.AddMotion("neutral", "../../_Data/FBX/danbo_fbx/danbo_atk.fbx");
	load.LoadFbxFile("../../_Data/FBX/iga/iga.fbx");
	load.AddMotion("neutral", "../../_Data/FBX/iga/iga@01wait.fbx");
	Lib_3D::Lib_Render::ModelManager::SetDataFbx("iga", &load);
	Lib_3D::Lib_Render::ModelManager::SetDataFbx("danbo", &load1);
	//シェーダをロード
	bool f = Lib_3D::ShaderInfo::ShaderCorrespondsShaderLoad::ShaderCreate();
	model0.angle = { 0,6.4416f,0 };
	model0.position = { 0, 0, -12 };
	model0.scale = { 0.1f, 0.1f, 0.1f };
	model0.shaderTypeNum = 0;
	model0.modelname = "danbo";
	//model0.modelname = "iga";

	model1.angle = { 0,6.4416f,0 };
	model1.position = { 1, 0, -12 };
	model1.scale = { 0.5f,0.5f,0.5f };
	model1.shaderTypeNum = 0;
	model1.modelname = "iga";
	
	//fps固定
	this->SetFpsLockFlag(true);
	return IS_SAFE;
}

//コマンドで別のゲームループへ
void Main::MainLoop()
{
	float deltaTime = 1.0f / 5.0f;
	static float flame = 0;
	static float flame1 = 0;
	static float time = 0;
	time++;
	model0.angle = { 0 , time / 100, 0 };
	this->ShowFps(WINMAIN_CLASS_NAME);
	load.Animate(deltaTime, flame1, "neutral");
	load1.Animate(deltaTime, flame, "neutral");
	model0.Update();
	model0.Skinning("neutral", flame);
	model1.Update();
	model1.Skinning("neutral", flame1);
#define VIEW_NUM 1
	// 画面描画ターゲットの領域の設定		viewport(ビューポートの寸法を定義します。)ビューポートは -1.0〜1.0 の範囲で作られたワールド座標をスクリーン座標（表示するウインドウのサイズ）に変換するための情報)//
	D3D11_VIEWPORT Viewport[VIEW_NUM];
	Viewport[0].TopLeftX = 0;
	Viewport[0].TopLeftY = 0;
	Viewport[0].Width = static_cast<FLOAT>(coms[0].GetScreenWidth());
	Viewport[0].Height = static_cast<FLOAT>(coms[0].GetScreenHeight());//1lo0O10O0O8sSBloO
	Viewport[0].MinDepth = 0.0f;
	Viewport[0].MaxDepth = 1.0f;

	// 描画ターゲット・ビューの設定
	GetDeviceContext->OMSetRenderTargets(VIEW_NUM, coms[0].GetRenderTargetViewA(), coms[0].GetDepthStencilView());
	GetDeviceContext->RSSetViewports(VIEW_NUM, Viewport);



#undef VIEW_NUM
	// 描画ターゲットのクリア
	float ClearColor[4] = { 0.0f,255.0f,0.0f,0.0f };//クリアする値
	GetDeviceContext->ClearRenderTargetView(coms[0].GetRenderTargetView(), ClearColor);
	// 深度ステンシル リソースをクリアします。
	GetDeviceContext->ClearDepthStencilView(coms[0].GetDepthStencilView(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	//震度ステンシルステート
	GetDeviceContext->OMSetDepthStencilState(p_DepthStencilState, 0);//震度ステンシルステート

																	// �Cラスタライザ・ステート・オブジェクトの設定
	GetDeviceContext->RSSetState((true ? p_RasterizerStatePaint : p_RasterizerStateLine));//ラスタライザステート


	Lib_3D::Lib_Render::Renderer::RenderSwitch(&model0);
	Lib_3D::Lib_Render::Renderer::RenderSwitch(&model1);
	coms[0].GetSwapChain()->Present(0, 0);

	return;
}

//restary時やthis->_my_instance = nullptr;になったとき
bool Main::UnInit()
{
	bool hr = IS_SAFE;
	//コンスタントバッファ削除
	//Lib_3D::ShaderInfo::CBufferSelector::ReleaseCBuffer();
	Lib_Window::WindowCreateManager::GetInstance()->DestoroyAllWindow();
	
	return hr;
}

//終わりに１度だけ呼ばれる
Main::~Main(void)
{
}
